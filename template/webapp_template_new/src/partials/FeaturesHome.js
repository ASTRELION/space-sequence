import React from 'react';
import { Link } from 'react-router-dom';

function FeaturesHome() {
  return (
    <section className="relative">
          <div className="max-w-full h-full mx-auto px-4 sm:px-6">
            <div className="pt-32 pb-12 md:pt-40 md:pb-20">

            

              {/* Form */}
              <div className="max-w-4xl mx-auto max-h-80">
              <img className="flex flex-wrap -mx-3 mb-4 w-full " src={require('../images/bg1.jpg')} width="768" height="432" alt="Hero"  data-aos-delay="400" />
                  <form>
                  <div className="flex flex-wrap -mx-3 mb-4 w-full h-40">
                    <div className="w-full px-3">
                      <p id="parser" className="form-input w-full h-40 text-gray-300 overflow-scroll" >Commands will be displayed here</p>
                    </div>
                  </div>
                  <div className="flex flex-wrap -mx-3 mb-4 w-full">
                    <div className="w-full px-3">
                      <input id="parser" type="text" className="form-input w-full text-gray-300 overscroll-auto" placeholder="Type your command"/>
                    </div>
                  </div>
                  
                  
                 
                </form>
                
              </div>

            </div>
          </div>
        </section>
  );
}

export default FeaturesHome;
