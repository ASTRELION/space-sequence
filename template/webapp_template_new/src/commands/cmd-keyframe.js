export default function makeCommand(program)
{
    program
        .command("keyframe")
        .description("Keyframe command")
        .action((arg1) => {
            console.log("Called keyframe command");
        })
}