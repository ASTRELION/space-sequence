export default function makeCommand(program)
{
    program
        .command("hide <object>")
        .description("Hide an object")
        .action(() => {
            console.log("Called hide command");
        })
}