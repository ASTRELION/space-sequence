export default function makeCommand(program)
{
    program
        .command("variable")
        .description("Variable command")
        .action(() => {
            console.log("Called variable command");
        })
}