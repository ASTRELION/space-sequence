export default function makeCommand(program)
{
    program
        .command("rotation <object> <angle> <vector...>")
        .description("Rotate an object about the given line")
        .action(() => {
            console.log("Called rotation command");
        })
}