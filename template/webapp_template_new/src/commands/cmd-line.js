export default function makeCommand(program)
{
    program
        .command("line [x1] [y1] [z1] [x2] [y2] [z2]")
        .description("Create a line between two points")
        .action(() => {
            console.log("Called line command");
        })
}