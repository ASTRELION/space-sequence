export default function makeCommand(program)
{
    program
        .command("empty [x] [y] [z]")
        .description("Create a new empty object")
        .action(() => {
            console.log("Called empty command");
        })
}