export default function makeCommand(program)
{
    program
        .command("rect [x] [y] [z] [w] [h]")
        .description("Create a new rectangle object")
        .action(() => {
            console.log("Called rect command");
        })
}