export default function makeCommand(program)
{
    program
        .command("size <object> <w> <h>")
        .description("Set the size of an object")
        .action(() => {
            console.log("Called size object");
        })
}