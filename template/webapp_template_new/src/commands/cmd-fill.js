export default function makeCommand(program)
{
    program
        .command("fill <object> <color...>")
        .description("Fill an object with a color")
        .action(() => {
            console.log("Called fill command");
        })
}