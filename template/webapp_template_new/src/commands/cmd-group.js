export default function makeCommand(program)
{
    program
        .command("group <objects...>")
        .description("Group objects together")
        .action(() => {
            console.log("Called group command");
        })
}