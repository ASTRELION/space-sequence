export default function makeCommand(program)
{
    program
        .command("position <object> <x> <y> [z]")
        .description("Change the position of an object")
        .action(() => {
            console.log("Called position command");
        })
}