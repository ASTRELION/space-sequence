export default function makeCommand(program)
{
    program
        .command("save")
        .description("Export command history to a file")
        .action(() => {
            console.log("Called save command");
        })
}