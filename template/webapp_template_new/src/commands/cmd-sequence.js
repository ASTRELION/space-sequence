export default function makeCommand(program)
{
    program
        .command("sequence")
        .description("Sequence command")
        .action(() => {
            console.log("Called sequence command");
        })
}