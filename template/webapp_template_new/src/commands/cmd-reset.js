export default function makeCommand(program)
{
    program
        .command("reset <object> [keyframe]")
        .description("Reset an object to its initial state or a keyframes state")
        .action(() => {
            console.log("Called reset command");
        })
}