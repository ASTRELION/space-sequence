export default function makeCommand(program)
{
    program
        .command("macro")
        .description("Macro command")
        .action(() => {
            console.log("Called macro command");
        })
}