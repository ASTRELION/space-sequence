export default function makeCommand(program)
{
    program
        .command("ungroup <object>")
        .description("Ungroup an existing group")
        .action(() => {
            console.log("Called ungroup command");
        })
}