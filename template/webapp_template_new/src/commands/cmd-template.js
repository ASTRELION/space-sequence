export default function makeCommand(program)
{
    program
        .command("template <arg1> [arg2]")
        .description("This command is to be used a template")
        .action((arg1) => {
            console.log("Called template command with args: %o", arg1);
        })
}