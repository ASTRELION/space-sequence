export default function makeCommand(program)
{
    program
        .command("show <object>")
        .description("Show an object (un-hide)")
        .action(() => {
            console.log("Called show command");
        })
}