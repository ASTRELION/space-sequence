export default function makeCommand(program)
{
    program
        .command("data <object>")
        .description("Returns the data of an object")
        .action(() => {
            console.log("Called data command");
        })
}