export default function makeCommand(program)
{
    program
        .command("scale <object> [dx] [dy] [dz]")
        .description("Scale an object by given deltas")
        .action(() => {
            console.log("Called scale command");
        })
}