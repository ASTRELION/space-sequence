export default function makeCommand(program)
{
    program
        .command("circle [x] [y] [z] [w] [h]")
        .description("Create a new circle object")
        .action(() => {
            console.log("Called circle");
        })
}