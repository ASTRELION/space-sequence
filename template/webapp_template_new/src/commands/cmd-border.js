export default function makeCommand(program)
{
    program
        .command("border <object> <size> <color...>")
        .description("Adjust border of an object")
        .action(() => {
            console.log("Called border command");
        });
}