export default function makeCommand(program)
{
    program
        .command("delete <object>")
        .description("Delete an object or other data")
        .action(() => {
            console.log("Called delete command");
        })
}