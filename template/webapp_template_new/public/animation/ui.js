let darkImage = new Array(42);

function newUI(type, parent)
{
  if(type == null)
  {
    type = "div";
  }

  let ui = document.createElement(type);
  ui.id = globalID++;

  ui.style.position = "absolute";

  if(parent != null)
  {
    parent.appendChild(ui);
  }

  return ui;
}

function changeUIStatus(ui, x, y, width, height)
{
  ui.style.left = x + "%";
  ui.style.top = y + "%";
  ui.style.width = width + "%";
  ui.style.height = height + "%";
}

function newImage(x, y, width, height, pic, r, g, b, a, parent)
{
  let ui = newUI();
  ui.style.left = x + "%";
  ui.style.top = y + "%";
  ui.style.width = width + "%";
  ui.style.height = height + "%";
  ui.style.backgroundColor = "rgba(" + r * 255 + "," + g * 255 + "," + b * 255 + "," + a + ")";
  ui.style.backgroundImage = pic;
  ui.style.backgroundSize = "100% 100%";
  ui.style.backgroundRepeat = "no-repeat";
  ui.style.backgroundBlendMode = "Overlay";

  parent.appendChild(ui);

  return ui;
}

function newText(x, y, what, parent)
{
  let ui = newUI();
  ui.style.position = "relative";
  ui.style.left = x + "%";
  ui.style.top = y + "%";

  let text = document.createTextNode(what);
  ui.appendChild(text);

  parent.appendChild(ui);

  return ui;
}

function newButton(x, y, width, height, what, pic, r, g, b, a, parent, mode)
{
  let ui = newUI("button");
  ui.style.left = x + "%";
  ui.style.top = y + "%";
  ui.style.width = width + "%";
  ui.style.height = height + "%";
  ui.style.backgroundColor = "rgba(" + r * 255 + "," + g * 255 + "," + b * 255 + "," + a + ")";
  ui.style.backgroundImage = pic;
  ui.style.backgroundSize = "100% 100%";
  ui.style.backgroundRepeat = "no-repeat";
  ui.style.backgroundBlendMode = "Overlay";

  ui.onmouseout = function()
  {
    ui.style.cursor = "auto";
  };

  ui.onmouseover = function()
  {
    ui.style.cursor = "pointer";
  };

  parent.appendChild(ui);

  let text = newText(0, 0, what, ui)

  if(mode == 1)
  {
    return [ui, text];
  }
  else
  {
    return ui;
  }
}

function destroy(ui)
{
  ui.parentNode.removeChild(ui);
}

function addOnClickedListener(ui, callBack)
{
  ui.onclick = function()
  {
    callBack();
  }
}
