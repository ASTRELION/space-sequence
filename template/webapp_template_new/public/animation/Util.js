function load(path, objSelf, callBack)
{
  var loader = new THREE.OBJLoader();

  loader.load(path, function(object)
  {
    objSelf[0] = object;

    if(object.material == null)
    {
      object.material = new THREE.MeshPhongMaterial( { color: 0xb6b311, specular: 0x222222, shininess: 50} );
    }

    object.material.transparent = true

    if(callBack != null)
    {
      callBack()
    }
  },
  function()
  {

  },
  function(e)
  {
    console.error( e );
  });
}

function checkExists(mousePos, zone)
{
  if((mousePos.x + 1) / 2 < zone.x / 100)
  {
    return false
  }

  if((mousePos.x + 1) / 2 > (zone.x + zone.width) / 100)
  {
    return false
  }

  if((mousePos.y + 1) / 2 < zone.y / 100)
  {
    return false
  }

  if((mousePos.y + 1) / 2 > (zone.y + zone.height) / 100)
  {
    return false
  }

  return true
}

function setObjectsVisibilityInScene(visibility, scene)
{
  for(let i = 0; i < objectInfo.length; i += 1)
  {
    objectInfo[i].object.visible = visibility
  }
}

function initBasicScene()
{
  initHandler()

  let light = new THREE.SpotLight( new THREE.Color(0.627, 0.627, 0.627) );
  light.position.set(0, 2, 15);
  light.castShadow = true;
  light.penumbra = 0.384;
  light.intensity = 4;
  light.shadow.mapSize.width = 100;
  light.shadow.mapSize.height = 100;
  light.shadow.camera.near = 0.5;
  light.shadow.camera.far = 30;
  scene.add( light );
}

function clearSceneObjects()
{
  for(let i = 0; i < objectInfo.length; i += 1)
  {
    scene.remove(objectInfo[i].object)
    deleteObject(objectInfo[i].object)
  }

  objectInfo = []
}

function saveJson()
{
  clearAnimationData(animationFragments)

  let savingFragments = JSON.parse(JSON.stringify(animationFragments))
  toBasicStructure(animationFragments, savingFragments)
  slLoader.value = JSON.stringify(savingFragments, null, 4)
}

function loadJson(jsonString)
{
  clearSceneObjects()
  animationFragments = JSON.parse(jsonString)

  let curObjects = []

  //init list first
  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(curObjects[animationFragments[i].objectInfo.id] == null)
    {
      if(animationFragments[i].objectInfo.type == "PlaneGeometry")
      {
        animationFragments[i].object = newPlane(0, 0, 0, 1, 1)
      }
      else if (animationFragments[i].objectInfo.type == "SphereGeometry")
      {
        animationFragments[i].object = newSphere(0, 0, 0, 1)
      }
      else if (animationFragments[i].objectInfo.type == "CubeGeometry")
      {
        animationFragments[i].object = newPlane(0, 0, 0, 1, 1, 1)
      }

      if(animationFragments[i].objectInfo.texture != null)
      {
        animationFragments[i].object.material.map = animationFragments[i].objectInfo.texture
      }

      if(animationFragments[i].objectInfo.color != null)
      {
        animationFragments[i].object.material.color = new THREE.Color(animationFragments[i].objectInfo.color.x, animationFragments[i].objectInfo.color.y, animationFragments[i].objectInfo.color.z)
      }

      curObjects[animationFragments[i].objectInfo.id] = animationFragments[i].object

      scene.add(animationFragments[i].object)
      objectInfo[objectInfo.length] =
      {
        "object": animationFragments[i].object
      }
    }
    else
    {
      animationFragments[i].object = curObjects[animationFragments[i].objectInfo.id]

      objectStatus[animationFragments[i].object.id].curID = animationFragments[i].id
      console.log("F = " + animationFragments[i].id);
    }
  }

  //link fragments
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if(animationFragments[i].ballisticMovementNext != null)
    {
      animationFragments[i].ballisticMovementNext = animationFragments[animationFragments[i].ballisticMovementNext]
    }

    if(animationFragments[i].parentFragment != null)
    {
      animationFragments[i].parentFragment = animationFragments[animationFragments[i].parentFragment]
    }


    for (let j = 0; j < animationFragments[i].childFragments.length; j += 1)
    {
      animationFragments[i].childFragments[j] = animationFragments[animationFragments[i].childFragments[j]]
    }
  }
}

function toBasicStructure(fragments, dstFragments)
{
  console.log(dstFragments.length);

  for(let i = 0; i < fragments.length; i += 1)
  {
    if(fragments[i].ballisticMovementNext != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].ballisticMovementNext == fragments[j])
        {
          dstFragments[i].ballisticMovementNext = j
          break
        }
      }
    }

    if(fragments[i].childFragments.length > 0)
    {
      for(let j = 0; j < fragments[i].childFragments.length; j += 1)
      {
        for(let k = 0; k < fragments.length; k += 1)
        {
          if(fragments[i].childFragments[j] == fragments[k])
          {
            dstFragments[i].childFragments[j] = k
            break
          }
        }
      }
    }

    if(fragments[i].parent != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].parent == fragments[j])
        {
          dstFragments[i].parent = j
          break
        }
      }
    }
  }

  for(let i = 0; i < dstFragments.length; i += 1)
  {
    dstFragments[i].objectInfo.id = fragments[i].object.id
    dstFragments[i].objectInfo.type = fragments[i].object.geometry.type
    dstFragments[i].objectInfo.color = {"x": fragments[i].object.material.color.r, "y": fragments[i].object.material.color.g, "z": fragments[i].object.material.color.b}

    dstFragments[i].object = undefined
  }
}

function atOnePoint(obj, vec3a, vec3b)
{
  return vec3a.x == obj.position.x && vec3a.y == obj.position.y && vec3a.z == obj.position.z ||
    vec3b.x == obj.position.x && vec3b.y == obj.position.y && vec3b.z == obj.position.z
}

function addHelpText(content)
{
  let curData = {}

  curData.pic = newImage(0, 0, 20, 10, "", 0, 0, 0, 0, document.getElementById("root"))
  curData.text = newText(0, 0, content, curData.pic)
  curData.text.style.color = "red"
  curData.duration = 3

  systemInfo.splice(0, 0, curData)
}

function physicallyEqual(fragment1, fragment2)
{
  let bias = 0.01

  return Math.abs(fragment1.endPosition.x - fragment2.startPosition.x) < bias &&
    Math.abs(fragment1.endPosition.y - fragment2.startPosition.y) < bias &&
    Math.abs(fragment1.endPosition.z - fragment2.startPosition.z) < bias
    // &&
    // Math.abs(fragment1.endEulerAngle.x - fragment2.startEulerAngle.x) < bias &&
    // Math.abs(fragment1.endEulerAngle.y - fragment2.startEulerAngle.y) < bias &&
    // Math.abs(fragment1.endEulerAngle.z - fragment2.startEulerAngle.z) < bias &&
    // Math.abs(fragment1.endScale.x - fragment2.endScale.x) < bias &&
    // Math.abs(fragment1.endScale.y - fragment2.endScale.y) < bias &&
    // Math.abs(fragment1.endScale.z - fragment2.endScale.z) < bias
}

function obtainReviseFragmentsInBothSides(object, count)
{
  let cur = findObjectCurFragment(object)
  let midIndex = -1

  //no fragment for this object
  if (cur == null)
  {
    return
  }

  // left
  while(cur != null)
  {
    let pre = findPreviousFragment(object, cur.startFrame)

    if(pre == null || !physicallyEqual(pre, cur))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = pre
    cur = pre
  }

  //revert
  for(let i = 0; i < anotherWorldFragments.length / 2; i += 1)
  {
    let copy = anotherWorldFragments[i]
    anotherWorldFragments[i] = anotherWorldFragments[anotherWorldFragments.length - i - 1]
    anotherWorldFragments[anotherWorldFragments.length - i - 1] = copy
  }

  cur = findObjectCurFragment(object)

  // middle
  if(cur != null)
  {
    midIndex = anotherWorldFragments.length
    anotherWorldFocusIndex = midIndex
    anotherWorldFragments[anotherWorldFragments.length] = cur
  }

  // right
  while(cur != null)
  {
    let next = findNextFragment(object, cur.startFrame + cur.duration)

    if(next == null || !physicallyEqual(cur, next))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = next
    cur = next
  }

  let startIndex = midIndex - parseInt(count / 2)
  let endIndex = midIndex + parseInt(count / 2)

  //check if previous continuely curve movement
  for (let i = midIndex; i >= 0; i -= 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      startIndex = i
    }
  }

  for (let i = midIndex; i < anotherWorldFragments.length; i += 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      endIndex = i + 2
    }
  }

  //change max present
  if (startIndex != midIndex - parseInt(count / 2) || endIndex != midIndex + parseInt(count / 2))
  {
    anotherWorldMaxPresent = Math.max(endIndex - midIndex, midIndex - startIndex) * 2
    console.log(endIndex + " !!!" + startIndex);
  }

  for(let i = startIndex; i < endIndex; i += 1)
  {
    if (i < 0 || i >= anotherWorldFragments.length)
    {
      continue
    }

    let cloneObject = cloneAll(object)

    cloneObject.position.set(anotherWorldFragments[i].startPosition.x, anotherWorldFragments[i].startPosition.y, anotherWorldFragments[i].startPosition.z)
    cloneObject.rotation.set(anotherWorldFragments[i].startEulerAngle.x, anotherWorldFragments[i].startEulerAngle.y, anotherWorldFragments[i].startEulerAngle.z)
    cloneObject.scale.set(anotherWorldFragments[i].startScale.x, anotherWorldFragments[i].startScale.y, anotherWorldFragments[i].startScale.z)

    cloneObject.material.opacity = 0.5
    cloneObject.visible = true

    anotherWorldObjects[anotherWorldObjects.length] = cloneObject
    scene.add(cloneObject)

    //DELETE Border
    for (let j = 0; j < cloneObject.children.length; j += 1)
    {
      if(cloneObject.children[j].material.side == THREE.BackSide)
      {
        deleteObject(cloneObject.children[j])
      }
    }

    if(i == midIndex)
    {
      addBorder(cloneObject, 1, 0, 0, 0.5, 0.05, false)
    }

    if(i == endIndex - 1 || i == anotherWorldFragments.length - 1)
    {
      let cloneObject = cloneAll(object)

      //DELETE Border
      for (let j = 0; j < cloneObject.children.length; j += 1)
      {
        if(cloneObject.children[j].material.side == THREE.BackSide)
        {
          deleteObject(cloneObject.children[j])
        }
      }

      cloneObject.position.set(anotherWorldFragments[i].endPosition.x, anotherWorldFragments[i].endPosition.y, anotherWorldFragments[i].endPosition.z)
      cloneObject.rotation.set(anotherWorldFragments[i].endEulerAngle.x, anotherWorldFragments[i].endEulerAngle.y, anotherWorldFragments[i].endEulerAngle.z, "YZX")
      cloneObject.scale.set(anotherWorldFragments[i].endScale.x, anotherWorldFragments[i].endScale.y, anotherWorldFragments[i].endScale.z)

      cloneObject.material.opacity = 0.5
      cloneObject.visible = true

      anotherWorldObjects[anotherWorldObjects.length] = cloneObject
      scene.add(cloneObject)
    }
  }

  for(let i = 0; i < anotherWorldObjects.length - 1; i += 1)
  {
    if(anotherWorldFragments[Math.max(0, startIndex) + i].ballisticMovementNext == null)
    {
      let line = new THREE.LineCurve3(anotherWorldObjects[i].position, anotherWorldObjects[i + 1].position)

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      anotherWorldLines[i] = curveObject
      scene.add(curveObject)
    }
    else
    {
      let curvePoints = []

      while (i < anotherWorldObjects.length - 1)
      {
        let curPos = new THREE.Vector3()
        curPos.x = anotherWorldFragments[startIndex + i].startPosition.x
        curPos.y = anotherWorldFragments[startIndex + i].startPosition.y
        curPos.z = anotherWorldFragments[startIndex + i].startPosition.z
        curvePoints[curvePoints.length] = curPos

        if(anotherWorldFragments[startIndex + i].ballisticMovementNext == null)
        {
          curPos = new THREE.Vector3()
          curPos.x = anotherWorldFragments[startIndex + i].endPosition.x
          curPos.y = anotherWorldFragments[startIndex + i].endPosition.y
          curPos.z = anotherWorldFragments[startIndex + i].endPosition.z
          curvePoints[curvePoints.length] = curPos

          break
        }

        i += 1
      }

      let line = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      for(let j = i; j >= 0; j -= 1)
      {
        if(anotherWorldLines[j] != null)
        {
          break
        }

        anotherWorldLines[j] = curveObject
      }

      scene.add(curveObject)
    }
  }
}

function updateObjectsInFrame(object, curves, tObjects, fragments)
{
  if(object == thisWorldObject)
  {
    return
  }

  let curIndex = -1
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))

  for(let i = 0; i < tObjects.length; i += 1)
  {
    if(object == tObjects[i])
    {
      curIndex = i

      break
    }
  }

  let curveMovementPoints = []
  let isEntered = false
  let updated = false

  console.log(curIndex + " " + prefix + " " + anotherWorldMaxPresent + " " + anotherWorldFocusIndex);

  //obtain the curve movements points that close to the mid point
  for(let i = 0; i < curves.length; i += 1)
  {
    //check if this is the last element
    if (fragments[prefix + i].ballisticMovementNext != null || curveMovementPoints.length > 0 && fragments[prefix + i].ballisticMovementNext == null)
    {
      curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
        fragments[prefix + i].startPosition.x,
        fragments[prefix + i].startPosition.y,
        fragments[prefix + i].startPosition.z
      )
    }

    if (Math.min(curves.length - 1, curIndex) == i && curveMovementPoints.length > 0)
    {
      isEntered = true
    }

    if (fragments[prefix + i].ballisticMovementNext == null && curveMovementPoints.length > 0)
    {
      if (isEntered)
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + i].endPosition.x,
          fragments[prefix + i].endPosition.y,
          fragments[prefix + i].endPosition.z
        )
      }
      else
      {
        curveMovementPoints = []
      }
    }
  }

  if(0 <= prefix + curIndex - 1)
  {
    fragments[prefix + curIndex - 1].endPosition.x = object.position.x
    fragments[prefix + curIndex - 1].endPosition.y = object.position.y
    fragments[prefix + curIndex - 1].endPosition.z = object.position.z

    fragments[prefix + curIndex - 1].endEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex - 1].endEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex - 1].endEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex - 1].endScale.x = object.scale.x
    fragments[prefix + curIndex - 1].endScale.y = object.scale.y
    fragments[prefix + curIndex - 1].endScale.z = object.scale.z

    fragments[prefix + curIndex - 1].endAlpha.x = object.scale.x
    fragments[prefix + curIndex - 1].endAlpha.y = object.scale.y
    fragments[prefix + curIndex - 1].endAlpha.z = object.scale.z

    if(0 <= curIndex - 1)
    {
      let curve

      if(curveMovementPoints.length == 0)
      {
        curve = new THREE.LineCurve3(tObjects[curIndex - 1].position, object.position)
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")

        updated = true
      }

      let curPoints = curve.getPoints( 50 );

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex - 1].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex - 1].geometry.attributes.position.needsUpdate = true
    }
  }

  //prevent curve movement double update
  if (prefix + curIndex < fragments.length || curveMovementPoints.length > 0 && !updated)
  {
    fragments[prefix + curIndex].startPosition.x = object.position.x
    fragments[prefix + curIndex].startPosition.y = object.position.y
    fragments[prefix + curIndex].startPosition.z = object.position.z

    fragments[prefix + curIndex].startEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex].startEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex].startEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex].startScale.x = object.scale.x
    fragments[prefix + curIndex].startScale.y = object.scale.y
    fragments[prefix + curIndex].startScale.z = object.scale.z

    fragments[prefix + curIndex].startAlpha.x = object.scale.x
    fragments[prefix + curIndex].startAlpha.y = object.scale.y
    fragments[prefix + curIndex].startAlpha.z = object.scale.z

    if(curIndex < anotherWorldLines.length)
    {
      let curve

      if(curveMovementPoints.length == 0)
      {
        curve = new THREE.LineCurve3(object.position, tObjects[curIndex + 1].position)
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
      }

      let curPoints = curve.getPoints( 50 );

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex].geometry.attributes.position.needsUpdate = true
    }
  }
}

/**
 * View a scene at specific frame, note that initAnimationLoop()
 * should be called first
 */
function viewAnimationStatusAt(curFrame)
{
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if (animationFragments[i].startFrame <= curFrame && curFrame < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      setFragmentStatusAt(animationFragments[i], curFrame - animationFragments[i].startFrame)
    }
  }
}

function EditStartFrameFrom(fragment, durationChangeValue)
{
  let curFragment = findNextFragment(fragment.object, fragment.startFrame + fragment.duration)

  while(curFragment != null)
  {
    curFragment.startFrame += durationChangeValue

    curFragment = findNextFragment(curFragment.object, curFragment.startFrame + curFragment.duration)
  }
}

function editHelpText(mode)
{
  if(mode == "Standard")
  {
    systemHelp.text.innerText = StandardHelpText
  }
  else if(mode == "Revise")
  {
    systemHelp.text.innerText = ReviseHelpText
  }
  else if (mode == "Animation" || mode == "Animation (Root)")
  {
    systemHelp.text.innerText = AnimationHelpText
  }
}

/**
 * point : [in] Vector2 : NewKeyframe's location
 * curves : [in] Object3D Array : Lines / Easing curves
 * tObjects : [in/out] Object3D Array: Temperory objects
 * fragments : [in/out] AnimationFragments Json Array : fragments to insert a new fragment
 */
function insertFrame(point, curves, tObjects, fragments)
{
  let curve=[]
  min=Number.MAX_VALUE
  index=0
  for(var i=0;i<curves.length;i++){
    for(var j=0;j<curves[i].geometry.attributes.position.array.length/3;j++){
        curve[j].x=curves.geometry.attributes.position.array[j*3]
        curve[j].y=curves.geometry.attributes.position.array[j*3+1]
        curve[j].z=curves.geometry.attributes.position.array[j*3+2]

        curve[j].x=(curve[j].x+1)*window.innerWidth / 2
        curve[j].y=-(curve[j].y - 1)*window.innerHeight/2
        curve[j].z=0

        let temp=Math.sqrt(curve[j].x*curve[j].x+curve[j].y*curve[j].y)
        if(temp<min){
            min=temp;
            index = i+Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))
            }
        }
  }
    if(index != 0)
    {
        raycaster.setFromCamera(point, camera)
        raycaster.ray.intersectPlane(plane, intersects)
        let point3D = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

        let newObject = thisWorldObject.clone()
        newObject.position.set(point3D.x, point3D.y, point3D.z)
        newObject.material.visible = true
        newObject.material.opacity = 0.5
        newObject.material.transparent = true

        let newFragment =
        {
          "id": fragmentGlobalID++,
          "object": newObject,
          "objectInfo":
          {
            "name": startInfo.objectInfo.name,
            "vertex": [],
            "posWeight": [],
            "rotationWeight": [],
            "scaleWeight": [],
            "alphaWeight": [],

            // "border":
            // {
            //   show = 1,
            //   color = {"x": 1, "y": 1, "z": 1},
            //   size = 0.05,
            //   borderTest = -1
            // }
          },

          "parentFragment": undefined,
          "childFragments": [],

          "startPosition": {"x": newObject.position.x, "y": newObject.position.y, "z": newObject.position.z},
          "endPosition": {"x": fragments[i].endPosition.x, "y": fragments[i].endPosition.y, "z": fragments[i].endPosition.z},
          "startEulerAngle": null,
          "endEulerAngle": null,
          "startScale": null,
          "endScale": null,
          "startAlpha": null,
          "endAlpha": null,

          "activeBefore": 1,
          "activeAfter": 1,

          "callBack": undefined,

          "startFrame": worldFrame,
          "duration": fragments[index].startFrame + fragments[index].duration - worldFrame,

          "posEasePoints": easeArray,
          "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

          "ballisticMovementNext": undefined
        }

        let insertWidth = (fragment[index].startFrame + fragments[index].duration) / (fragments[index].startFrame + fragments[index].duration)

        //update two closest frames' info
        if (fragments[index].ballisticMovementNext != undefined)
        {
          newFragment.ballisticMovementNext = fragments[i].ballisticMovementNext
          fragments[index].ballisticMovementNext = newFragment
        }

        let easeCurvesToUpdate = [fragments[index].posEasePoints, fragments[index].rotationEasePoints, fragments[index].scaleEasePoints, fragments[index].alphaEasePoints]
        let easeCurvesToChange = [newFragment.posEasePoints, newFragment.rotationEasePoints, newFragment.scaleEasePoints, newFragment.alphaEasePoints]

        for(let i = 0; i < 4; i += 1)
        {
          let curve = new THREE.CatmullRomCurve3(easeCurvesToUpdate[i], false, "chordal")
          let insertHeight = curve.getPointAt(insertWidth).y

          let leftCurve = []
          let rightCurve = []

          for (var j = 0; j < easeCurvesToUpdate[j].length; j++)
          {
            if (easeCurvesToUpdate[j].x < insertWidth)
            {
              leftCurve[leftCurve.length] = easeCurvesToUpdate[j]
            }
            else
            {
              rightCurve[rightCurve.length] = easeCurvesToUpdate[j]
            }
          }

          leftCurve[leftCurve.length] = {"x": insertWidth, "y": insertHeight}
          rightCurve.splice(0, 0, {"x": insertWidth, "y": insertHeight});

          //normalize left
          let highestX = 0
          let highestY = 0
          let xScaler = 0
          let yScaler = 0

          for (var j = 0; j < leftCurve.length; j += 1)
          {
            if (leftCurve[j].x > highestX)
            {
              highestX = leftCurve[j].x
            }

            if (leftCurve[j].y > highestY)
            {
              highestY = leftCurve[j].y
            }
          }

          xScaler = 1 / xScaler
          yScaler = 1 / yScaler

          for (var j = 0; j < leftCurve.length; j += 1)
          {
            leftCurve[j].x *= xScaler
            leftCurve[j].y *= yScaler
          }

          //normalize right
          for (var j = 0; j < leftCurve.length; j += 1)
          {
            if (leftCurve[j].x != 1)
            {
              leftCurve[j].x /= leftCurve[0].x
              leftCurve[j].y /= leftCurve[0].y
            }
          }

          easeCurvesToUpdate[j] = leftCurve
          easeCurvesToChange[j] = rightCurve
        }

        //update left curve Object
        //if curve movement
        if (fragments[index].ballisticMovementNext != undefined)
        {
          let newStartIndex = curves[index - prefix].geometry.attributes.position.array.length - 3
          curves[index - prefix].geometry.attributes.position.array[newStartIndex] = newObject.x
          curves[index - prefix].geometry.attributes.position.array[newStartIndex + 1] = newObject.y
          curves[index - prefix].geometry.attributes.position.array[newStartIndex + 2] = newObject.z
          curves.splice(index - prefix + 1, 0, curves[index - prefix])
        }
        //normal line movement
        else
        {
          let newCurve = new THREE.LineCurve3(newObject.position, new THREE.Vector3(newFragment.position.x, newFragment.position.y, newFragment.position.z))
          scene.add(newCurve)

          curves.splice(index - prefix + 1, 0, newCurve)
        }

        //update basic info
        fragments[index].endPosition.x = newObject.position.x
        fragments[index].endPosition.y = newObject.position.y
        fragments[index].endPosition.z = newObject.position.z
        fragments[index].duration = worldFrame - fragments[index].startFrame

        //insert new info
        tObjects.splice(index - prefix + 1, 0, newObject)
        fragments.splice(index + 1, 0, newFragment)

        return true
    }

    return false
  }
  //FOR each curve in curves
  //  通过curves[index].geometry.attributes.position.array 获取线上顶点
  //  具体怎么获取参照 Main.js 的 updateEaseCurve()
  //  判断point跟这50个点的最小距离
  //  获取最小距离 记录最小距离的时候的index
  //
  //如果 最小距离时候的index存在
  //  在fragments的相同index插入这一帧 更新index - 1和index + 1的fragments的信息(如果存在)
  //  信息包括 position posEasePoints rotationEasePoints scaleEasePoints alphaEasePoints
  //  通过 thisWorldObject.clone() 创造一个新的object 他的初始位置基于point 加入 tObjects 的相同index
  //  判断是line还是ease curve来判断是否需要新创造curve object3d还是更改或加入顶点坐标 (参照 Main.js 的 updateEaseCurve())
  //  加入创建的新object于scene.add()
  //  如果新创键了curve object 也通过scene.add()加入scene
  //  return true
  //如果 不存在


function deleteFrame(object, tObjects, fragments)
{
  //删除fragments里的信息
  //删除的位置等于当 object等于tObjects[index] 中的index的位置
  //同时像insertFrame()一样更新fragments里的信息
  //信息包括 position rotation scale alpha posEasePoints rotationEasePoints scaleEasePoints alphaEasePoints
  //同时删除tObjects的元素
}
