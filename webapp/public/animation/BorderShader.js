function initBorderShader(texture)
{
  var material = new THREE.MeshPhongMaterial( {
    map: texture,
} );

  material.onBeforeCompile = function ( shader )
  {
    shader.uniforms.tex = { value: texture };
    shader.uniforms.borderSize = { value: 0.2 };

    shader.vertexShader = "uniform float borderSize;\n" + shader.vertexShader;
    shader.fragmentShader = "uniform sampler2D tex;\n" + "uniform float borderSize;\n" + shader.fragmentShader;
    //
    shader.vertexShader = shader.vertexShader.replace(
      '#include <begin_vertex>',
      [
        '#include <begin_vertex>',
        'transformed = vec3( position) * (1.0 + borderSize);'
      ].join("\n")
    );

    shader.fragmentShader = shader.fragmentShader.replace(
      "uniform vec3 diffuse;",
      [
        "uniform vec3 diffuse;",

        "vec3 lerp(vec3 v1, vec3 v2, float a)",
        "{",
          "return v1 + (v2 - v1) * a;",
        "}"
      ].join("\n")
    )

    shader.fragmentShader = shader.fragmentShader.replace(
        'gl_FragColor = vec4( outgoingLight, diffuseColor.a );',
        [
          //"gl_FragColor = vec4(outgoingLight, texture2D(tex, vUv * 1.1 - 0.05).a);",
          "float la = texture2D(tex, vUv * (1.0 + borderSize) - borderSize * 0.5).a;",
          "float sa = texture2D(tex, vUv * (1.0 + borderSize * 2.0) - borderSize).a;",
          "float a1 = la - texture2D(tex, vUv).a;",
          "float a2 = sa - texture2D(tex, vUv).a;",

          "if(a1 >= 0.0)",
          "{",
            "if(la != 0.0)",
            "{",
              "gl_FragColor = vec4(1, 0, 0, 1);",
            "}",
          "}",
          "else",
          "{",
            // "gl_FragColor = vec4(0, a1, 0.0, 1.0);",
            "a1 = 245.0;",
          "}",

          "if(a2 > 0.0)",
          "{",
            "if(sa != 0.0)",
            "{",
              "gl_FragColor = vec4(1, 0, 0, 1);",
              "a2 = 245.0;",
            "}",
          "}",
          "else",
          "{",
            "gl_FragColor = vec4(0, a2, 0.0, 1.0);",
          "}",

          "if(a1 != 245.0) a1 = 0.0; if(a2!= 245.0) a2 = 0.0;",

          "gl_FragColor = vec4(lerp(outgoingLight, vec3(1.0, 0.0, 0.0), clamp(a1 + a2, 0.0, 1.0)), max(clamp(a1 + a2, 0.0, 1.0), diffuseColor.a));"
        ].join( '\n' )
    );
  };

  material.transparent = true

  return material
}

function vDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    void main()
    {
       vUv = uv;
       vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
       gl_Position = projectionMatrix * mvPosition;
    }
  `
}

function fDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    uniform sampler2D tEmissive;
    uniform vec4 borderColor;
    uniform float borderSize;
    uniform bool borderTest;

    float lerp(float v1, float v2, float a)
    {
      return v1 + (v2 - v1) * a;
    }

    void main()
    {
      if(!borderTest)
      {
        gl_FragColor = vec4(borderColor);

        return;
      }

      bool isBorder = false;
      vec2 newUv = vec2(vUv.x * (1.0 + borderSize * 2.0) - borderSize, vUv.y * (1.0 + borderSize * 2.0) - borderSize);

      vec2[] points = vec2[8]
      (
        vec2(newUv.x, clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(newUv.x, clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0))
      );

      for(int i = 0; i <= 8; i += 1)
      {
        if(texture2D(tEmissive, points[i]).a != 0.0)
        {
          isBorder = true;
          break;
        }
      }

      if(isBorder)
      {
        gl_FragColor = vec4(borderColor);
      }
      else
      {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
      }
    }
  `
}

THREE.BorderShader =
{
	uniforms:
  {
		"tDiffuse": { value: null },

    "borderColor": { value: new THREE.Vector4(1, 0, 0, 1)},
    "pixelSize": { value : new THREE.Vector2(1 / window.innerWidth, 1 / window.innerHeight) },
    "borderSize": { value : 3 }
	},

	vertexShader:
  [
		"varying vec2 vUv;",

		"void main() {",

			"vUv = uv;",
			"gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",

		"}"
	].join( "\n" ),

	fragmentShader:
  [
    "#include <packing>",

    "uniform sampler2D tDiffuse;",

    "uniform vec4 borderColor;",
    "uniform vec2 pixelSize;",
    "uniform int borderSize;",

		"varying vec2 vUv;",

    "bool compareTop(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(vUv.x, clamp(vUv.y + pixelSize.y * float(count), 0.0, 1.0))).a != 0.0;",
    "}",

    "bool compareLeft(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(clamp(vUv.x - pixelSize.x * float(count), 0.0, 1.0), vUv.y)).a != 0.0;",
    "}",

    "bool compareBottom(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(vUv.x, clamp(vUv.y - pixelSize.y * float(count), 0.0, 1.0))).a != 0.0;",
    "}",

    "bool compareRight(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(clamp(vUv.x + pixelSize.x * float(count), 0.0, 1.0), vUv.y)).a != 0.0;",
    "}",

    "bool compareBottomLeft(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(clamp(vUv.x + pixelSize.x * float(count), 0.0, 1.0), clamp(vUv.y - pixelSize.y * float(count), 0.0, 1.0))).a != 0.0;",
    "}",

    "bool compareBottomRight(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(clamp(vUv.x + pixelSize.x * float(count), 0.0, 1.0), clamp(vUv.y + pixelSize.y * float(count), 0.0, 1.0))).a != 0.0;",
    "}",

    "bool compareTopleft(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(clamp(vUv.x - pixelSize.x * float(count), 0.0, 1.0), clamp(vUv.y - pixelSize.y * float(count), 0.0, 1.0))).a != 0.0;",
    "}",

    "bool compareTopRight(int count)",
    "{",
      "return texture2D(tDiffuse, vUv).a == 0.0 && texture2D(tDiffuse, vec2(clamp(vUv.x - pixelSize.x * float(count), 0.0, 1.0), clamp(vUv.y + pixelSize.y * float(count), 0.0, 1.0))).a != 0.0;",
    "}",

		"void main()",
    "{",
      "bool isBorder = false;",

      "for(int i = 1; i <= borderSize; i += 1)",
      "{",
        "if(compareTop(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",

        "if(compareBottom(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",

        "if(compareLeft(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",

        "if(compareRight(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",

        "if(compareTopRight(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",

        "if(compareTopleft(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",

        "if(compareBottomLeft(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",

        "if(compareBottomRight(i))",
        "{",
          "isBorder = true;",
          "break;",
        "}",
      "}",

      "if(isBorder)",
      "{",
        "gl_FragColor = vec4(borderColor);",
      "}",
      "else",
      "{",
        "gl_FragColor = texture2D(tDiffuse, vUv);",
      "}",
		"}"
	].join( "\n" )
};
