export function vDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    void main()
    {
       vUv = uv;
       vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
       gl_Position = projectionMatrix * mvPosition;
    }
  `
}

export function fDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    uniform sampler2D tEmissive;
    uniform vec4 borderColor;
    uniform float borderSize;
    uniform bool borderTest;

    float lerp(float v1, float v2, float a)
    {
      return v1 + (v2 - v1) * a;
    }

    void main()
    {
      if(!borderTest)
      {
        gl_FragColor = vec4(borderColor);

        return;
      }

      bool isBorder = false;
      vec2 newUv = vec2(vUv.x * (1.0 + borderSize * 2.0) - borderSize, vUv.y * (1.0 + borderSize * 2.0) - borderSize);

      vec2[] points = vec2[8]
      (
        vec2(newUv.x, clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(newUv.x, clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0))
      );

      for(int i = 0; i <= 8; i += 1)
      {
        if(texture2D(tEmissive, points[i]).a != 0.0)
        {
          isBorder = true;
          break;
        }
      }

      if(isBorder)
      {
        gl_FragColor = vec4(borderColor);
      }
      else
      {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
      }
    }
  `
}
