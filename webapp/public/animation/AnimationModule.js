/**
 * record the start fragment
 * record the whole fragment if the start has already existed
 */
export var animationBuffer = []

/**
 * the id for current operating fragment
 * (update when left/right arrow or create new whole fragment)
 */
export var objectStatus = []

export function initAnimationLoop(data)
{
  for(let i = 0; i < data.length; i += 1)
  {
    let positionPoints = []
    let rotationPoints = []
    let scalePoints = []
    let alphaPoints = []

    for(let j = 0; j < data[i].posEasePoints.length; j += 1)
    {
      positionPoints[j] = new THREE.Vector3(data[i].posEasePoints[j].x, data[i].posEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].rotationEasePoints.length; j += 1)
    {
      rotationPoints[j] = new THREE.Vector3(data[i].rotationEasePoints[j].x, data[i].rotationEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].scaleEasePoints.length; j += 1)
    {
      scalePoints[j] = new THREE.Vector3(data[i].scaleEasePoints[j].x, data[i].scaleEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].alphaEasePoints.length; j += 1)
    {
      alphaPoints[j] = new THREE.Vector3(data[i].alphaEasePoints[j].x, data[i].alphaEasePoints[j].y, 0)
    }

    data[i].positionCurve = new THREE.CatmullRomCurve3(positionPoints, false, "chordal")
    data[i].rotationCurve = new THREE.CatmullRomCurve3(rotationPoints, false, "chordal")
    data[i].scaleCurve = new THREE.CatmullRomCurve3(scalePoints, false, "chordal")
    data[i].alphaCurve = new THREE.CatmullRomCurve3(alphaPoints, false, "chordal")

    data[i].object.rotation.x = toRadians(data[i].startEulerAngle.x)
    data[i].object.rotation.y = toRadians(data[i].startEulerAngle.y)
    data[i].object.rotation.z = toRadians(data[i].startEulerAngle.z)

    if(data[i].movementCurve == null)
    {
      let movementPoints = []
      let cur = data[i]

      while(true)
      {
        movementPoints[movementPoints.length] = new THREE.Vector3(cur.startPosition.x, cur.startPosition.y, cur.startPosition.z)

        if(cur.ballisticMovementNext != null)
        {
          cur = cur.ballisticMovementNext
        }
        else
        {
          movementPoints[movementPoints.length] = new THREE.Vector3(cur.endPosition.x, cur.endPosition.y, cur.endPosition.z)

          break
        }
      }

      let curve = new THREE.CatmullRomCurve3(movementPoints, false, "chordal")
      // let curve = new THREE.QuadraticBezierCurve3(movementPoints[0], movementPoints[1], movementPoints[2])
      cur = data[i]

      for(let j = 0; j < movementPoints.length - 1; j += 1)
      {
        cur.movementCurve = curve
        cur.movementCurveIndex = j
        cur.movementCurveLength = movementPoints.length - 1

        cur = cur.ballisticMovementNext
      }
    }

    if (data[i].rotationType == "Object" && data[i].autoRotationCurve == null)
    {
      let sequenceFragments = []
      let cur = data[i]

      while(cur != null)
      {
        sequenceFragments[sequenceFragments.length] = cur
        cur = cur.autoRotationNext
      }

      generateAutoRotationCurve(sequenceFragments)
    }
  }
}

export function animateAllFragments(data)
{
  let animationLength = data.length

  for(let i = 0; i < animationLength; i += 1)
  {
    if(data[i].activeBefore != 1)
    {
      setAlpha(0, data[i])
    }
  }

  systemMode.text.style.opacity = 0
  systemHelp.text.style.opacity = 0

  let finalAnimationTime = 9999999

  console.log(data);
  handler.core.visible = false

  var loop = function()
  {
    worldFrame += 1
    document.getElementById(curTimeText.id).innerHTML = worldFrame

    if(worldFrame >= finalAnimationTime)
    {
      isAnimating = false
      commandAnimating = false

      return
    }

    for(let i = 0; i < data.length; i += 1)
    {
      if(data[i].animated == null && data[i].startFrame <= worldFrame)
      {
        animateNow(data[i])

        data[i].animated = true
        animationLength -= 1

        if(animationLength == 0)
        {
          finalAnimationTime = data[i].startFrame + data[i].duration
        }
      }
    }

    requestAnimationFrame(loop)
  }

  loop()
}

export function animateNow(fragment)
{
  let curFrame = 0

  for(let i = 0; i < fragment.childFragments.length; i += 1)
  {
    animateNow(fragment.childFragments[i])
  }

  for (let i = 0; i < fragment.callBefore.length; i += 1)
  {
    fragment.callBefore[i]()
  }

  var animationLoop = function()
  {
    if(curFrame == fragment.duration + 1)
    {
      if(fragment.activeAfter != 1)
      {
        setOpacity(0, fragment)
      }

      for (let i = 0; i < fragment.callBack.length; i += 1)
      {
        fragment.callBack[i]()
      }

      return
    }

    setFragmentStatusAt(fragment, curFrame)

    curFrame += 1

    requestAnimationFrame(animationLoop)
  };

  animationLoop()
}

export function setFragmentStatusAt(fragment, curFrame)
{
  let curStartPortition = fragment.movementCurveIndex / fragment.movementCurveLength
  let curTotalPortition = 1 / fragment.movementCurveLength

  let curPosition = fragment.movementCurve.getPointAt(curStartPortition + curTotalPortition * clamp(fragment.positionCurve.getPointAt(curFrame / fragment.duration).y, 0, 1))
  let curScale = lerp3(fragment.startScale, fragment.endScale, fragment.scaleCurve.getPointAt(curFrame / fragment.duration).y)
  let curAlpha = lerp1(fragment.startAlpha, fragment.endAlpha, fragment.alphaCurve.getPointAt(curFrame / fragment.duration).y)

  if (fragment.rotationType == "Object")
  {
    let curRotation = fragment.autoRotationCurve.getTangentAt(lerp1(curStartPortition, curStartPortition + curTotalPortition, curFrame / fragment.duration))
    fragment.object.position.set(0, 0, 0)
    fragment.object.lookAt(curRotation.x, curRotation.y, curRotation.z)
    fragment.object.rotateY(toRadians(90))
  }
  else if (fragment.rotationType == "Vertex")
  {

  }
  else
  {
    let curRotation = lerp3(fragment.startEulerAngle, fragment.endEulerAngle, fragment.rotationCurve.getPointAt(curFrame / fragment.duration).y)
    fragment.object.rotation.set(0, 0, 0, "YZX")
    fragment.object.rotateY(curRotation.y)
    fragment.object.rotateZ(curRotation.z)
    fragment.object.rotateX(curRotation.x)
  }

  fragment.object.position.set(curPosition.x, curPosition.y, curPosition.z)
  fragment.object.scale.set(curScale.x, curScale.y, curScale.z)
  setAlpha(curAlpha, fragment)
}

export function clearAnimationData(data)
{
  for(let i = 0; i < data.length; i += 1)
  {
    data[i].positionCurve = undefined
    data[i].rotationCurve = undefined
    data[i].scaleCurve = undefined
    data[i].alphaCurve = undefined

    data[i].movementCurve = undefined
    data[i].movementCurveIndex = undefined
    data[i].movementCurveLength = undefined

    data[i].autoRotationCurve = undefined
    data[i].autoRotationCurveIndex = undefined
    data[i].autoRotationCurveLength = undefined

    data[i].animated = undefined
  }
}

export function recordFrame(object)
{
  if(animationBuffer[object.id] != null && animationBuffer[object.id].frame != worldFrame)
  {
    recordAnimationFrame(animationBuffer[object.id], recordEndFrame(object))

    animationBuffer[object.id] = null
    objectStatus[object.id].curID = animationFragments[animationFragments.length - 1].id

    addHelpText("Fragment Create Completed")
  }
  else if (animationBuffer[object.id] != null && animationBuffer[object.id].frame == worldFrame)
  {
    addHelpText("Created failed: still in the same time line")
  }
  else
  {
    animationBuffer[object.id] = recordStartFrame(object)
    objectStatus[object.id].curID = -1

    addHelpText("Start Frame Created")
  }
}

export function recordStartFrame(object)
{
  let startInfo =
  {
    "object": object,
    "objectInfo":
    {
      "name": "TestName",
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": []
    },

    "position": {"x": object.position.x, "y": object.position.y, "z": object.position.z},
    "rotation": {"x": object.rotation.x, "y": object.rotation.y, "z": object.rotation.z},
    "scale": {"x": object.scale.x, "y": object.scale.y, "z": object.scale.z},
    "alpha": object.material.opacity,

    "frame": worldFrame,

    "callBack": []
  }

  return startInfo
}

export function recordEndFrame(object)
{
  let endInfo =
  {
    "object": object,
    "objectInfo":
    {
      "name": "TEST",
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": []
    },

    "childFragments": [],

    "position": {"x": object.position.x, "y": object.position.y, "z": object.position.z},
    "rotation": {"x": object.rotation.x, "y": object.rotation.y, "z": object.rotation.z},
    "scale": {"x": object.scale.x, "y": object.scale.y, "z": object.scale.z},
    "alpha": object.material.opacity,

    "frame": worldFrame,

    "callBack": []
  }

  return endInfo
}

export function recordAnimationFrame(startInfo, endInfo)
{
  animationFragments[animationFragments.length] =
  {
    "id": globalID++,
    "object": startInfo.object,
    "objectInfo":
    {
      "name": startInfo.objectInfo.name,
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": [],

      // "border":
      // {
      //   show = 1,
      //   color = {"x": 1, "y": 1, "z": 1},
      //   size = 0.05,
      //   borderTest = -1
      // }
    },

    "parentFragment": undefined,
    "childFragments": [],

    "startPosition": startInfo.position,
    "endPosition": endInfo.position,
    "startEulerAngle": startInfo.rotation,
    "endEulerAngle": endInfo.rotation,
    "startScale": startInfo.scale,
    "endScale": endInfo.scale,
    "startAlpha": startInfo.alpha,
    "endAlpha": endInfo.alpha,

    "activeBefore": 1,
    "activeAfter": 1,

    "callBefore": startInfo.callBack,
    "callBack": endInfo.callBack,

    "startFrame": startInfo.frame,
    "duration": endInfo.frame - startInfo.frame,

    //advanced elements
    //TODO
    "posEasePoints": JSON.parse(JSON.stringify(easeArray)),
    "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
    "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
    "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

    "ballisticMovementNext": undefined
  }

  if(mode == "Revise")
  {
    leaveReviseMode()
    enterReviseMode()
    curTarget = thisWorldObject
  }
}

export function setAlpha(opacity, fragment)
{
  let parentOpacity = 1

  if(fragment.parentFragment != null)
  {
    parentOpacity = fragment.parentFragment.object.material.opacity
  }

  fragment.object.material.opacity = parentOpacity * opacity

  for(let i = 0; i < fragment.childFragments.length; i += 1)
  {
    setOpacity(fragment.childFragments[i], opacity)
  }
}

export function findLastFragment(object)
{
  let preFrame = -99999999
  let preIndex = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(object == animationFragments[i].object && preFrame <= animationFragments[i].startFrame + animationFragments[i].duration)
    {
      preFrame = animationFragments[i].startFrame + animationFragments[i].duration
      preIndex = i
    }
  }

  if(preIndex == -1)
  {
    return null
  }
  else
  {
    return animationFragments[preIndex]
  }
}

export function findPreviousFragment(object, curFrame)
{
  let distance = 99999999
  let index = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    let curDistance = curFrame - animationFragments[i].startFrame

    if(object == animationFragments[i].object && curDistance > 0 && curDistance < distance)
    {
      distance = curDistance
      index = i
    }
  }

  if(index != -1)
  {
    return animationFragments[index]
  }
  else
  {
    return null
  }
}

export function findNextFragment(object, curFrame)
{
  let distance = -99999999
  let index = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    let curDistance = curFrame - animationFragments[i].startFrame

    if(object == animationFragments[i].object && curDistance <= 0 && curDistance > distance)
    {
      distance = curDistance
      index = i
    }
  }

  if(index != -1)
  {
    return animationFragments[index]
  }
  else
  {
    return null
  }
}

export function findObjectCurFragment(object)
{
  if(objectStatus[object.id].curID != -1)
  {
    return findFragment(objectStatus[object.id].curID)
  }

  return null
}

export function findFragment(id)
{
  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(id == animationFragments[i].id)
    {
      return animationFragments[i]
    }
  }

  return null
}

export function renderHelpText()
{
  for(let i = 0; i < systemInfo.length; i += 1)
  {
    if(systemInfo[i].duration < 0)
    {
      destroy(systemInfo[i].pic)

      systemInfo.splice(i, 1)
      i -= 1

      continue
    }

    changeUIStatus(systemInfo[i].pic, 0, 5 + 2.5 * i, 50, 10)

    if(systemInfo[i].duration < 0.5)
    {
      systemInfo[i].text.style.opacity = systemInfo[i].duration * 2
    }

    systemInfo[i].duration -= 0.0167
  }
}

export function reEnterMode(target)
{
  let modeIndex = 0
  let targetIndex = 0
  let modeStringArr = ["Standard", "Revise", "Animation", "Animation (Root)"]
  let modeEnterFunctionArr = [enterStandardMode, enterReviseMode, enterAnimationMode, enterAnimationMode]
  let modeLeaveFunctionArr = [leaveStandardMode, leaveReviseMode, leaveAnimationMode, leaveAnimationMode]

  for (let i = 0; i < modeStringArr.length; i += 1)
  {
    if (mode == modeStringArr[i])
    {
        modeIndex = i
    }

    if (target == modeStringArr[i])
    {
        targetIndex = i
    }
  }

  modeLeaveFunctionArr[modeIndex]()
  modeEnterFunctionArr[targetIndex]()

  editHelpText(target)
}

export function enterStandardMode()
{
  if(thisWorldObject != null)
  {
    curTarget = thisWorldObject
    curTarget.visible = true
  }

  thisWorldObject = null
}

export function leaveStandardMode()
{
  if(curTarget != null)
  {
    thisWorldObject = curTarget
  }
}

export function enterReviseMode()
{
  if(thisWorldObject != null)
  {
    if(objectStatus[thisWorldObject.id].curID == -1)
    {
      let lastFragment = findLastFragment(thisWorldObject)

      if(lastFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = lastFragment.id
      }
    }

    obtainReviseFragmentsInBothSides(thisWorldObject, anotherWorldMaxPresent)
  }
}

export function leaveReviseMode()
{
  for(let i = 0; i < anotherWorldObjects.length; i += 1)
  {
    deleteObject(anotherWorldObjects[i])
  }

  for(let i = 0; i < anotherWorldLines.length; i += 1)
  {
    deleteObject(anotherWorldLines[i])
  }

  curTarget = null

  anotherWorldFragments = []
  anotherWorldObjects = []
  anotherWorldLines = []

  anotherWorldFocusIndex = -1

  handler.core.visible = false
}

export function enterAnimationMode()
{
  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  viewAnimationStatusAt(worldFrame)
}

export function leaveAnimationMode()
{

}
