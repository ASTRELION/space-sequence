export var sequences = []
export var variables = []
export var curSequenceName
export var curMacroName
export var curAnimationFragment

export var commandAnimating = false

//Creating objects
//circle [<x> <y> [z]] [<w> <h>] creates a new circle object at optional position with optional size
export function circle(x, y, z, r)
{
    let obj = newSphere(x, y, z, r, null, new THREE.Color(0, 1, 1))
    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }

    scene.add(obj)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        circle(x,y,z,r)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        circle(x,y,z,r)
      }
    }
}
//rect [<x> <y> [z]] [<w> <h>]
export function rect(x, y, z, w, h, l)
{
  let obj = newCube(x, y, z, w, h, l, null, new THREE.Color(0, 1, 1))

  objectInfo[objectInfo.length] =
  {
    "object": obj,
    "id": globalID++
  }

  scene.add(obj)

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      rect(x, y, z, w, h, l)
    }
  }

  if (!commandAnimating && curAnimationFragment != null)
  {
    curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
    {
      rect(x, y, z, w, h, l)
    }
  }
}
//line [<x> <y> [z]] [length]
export function line(x, y, z, length)
{
    let v1 = new THREE.Vector3(x + -length / 2, y + -length / 2, z + -length / 2)
    let v2 = new THREE.Vector3(x + length / 2, y + length / 2, z + length / 2)

    let obj = newLine(v1, v2, new THREE.Color(0, 0, 0))

    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }

    scene.add(obj)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        line(x, y, z, length)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        line(x, y, z, length)
      }
    }
}
//line [<x> <y> [z] / <object_id>] [<x> <y> [z] / <object_id>] creates a new line between given objects or points
export function line2(x,y,z,x2,y2,z2)
{
    let v1=new THREE.Vector3(x,y,z)
    let v2=new THREE.Vector3(x2,y2,z2)
    let obj = newLine(v1,v2,new THREE.Color(0, 0, 0))
    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }
    scene.add(obj)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        line2(x,y,z,x2,y2,z2)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        line2(x,y,z,x2,y2,z2)
      }
    }
}
//empty [<x> <y> [z]] creates an "empty object" at the given position for use with other objects (like creating lines to the empty, etc.)
export function empty(x,y,z)
{
    let a = new THREE.Group()
    a.position.set(x,y,z)
    scene.add(a)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        empty(x,y,z)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        empty(x,y,z)
      }
    }
}
//group <object_id> <object_id> ... groups infinitely many objects together; this creates a unique object_id for the group; grouping groups together ungroups previous groups and creates a union of the groups; individual objects can still be manipulated by invoking their ID
export function group(ids){
     var group = new THREE.Group();
     objectInfo[objectInfo.length] =
     {
          "object": group,
          "id": globalID++
     }
     for (let i = 0; i < ids.length; i += 1)
        {
          for (var j = 0; j < objectInfo.length; j += 1)
          {
            if (objectInfo[j].id == ids[i])
            {
              group.add(objectInfo[j].object)
            }
          }
     }

     scene.add(group)

     if (!commandAnimating && curSequenceName != null && curMacroName != null)
     {
       sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
       {
         group(ids)
       }
     }

     if (!commandAnimating && curAnimationFragment != null)
     {
       curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
       {
         group(ids)
       }
     }
}
//ungroup <object_id> alias for delete <object_id>
export function ungroup(object_id){
    for (var i = 0; i < objectInfo.length; i++)
    {
        if (objectInfo[i].id == id)
        {
          deleteObject(objectInfo[i].object)
          objectInfo.splice(i, 1)
        }
    }
    scene.add(group)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        ungroup(object_id)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        ungroup(object_id)
      }
    }
}
//Altering objects
//Altering objects
//position <object_id> <x> <y> [z]
export function position(id,x,y,z){
    data(id).position.set(x,y,z)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        position(id,x,y,z)
      }
    }
}
//size <object_id> <w> <h>
export function size(id,w,h){
    data(id).scale.set(w,h,data(id).position.scale.z)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        size(id,w,h)
      }
    }
}
//scale <object_id> <scale>
export function scale(id,sc){
    data(id).scale.set(sc,sc,sc)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        scale(id,sc)
      }
    }
}
//scale <object_id> [x_scale] [y_scale] [z_scale]
export function scale2(id,x_scale,y_scale,z_scale){
    data(id).scale.set(x_scale,y_scale,z_scale)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        scale2(id,x_scale,y_scale,z_scale)
      }
    }
}
//rotation <object_id> <degrees_or_radians> <vector> rotates the object about the given vector
export function rotation(id,dor,v)
{
  let curObject = data(id)

  let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(v.x, v.y, v.z), toRadians(dor));
  let q2 = new THREE.Quaternion().copy(curObject.quaternion);
  curObject.quaternion.copy(q).multiply(q2);

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      rotation(id,dor,v)
    }
  }
}
//rotation <object_id> <degrees_or_radians> <x/y/z> rotates the object about the chosen axis (x, y, or z)
export function rotation2(id,dor,w)
{
  let curObject = data(id)

  if (w == "x")
  {
    curObject.rotateX(toRadians(x))
  }
  else if (w == "y")
  {
    curObject.rotateY(toRadians(y))
  }
  else
  {
    curObject.rotateZ(toRadians(z))
  }

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      rotation2(id,x,y,z)
    }
  }
}
//fill <object_id> <alpha-hex>
export function fill(id,alh){
    data(id).material.color = new THREE.Color(alh)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        fill(id,alh)
      }
    }
}
//fill <object_id> <r> <g> <b> [a]
export function fill2(id,r,g,b,a){
    data(id).material.color=new THREE.Color(r,g,b)
    data(id).material.opacity = a

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        fill2(id,r,g,b,a)
      }
    }
}
//border <object_id> <width> <alpha-hex>
export function border(id,w,h){
   var t=h.toString();
   var r=parseInt(t.substring(1,3),16)
   var g=parseInt(t.substring(3,5),16)
   var b=parseInt(t.substring(5,7),16)
   r=r/255
   g=g/255
   b=b/255
   addBorder(data(id),r,g,b,1,w)

   if (!commandAnimating && curSequenceName != null && curMacroName != null)
   {
     sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
     {
       border(id,w,h)
     }
   }
}
//border <object_id> <width> <r> <g> <b> [a]
export function border2(object,w,r,g,b,a){
    addBorder(object,r,g,b,a,w)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        border2(object,w,r,g,b,a)
      }
    }
}
//reset <object_id> [keyframe_id] resets the object to its inital state or to its state at given keyframe
export function reset(objID, keyframeID)
{
    let curObject = data(objID)
    let targetFragment = null

    if(id2 != null)
    {
      targetFragment = findFragment(keyframeID)
    }
    else
    {
      let cur = findObjectCurFragment(curObject, 9999999)

      while (true)
      {
        let pre = findObjectCurFragment(curObject, cur.startFrame)

        if (pre == null)
        {
          break
        }

        cur = pre
      }

      targetFragment = cur
    }

    curObject[i].position.x = curFragment.startPosition.x
    curObject[i].position.y = curFragment.startPosition.y
    curObject[i].position.z = curFragment.startPosition.z

    curObject[i].rotation.x = curFragment.startEulerAngle.x
    curObject[i].rotation.y = curFragment.startEulerAngle.y
    curObject[i].rotation.z = curFragment.startEulerAngle.z

    curObject[i].scale.x = curFragment.startScale.x
    curObject[i].scale.y = curFragment.startScale.y
    curObject[i].scale.z = curFragment.startScale.z

    curObject[i].material.opacity = curFragment.startAlpha

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        reset(objID, keyframeID)
      }
    }
}
//hide <object_id> hides the object completely, indepedent of alpha values
export function hide(id){
    data[i].visible=false

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        hide(id)
      }
    }
}
//show <object_id> shows the object again
export function show(id){
    data[i].visible=true

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        show(id)
      }
    }
}

//Animating
//keyframe create creates a new keyframe and adds all objects and their states to it
export function createKeyFrame()
{
  for (var i = 0; i < objectInfo.length; i++)
  {
    if (animationBuffer[objectInfo[i].object.id] == null)
    {
      let start = recordStartFrame(objectInfo[i].object)
      worldFrame += 1
      let end = recordEndFrame(objectInfo[i].object)

      recordAnimationFrame(start, end)
      recordFrame(objectInfo[i].object)
    }
    else
    {
      let curFrame = recordStartFrame(objectInfo[i].object)
      animationBuffer[objectInfo[i].object.id].frame = worldFrame

      recordAnimationFrame(animationBuffer[objectInfo[i].object.id], curFrame)
      animationBuffer[objectInfo[i].object.id] = curFrame
    }
  }

  curAnimationFragment = animationFragments[animationFragments.length - 1]

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = createKeyFrame
  }
}
//keyframe duration <keyframe_id> <duration> sets the keyframe's duration in milliseconds
export function setKeyframeDuration(id, duration)
{
  let curFragment = data(id)
  curFragment.duration = duration
  worldFrame = curFragment.startFrame + duration

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      setKeyframeDuration(id, duration)
    }
  }
}

//keyframe clear <keyframe_id> clears the given keyframe of all data except its duration and position in the sequence (essentially reverts it to an empty keyframe, a copy of the previous one)
export function clearKeyFrame(id)
{
  deleteContent(id)

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      clearKeyFrame(id)
    }
  }
}
//keyframe goto <keyframe_id> sets the visual state of the animation to the given keyframe's state
export function goToKeyFrame(id)
{
  worldFrame = data(id).startFrame
  enterAnimationMode()
}

//keyframe return effectively a macro for keyframe goto for the last created keyframe
export function returnKeyframe()
{
  let lastKeyFrameDuration = 0

  for (var i = 0; i < animationFragments.length; i++)
  {
    if(lastKeyFrameDuration < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      lastKeyFrameDuration = animationFragments[i].startFrame + animationFragments[i].duration
    }
  }

  worldFrame = lastKeyFrameDuration
  enterAnimationMode()
}
//keyframe preview <keyframe_id> plays the animation from the keyframe preceding the given keyframe to the given keyframe
export function previewKeyframe(id)
{
  let lastKeyFrameDuration = 0
  let lastKeyFrameID = -1

  for (var i = 0; i < animationFragments.length; i++)
  {
    if(lastKeyFrameDuration < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      lastKeyFrameDuration = animationFragments[i].startFrame + animationFragments[i].duration
      lastKeyFrameID = animationFragments[i].id
    }
  }

  playKeyFrameBetween(id, lastKeyFrameID)
}

//keyframe preview <keyframe_id> <keyframe_id> plays the animations between the two keyframes
export function playKeyFrameBetween(id1, id2)
{
  let curFrame = data[id1].startFrame
  let frame = data[id2].startFrame

  worldFrame = curFrame
  enterAnimationMode()

  var loop = function()
  {
    if (curFrame == frame + 1)
    {
      commandAnimating = false

      return
    }

    viewAnimationStatusAt(curFrame)

    requestAnimationFrame(loop)
  };

  commandAnimating = true
  loop()
}
//macro record [name] starts recording a new macro; commands from this point on will be recorded and added to this macro
export function macroStartRecording(name)
{
  animationFragments = []
  objectInfo = []

  worldFrame = 0

  sequences[curSequenceName].macros[name] =
  {
    "id": globalID++,
    "name": name,
    "commands": [],
    "animationFragments": animationFragments,
    "objectInfo": objectInfo
  }

  curMacroName = name
}
//macro stop stops recording the macro
export function macroStopRecording()
{
  animationFragments = []
  objectInfo = []

  curMacroName = null
  curAnimationFragment = null
}
//macro [name_or_id] executes an existing macro; they can also be executed like a command by invoking their name
export function exec_enter_macro(name_or_id)
{
  let curMacro = null

  if (sequences[curSequenceName].macros[name_or_id] != null)
  {
    curMacro = sequences[curSequenceName].macros[name_or_id]
  }
  else
  {
    curMacro = data[name_or_id]
  }

  curMacroName = curMacro.name
  animationFragments = curMacro.animationFragments
  objectInfo = curMacro.objectInfo

  worldFrame = 0
  isAnimating = true
  commandAnimating = true

  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  animateAllFragments(animationFragments)
}
//sequence create [animation_name] creates a new animation; commands from this point on are part of the animation
export function createSequence(name)
{
  sequences[name] =
  {
    "id": globalID++,
    "name": name,
    "macros": {}
  }

  curSequenceName = name
}
//sequence duration <duration> sets the duration of the animation in milliseconds
export function setSequenceDuration(duration)
{
  let animationFragmentsCopy = []
  let curDuration = 0

  for (let macroKey in sequences[curSequenceName].macros)
  {
    animationFragmentsCopy = animationFragmentsCopy.concat(sequences[curSequenceName].macros[macroKey].animationFragments)
  }

  for (var i = 0; i < animationFragmentsCopy.length; i++)
  {
    curDuration += animationFragmentsCopy[i].duration
  }

  for (var i = 0; i < animationFragmentsCopy.length; i++)
  {
    animationFragmentsCopy[i].duration *= (duration / curDuration)
  }
}

//sequence play plays the animation from beginning to end
export function playSequence()
{
  animationFragments = []
  objectInfo = []

  for (let macroKey in sequences[curSequenceName].macros)
  {
    animationFragments = animationFragments.concat(sequences[curSequenceName].macros[macroKey].animationFragments)
    objectInfo = objectInfo.concat(sequences[curSequenceName].macros[macroKey].objectInfo)
  }

  worldFrame = 0
  isAnimating = true
  commandAnimating = true

  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  animateAllFragments(animationFragments)
}
//save export commands to text file available to download; this command is never recorded
export function save()
{

}

//unility
//variable create <name> <value> create a variable to be used by name in place of actual values from this point on
export function variableCreate(name, v)
{
  variables[name] =
  {
    "data": v,
    "id": globalID++
  }

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      variableCreate(name, v)
    }
  }
}
//variable set <name> <value> change the value of an existing variable
export function variableSet(name, v)
{
  if (variables[name] == null)
  {
    return
  }

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      variableSet(name, v)
    }
  }

  variables[name].data = v
}
//data <id> returns a map of the given object/keyframe/macro/variable data to be used in variables or simply for display;
//values are by reference, but readonly
export function data(id)
{
  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      data(id)
    }
  }

  for (let sequenceKey in sequences)
  {
    for (let macroKey in sequences[sequenceKey].macros)
    {
      if(sequences[sequenceKey].macros[macroKey].id == id)
      {
        return sequences[sequenceKey].macros[macroKey]
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].objectInfo.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].objectInfo[i].id == id)
        {
          return sequences[sequenceKey].macros[macroKey].objectInfo[i].object
        }
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].animationFragments.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].animationFragments[i].id == id)
        {
          return sequences[sequenceKey].macros[macroKey].animationFragments[i]
        }
      }
    }
  }

  for (let key in variables)
  {
    if(variables[key].id == id)
    {
      return variables[key]
    }
  }
}
//delete <object_id>
export function deleteContent(id)
{
  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      deleteContent(id)
    }
  }

  for (let sequenceKey in sequences)
  {
    for (let macroKey in sequences[sequenceKey].macros)
    {
      if(sequences[sequenceKey].macros[macroKey].id == id)
      {
        sequences[sequenceKey].macros[macroKey] = undefined

        return
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].objectInfo.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].objectInfo[i].id == id)
        {
          deleteObject(sequences[sequenceKey].macros[macroKey].objectInfo[i].object)
          sequences[sequenceKey].macros[macroKey].objectInfo.splice(i, 1)

          return
        }
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].animationFragments.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].animationFragments[i].id == id)
        {
          sequences[sequenceKey].macros[macroKey].animationFragments.splice(i, 1)

          return
        }
      }
    }
  }

  for (let key in variables)
  {
    if(variables[key].id == id)
    {
      variables[key] = undefined

      return
    }
  }
}
//help lists all existing commands and short descriptions; this command is never recorded
export function help()
{
  //TODO
}
//man <command> our equivalent of man pages for our commands; this command is never recorded
export function man()
{
  //TODO
}
