export function load(path, objSelf, callBack)
{
  var loader = new THREE.OBJLoader();

  loader.load(path, function(object)
  {
    objSelf[0] = object;

    if(object.material == null)
    {
      object.material = new THREE.MeshPhongMaterial( { color: 0xb6b311, specular: 0x222222, shininess: 50} );
    }

    object.material.transparent = true

    if(callBack != null)
    {
      callBack()
    }
  },
  function()
  {

  },
  function(e)
  {
    console.error( e );
  });
}

export function checkExists(mousePos, zone)
{
  if((mousePos.x + 1) / 2 < zone.x / 100)
  {
    return false
  }

  if((mousePos.x + 1) / 2 > (zone.x + zone.width) / 100)
  {
    return false
  }

  if((mousePos.y + 1) / 2 < zone.y / 100)
  {
    return false
  }

  if((mousePos.y + 1) / 2 > (zone.y + zone.height) / 100)
  {
    return false
  }

  return true
}

export function setObjectsVisibilityInScene(visibility, scene)
{
  for(let i = 0; i < objectInfo.length; i += 1)
  {
    objectInfo[i].object.visible = visibility
  }
}

export function initBasicScene()
{
  initHandler()

  let light = new THREE.SpotLight( new THREE.Color(0.627, 0.627, 0.627) );
  light.position.set(0, 2, 15);
  light.castShadow = true;
  light.penumbra = 0.384;
  light.intensity = 4;
  light.shadow.mapSize.width = 100;
  light.shadow.mapSize.height = 100;
  light.shadow.camera.near = 0.5;
  light.shadow.camera.far = 30;
  scene.add( light );
}

export function clearSceneObjects()
{
  for(let i = 0; i < objectInfo.length; i += 1)
  {
    scene.remove(objectInfo[i].object)
    deleteObject(objectInfo[i].object)
  }

  objectInfo = []
}

export function saveJson()
{
  clearAnimationData(animationFragments)

  let savingFragments = JSON.parse(JSON.stringify(animationFragments))
  toBasicStructure(animationFragments, savingFragments)
  slLoader.value = JSON.stringify(savingFragments, null, 4)
}

export function loadJson(jsonString)
{
  clearSceneObjects()
  globalID = 0

  animationFragments = JSON.parse(jsonString)

  let curObjects = []

  //init list first
  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(curObjects[animationFragments[i].objectInfo.id] == null)
    {
      if(animationFragments[i].objectInfo.type == "PlaneGeometry")
      {
        animationFragments[i].object = newPlane(0, 0, 0, 1, 1)
      }
      else if (animationFragments[i].objectInfo.type == "SphereGeometry")
      {
        animationFragments[i].object = newSphere(0, 0, 0, 1)
      }
      else if (animationFragments[i].objectInfo.type == "BoxGeometry")
      {
        animationFragments[i].object = newCube(0, 0, 0, 1, 1, 1)
      }

      if(animationFragments[i].objectInfo.texture != null)
      {
        animationFragments[i].object.material.map = animationFragments[i].objectInfo.texture
      }

      if(animationFragments[i].objectInfo.color != null)
      {
        animationFragments[i].object.material.color = new THREE.Color(animationFragments[i].objectInfo.color.x, animationFragments[i].objectInfo.color.y, animationFragments[i].objectInfo.color.z)
      }

      curObjects[animationFragments[i].objectInfo.id] = animationFragments[i].object

      scene.add(animationFragments[i].object)
      objectInfo[objectInfo.length] =
      {
        "object": animationFragments[i].object
      }
    }
    else
    {
      animationFragments[i].object = curObjects[animationFragments[i].objectInfo.id]

      objectStatus[animationFragments[i].object.id].curID = animationFragments[i].id
      console.log("F = " + animationFragments[i].id);
    }

    globalID = Math.max(globalID, animationFragments[i].id + 1)
  }

  //link fragments
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if(animationFragments[i].ballisticMovementNext != null)
    {
      animationFragments[i].ballisticMovementNext = animationFragments[animationFragments[i].ballisticMovementNext]
    }

    if(animationFragments[i].autoRotationNext != null)
    {
      animationFragments[i].autoRotationNext = animationFragments[animationFragments[i].autoRotationNext]
    }

    if(animationFragments[i].parentFragment != null)
    {
      animationFragments[i].parentFragment = animationFragments[animationFragments[i].parentFragment]
    }


    for (let j = 0; j < animationFragments[i].childFragments.length; j += 1)
    {
      animationFragments[i].childFragments[j] = animationFragments[animationFragments[i].childFragments[j]]
    }
  }
}

export function toBasicStructure(fragments, dstFragments)
{
  for(let i = 0; i < fragments.length; i += 1)
  {
    if(fragments[i].ballisticMovementNext != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].ballisticMovementNext == fragments[j])
        {
          dstFragments[i].ballisticMovementNext = j
          break
        }
      }
    }

    if(fragments[i].autoRotationNext != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].autoRotationNext == fragments[j])
        {
          dstFragments[i].autoRotationNext = j
          break
        }
      }
    }

    if(fragments[i].childFragments.length > 0)
    {
      for(let j = 0; j < fragments[i].childFragments.length; j += 1)
      {
        for(let k = 0; k < fragments.length; k += 1)
        {
          if(fragments[i].childFragments[j] == fragments[k])
          {
            dstFragments[i].childFragments[j] = k
            break
          }
        }
      }
    }

    if(fragments[i].parent != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].parent == fragments[j])
        {
          dstFragments[i].parent = j
          break
        }
      }
    }
  }

  for(let i = 0; i < dstFragments.length; i += 1)
  {
    dstFragments[i].objectInfo.id = fragments[i].object.id
    dstFragments[i].objectInfo.type = fragments[i].object.geometry.type
    dstFragments[i].objectInfo.color = {"x": fragments[i].object.material.color.r, "y": fragments[i].object.material.color.g, "z": fragments[i].object.material.color.b}

    dstFragments[i].object = undefined
  }
}

export function atOnePoint(obj, vec3a, vec3b)
{
  return vec3a.x == obj.position.x && vec3a.y == obj.position.y && vec3a.z == obj.position.z ||
    vec3b.x == obj.position.x && vec3b.y == obj.position.y && vec3b.z == obj.position.z
}

export function addHelpText(content)
{
  let curData = {}

  curData.pic = newImage(0, 0, 20, 10, "", 0, 0, 0, 0, document.getElementById("root"))
  curData.text = newText(0, 0, content, curData.pic)
  curData.text.style.color = "red"
  curData.duration = 3

  systemInfo.splice(0, 0, curData)
}

export function physicallyEqual(fragment1, fragment2)
{
  let bias = 0.01

  return Math.abs(fragment1.endPosition.x - fragment2.startPosition.x) < bias &&
    Math.abs(fragment1.endPosition.y - fragment2.startPosition.y) < bias &&
    Math.abs(fragment1.endPosition.z - fragment2.startPosition.z) < bias
    // &&
    // Math.abs(fragment1.endEulerAngle.x - fragment2.startEulerAngle.x) < bias &&
    // Math.abs(fragment1.endEulerAngle.y - fragment2.startEulerAngle.y) < bias &&
    // Math.abs(fragment1.endEulerAngle.z - fragment2.startEulerAngle.z) < bias &&
    // Math.abs(fragment1.endScale.x - fragment2.endScale.x) < bias &&
    // Math.abs(fragment1.endScale.y - fragment2.endScale.y) < bias &&
    // Math.abs(fragment1.endScale.z - fragment2.endScale.z) < bias
}

export function vector3Equals(v1, v2)
{
  return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z
}

export function findObjectExistsFragment(object, fragments, mode)
{
  let startFirst = function()
  {
    for (let i = 0; i < fragments.length; i += 1)
    {
      if (vector3Equals(object.position, fragments[i].startPosition) )
      {
        return fragments[i]
      }
    }
  }

  let endFirst = function()
  {
    for (let i = 0; i < fragments.length; i += 1)
    {
      if (vector3Equals(object.position, fragments[i].endPosition) )
      {
        return fragments[i]
      }
    }
  }

  if (mode == 1)
  {
    let curFragment = startFirst()

    if (curFragment != null)
    {
      return curFragment
    }
  }

  return endFirst()
}

export function obtainReviseFragmentsInBothSides(object, count)
{
  let cur = findObjectCurFragment(object)
  let midIndex = -1

  //no fragment for this object
  if (cur == null)
  {
    return
  }

  // left
  while(cur != null)
  {
    let pre = findPreviousFragment(object, cur.startFrame)

    if(pre == null || !physicallyEqual(pre, cur))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = pre
    cur = pre
  }

  //revert
  for(let i = 0; i < anotherWorldFragments.length / 2; i += 1)
  {
    let copy = anotherWorldFragments[i]
    anotherWorldFragments[i] = anotherWorldFragments[anotherWorldFragments.length - i - 1]
    anotherWorldFragments[anotherWorldFragments.length - i - 1] = copy
  }

  cur = findObjectCurFragment(object)

  // middle
  if(cur != null)
  {
    midIndex = anotherWorldFragments.length
    anotherWorldFocusIndex = midIndex
    anotherWorldFragments[anotherWorldFragments.length] = cur
  }

  // right
  while(cur != null)
  {
    let next = findNextFragment(object, cur.startFrame + cur.duration)

    if(next == null || !physicallyEqual(cur, next))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = next
    cur = next
  }

  let startIndex = Math.max(0, midIndex - parseInt(count / 2))
  let endIndex = Math.min(anotherWorldFragments.length, midIndex + Math.ceil(count / 2))

  //console.log("org:" + startIndex + " !!! " + midIndex + " !!! " + endIndex + " !!! " + anotherWorldMaxPresent);

  //check if previous continuely curve movement
  for (let i = midIndex; i >= 0 && i >= midIndex - anotherWorldMaxPresent / 2; i -= 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      startIndex = Math.min(startIndex, i)
    }
  }

  for (let i = midIndex; i < anotherWorldFragments.length && i <= midIndex + anotherWorldMaxPresent / 2; i += 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      endIndex = i + 2
    }
  }

  //change max present
  if (startIndex != midIndex - parseInt(count / 2) || endIndex != midIndex + parseInt(count / 2))
  {
    anotherWorldMaxPresent = Math.max(endIndex - midIndex, midIndex - startIndex) * 2
    //console.log(startIndex + " !!! " + midIndex + " !!! " + endIndex + " !!! " + anotherWorldMaxPresent);
  }

  for(let i = startIndex; i < endIndex; i += 1)
  {
    if (i < 0 || i >= anotherWorldFragments.length)
    {
      continue
    }

    let cloneObject = cloneAll(object)

    cloneObject.position.set(anotherWorldFragments[i].startPosition.x, anotherWorldFragments[i].startPosition.y, anotherWorldFragments[i].startPosition.z)

    if (cloneObject.rotationType == "Object")
    {

    }
    else if (cloneObject.rotationType == "Vertex")
    {

    }
    else
    {
      cloneObject.rotation.set(anotherWorldFragments[i].startEulerAngle.x, anotherWorldFragments[i].startEulerAngle.y, anotherWorldFragments[i].startEulerAngle.z)
    }

    cloneObject.scale.set(anotherWorldFragments[i].startScale.x, anotherWorldFragments[i].startScale.y, anotherWorldFragments[i].startScale.z)

    cloneObject.material.opacity = 0.5
    cloneObject.visible = true

    anotherWorldObjects[anotherWorldObjects.length] = cloneObject
    scene.add(cloneObject)

    //DELETE Border
    for (let j = 0; j < cloneObject.children.length; j += 1)
    {
      if(cloneObject.children[j].material.side == THREE.BackSide)
      {
        deleteObject(cloneObject.children[j])
      }
    }

    if(i == midIndex)
    {
      addBorder(cloneObject, 1, 0, 0, 0.5, 0.05, false)
    }

    if(i == endIndex - 1 || i == anotherWorldFragments.length - 1)
    {
      let cloneObject = cloneAll(object)

      //DELETE Border
      for (let j = 0; j < cloneObject.children.length; j += 1)
      {
        if(cloneObject.children[j].material.side == THREE.BackSide)
        {
          deleteObject(cloneObject.children[j])
        }
      }

      cloneObject.position.set(anotherWorldFragments[i].endPosition.x, anotherWorldFragments[i].endPosition.y, anotherWorldFragments[i].endPosition.z)
      cloneObject.rotation.set(anotherWorldFragments[i].endEulerAngle.x, anotherWorldFragments[i].endEulerAngle.y, anotherWorldFragments[i].endEulerAngle.z, "YZX")
      cloneObject.scale.set(anotherWorldFragments[i].endScale.x, anotherWorldFragments[i].endScale.y, anotherWorldFragments[i].endScale.z)

      cloneObject.material.opacity = 0.5
      cloneObject.visible = true

      anotherWorldObjects[anotherWorldObjects.length] = cloneObject
      scene.add(cloneObject)
    }
  }

  for(let i = 0; i < anotherWorldObjects.length - 1; i += 1)
  {
    if(anotherWorldFragments[Math.max(0, startIndex) + i].ballisticMovementNext == null)
    {
      let line = new THREE.LineCurve3(anotherWorldObjects[i].position, anotherWorldObjects[i + 1].position)

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      anotherWorldLines[i] = curveObject
      scene.add(curveObject)
    }
    else
    {
      let curvePoints = []

      while (i < anotherWorldObjects.length - 1)
      {
        let curPos = new THREE.Vector3()
        curPos.x = anotherWorldFragments[startIndex + i].startPosition.x
        curPos.y = anotherWorldFragments[startIndex + i].startPosition.y
        curPos.z = anotherWorldFragments[startIndex + i].startPosition.z
        curvePoints[curvePoints.length] = curPos

        if(anotherWorldFragments[startIndex + i].ballisticMovementNext == null)
        {
          curPos = new THREE.Vector3()
          curPos.x = anotherWorldFragments[startIndex + i].endPosition.x
          curPos.y = anotherWorldFragments[startIndex + i].endPosition.y
          curPos.z = anotherWorldFragments[startIndex + i].endPosition.z
          curvePoints[curvePoints.length] = curPos

          break
        }

        i += 1
      }

      let line = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      for(let j = i; j >= 0; j -= 1)
      {
        if(anotherWorldLines[j] != null)
        {
          break
        }

        anotherWorldLines[j] = curveObject
      }

      scene.add(curveObject)
    }
  }
}

export function updateObjectsInFrame(object, curves, tObjects, fragments)
{
  if(object == thisWorldObject)
  {
    return
  }

  let curIndex = -1
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))

  for(let i = 0; i < tObjects.length; i += 1)
  {
    if(object == tObjects[i])
    {
      curIndex = i

      break
    }
  }

  let curveMovementPoints = []
  let isEntered = false
  let updated = false

  console.log(curIndex + " " + prefix + " " + anotherWorldMaxPresent + " " + anotherWorldFocusIndex);

  //obtain the curve movements points that close to the mid point
  for(let i = 0; i < curves.length; i += 1)
  {
    //check if this is the last element
    if (fragments[prefix + i].ballisticMovementNext != null || curveMovementPoints.length > 0 && fragments[prefix + i].ballisticMovementNext == null)
    {
      curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
        fragments[prefix + i].startPosition.x,
        fragments[prefix + i].startPosition.y,
        fragments[prefix + i].startPosition.z
      )
    }

    if (Math.min(curves.length - 1, curIndex) == i && curveMovementPoints.length > 0)
    {
      isEntered = true
    }

    if (fragments[prefix + i].ballisticMovementNext == null && curveMovementPoints.length > 0)
    {
      if (isEntered)
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + i].endPosition.x,
          fragments[prefix + i].endPosition.y,
          fragments[prefix + i].endPosition.z
        )
      }
      else
      {
        curveMovementPoints = []
      }
    }
  }

  if(0 <= prefix + curIndex - 1)
  {
    fragments[prefix + curIndex - 1].endPosition.x = object.position.x
    fragments[prefix + curIndex - 1].endPosition.y = object.position.y
    fragments[prefix + curIndex - 1].endPosition.z = object.position.z

    fragments[prefix + curIndex - 1].endEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex - 1].endEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex - 1].endEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex - 1].endScale.x = object.scale.x
    fragments[prefix + curIndex - 1].endScale.y = object.scale.y
    fragments[prefix + curIndex - 1].endScale.z = object.scale.z

    fragments[prefix + curIndex - 1].endAlpha.x = object.scale.x
    fragments[prefix + curIndex - 1].endAlpha.y = object.scale.y
    fragments[prefix + curIndex - 1].endAlpha.z = object.scale.z

    //pre
    if(0 <= curIndex - 1)
    {
      let curve
      let curPoints

      if(fragments[prefix + curIndex - 1].movementType != "Ballist")
      {
        curve = new THREE.LineCurve3(tObjects[curIndex - 1].position, object.position)
        curPoints = curve.getPoints( 50 );
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
        curPoints = curve.getPoints((curveMovementPoints.length - 1) * 50)

        updated = true
      }

      curves[curIndex - 1].geometry.attributes.position.count = curPoints.length
      curves[curIndex - 1].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex - 1].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex - 1].geometry.attributes.position.needsUpdate = true
    }
  }

  //next
  //prevent curve movement double update
  if (prefix + curIndex < fragments.length || curveMovementPoints.length > 0 && !updated)
  {
    fragments[prefix + curIndex].startPosition.x = object.position.x
    fragments[prefix + curIndex].startPosition.y = object.position.y
    fragments[prefix + curIndex].startPosition.z = object.position.z

    fragments[prefix + curIndex].startEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex].startEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex].startEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex].startScale.x = object.scale.x
    fragments[prefix + curIndex].startScale.y = object.scale.y
    fragments[prefix + curIndex].startScale.z = object.scale.z

    fragments[prefix + curIndex].startAlpha.x = object.scale.x
    fragments[prefix + curIndex].startAlpha.y = object.scale.y
    fragments[prefix + curIndex].startAlpha.z = object.scale.z

    if(curIndex < anotherWorldLines.length)
    {
      let curve
      let curPoints

      if(fragments[prefix + curIndex].movementType != "Ballist")
      {
        curve = new THREE.LineCurve3(object.position, tObjects[curIndex + 1].position)
        curPoints = curve.getPoints( 50 );
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
        curPoints = curve.getPoints((curveMovementPoints.length - 1) * 50)
      }

      curves[curIndex].geometry.attributes.position.count = curPoints.length
      curves[curIndex].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex].geometry.attributes.position.needsUpdate = true
    }
  }
}

/**
 * View a scene at specific frame, note that initAnimationLoop()
 * should be called first
 */
export function viewAnimationStatusAt(curFrame)
{
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if (animationFragments[i].startFrame <= curFrame && curFrame < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      for (let j = 0; i < animationFragments[i].callBefore.length; j += 1)
      {
        animationFragments[i].callBefore[i]()
      }

      if (curFrame == animationFragments[i].startFrame + animationFragments[i].duration - 1)
      {
        for (let j = 0; i < animationFragments[i].callBack.length; j += 1)
        {
          animationFragments[i].callBack[i]()
        }
      }

      setFragmentStatusAt(animationFragments[i], curFrame - animationFragments[i].startFrame)
    }
  }
}

export function EditStartFrameFrom(fragment, durationChangeValue)
{
  if (fragment == null)
  {
    return
  }

  EditStartFrameFrom(findNextFragment(fragment.object, fragment.startFrame + fragment.duration), durationChangeValue)
  fragment.startFrame += durationChangeValue
}

export function deleteFragmentIn(fragment, fragments)
{
  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i] == fragment)
    {
      fragments.splice(i, 1)

      break
    }
  }
}

export function editHelpText(mode)
{
  if(mode == "Standard")
  {
    systemHelp.text.innerText = StandardHelpText
  }
  else if(mode == "Revise")
  {
    systemHelp.text.innerText = ReviseHelpText
  }
  else if (mode == "Animation" || mode == "Animation (Root)")
  {
    systemHelp.text.innerText = AnimationHelpText
  }
}

/**
 * point : [in] Vector2 : NewKeyframe's location
 * curves : [in] Object3D Array : Lines / Easing curves
 * tObjects : [in/out] Object3D Array: Temperory objects
 * fragments : [in/out] AnimationFragments Json Array : fragments to insert a new fragment
 */
export function insertFrame(point, curves, tObjects, fragments)
{
  let min=Number.MAX_VALUE
  let index = -1
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))

  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].startFrame < worldFrame && worldFrame < fragments[i].startFrame + fragments[i].duration)
    {
      index = i + prefix

      break
    }
  }

  if(index != -1)
  {
    let intersects = new THREE.Vector3()

    raycaster.setFromCamera(point, camera)
    raycaster.ray.intersectPlane(plane, intersects)
    let point3D = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

    let newObject = cloneAll(thisWorldObject)
    newObject.position.set(point3D.x, point3D.y, point3D.z)
    newObject.material.visible = true
    newObject.material.opacity = 0.5
    newObject.material.transparent = true

    let newFragment =
    {
      "id": globalID++,
      "object": thisWorldObject,
      "objectInfo":
      {
        "name": fragments[index].objectInfo.name,
        "vertex": [],
        "posWeight": [],
        "rotationWeight": [],
        "scaleWeight": [],
        "alphaWeight": [],

        // "border":
        // {
        //   show = 1,
        //   color = {"x": 1, "y": 1, "z": 1},
        //   size = 0.05,
        //   borderTest = -1
        // }
      },

      "parentFragment": undefined,
      "childFragments": [],

      "startPosition": {"x": newObject.position.x, "y": newObject.position.y, "z": newObject.position.z},
      "endPosition": {"x": fragments[index].endPosition.x, "y": fragments[index].endPosition.y, "z": fragments[index].endPosition.z},
      "startEulerAngle": null,
      "endEulerAngle": {"x": fragments[index].endEulerAngle.x, "y": fragments[index].endEulerAngle.y, "z": fragments[index].endEulerAngle.z},
      "startScale": null,
      "endScale": {"x": fragments[index].endScale.x, "y": fragments[index].endScale.y, "z": fragments[index].endScale.z},
      "startAlpha": null,
      "endAlpha": fragments[index].endAlpha,

      "activeBefore": 1,
      "activeAfter": 1,

      "callBefore": [],
      "callBack": [],

      "startFrame": worldFrame,
      "duration": fragments[index].startFrame + fragments[index].duration - worldFrame,

      "posEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

      "movementType": fragments[index].movementType,
      "ballisticMovementNext": fragments[index].ballisticMovementNext,

      "rotationType": fragments[index].rotationType,
      "autoRotationNext": fragments[index].autoRotationNext
    }

    let insertWidth = (worldFrame - fragments[index].startFrame) / fragments[index].duration

    //update two closest frames' info
    if (fragments[index].ballisticMovementNext != undefined || fragments[index - 1] != null && fragments[index - 1].ballisticMovementNext != undefined)
    {
      fragments[index].ballisticMovementNext = newFragment
    }

    if (fragments[index].autoRotationNext != undefined || fragments[index - 1] != null && fragments[index - 1].autoRotationNext != undefined)
    {
      fragments[index].autoRotationNext = newFragment
    }

    let easeCurvesToUpdate = [fragments[index].posEasePoints, fragments[index].rotationEasePoints, fragments[index].scaleEasePoints, fragments[index].alphaEasePoints]
    let easeCurvesToChange = [newFragment.posEasePoints, newFragment.rotationEasePoints, newFragment.scaleEasePoints, newFragment.alphaEasePoints]

    for(let i = 0; i < 4; i += 1)
    {
      let curCurvePosition = []

      for(let j = 0; j < easeCurvesToUpdate[i].length; j += 1)
      {
        curCurvePosition[j] = new THREE.Vector3(easeCurvesToUpdate[i][j].x, easeCurvesToUpdate[i][j].y, 0)
      }

      let curve = new THREE.CatmullRomCurve3(curCurvePosition, false, "chordal")
      let insertHeight = curve.getPointAt(insertWidth).y

      if (i == 1)
      {
        fragments[index].endEulerAngle = lerp3(fragments[index].startEulerAngle, fragments[index].endEulerAngle, insertHeight)
        newFragment.startEulerAngle = lerp3(fragments[index].startEulerAngle, fragments[index].endEulerAngle, insertHeight)
      }
      else if(i == 2)
      {
        fragments[index].endScale = lerp3(fragments[index].startScale, fragments[index].endScale, insertHeight)
        newFragment.startScale = lerp3(fragments[index].startScale, fragments[index].endScale, insertHeight)
      }
      else if (i == 3)
      {
        fragments[index].endAlpha = lerp1(fragments[index].startAlpha, fragments[index].endAlpha, insertHeight)
        newFragment.startAlpha = lerp1(fragments[index].startAlpha, fragments[index].endAlpha, insertHeight)
      }

      let leftCurve = []
      let rightCurve = []

      for (var j = 0; j < easeCurvesToUpdate[i].length; j++)
      {
        if (easeCurvesToUpdate[i][j].x < insertWidth)
        {
          leftCurve[leftCurve.length] = easeCurvesToUpdate[i][j]
        }
        else if (easeCurvesToUpdate[i][j].x > insertWidth)
        {
          rightCurve[rightCurve.length] = easeCurvesToUpdate[i][j]
        }
      }

      leftCurve[leftCurve.length] = {"x": insertWidth, "y": insertHeight}
      rightCurve.splice(0, 0, {"x": insertWidth, "y": insertHeight});

      //check validibility
      for (let j = 0; j < leftCurve.length; j += 1)
      {
        if (leftCurve[j].y < 0 || insertHeight < leftCurve[j].y)
        {
          deleteObject(newObject)

          return "Invalid fragment because an irregular interpolation curve"
        }
      }

      for (let j = 0; j < rightCurve.length; j += 1)
      {
        if (rightCurve[j].y < insertHeight || 1 < rightCurve[j].y)
        {
          deleteObject(newObject)

          return "Invalid fragment because an irregular interpolation curve"
        }
      }

      //normalize left
      for (var j = 0; j < leftCurve.length; j += 1)
      {
        leftCurve[j].x = Math.round(leftCurve[j].x / insertWidth * 1000) / 1000
        leftCurve[j].y = Math.round(leftCurve[j].y / insertHeight * 1000) / 1000
      }

      //normalize right
      for (var j = 0; j < rightCurve.length; j += 1)
      {
        rightCurve[j].x = Math.round((rightCurve[j].x - insertWidth) / (1 - insertWidth) * 1000) / 1000
        rightCurve[j].y = Math.round((rightCurve[j].y - insertHeight) / (1 - insertHeight) * 1000) / 1000
      }

      easeCurvesToUpdate[i] = leftCurve
      easeCurvesToChange[i] = rightCurve
    }

    //update left curve Object
    //if curve movement
    if (fragments[index].ballisticMovementNext != undefined || fragments[index - 1] != null && fragments[index - 1].ballisticMovementNext != undefined)
    {
      let curveMovementPoints = []

      //record current frame's position
      for (let j = 0; j < curves.length; j += 1)
      {
        if (curves[j] == curves[index - prefix])
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            fragments[prefix + j].startPosition.x,
            fragments[prefix + j].startPosition.y,
            fragments[prefix + j].startPosition.z
          )
        }

        //mid
        if (j == index - prefix)
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            newFragment.startPosition.x,
            newFragment.startPosition.y,
            newFragment.startPosition.z
          )
        }

        //not or last
        if (curveMovementPoints.length > 0 && curves[j] != curves[index - prefix] || j == curves.length - 1)
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            fragments[prefix + j].endPosition.x,
            fragments[prefix + j].endPosition.y,
            fragments[prefix + j].endPosition.z
          )

          break
        }
      }

      let curCurve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
      let curPoints = curCurve.getPoints((curveMovementPoints.length - 1) * 50)

      curves[index - prefix].geometry.attributes.position.count = curPoints.length
      curves[index - prefix].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[index - prefix].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[index - prefix].geometry.attributes.position.needsUpdate = true

      curves.splice(index - prefix + 1, 0, curves[index - prefix])
    }
    //normal line movement
    else
    {
      let curCurve = new THREE.LineCurve3(tObjects[index - prefix].position, newObject.position)
      let curPoints = curCurve.getPoints(50)

      curves[index - prefix].geometry.attributes.position.count = curPoints.length
      curves[index - prefix].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[index - prefix].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[index - prefix].geometry.attributes.position.needsUpdate = true

      let newCurve = new THREE.LineCurve3(newObject.position, new THREE.Vector3(newFragment.endPosition.x, newFragment.endPosition.y, newFragment.endPosition.z))
      let points = newCurve.getPoints( 50 );
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      scene.add(curveObject)
      curves.splice(index - prefix + 1, 0, curveObject)
    }

    //update basic info
    fragments[index].endPosition.x = newObject.position.x
    fragments[index].endPosition.y = newObject.position.y
    fragments[index].endPosition.z = newObject.position.z
    fragments[index].duration = worldFrame - fragments[index].startFrame

    anotherWorldMaxPresent += 2

    if (index < anotherWorldFocusIndex)
    {
        anotherWorldMaxPresent += 1
    }

    //insert new info
    tObjects.splice(index - prefix + 1, 0, newObject)
    fragments.splice(index + 1, 0, newFragment)
    scene.add(newObject)
    animationFragments[animationFragments.length] = newFragment

    return "OK"
  }

  return "No fragment have found for this object"
}

export function deleteFrame(object, curves, tObjects, fragments)
{
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))
  let curIndex = -1

  let curveMovementStartIndex = -1

  for (let i = 0; i < tObjects.length; i += 1)
  {
    if(object == tObjects[i])
    {
      curIndex = i + prefix
      break
    }
  }

  //first object
  if(curIndex - prefix == 0)
  {
    if (fragments[curIndex].ballisticMovementNext != null)
    {
      curveMovementStartIndex = 0
    }
    else
    {
      deleteObject(curves[0])
    }

    //if current fragment is this fragment, change to the
    //next one
    if (fragments.length > 1 && fragments[curIndex] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex + 1].id
    }
    else
    {
      objectStatus[thisWorldObject.id].curID = -1
    }

    deleteObject(tObjects[0])
    deleteFragmentIn(fragments[curIndex], animationFragments)

    tObjects.splice(0, 1)
    curves.splice(0, 1)
    fragments.splice(curIndex, 1)
  }
  //last object
  else if (curIndex - prefix == tObjects.length - 1)
  {
    if (fragments[curIndex - 2] != null && fragments[curIndex - 2].ballisticMovementNext != null)
    {
      fragments[curIndex - 2].ballisticMovementNext = null

      curveMovementStartIndex = curIndex - 2
    }
    else
    {
      deleteObject(curves[curIndex - prefix - 1])
    }

    //if current fragment is this fragment, change to the
    //previous one
    if (fragments[curIndex - 2] != null && fragments[curIndex - 1] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex - 2].id
    }
    else
    {
      objectStatus[thisWorldObject.id].curID = -1
    }

    deleteObject(tObjects[curIndex - prefix])
    deleteFragmentIn(fragments[curIndex - 1], animationFragments)

    tObjects.splice(curIndex - prefix, 1)
    curves.splice(curIndex - prefix - 1, 1)
    fragments.splice(curIndex - 1, 1)
  }
  //mid
  else
  {
    if (fragments[curIndex - 1].ballisticMovementNext != null)
    {
      fragments[curIndex - 1].ballisticMovementNext = fragments[curIndex + 1]
      fragments[curIndex - 1].autoRotationNext = fragments[curIndex + 1]

      curveMovementStartIndex = curIndex - 1
    }
    //line reconnection
    else
    {
      let curCurve = new THREE.LineCurve3(tObjects[curIndex - prefix - 1].position, tObjects[curIndex - prefix + 1].position)
      let curPoints = curCurve.getPoints(50)

      curves[curIndex - prefix - 1].geometry.attributes.position.count = curPoints.length
      curves[curIndex - prefix - 1].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex - prefix - 1].geometry.attributes.position.needsUpdate = true

      deleteObject(curves[curIndex - prefix])
    }

    //adjust easing
    let easeCurvesToUpdate = [fragments[curIndex - 1].posEasePoints, fragments[curIndex - 1].rotationEasePoints, fragments[curIndex - 1].scaleEasePoints, fragments[curIndex - 1].alphaEasePoints]
    let easeCurvesToDelete = [fragments[curIndex].posEasePoints, fragments[curIndex].rotationEasePoints, fragments[curIndex].scaleEasePoints, fragments[curIndex].alphaEasePoints]
    let insertWidth = fragments[curIndex].duration / (fragments[curIndex].duration + fragments[curIndex - 1].duration)

    for(let i = 0; i < 4; i += 1)
    {
      for (let j = 0; j < easeCurvesToUpdate[i].length; j += 1)
      {
        easeCurvesToUpdate[i][j].x = lerp1(0, insertWidth, easeCurvesToUpdate[i][j].x)
        easeCurvesToUpdate[i][j].y = lerp1(0, insertWidth, easeCurvesToUpdate[i][j].y)
      }

      for (let j = 1; j < easeCurvesToDelete[i].length; j += 1)
      {
        easeCurvesToUpdate[i][easeCurvesToUpdate[i].length] =
        {
          "x": lerp1(insertWidth, 1, easeCurvesToDelete[i][j].x),
          "y": lerp1(insertWidth, 1, easeCurvesToDelete[i][j].y)
        }
      }
    }

    fragments[curIndex - 1].endPosition = fragments[curIndex].endPosition
    fragments[curIndex - 1].endEulerAngle = fragments[curIndex].endEulerAngle
    fragments[curIndex - 1].endScale = fragments[curIndex].endScale
    fragments[curIndex - 1].endAlpha = fragments[curIndex].endAlpha
    fragments[curIndex - 1].duration += fragments[curIndex].duration

    //if current fragment is this fragment, change to the
    //previous one
    if (fragments[curIndex] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex - 1].id
    }

    deleteObject(tObjects[curIndex - prefix])
    deleteFragmentIn(fragments[curIndex], animationFragments)

    tObjects.splice(curIndex - prefix, 1)
    curves.splice(curIndex - prefix, 1)
    fragments.splice(curIndex, 1)
  }

  //clear final object if nothing left
  if (fragments.length == 0)
  {
    deleteObject(tObjects[0])
  }

  //handle curve movement changes
  if (curveMovementStartIndex != -1)
  {
    let curveMovementPoints = []

    //record current frame's position
    for (let j = 0; j < curves.length; j += 1)
    {
      if (curves[j] == curves[curveMovementStartIndex])
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + j].startPosition.x,
          fragments[prefix + j].startPosition.y,
          fragments[prefix + j].startPosition.z
        )
      }

      //not or last
      if (curveMovementPoints.length > 0 && curves[j] != curves[curveMovementStartIndex] || j == curves.length - 1)
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + j].endPosition.x,
          fragments[prefix + j].endPosition.y,
          fragments[prefix + j].endPosition.z
        )

        break
      }
    }

    let curCurve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
    let curPoints = curCurve.getPoints((curveMovementPoints.length - 1) * 50)

    curves[curveMovementStartIndex].geometry.attributes.position.count = curPoints.length
    curves[curveMovementStartIndex].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

    for(let i = 0; i < curPoints.length; i += 1)
    {
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3] = curPoints[i].x
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
    }

    curves[curveMovementStartIndex].geometry.attributes.position.needsUpdate = true
  }
}

export function traverseAnimationFragments(from, to)
{
  let sequenceFragments = [from]
  let cur = findPreviousFragment(from.object, from.startFrame)

  while (cur != null)
  {
    sequenceFragments[sequenceFragments.length] = cur

    if (cur == to)
    {
      return sequenceFragments
    }

    cur = findPreviousFragment(cur.object, cur.startFrame)
  }

  sequenceFragments = [from]
  cur = findNextFragment(from.object, from.startFrame + from.duration)

  while (cur != null)
  {
    sequenceFragments[sequenceFragments.length] = cur

    if (cur == to)
    {
      console.log(sequenceFragments);
      return sequenceFragments
    }

    cur = findNextFragment(cur.object, cur.startFrame + from.duration)
  }

  return [from]
}

export function setRotationMode(fragments, mode)
{
  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].rotationType == mode)
    {
      fragments[i].rotationType = "Normal"
      fragments[i].autoRotationNext = undefined
    }
    else
    {
      fragments[i].rotationType = mode
      fragments[i].autoRotationNext = fragments[i + 1]
    }
  }
}

export function generateAutoRotationCurve(fragments)
{

  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].ballisticMovementNext != null)
    {
      fragments[i].autoRotationCurve = fragments[i].movementCurve
    }
    else
    {
      let startPosition = new THREE.Vector3(
        fragments[i].startPosition.x,
        fragments[i].startPosition.y,
        fragments[i].startPosition.z
      )
      let endPosition = new THREE.Vector3(
        fragments[i].endPosition.x,
        fragments[i].endPosition.y,
        fragments[i].endPosition.z
      )

      let curve = new THREE.LineCurve3(startPosition, endPosition)

      fragments[i].autoRotationCurve = curve
    }
  }

}

export function animation_objectBaseRotationCurve(curFragment)
{

}

export function animation_vertexBaseRotationCurve(curFragment)
{

}
