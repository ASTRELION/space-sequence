var deleteText = []

var handlerSize = 0.3125
var handler

var curPressedMouse = [false, false, false]

var plane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0)
var pressedMousePos = new THREE.Vector2()
var mousePos = new THREE.Vector2()
var currentlyPressedKeys = []
var hasPressedKeys = []

var cameraDirection
var cameraStartDistance
var curTarget
var curHandlerTarget
var ifCoreMove = false
var isCopying = false
var isBoneMove = false

var autoRotationStartObject
var autoRotationStartFragment

/**
 * current selected object's copy when entering other mode
 */
var thisWorldObject

/**
 * max node traverse for both sides in revise mode
 */
var anotherWorldMaxPresent = 4

/**
 *  the data for fragments for other modes
 */
var anotherWorldFragments = []

/**
 *  temperary objects for other modes
 */
var anotherWorldObjects = []

/**
 *  visible lines to help create animation for other modes
 */
var anotherWorldLines = []

/**
 *  current mid index for editing in other modes
 */
var anotherWorldFocusIndex = -1

var curEasePointTarget

var cameraMovementFactor = 1.0 / 10.0
var cameraRotationFactor = 1.0 / 10.0

function handleKeyDown(event)
{
  currentlyPressedKeys[event.keyCode] = true
  hasPressedKeys[event.keyCode] = true

  console.log(event.keyCode);

  //1
  if(event.keyCode == 49)
  {
    worldFrame = Math.max(0, worldFrame - 10)

    addHelpText("Move world fragment 10 Back")
  }

  //2
  if(event.keyCode == 50)
  {
    worldFrame += 10

    addHelpText("Move world fragment 10 Forward")
  }

  //p
  if(event.keyCode == 80)
  {
    threeD = !threeD
    switchTo3DView(threeD)

    addHelpText("View switched")
  }

  //s
  if(event.keyCode == 83)
  {
    saveJson()

    addHelpText("Saved data")
  }

  //l
  if(event.keyCode == 76)
  {
    loadJson(slLoader.value)

    addHelpText("Loaded data")
  }

  //tab
  if(event.keyCode == 9)
  {
    if(mode == "Revise")
    {
      curTarget = thisWorldObject

      handler.core.visible = true
      handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)

      addHelpText("Select the current original object")
    }
  }

  //ctrl + c
  if(event.keyCode == 67)
  {
    if(mode == "Standard" && currentlyPressedKeys[17])
    {
      copiedObject = curTarget

      isCopying = true

      addHelpText("Copied one object")
    }
  }

  //i
  if (event.keyCode == 73)
  {
    if (mode == "Revise")
    {
      if (curTarget != null)
      {
        if (autoRotationStartFragment == null)
        {
          autoRotationStartObject = curTarget
          autoRotationStartFragment = findObjectExistsFragment(curTarget, anotherWorldFragments, 1)
          addHelpText("Set/Reset Object auto rotation start frame")
        }
        else
        {
          if (curTarget == autoRotationStartObject)
          {
            addHelpText("Auto rotation created failed because still in the same frame")
          }
          else
          {
            let sequenceFragments = traverseAnimationFragments(autoRotationStartFragment, findObjectExistsFragment(curTarget, anotherWorldFragments, 2))
            console.log(autoRotationStartFragment);
            console.log(findObjectExistsFragment(curTarget, anotherWorldFragments, 2));
            setRotationMode(sequenceFragments, "Object")

            autoRotationStartObject = null
            autoRotationStartFragment = null
            addHelpText("Object auto rotation (re)set finished")
          }
        }
      }
      else
      {
        addHelpText("No object have been selected")
      }
    }
  }

  //u
  if (event.keyCode == 85)
  {
    if (mode == "Revise")
    {
      if (curTarget != null)
      {
        if (autoRotationStartFragment == null)
        {
          autoRotationStartObject = curTarget
          autoRotationStartFragment = findObjectExistsFragment(curTarget, anotherWorldFragments)
          addHelpText("Set/Reset Vertices auto rotation start frame")
        }
        else
        {
          if (curTarget == autoRotationStartObject)
          {
            addHelpText("Auto rotation created failed because still in the same frame")
          }
          else
          {
            let sequenceFragments = traverseAnimationFragments(autoRotationStartFragment, findObjectExistsFragment(curTarget, anotherWorldFragments))
            setRotationMode(sequenceFragments, "Vertex")

            autoRotationStartObject = null
            autoRotationStartFragment = null
            addHelpText("Vertices auto rotation (re)set finished")
          }
        }
      }
      else
      {
        addHelpText("No object have been selected")
      }
    }
  }

  //ctrl + v
  if(event.keyCode == 86)
  {
    if(mode == "Standard" && currentlyPressedKeys[17])
    {
      if(!isCopying)
      {
        return
      }

      let newObj = cloneAll(copiedObject)

      objectInfo[objectInfo.length] =
      {
        "object": newObj
      }

      objectStatus[newObj.id] =
      {
        "curID": -1
      }

      scene.add(newObj)
      curTarget = newObj

      isCopying = true

      addHelpText("Pasted one object")
    }
    else
    {
      if(curTarget != null)
      {
        handler.core.visible = true
      }

      reEnterMode("Standard")
      mode = "Standard"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Standard mode")
    }
  }

  //r
  if(event.keyCode == 82)
  {
    addHelpText("Switch to Revise mode")

    reEnterMode("Revise")
    mode = "Revise"
    systemMode.text.innerHTML = mode
  }

  //left Arrow
  if(event.keyCode == 37)
  {
    if(mode == "Revise")
    {
      anotherWorldMaxPresent = 4 //Math.max(2, anotherWorldMaxPresent - 2)
      let curIndexCopy = anotherWorldFocusIndex

      leaveReviseMode()
      enterReviseMode()

      anotherWorldFocusIndex = curIndexCopy
      handler.core.visible = true

      if(anotherWorldMaxPresent == 2)
      {
        addHelpText("Nodes Travel length Reached min value")
      }
      else
      {
        addHelpText("Decrease nodes travel length")
      }
    }
    else
    {
      addHelpText("Not in Revise Mode")
    }
  }

  //right Arrow
  if(event.keyCode == 39)
  {
    if(mode == "Revise")
    {
      anotherWorldMaxPresent = Math.min(anotherWorldFragments.length * 2, anotherWorldMaxPresent + 2)
      let curIndexCopy = anotherWorldFocusIndex

      leaveReviseMode()
      enterReviseMode()

      anotherWorldFocusIndex = curIndexCopy
      handler.core.visible = true

      if(anotherWorldMaxPresent == anotherWorldFragments.length * 2)
      {
        addHelpText("Nodes Travel length Reached max value")
      }
      else
      {
        addHelpText("Increase nodes travel length")
      }
    }
    else
    {
      addHelpText("Not in Revise Mode")
    }
  }

  //space
  if(event.keyCode == 32)
  {
    if(mode == "Standard" && !easeCurveInControl && curTarget != null)
    {
      if(animationBuffer[curTarget.id] == null)
      {
        animationBuffer[curTarget.id] = recordStartFrame(curTarget)
        objectStatus[curTarget.id].curID = -1

        addHelpText("Start Frame Created")
      }
      else
      {
        recordFrame(curTarget)
        recordFrame(curTarget)

        addHelpText("Continuely Created Frames")
      }
    }
  }

  //f : force create
  if (event.keyCode == 70)
  {
    if (mode == "Standard" && curTarget != null || mode == "Revise" && curTarget == thisWorldObject)
    {
      let curObject = null

      if (mode == "Standard")
      {
        curObject = curTarget
      }
      else
      {
        curObject = thisWorldObject
      }

      let preFragment = findLastFragment(curObject)

      if(preFragment != null && preFragment.startFrame + preFragment.duration == worldFrame)
      {
        addHelpText("Created failed: still in the same time line")
      }
      else if(preFragment != null)
      {
        animationFragments[animationFragments.length] =
        {
          "id": globalID++,
          "object": curObject,
          "objectInfo":
          {
            "name": "TEST0",
            "vertex": [],
            "posWeight": [],
            "rotationWeight": [],
            "scaleWeight": [],
            "alphaWeight": [],
          },

          "parentFragment": undefined,
          "childFragments": [],

          "startPosition": {"x": preFragment.endPosition.x, "y": preFragment.endPosition.y, "z": preFragment.endPosition.z},
          "endPosition": {"x": curObject.position.x, "y": curObject.position.y, "z": curObject.position.z},
          "startEulerAngle": {"x": preFragment.endEulerAngle.x, "y": preFragment.endEulerAngle.y, "z": preFragment.endEulerAngle.z},
          "endEulerAngle": {"x": curObject.rotation.x, "y": curObject.rotation.y, "z": curObject.rotation.z},
          "startScale": {"x": preFragment.endScale.x, "y": preFragment.endScale.y, "z": preFragment.endScale.z},
          "endScale": {"x": curObject.scale.x, "y": curObject.scale.y, "z": curObject.scale.z},
          "startAlpha": preFragment.endAlpha,
          "endAlpha": curObject.material.opacity,

          "activeBefore": 1,
          "activeAfter": 1,

          "callBefore": [],
          "callBack": [],

          "startFrame": preFragment.startFrame + preFragment.duration,
          "duration": worldFrame - (preFragment.startFrame + preFragment.duration),

          "posEasePoints": JSON.parse(JSON.stringify(easeArray)),
          "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

          "movementType": "Ballist",
          "ballisticMovementNext": undefined
        }

        if (preFragment.movementType == "Ballist")
        {
          preFragment.ballisticMovementNext = animationFragments[animationFragments.length - 1]
        }
      }
      else
      {
        recordFrame(curObject)

        //if created, set ballisticMovementNext
        if (animationBuffer[curObject.id] == null)
        {
          animationFragments[animationFragments.length - 1].movementType = "Ballist"
        }
      }
    }

    if(mode == "Revise")
    {
      leaveReviseMode()
      enterReviseMode()
      curTarget = thisWorldObject
    }
  }

  //a
  if(event.keyCode == 65)
  {
    if(mode != "Animation")
    {
      reEnterMode("Animation")
      mode = "Animation"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Animation mode")
    }
    else
    {
      mode = "Animation (Root)"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Animation (Root) mode")
    }
  }

  //e
  if (event.keyCode == 69)
  {
    if (mode == "Animation" || mode == "Animation (Root)")
    {
      if(curTarget != null)
      {
        let preFragment = findPreviousFragment(curTarget, worldFrame)

        if (preFragment != null)
        {
          if (mode == "Animation (Root)")
          {
            EditStartFrameFrom(findNextFragment(curTarget, worldFrame), 10)

            addHelpText("Increased 10 duration(" + (preFragment.duration + 10) + ") for current fragment with subsequence effect")
          }
          else
          {
            addHelpText("Increased 10 duration(" + (preFragment.duration + 10) + ") for current fragment")
          }

          preFragment.duration += 10

          viewAnimationStatusAt(worldFrame)

          if (handler.core.visible)
          {
            handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
          }
        }
        else
        {
          addHelpText("No fragment have found for this object")
        }
      }
      else
      {
        viewAnimationStatusAt(++worldFrame)
      }
    }
  }

  //q
  if (event.keyCode == 81)
  {
    if (mode == "Animation" || mode == "Animation (Root)")
    {
      if(curTarget != null)
      {
        let preFragment = findPreviousFragment(curTarget, worldFrame)

        if (preFragment != null)
        {
          //check if valid first
          if (preFragment.duration != 10)
          {
            if (mode == "Animation (Root)")
            {
              EditStartFrameFrom(findNextFragment(curTarget, worldFrame), -10)

              addHelpText("Decrease 10 duration(" + (preFragment.duration - 10) + ") for current fragment with subsequence effect")
            }
            else
            {
              addHelpText("Decrease 10 duration(" + (preFragment.duration - 10) + ") for current fragment")
            }
          }

          preFragment.duration = Math.max(10, preFragment.duration - 10)

          viewAnimationStatusAt(worldFrame)

          if (handler.core.visible)
          {
            handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
          }
        }
        else
        {
          addHelpText("No fragment have found for this object")
        }
      }
      else
      {
        worldFrame = Math.max(0, worldFrame - 1)
        viewAnimationStatusAt(worldFrame)
      }
    }
  }
}

function handleKeyUp(event)
{
  //ctrl
  if(event.keyCode == 17)
  {
    if(isCopying)
    {
      isCopying = false
      currentlyPressedKeys[event.keyCode] = false

      return
    }

    if(curTarget == null)
    {
      if (mode == "Revise")
      {
        let status = insertFrame(mousePos, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)

        if (status == "OK")
        {
          addHelpText("Inserted frame in " + worldFrame)
        }
        else
        {
          addHelpText("Inserted frame error with " + status)
        }
      }
      else
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false
      }

      return
    }

    if(easeCurveInControl)
    {
      const rect = UIRenderer.domElement.getBoundingClientRect();
      let mouseXPosInUICamera = ((mousePos.x + 1) / 2 * window.innerWidth - parseInt(UICanvas.style.left)) / parseInt(UICanvas.style.width)
      let mouseYPosInUICamera = ((mousePos.y + 1) / 2 * window.innerHeight - (window.innerHeight + parseInt(UICanvas.style.top) - parseInt(UICanvas.style.height))) / parseInt(UICanvas.style.height)

      for(let i = 0; i < easePointsOnScreen.length; i += 1)
      {
        if(mouseXPosInUICamera < easePointsOnScreen[i].position.x)
        {
          let newPoint = newSphere(mouseXPosInUICamera, mouseYPosInUICamera, 0, 0.05)
          newPoint.material.emissive = new THREE.Color(1, 0, 0)
          UIScene.add(newPoint)

          easePointsOnScreen.splice(i, 0, newPoint);

          updateEaseCurve()
          break
        }
      }
    }
    else
    {
      recordFrame(curTarget)
    }
  }

  //backspace
  if(event.keyCode == 8)
  {
    if(easeCurveInControl)
    {
      if(curEasePointTarget != null)
      {
        for(let i = 0; i < easePointsOnScreen.length; i += 1)
        {
          if(easePointsOnScreen[i] == curEasePointTarget)
          {
            UIScene.remove(easePointsOnScreen[i])
            easePointsOnScreen[i].geometry.dispose()
            easePointsOnScreen[i].material.dispose()

            easePointsOnScreen.splice(i, 1)

            updateEaseCurve()
            break
          }
        }
      }
    }
    else
    {
      if(curTarget != null)
      {
        if (mode == "Standard")
        {
          let curFragment = findObjectCurFragment(curTarget)

          if (curFragment != null)
          {
            deleteFragmentIn(curFragment, animationFragments)
            addHelpText("Deleted current fragment")
          }
          else
          {
            deleteObject(curTarget)
            addHelpText("Deleted current object")
          }
        }
        else if (mode == "Revise")
        {
          for (let i = 0; i < anotherWorldObjects.length; i += 1)
          {
            if (curTarget == anotherWorldObjects[i])
            {
              deleteFrame(curTarget, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)
              addHelpText("Deleted selected frame")

              handler.core.visible = false

              break
            }

            if (i - 1 == anotherWorldObjects.length)
            {
              addHelpText("Bug")
            }
          }
        }
      }
    }
  }

  //q
  if(event.keyCode == 81)
  {
    if(mode == "Revise")
    {
      let preFragment = findPreviousFragment(thisWorldObject, findObjectCurFragment(thisWorldObject).startFrame)

      if(preFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = preFragment.id
        worldFrame = preFragment.startFrame

        leaveReviseMode()
        enterReviseMode()

        addHelpText("Back to the previous frame for this object")
      }
      else
      {
        addHelpText("No previous frame for this object")
      }
    }
    else if(mode == "Standard")
    {
      if(curTarget == null)
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false

        return
      }

      // try to find current fragment first
      let preFragment = findObjectCurFragment(curTarget)

      //if not find, check if start frame has recorded
      if(preFragment == null)
      {
        preFragment = animationBuffer[curTarget.id]

        //if find
        if(preFragment != null)
        {
          //if position is not equal, return to start frame
          if(!atOnePoint(curTarget, preFragment.position, new THREE.Vector3()))
          {
            curTarget.position.set(preFragment.position.x, preFragment.position.y, preFragment.position.z)
            handler.core.position.set(preFragment.position.x, preFragment.position.y, preFragment.position.z)
            worldFrame = preFragment.frame

            addHelpText("Back to the start frame")
          }
          //delete all cur animation info
          else
          {
            worldFrame = preFragment.frame
            animationBuffer[curTarget.id] = null

            addHelpText("The start frame clear finished")
          }
        }
        else
        {
          preFragment = findPreviousFragment(curTarget, worldFrame)

          //if find:
          //  back to the previous fragment's position
          //  set object status to previous fragment
          if(preFragment != null)
          {
            curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
            handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
            objectStatus[curTarget.id].curID = preFragment.id
            worldFrame = preFragment.startFrame

            addHelpText("Back to the previous fragment's start position")
          }
          else
          {
            addHelpText("No fragment have found for this object")
          }
        }
      }
      else if(atOnePoint(curTarget, preFragment.startPosition, preFragment.endPosition))
      {
        preFragment = findPreviousFragment(curTarget, preFragment.startFrame)

        //if find:
        //  back to the previous fragment's position
        //  set object status to previous fragment
        if(preFragment != null)
        {
          curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
          handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
          objectStatus[curTarget.id].curID = preFragment.id
          worldFrame = preFragment.startFrame

          addHelpText("Back to the previous fragment's start position")
        }
        else
        {
          addHelpText("Already in the first fragment")
        }
      }
      else
      {
        curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
        handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
        worldFrame = preFragment.startFrame

        addHelpText("Back to original position of this fragment")
      }
    }
  }

  //e
  if(event.keyCode == 69)
  {
    if(mode == "Revise")
    {
      let nextFragment = findNextFragment(thisWorldObject, findObjectCurFragment(thisWorldObject).startFrame + findObjectCurFragment(thisWorldObject).duration)

      if(nextFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = nextFragment.id
        worldFrame = nextFragment.startFrame

        leaveReviseMode()
        enterReviseMode()

        addHelpText("Go to the next frame for this object")
      }
      else
      {
        addHelpText("No next frame for this object")
      }
    }
    else if(mode == "Standard")
    {
      if(curTarget == null)
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false

        return
      }

      let curFrame = worldFrame
      let curFragment = findObjectCurFragment(curTarget)

      if(curFragment != null)
      {
        curFrame = curFragment.startFrame + curFragment.duration
      }

      // try to find next fragment
      let nextFragment = findNextFragment(curTarget, curFrame)

      //if find, go to next fragment, clear
      //current buffer data
      if(nextFragment != null)
      {
        curTarget.position.set(nextFragment.startPosition.x, nextFragment.startPosition.y, nextFragment.startPosition.z)
        handler.core.position.set(nextFragment.startPosition.x, nextFragment.startPosition.y, nextFragment.startPosition.z)
        objectStatus[curTarget.id].curID = nextFragment.id
        worldFrame = nextFragment.startFrame

        animationBuffer[curTarget.id] = null

        addHelpText("Go to next fragment's start position")
      }
      else
      {
        addHelpText("No next fragment for this object")
      }
    }
  }

  currentlyPressedKeys[event.keyCode] = false
}

function handleMouseDown(event)
{
  curPressedMouse[event.button] = true

  if(checkExists(mousePos, sceneZone))
  {
    handleMouseDownScene(event)
  }
  else
  {
    handleMouseDownUI(event)
  }

  pressedMousePos.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)
}

function handleMouseDownScene(event)
{
  if(event.button == 0)
  {
    ifCoreMove = false

    raycaster.setFromCamera(mousePos, camera);

    let sceneObjects = []
    let anotherObjects = []
    let handlerObjects = []

    for(let i = 0; i < objectInfo.length; i += 1)
    {
      sceneObjects[i] = objectInfo[i].object
    }

    if(mode == "Revise")
    {
      for(let i = 0; i < anotherWorldObjects.length; i += 1)
      {
        anotherObjects[i] = anotherWorldObjects[i]
      }
    }

    let intersectsObjects = raycaster.intersectObjects(sceneObjects);
    let intersectsAnotherObjects = raycaster.intersectObjects(anotherObjects)

    handlerObjects[handlerObjects.length] = handler.core
    handlerObjects[handlerObjects.length] = handler.xMove
    handlerObjects[handlerObjects.length] = handler.yMove
    handlerObjects[handlerObjects.length] = handler.zMove
    handlerObjects[handlerObjects.length] = handler.xRotate
    handlerObjects[handlerObjects.length] = handler.yRotate
    handlerObjects[handlerObjects.length] = handler.zRotate
    handlerObjects[handlerObjects.length] = handler.xScale
    handlerObjects[handlerObjects.length] = handler.yScale
    handlerObjects[handlerObjects.length] = handler.zScale

    let intersectshandlerObjects = raycaster.intersectObjects(handlerObjects);

    if(intersectsObjects.length > 0 || intersectshandlerObjects.length > 0 || intersectsAnotherObjects.length > 0)
    {
      if ( intersectshandlerObjects.length > 0 )
      {
        curHandlerTarget = intersectshandlerObjects[0].object

        ifCoreMove = curHandlerTarget == handler.core
      }
      else if ( intersectsObjects.length > 0 )
      {
        curTarget = intersectsObjects[0].object

        handler.core.position.set(intersectsObjects[0].object.position.x, intersectsObjects[0].object.position.y, intersectsObjects[0].object.position.z)
        handler.core.visible = true

        ifCoreMove = true

        if(mode == "Revise" && curTarget != thisWorldObject)
        {
          thisWorldObject = curTarget
          leaveReviseMode()
          enterReviseMode()
          curTarget = thisWorldObject

          handler.core.visible = true
        }
      }
      else if ( intersectsAnotherObjects.length > 0 )
      {
        curTarget = intersectsAnotherObjects[0].object

        handler.core.position.set(intersectsAnotherObjects[0].object.position.x, intersectsAnotherObjects[0].object.position.y, intersectsAnotherObjects[0].object.position.z)
        handler.core.visible = true

        ifCoreMove = true
      }

      cameraDirection = new THREE.Vector3(curTarget.position.x - camera.position.x, curTarget.position.y - camera.position.y, curTarget.position.z - camera.position.z).normalize()
    }
    else
    {
      curTarget = null
      curHandlerTarget = null

      handler.core.visible = false
    }
  }
  else if(event.button == 2)
  {
    if(cameraStartDistance == null)
    {
      if(curTarget != null)
      {
        cameraStartDistance = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z).distanceTo(curTarget.position)
      }
      else
      {
        cameraStartDistance = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z).distanceTo(new THREE.Vector3())
      }
    }
  }
}

function handleMouseDownUI(event)
{
  if(event.button == 0)
  {
    let mouse = {}
    const rect = UIRenderer.domElement.getBoundingClientRect();
    mouse.x = ( ( event.clientX - rect.left ) / ( rect.right - rect.left ) ) * 2 - 1;
    mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

    raycaster.setFromCamera(mouse, UICamera);

    let sceneObjects = []

    for (let i = 1; i < easePointsOnScreen.length - 1; i += 1)
    {
      sceneObjects[i - 1] = easePointsOnScreen[i]
    }

    let intersectsObjects = raycaster.intersectObjects(sceneObjects);

    if(intersectsObjects.length > 0)
    {
      curEasePointTarget = intersectsObjects[0].object
    }
    else
    {
      curEasePointTarget = null
    }
  }
}

function handleMouseMove(event)
{
  if(checkExists(mousePos, sceneZone))
  {
    handleMouseMoveScene(event)
  }
  else
  {
    handleMouseMoveUI(event)
  }

  if(!isAnimating && checkExists(mousePos, {width: "100", height: "65", x: "0", y: "35"}))
  {
    systemMode.text.style.opacity = 0.5
    systemHelp.text.style.opacity = 0.75
  }
  else
  {
    systemMode.text.style.opacity = 0
    systemHelp.text.style.opacity = 0
  }

  mousePos.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)
}

function handleMouseMoveScene(event)
{
  let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  if(curPressedMouse[0])
  {
    //transform object
    if(curTarget == null)
    {
      return
    }

    //if in Animation mode, prevent
    //object's moving
    if((mode == "Animation" || mode == "Animation (Root)") && !isBoneMove)
    {
      return
    }

    //free move
    if(ifCoreMove)
    {
      //scaler
      if(hasPressedKeys[16])
      {
        let v1 = 1

        if((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x < 0)
        {
          v1 = -1
        }

        let curPosition = new THREE.Vector2((event.clientX / window.innerWidth) * 2 - 1, 0)
        let distance = 1 + curPosition.distanceTo(mousePos) * 0.8 * cameraDistance / 10 * v1

        curTarget.scale.set(curTarget.scale.x * distance, curTarget.scale.y * distance, curTarget.scale.z * distance)
      }
      //drag
      else
      {
        let intersects = new THREE.Vector3()
        let curPosition = new THREE.Vector2((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)

        raycaster.setFromCamera(mousePos, camera)
        raycaster.ray.intersectPlane(plane, intersects)

        let distance = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

        raycaster.setFromCamera(curPosition, camera)
        raycaster.ray.intersectPlane(plane, intersects)

        distance = new THREE.Vector3(intersects.x - distance.x, intersects.y - distance.y, intersects.z - distance.z)

        curTarget.position.add(distance)
        handler.core.position.add(distance)
      }
    }
    //move
    else if(curHandlerTarget == handler.xMove || curHandlerTarget == handler.yMove || curHandlerTarget == handler.zMove)
    {
      if(curHandlerTarget == handler.xMove)
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.z > 0)
        {
          v1 = -1
        }

        if(cameraDirection.x < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.x)), 0, 0)
        let distance2 = new THREE.Vector3((-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.z)), 0, 0)

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
      else if(curHandlerTarget == handler.yMove)
      {
        let distance1 = new THREE.Vector3(0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance * 0.05, 0)
        let distance2 = new THREE.Vector3(0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance * lerp1(0.5, 0.05, Math.abs(cameraDirection.y)), 0, 0)

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
      else
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.x < 0)
        {
          v1 = -1
        }

        if(cameraDirection.z < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(0, 0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.z)))
        let distance2 = new THREE.Vector3(0, 0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.x)))

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
    }
    //rotate
    else if(curHandlerTarget == handler.xRotate || curHandlerTarget == handler.yRotate || curHandlerTarget == handler.zRotate)
    {
      let rotationFactor = 15
      let v1 = 1

      if(cameraDirection.z < 0)
      {
        v1 = -1
      }

      let distance = ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * v1

      if(curHandlerTarget == handler.xRotate)
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(1, 0, 0), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
      else if(curHandlerTarget == handler.yRotate)
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
      else
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 0, 1), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
    }
    //scale
    else
    {
      let scaleFactor = 10

      if(curHandlerTarget == handler.xScale)
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.z > 0)
        {
          v1 = -1
        }

        if(cameraDirection.x < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * scaleFactor * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.x)), 0, 0)
        let distance2 = new THREE.Vector3((-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * scaleFactor * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.z)), 0, 0)

        curTarget.scale.add(distance1).add(distance2)
      }
      else if(curHandlerTarget == handler.yScale)
      {
        let distance1 = new THREE.Vector3(0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * scaleFactor * 0.05, 0)
        let distance2 = new THREE.Vector3(0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * scaleFactor * lerp1(0.5, 0.05, Math.abs(cameraDirection.y)), 0, 0)

        curTarget.scale.add(distance1).add(distance2)
      }
      else
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.x < 0)
        {
          v1 = -1
        }

        if(cameraDirection.z < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(0, 0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * scaleFactor * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.z)))
        let distance2 = new THREE.Vector3(0, 0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * scaleFactor * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.x)))

        curTarget.scale.add(distance1).add(distance2)
      }
    }

    if(mode == "Revise")
    {
      updateObjectsInFrame(curTarget, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)
    }
  }
  else if(curPressedMouse[1])
  {
    let distance1 = ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance / 10 * 5
    let distance2 = (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance / 10 * 5

    camera.translateX(-distance1)
    camera.translateY(-distance2)
  }
  else if(curPressedMouse[2])
  {
    if(!threeD)
    {
      return
    }

    let distance1 = ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * 250
    let distance2 = (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * 250

    camera.translateZ(-cameraStartDistance)
    camera.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), toRadians(-distance1));
    camera.translateZ(cameraStartDistance)

    camera.translateZ(-cameraStartDistance)
    camera.rotateX(toRadians(distance2));
    camera.rotation.x = clamp(camera.rotation.x, -Math.PI / 2, Math.PI / 2)
    camera.translateZ(cameraStartDistance)

    let dir = camera.getWorldDirection(new THREE.Vector3())
    plane.normal = new THREE.Vector3(dir.x, dir.y, dir.z)
  }
}

function handleMouseMoveUI(event)
{
  if(curPressedMouse[0])
  {
    if(curEasePointTarget != null)
    {
      let intersects = new THREE.Vector3()
      let curPosition = new THREE.Vector2((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)

      raycaster.setFromCamera(mousePos, UICamera)
      raycaster.ray.intersectPlane(plane, intersects)

      let distance = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

      raycaster.setFromCamera(curPosition, UICamera)
      raycaster.ray.intersectPlane(plane, intersects)

      let xDistance = (intersects.x - distance.x) * (renderer.getSize(new THREE.Vector2()).x / parseInt(UICanvas.style.width))
      let yDistance = (intersects.y - distance.y) * (renderer.getSize(new THREE.Vector2()).y / parseInt(UICanvas.style.height))
      distance = new THREE.Vector3(xDistance, yDistance, 0)

      curEasePointTarget.position.add(distance)

      curEasePointTarget.position.x = clamp(curEasePointTarget.position.x, 0, 1)
      curEasePointTarget.position.y = clamp(curEasePointTarget.position.y, 0, 1)

      updateEaseCurve()
    }
  }
}

function handleMouseUp(event)
{
  curPressedMouse[event.button] = false

  if(checkExists(mousePos, sceneZone))
  {
    handleMouseUpScene(event)
  }
  else
  {
    handleMouseUpUI(event)
  }
}

function handleMouseUpScene(event)
{
  hasPressedKeys[16] = false
  cameraStartDistance = null
}

function handleMouseUpUI(event)
{

}

function handleMouseWheel(event)
{
  if(checkExists(pressedMousePos, sceneZone))
  {
    handleMouseWheelScene(event)
  }
  else
  {
    handleMouseWheelUI(event)
  }
}

function handleMouseWheelScene(event)
{
  let translateZValue = 2.5
  let cameraDistance = Math.abs(camera.position.z)

  if(curTarget != null)
  {
    if(!threeD)
    {
      cameraDistance = Math.abs(camera.position.z - curTarget.position.z)
    }
    else
    {
      cameraDistance = camera.position.distanceTo(curTarget.position)
    }
  }

  if(event.wheelDelta != null)
  {
    if(event.wheelDelta > 0 && translateZValue < cameraDistance)
    {
      camera.translateZ(-translateZValue)
    }
    else if(event.wheelDelta < 0 && cameraDistance < 50 - translateZValue)
    {
      camera.translateZ(translateZValue)
    }
  }
  else if(event.detail != null)
  {
    if(event.detail > 0 && cameraDistance < 50 - translateZValue)
    {
      camera.translateZ(translateZValue)
    }
    else if(event.detail < 0 && translateZValue < cameraDistance)
    {
      camera.translateZ(-translateZValue)
    }
  }

  cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  handler.core.scale.set(cameraDistance / 8, cameraDistance / 8, cameraDistance / 8)

  if(!threeD)
  {
    let cameraDistance2 = Math.abs(camera.position.z)

    if(curTarget != null)
    {
      cameraDistance2 = Math.abs(camera.position.z - curTarget.position.z)
    }

    let height = cameraDistance2
    let width = cameraDistance2 * window.innerWidth / window.innerHeight

    camera.left = width / -2
    camera.right = width / 2
    camera.top = height / 2
    camera.bottom = height / -2

    camera.updateProjectionMatrix()
  }
}

function handleMouseWheelUI(event)
{

}

function handleKeys()
{

}

function onWindowResize()
 {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}
