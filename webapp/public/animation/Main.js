var globalID = 0;

var canvas;
var renderer;
var scene;
var camera;
var target;
let composer;
let composer2
var raycaster;
var sceneZone =
{
  width: "75",
  height: "100",
  x: "0",
  y: "0"
}

let obj1
let borderEffect
let slLoader
let copiedObject

var objectInfo = []
var animationFragments = []

var threeD = false

var UICanvas
var UIRenderer
var UIScene
var UICamera

var curTimeText
var systemInfo = []
var systemMode = {}
var systemHelp = {}
var mode = "Standard"

var easeCurve
var easeArray
var easeCurveInControl
var easePointsOnScreen = []

var worldFrame = 0
var isAnimating = false

function main()
{
  init()
  initUI()

  switchTo3DView(false)

  let curTimeControl = newButton(90, 80, 10, 10, "World Frame", "", 1, 0, 0, 1, document.getElementById("root"))
  curTimeText = newText(0, 0, worldFrame, curTimeControl)

  let frameText = newText(0, 0, 0, newImage(0, 0, 5, 5, "", 0, 0, 0, 0, document.getElementById("root")))
  frameText.style.opacity = 0.5

  let startTime = new Date()
  let counter = 0

  var loop = function()
  {
    if(counter % 60 == 0)
    {
      document.getElementById(frameText.id ).innerHTML = "FPS: " + (60000 / (new Date().getTime() - startTime.getTime())).toFixed(1)

      startTime = new Date()
    }

    counter += 1

    document.getElementById(curTimeText.id).innerHTML = worldFrame

    renderer.clear()
    renderer.render(scene, camera);

    UIRenderer.render(UIScene, UICamera)
    renderHelpText()

    handleKeys();
    requestAnimationFrame(loop);

    if (counter == 30)
    {
      // createSequence("a")
      //
      // macroStartRecording("c")
      // let macroID = globalID - 1
      //
      // rect(0, 0, 0, 1, 1, 1)
      // let cur2 = objectInfo[objectInfo.length - 1].object
      // let cur2ID = objectInfo[objectInfo.length - 1].id
      // createKeyFrame()
      // position(cur2ID, -2, 2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 100)
      // position(cur2ID, 2, -2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 30)
      // macroStopRecording()
      //
      // macroStartRecording("b")
      // circle(0, 0, 0, 0.5)
      // let cur = objectInfo[objectInfo.length - 1].object
      // createKeyFrame()
      // cur.position.set(-2, -2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 100)
      // cur.position.set(2, 2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 30)
      // macroStopRecording()
      //
      // playSequence()
    }
  };

  loop();
}

function init()
{
  canvas = document.getElementById('canvas');
  UICanvas = document.getElementById('UICanvas');
  UICanvas.onmouseover = function()
  {
    easeCurveInControl = true
  }
  UICanvas.onmouseout = function()
  {
    easeCurveInControl = false
  }

  canvas.style.position = "relative"
  UICanvas.style.position = "relative"

  scene = new THREE.Scene();
  UIScene = new THREE.Scene()

  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.01, 3000);
  camera.position.set(0, 6, 5);
  camera.rotation.set(0, 0, 0, "YZX")
  camera.rotateX(toRadians(-30))

  let size = 1
  UICamera = new THREE.OrthographicCamera(size / -2, size / 2, size / 2, size / -2, 0.01, 3000)
  UICamera.position.set(0.5, 0.5, 10)
  UICamera.rotation.set(0, 0, 0, "YZX")

  renderer = new THREE.WebGLRenderer( { canvas : canvas, context : canvas.getContext( 'webgl2') });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.autoClear = false
  renderer.antialias = true

  UIRenderer = new THREE.WebGLRenderer( { canvas : UICanvas, context : UICanvas.getContext( 'webgl2') });
  UIRenderer.setPixelRatio( window.devicePixelRatio );
  UIRenderer.setSize( window.innerWidth * 0.1, window.innerWidth * 0.1, false);

  raycaster = new THREE.Raycaster();

  initBasicScene()

  window.addEventListener('resize', onWindowResize, false );
  document.onkeydown = handleKeyDown;
  document.onkeyup = handleKeyUp;
  document.onmousedown = handleMouseDown;
  document.onmousemove = handleMouseMove;
  document.onmouseup = handleMouseUp;
  document.onmousewheel = handleMouseWheel;
  if (document.addEventListener)
  {
    document.addEventListener('DOMMouseScroll', handleMouseWheel, false);
  }

  // load texture map
  var loader = new THREE.TextureLoader();
  var texture = loader.load("b.png");

  texture.magFilter = THREE.NearestFilter;
  texture.minFilter = THREE.NearestFilter;

  imageNames = ["AllWhite.png","AllWhite.png","AllWhite.png","AllWhite.png","AllWhite.png","AllWhite.png"];
  var ourCubeMap = new THREE.CubeTextureLoader().load( imageNames );
  ourCubeMap.repeat.set(100, 100);

  UIScene.background = ourCubeMap;

  obj1 = newPlane(0, 0, 0, 1, 1, null, new THREE.Color(1, 0, 0))
  let obj2 = newCube(1, 1, 0, 1, 1, 1, null, new THREE.Color(0, 1, 0))
  let obj3 = newPlane(0, 0, 0, 1, 1, null, new THREE.Color(0, 0, 0))

  objectInfo[objectInfo.length] =
  {
    "object": obj2,
    "id": globalID++
  }

  scene.add(obj1)
  scene.add(obj2)
  addBorder(obj2, 0, 1, 0, 1, 0.05, false)
  console.log(obj1.geometry.faceVertexUvs);

  let arr = [{"x": 0, "y": 0}, {"x": 0.5, "y": 0.5}, {"x": 1, "y": 1}]
  setCurveCanvasSize(window.innerWidth - 200, window.innerHeight * 0.5, 100)
  drawEaseCurveOnScreen(arr)
}

function initHandler()
{
  let core = newSphere(0, 0, 0, handlerSize * 0.3, null, new THREE.Color(0, 0, 0))
  core.material.depthTest = false
  core.renderOrder = 2

  let xMove = newCone(2, 0, 0, handlerSize * 0.25, handlerSize, null, new THREE.Color(0.6, 0, 0), core)
  let yMove = newCone(0, 2, 0, handlerSize * 0.25, handlerSize, null, new THREE.Color(0, 0.6, 0), core)
  let zMove = newCone(0, 0, 2, handlerSize * 0.25, handlerSize, null, new THREE.Color(0, 0, 0.6), core)
  xMove.material.emissive = new THREE.Color(0.4, 0, 0)
  yMove.material.emissive = new THREE.Color(0, 0.4, 0)
  zMove.material.emissive = new THREE.Color(0, 0, 0.4)

  let xRotate = newSphere(1.7, 0, 0, handlerSize * 0.25, null, new THREE.Color(0.6, 0, 0), core)
  let yRotate = newSphere(0, 1.7, 0, handlerSize * 0.25, null, new THREE.Color(0, 0.6, 0), core)
  let zRotate = newSphere(0, 0, 1.7, handlerSize * 0.25, null, new THREE.Color(0, 0, 0.6), core)
  xRotate.material.emissive = new THREE.Color(0.4, 0, 0)
  yRotate.material.emissive = new THREE.Color(0, 0.4, 0)
  zRotate.material.emissive = new THREE.Color(0, 0, 0.4)

  let xLine = newCylinder(1, 0, 0, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  let yLine = newCylinder(0, 1, 0, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  let zLine = newCylinder(0, 0, 1, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  xLine.material.emissive = new THREE.Color(0.6, 0, 0)
  yLine.material.emissive = new THREE.Color(0, 0.6, 0)
  zLine.material.emissive = new THREE.Color(0, 0, 0.6)

  let scaleCore = newCube(0, 0, 0, handlerSize * 0.6, handlerSize * 0.6, handlerSize * 0.6, null, new THREE.Color(0, 0, 0), core)

  let faceLine = newCylinder(1, 0, 0, 0.01, 2, null, new THREE.Color(0, 0, 0), scaleCore)
  faceLine.material.emissive = new THREE.Color(0, 0, 0)

  let xScale = newCube(1.47, 0, 0, handlerSize * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0.6, 0, 0), core)
  let yScale = newCube(0, 1.47, 0, handlerSize  * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0, 0.6, 0), core)
  let zScale = newCube(0, 0, 1.47, handlerSize * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0, 0, 0.6), core)
  xScale.material.emissive = new THREE.Color(0.4, 0, 0)
  yScale.material.emissive = new THREE.Color(0, 0.4, 0)
  zScale.material.emissive = new THREE.Color(0, 0, 0.4)

  xMove.rotation.set(0, 0, toRadians(-90), "XYZ")
  yMove.rotation.set(0, 0, 0, "XYZ")
  zMove.rotation.set(toRadians(90), 0, 0, "XYZ")

  xRotate.rotation.set(0, 0, toRadians(-90), "XYZ")
  yRotate.rotation.set(0, 0, 0, "XYZ")
  zRotate.rotation.set(toRadians(90), 0, 0, "XYZ")

  xScale.rotation.set(0, 0, toRadians(-90), "XYZ")
  yScale.rotation.set(0, 0, 0, "XYZ")
  zScale.rotation.set(toRadians(90), 0, 0, "XYZ")

  xLine.rotation.set(0, 0, toRadians(-90), "XYZ")
  zLine.rotation.set(toRadians(90), 0, 0, "XYZ")

  faceLine.rotation.set(0, 0, toRadians(-90), "XYZ")
  for(let i = 0; i < core.children.length; i += 1)
  {
    core.children[i].material.depthTest = false
    core.children[i].renderOrder = 2
  }

  for(let i = 0; i < scaleCore.children.length; i += 1)
  {
    scaleCore.children[i].material.depthTest = false
    scaleCore.children[i].renderOrder = 2
  }

  xLine.material.depthTest = false
  xLine.renderOrder = 1
  yLine.material.depthTest = false
  yLine.renderOrder = 1
  zLine.material.depthTest = false
  zLine.renderOrder = 1

  faceLine.material.depthTest = false
  faceLine.renderOrder = 1

  handler =
  {
    "core": core,
    "xMove": xMove,
    "yMove": yMove,
    "zMove": zMove,

    "xRotate": xRotate,
    "yRotate": yRotate,
    "zRotate": zRotate,

    "scaleCore": scaleCore,

    "xScale": xScale,
    "yScale": yScale,
    "zScale": zScale,

    "xLine": xLine,
    "yLine": yLine,
    "zLine": zLine
  }

  scene.add(handler.core)
}

function switchTo3DView(mode)
{
  if(mode)
  {
    handler.zMove.translateZ(-10000)
    handler.zRotate.translateZ(-10000)
    handler.zScale.translateZ(-10000)
    handler.zLine.translateZ(-10000)

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.01, 3000)
    camera.position.set(0, 6, 10);
    camera.rotation.set(0, 0, 0, "YZX")
    camera.rotateX(toRadians(-30))
  }
  else
  {
    handler.zMove.translateZ(10000)
    handler.zRotate.translateZ(10000)
    handler.zScale.translateZ(10000)
    handler.zLine.translateZ(10000)

    let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

    let height = cameraDistance
    let width = cameraDistance * window.innerWidth / window.innerHeight
    camera = new THREE.OrthographicCamera(width / -2, width / 2, height / 2, height / -2, 0.01, 3000)

    camera.position.set(0, 0, 10)
    camera.rotation.set(0, 0, 0, "YZX")
  }

  let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  handler.core.scale.set(cameraDistance / 10, cameraDistance / 10, cameraDistance / 10)
}

function setCurveCanvasSize(x, y, size)
{
  UICanvas.style.left = x + "px"
  UICanvas.style.top = (-window.innerHeight + y) + "px"
  UICanvas.style.height = size + "px"
  UICanvas.style.width = size + "px"
}

function drawEaseCurveOnScreen(points)
{
  let curvePoints = []

  for(let i = 0; i < points.length; i += 1)
  {
    curvePoints[i] = new THREE.Vector3(points[i].x, points[i].y, 0)

    easePointsOnScreen[i] = newSphere(points[i].x, points[i].y, 0, 0.05)
    easePointsOnScreen[i].material.emissive = new THREE.Color(1, 0, 0)
    UIScene.add(easePointsOnScreen[i])
  }

  let curve = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")

  let curPoints = curve.getPoints( 50 );
  let geometry = new THREE.BufferGeometry().setFromPoints( curPoints );
  let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );

  easeCurve = new THREE.Line( geometry, material );
  UIScene.add(easeCurve)

  easeArray = points
}

function updateEaseCurve()
{
  let curvePoints = []
  easeArray = []

  for(let i = 0; i < easePointsOnScreen.length; i += 1)
  {
    easeArray[i] = {}
    easeArray[i].x = easePointsOnScreen[i].position.x
    easeArray[i].y = easePointsOnScreen[i].position.y

    curvePoints[i] = new THREE.Vector3(easePointsOnScreen[i].position.x, easePointsOnScreen[i].position.y, 0)
  }

  let curve = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")
  let curPoints = curve.getPoints( 50 );

  for(let i = 0; i < curPoints.length; i += 1)
  {
    easeCurve.geometry.attributes.position.array[i * 3] = curPoints[i].x
    easeCurve.geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
    easeCurve.geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
  }

  easeCurve.geometry.attributes.position.needsUpdate = true
}

function deleteEaseCurveOnScreen()
{
  UIScene.remove(easeCurve)
  easeCurve.geometry.dispose()
  easeCurve.material.dispose()

  for(let i = 0; i < easePointsOnScreen.length; i += 1)
  {
    UIScene.remove(easePointsOnScreen[i])
    easePointsOnScreen[i].geometry.dispose()
    easePointsOnScreen[i].material.dispose()
  }

  easePointsOnScreen = []
  easeCurve = null
}

function initUI()
{
  let animate = newButton(90, 90, 10, 10, "Animate", "", 1, 0, 0, 1, document.getElementById("root"))
  addOnClickedListener(animate, function()
  {
    if(isAnimating)return

    worldFrame = 0
    isAnimating = true

    clearAnimationData(animationFragments)
    initAnimationLoop(animationFragments)
    animateAllFragments(animationFragments)
  })
   let create = newButton(80,80,10,10, "create","",1,0,0,1,document.getElementById("root"))
   addOnClickedListener(create,function(){
           //circle(-0.87,0.87,0)
           //rect(0,0,0)
           //line(3,3)
           //line2(1,1,0,2,2,0)
           empty(-1,-2,0)
           group()
   })
  window.oncontextmenu = function(e)
  {
    e.preventDefault()
  }

  slLoader = newUI("textarea", document.getElementById("root"))
  changeUIStatus(slLoader, 80, 10, 20, 20)

  systemMode.pic = newImage(2, 90, 30, 10, "", 0, 0, 0, 0, document.getElementById("root"))
  systemMode.text = newText(0, 0, mode, systemMode.pic)
  systemMode.text.style.fontSize = "50px"
  systemMode.text.style.opacity = 0

  systemHelp.pic = newImage(1, 25, 20, 20, "", 0, 0, 0, 0, document.getElementById("root"))
  systemHelp.text = newText(0, 0, "", systemHelp.pic)
  systemHelp.text.innerText = StandardHelpText
  systemHelp.text.style.fontSize = "14px"
  systemHelp.text.style.color = "green"
  systemHelp.text.style.opacity = 0

  // let v1 = new THREE.Vector3(0, 2, 2)
  // let v2 = new THREE.Vector3(0, 5, 5)
  //
  // let line = newLine(v1, v2, new THREE.Color(0, 0, 0))
  // scene.add(line)
  //
  // let curve = new THREE.LineCurve3(v1, v2)
  // let t = curve.getTangentAt(0.25)
  // console.log(t);
  //
  // let teste = newCube(0.5, 2.5, 0, 2, 1, 1, null, new THREE.Color(1, 0, 0))
  // scene.add(teste)
  //
  // let test = newCube(0, 0, 0, 2, 1, 1, null, new THREE.Color(1, 0, 0))
  // scene.add(test)
  //
  // let rotationCosAngle = new THREE.Vector3().crossVectors(new THREE.Vector3(1, 0, 0), t)
  // console.log(rotationCosAngle);
  //
  // if (rotationCosAngle.x != 0)
  // {
  //   test.rotation.x =  Math.acos(rotationCosAngle.x)
  //
  //
  // }
  //
  // if (rotationCosAngle.y != 0)
  // {
  //   test.rotation.y = Math.PI / 2  - Math.acos(rotationCosAngle.y)
  //
  //   // if (t.y > 0)
  //   // {
  //   //   test.rotation.y = Math.PI - test.rotation.y
  //   // }
  // }
  //
  // if (rotationCosAngle.z != 0)
  // {
  //   test.rotation.z = Math.PI / 2 - Math.acos(rotationCosAngle.z)
  //
  //   if (t.x < 0)
  //   {
  //     test.rotation.z = Math.PI - test.rotation.z
  //   }
  // }
  //
  // console.log(test.rotation);
  // //test.lookAt(t.x, t.y, t.z)
  // //test.rotateY(toRadians(90))
  // console.log(test.rotation);
  //
  //
  //
  // test.position.set(lerp3(v1, v2, 0.5).x, lerp3(v1, v2, 0.5).y, lerp3(v1, v2, 0.5).z)
  // // for (var i = 0; i < test.geometry.vertices.length; i++)
  // // {
  // //   test.geometry.vertices[i].applyAxisAngle(new THREE.Vector3(0, 1, 0), (t.y))
  // //   test.geometry.vertices[i].applyAxisAngle(new THREE.Vector3(1, 0, 0), (t.x))
  // //   test.geometry.vertices[i].applyAxisAngle(new THREE.Vector3(0, 0, 1), (t.z))
  // // }
  //
  // test.geometry.verticesNeedUpdate = true
  //
  // console.log(test.geometry);
  //
  //  test.scale.set(1, 1.5 ,1)
  //
  // console.log(test.geometry.vertices);
}
