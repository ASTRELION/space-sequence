export var GeneralHelpText =
"1: Decrease WorldFrame 10\n" +
"2: Increase WorldFrame 10\n" +
"p: Switch to 2D/3D\n" +
"s/l: Save/Load data\n" +
"v: Enter Standard Mode\n" +
"r: Enter Revise Mode\n";

export var StandardHelpText =
"Mouse: Control camera/Control Object\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Create one (start/end) frame\n" +
"Ctrl + c/v: Copy/Paste one object\n" +
"space: Create one continuely frame\n" +
"backspace: Delete current fragment\n" +
"backspace: Delete current object (no fragment)\n" +
"f: Create/Start a curve movement frame\n" +
"q: Back to the previous frame\n" +
"e: Go to the next frame\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Enter Animation Mode";

export var ReviseHelpText =
"Mouse: Control camera/Control Object/Edit Fragment\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Create one (start/end) frame\n" +
"backspace: Delete current frame\n" +
"f: Start a curve movement fragment\n" +
"q: Back to previous frame\n" +
"e: Go to next frame\n" +
"i: Set/Reset auto-movement start/end frame\n" +
"Left Arrow: Decrease Node Travelling 1\n" +
"Right Arrow: Increase Node Travelling 1\n" +
"\n" +
"If no object have been selected:\n" +
"Ctrl: Insert one (start/end) frame\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Enter Animation Mode";

export var AnimationHelpText =
"Mouse:\n" +
"Control camera/Choose Object\n" +
"Edit Object's Weight/Rotation Point\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Add one Rotation Point\n" +
"backspace: Delete current fragment\n" +
"q: Decrease fragment's duration 1\n" +
"e: Increase fragment's duration 1\n" +
"\n" +
"If no object have been selected:\n" +
"q: Decrease frame 1 and Set the animation status\n" +
"e: Increase frame 1 and Set the animation status\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Switch between Animation/Animation (Root) Mode";
