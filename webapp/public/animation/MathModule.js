export function lerp1(v1, v2, a)
{
  return v1 + (v2 - v1) * a
}

export function lerp2(v1, v2, a)
{
  let v3 = {}
  v3.x = v1.x * (1 - a) + v2.x * a
  v3.y = v1.y * (1 - a) + v2.y * a

  return v3
}

export function lerp3(v1, v2, a)
{
  let v3 = {}
  v3.x = v1.x * (1 - a) + v2.x * a
  v3.y = v1.y * (1 - a) + v2.y * a
  v3.z = v1.z * (1 - a) + v2.z * a

  return v3
}

export function clamp(v, min, max)
{
  if(v < min)
  {
    v = min
  }

  if(v > max)
  {
    v = max
  }

  return v
}

export function toRadians(degrees)
{
	return degrees * Math.PI / 180
}

export function toDegrees(radians)
{
	return radians * 180 / Math.PI
}
