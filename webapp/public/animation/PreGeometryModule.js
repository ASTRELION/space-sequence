export function newPlane(x, y, z, xS, yS, texture, color, parent)
{
  let obj = new THREE.PlaneGeometry(xS, yS);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCube(x, y, z, xS, yS, zS, texture, color, parent)
{
  let obj = new THREE.CubeGeometry(xS, yS, zS);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCone(x, y, z, r, h, texture, color, parent)
{
  let obj = new THREE.ConeGeometry(r, h, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newSphere(x, y, z, r, texture, color, parent)
{
  var obj = new THREE.SphereGeometry(r, 48, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCylinder(x, y, z, r, h, texture, color, parent)
{
  var obj = new THREE.CylinderGeometry(r, r, h, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newLine(v1, v2, color, parent)
{
  const curve = new THREE.LineCurve3(v1, v2)
  const points = curve.getPoints(2);

  const geometry = new THREE.BufferGeometry().setFromPoints( points );
  const material = new THREE.LineBasicMaterial( { color : color } );
  const mesh = new THREE.Line( geometry, material );

  if(parent != null)
  {
    parent.add(mesh)
  }

  return mesh;
}

export function deleteObject(mesh)
{
  for(let i = 0; i < mesh.children.length; i += 1)
  {
    deleteObject(mesh.children[i])
  }

  if(mesh.parent != null)
  {
    mesh.parent.remove(mesh)
  }
  else
  {
    scene.remove(mesh)
  }

  mesh.geometry.dispose()
  mesh.material.dispose()
}

export function cloneAll(object)
{
  let newObj = object.clone()
  newObj.material = object.material.clone()

  return newObj
}

export function addBorder(object, r, g, b, a, size, borderTest)
{
  var outlineMaterial = new THREE.ShaderMaterial( { side: THREE.BackSide } );
  var outlineMesh = new THREE.Mesh( object.geometry, outlineMaterial );
  outlineMesh.scale.multiplyScalar(1 + size);
  object.add(outlineMesh)

  outlineMaterial.transparent = true

  outlineMaterial.uniforms =
  {
    tEmissive: {type: "texture2D", value: object.material.map},
    borderColor: {type: "vec4", value: new THREE.Vector4(r, g, b, a)},
    borderSize: {type: "float", value: size * 0.5},
    borderTest: {type: "bool", value: borderTest}
  },

  outlineMaterial.vertexShader = vDeleteTexAlpha(),
  outlineMaterial.fragmentShader = fDeleteTexAlpha()

  if(a != 1)
  {
    outlineMaterial.opecity = a
  }

  if(object.geometry.type == "PlaneGeometry")
  {
    outlineMaterial.side = THREE.FrontSide
    outlineMesh.position.z -= 0.001
  }

  for (let i = 0; i < objectInfo.length; i++)
  {
    if(objectInfo[i].object == object)
    {
      objectInfo[i].borderObject = outlineMesh
    }
  }

  return outlineMesh
}

export function setBorderTest(borderObject, active)
{
  borderObject.material.uniforms.borderTest.value = active
}

export function setObjectProperty(mesh, x, y, z, texture, color, parent)
{
  objectStatus[mesh.id] =
  {
    "curID": -1
  }

  if(texture != null)
  {
    mesh.material.map = texture
  }

  if(color != null)
  {
    mesh.material.color = color
  }

  mesh.position.set(x, y, z)
  mesh.rotation.set(0, 0, 0, "YZX")
  mesh.material.transparent = true

  if(parent != null)
  {
    parent.add(mesh)
  }
}
