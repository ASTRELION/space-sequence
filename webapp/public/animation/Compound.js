/**
 * record the start fragment
 * record the whole fragment if the start has already existed
 */
export var animationBuffer = []

/**
 * the id for current operating fragment
 * (update when left/right arrow or create new whole fragment)
 */
export var objectStatus = []

export function initAnimationLoop(data)
{
  for(let i = 0; i < data.length; i += 1)
  {
    let positionPoints = []
    let rotationPoints = []
    let scalePoints = []
    let alphaPoints = []

    for(let j = 0; j < data[i].posEasePoints.length; j += 1)
    {
      positionPoints[j] = new THREE.Vector3(data[i].posEasePoints[j].x, data[i].posEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].rotationEasePoints.length; j += 1)
    {
      rotationPoints[j] = new THREE.Vector3(data[i].rotationEasePoints[j].x, data[i].rotationEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].scaleEasePoints.length; j += 1)
    {
      scalePoints[j] = new THREE.Vector3(data[i].scaleEasePoints[j].x, data[i].scaleEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].alphaEasePoints.length; j += 1)
    {
      alphaPoints[j] = new THREE.Vector3(data[i].alphaEasePoints[j].x, data[i].alphaEasePoints[j].y, 0)
    }

    data[i].positionCurve = new THREE.CatmullRomCurve3(positionPoints, false, "chordal")
    data[i].rotationCurve = new THREE.CatmullRomCurve3(rotationPoints, false, "chordal")
    data[i].scaleCurve = new THREE.CatmullRomCurve3(scalePoints, false, "chordal")
    data[i].alphaCurve = new THREE.CatmullRomCurve3(alphaPoints, false, "chordal")

    data[i].object.rotation.x = toRadians(data[i].startEulerAngle.x)
    data[i].object.rotation.y = toRadians(data[i].startEulerAngle.y)
    data[i].object.rotation.z = toRadians(data[i].startEulerAngle.z)

    if(data[i].movementCurve == null)
    {
      let movementPoints = []
      let cur = data[i]

      while(true)
      {
        movementPoints[movementPoints.length] = new THREE.Vector3(cur.startPosition.x, cur.startPosition.y, cur.startPosition.z)

        if(cur.ballisticMovementNext != null)
        {
          cur = cur.ballisticMovementNext
        }
        else
        {
          movementPoints[movementPoints.length] = new THREE.Vector3(cur.endPosition.x, cur.endPosition.y, cur.endPosition.z)

          break
        }
      }

      let curve = new THREE.CatmullRomCurve3(movementPoints, false, "chordal")
      // let curve = new THREE.QuadraticBezierCurve3(movementPoints[0], movementPoints[1], movementPoints[2])
      cur = data[i]

      for(let j = 0; j < movementPoints.length - 1; j += 1)
      {
        cur.movementCurve = curve
        cur.movementCurveIndex = j
        cur.movementCurveLength = movementPoints.length - 1

        cur = cur.ballisticMovementNext
      }
    }

    if (data[i].rotationType == "Object" && data[i].autoRotationCurve == null)
    {
      let sequenceFragments = []
      let cur = data[i]

      while(cur != null)
      {
        sequenceFragments[sequenceFragments.length] = cur
        cur = cur.autoRotationNext
      }

      generateAutoRotationCurve(sequenceFragments)
    }
  }
}

export function animateAllFragments(data)
{
  let animationLength = data.length

  for(let i = 0; i < animationLength; i += 1)
  {
    if(data[i].activeBefore != 1)
    {
      setAlpha(0, data[i])
    }
  }

  systemMode.text.style.opacity = 0
  systemHelp.text.style.opacity = 0

  let finalAnimationTime = 9999999

  console.log(data);
  handler.core.visible = false

  var loop = function()
  {
    worldFrame += 1
    document.getElementById(curTimeText.id).innerHTML = worldFrame

    if(worldFrame >= finalAnimationTime)
    {
      isAnimating = false
      commandAnimating = false

      return
    }

    for(let i = 0; i < data.length; i += 1)
    {
      if(data[i].animated == null && data[i].startFrame <= worldFrame)
      {
        animateNow(data[i])

        data[i].animated = true
        animationLength -= 1

        if(animationLength == 0)
        {
          finalAnimationTime = data[i].startFrame + data[i].duration
        }
      }
    }

    requestAnimationFrame(loop)
  }

  loop()
}

export function animateNow(fragment)
{
  let curFrame = 0

  for(let i = 0; i < fragment.childFragments.length; i += 1)
  {
    animateNow(fragment.childFragments[i])
  }

  for (let i = 0; i < fragment.callBefore.length; i += 1)
  {
    fragment.callBefore[i]()
  }

  var animationLoop = function()
  {
    if(curFrame == fragment.duration + 1)
    {
      if(fragment.activeAfter != 1)
      {
        setOpacity(0, fragment)
      }

      for (let i = 0; i < fragment.callBack.length; i += 1)
      {
        fragment.callBack[i]()
      }

      return
    }

    setFragmentStatusAt(fragment, curFrame)

    curFrame += 1

    requestAnimationFrame(animationLoop)
  };

  animationLoop()
}

export function setFragmentStatusAt(fragment, curFrame)
{
  let curStartPortition = fragment.movementCurveIndex / fragment.movementCurveLength
  let curTotalPortition = 1 / fragment.movementCurveLength

  let curPosition = fragment.movementCurve.getPointAt(curStartPortition + curTotalPortition * clamp(fragment.positionCurve.getPointAt(curFrame / fragment.duration).y, 0, 1))
  let curScale = lerp3(fragment.startScale, fragment.endScale, fragment.scaleCurve.getPointAt(curFrame / fragment.duration).y)
  let curAlpha = lerp1(fragment.startAlpha, fragment.endAlpha, fragment.alphaCurve.getPointAt(curFrame / fragment.duration).y)

  if (fragment.rotationType == "Object")
  {
    let curRotation = fragment.autoRotationCurve.getTangentAt(lerp1(curStartPortition, curStartPortition + curTotalPortition, curFrame / fragment.duration))
    fragment.object.position.set(0, 0, 0)
    fragment.object.lookAt(curRotation.x, curRotation.y, curRotation.z)
    fragment.object.rotateY(toRadians(90))
  }
  else if (fragment.rotationType == "Vertex")
  {

  }
  else
  {
    let curRotation = lerp3(fragment.startEulerAngle, fragment.endEulerAngle, fragment.rotationCurve.getPointAt(curFrame / fragment.duration).y)
    fragment.object.rotation.set(0, 0, 0, "YZX")
    fragment.object.rotateY(curRotation.y)
    fragment.object.rotateZ(curRotation.z)
    fragment.object.rotateX(curRotation.x)
  }

  fragment.object.position.set(curPosition.x, curPosition.y, curPosition.z)
  fragment.object.scale.set(curScale.x, curScale.y, curScale.z)
  setAlpha(curAlpha, fragment)
}

export function clearAnimationData(data)
{
  for(let i = 0; i < data.length; i += 1)
  {
    data[i].positionCurve = undefined
    data[i].rotationCurve = undefined
    data[i].scaleCurve = undefined
    data[i].alphaCurve = undefined

    data[i].movementCurve = undefined
    data[i].movementCurveIndex = undefined
    data[i].movementCurveLength = undefined

    data[i].autoRotationCurve = undefined
    data[i].autoRotationCurveIndex = undefined
    data[i].autoRotationCurveLength = undefined

    data[i].animated = undefined
  }
}

export function recordFrame(object)
{
  if(animationBuffer[object.id] != null && animationBuffer[object.id].frame != worldFrame)
  {
    recordAnimationFrame(animationBuffer[object.id], recordEndFrame(object))

    animationBuffer[object.id] = null
    objectStatus[object.id].curID = animationFragments[animationFragments.length - 1].id

    addHelpText("Fragment Create Completed")
  }
  else if (animationBuffer[object.id] != null && animationBuffer[object.id].frame == worldFrame)
  {
    addHelpText("Created failed: still in the same time line")
  }
  else
  {
    animationBuffer[object.id] = recordStartFrame(object)
    objectStatus[object.id].curID = -1

    addHelpText("Start Frame Created")
  }
}

export function recordStartFrame(object)
{
  let startInfo =
  {
    "object": object,
    "objectInfo":
    {
      "name": "TestName",
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": []
    },

    "position": {"x": object.position.x, "y": object.position.y, "z": object.position.z},
    "rotation": {"x": object.rotation.x, "y": object.rotation.y, "z": object.rotation.z},
    "scale": {"x": object.scale.x, "y": object.scale.y, "z": object.scale.z},
    "alpha": object.material.opacity,

    "frame": worldFrame,

    "callBack": []
  }

  return startInfo
}

export function recordEndFrame(object)
{
  let endInfo =
  {
    "object": object,
    "objectInfo":
    {
      "name": "TEST",
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": []
    },

    "childFragments": [],

    "position": {"x": object.position.x, "y": object.position.y, "z": object.position.z},
    "rotation": {"x": object.rotation.x, "y": object.rotation.y, "z": object.rotation.z},
    "scale": {"x": object.scale.x, "y": object.scale.y, "z": object.scale.z},
    "alpha": object.material.opacity,

    "frame": worldFrame,

    "callBack": []
  }

  return endInfo
}

export function recordAnimationFrame(startInfo, endInfo)
{
  animationFragments[animationFragments.length] =
  {
    "id": globalID++,
    "object": startInfo.object,
    "objectInfo":
    {
      "name": startInfo.objectInfo.name,
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": [],

      // "border":
      // {
      //   show = 1,
      //   color = {"x": 1, "y": 1, "z": 1},
      //   size = 0.05,
      //   borderTest = -1
      // }
    },

    "parentFragment": undefined,
    "childFragments": [],

    "startPosition": startInfo.position,
    "endPosition": endInfo.position,
    "startEulerAngle": startInfo.rotation,
    "endEulerAngle": endInfo.rotation,
    "startScale": startInfo.scale,
    "endScale": endInfo.scale,
    "startAlpha": startInfo.alpha,
    "endAlpha": endInfo.alpha,

    "activeBefore": 1,
    "activeAfter": 1,

    "callBefore": startInfo.callBack,
    "callBack": endInfo.callBack,

    "startFrame": startInfo.frame,
    "duration": endInfo.frame - startInfo.frame,

    //advanced elements
    //TODO
    "posEasePoints": JSON.parse(JSON.stringify(easeArray)),
    "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
    "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
    "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

    "ballisticMovementNext": undefined
  }

  if(mode == "Revise")
  {
    leaveReviseMode()
    enterReviseMode()
    curTarget = thisWorldObject
  }
}

export function setAlpha(opacity, fragment)
{
  let parentOpacity = 1

  if(fragment.parentFragment != null)
  {
    parentOpacity = fragment.parentFragment.object.material.opacity
  }

  fragment.object.material.opacity = parentOpacity * opacity

  for(let i = 0; i < fragment.childFragments.length; i += 1)
  {
    setOpacity(fragment.childFragments[i], opacity)
  }
}

export function findLastFragment(object)
{
  let preFrame = -99999999
  let preIndex = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(object == animationFragments[i].object && preFrame <= animationFragments[i].startFrame + animationFragments[i].duration)
    {
      preFrame = animationFragments[i].startFrame + animationFragments[i].duration
      preIndex = i
    }
  }

  if(preIndex == -1)
  {
    return null
  }
  else
  {
    return animationFragments[preIndex]
  }
}

export function findPreviousFragment(object, curFrame)
{
  let distance = 99999999
  let index = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    let curDistance = curFrame - animationFragments[i].startFrame

    if(object == animationFragments[i].object && curDistance > 0 && curDistance < distance)
    {
      distance = curDistance
      index = i
    }
  }

  if(index != -1)
  {
    return animationFragments[index]
  }
  else
  {
    return null
  }
}

export function findNextFragment(object, curFrame)
{
  let distance = -99999999
  let index = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    let curDistance = curFrame - animationFragments[i].startFrame

    if(object == animationFragments[i].object && curDistance <= 0 && curDistance > distance)
    {
      distance = curDistance
      index = i
    }
  }

  if(index != -1)
  {
    return animationFragments[index]
  }
  else
  {
    return null
  }
}

export function findObjectCurFragment(object)
{
  if(objectStatus[object.id].curID != -1)
  {
    return findFragment(objectStatus[object.id].curID)
  }

  return null
}

export function findFragment(id)
{
  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(id == animationFragments[i].id)
    {
      return animationFragments[i]
    }
  }

  return null
}

export function renderHelpText()
{
  for(let i = 0; i < systemInfo.length; i += 1)
  {
    if(systemInfo[i].duration < 0)
    {
      destroy(systemInfo[i].pic)

      systemInfo.splice(i, 1)
      i -= 1

      continue
    }

    changeUIStatus(systemInfo[i].pic, 0, 5 + 2.5 * i, 50, 10)

    if(systemInfo[i].duration < 0.5)
    {
      systemInfo[i].text.style.opacity = systemInfo[i].duration * 2
    }

    systemInfo[i].duration -= 0.0167
  }
}

export function reEnterMode(target)
{
  let modeIndex = 0
  let targetIndex = 0
  let modeStringArr = ["Standard", "Revise", "Animation", "Animation (Root)"]
  let modeEnterFunctionArr = [enterStandardMode, enterReviseMode, enterAnimationMode, enterAnimationMode]
  let modeLeaveFunctionArr = [leaveStandardMode, leaveReviseMode, leaveAnimationMode, leaveAnimationMode]

  for (let i = 0; i < modeStringArr.length; i += 1)
  {
    if (mode == modeStringArr[i])
    {
        modeIndex = i
    }

    if (target == modeStringArr[i])
    {
        targetIndex = i
    }
  }

  modeLeaveFunctionArr[modeIndex]()
  modeEnterFunctionArr[targetIndex]()

  editHelpText(target)
}

export function enterStandardMode()
{
  if(thisWorldObject != null)
  {
    curTarget = thisWorldObject
    curTarget.visible = true
  }

  thisWorldObject = null
}

export function leaveStandardMode()
{
  if(curTarget != null)
  {
    thisWorldObject = curTarget
  }
}

export function enterReviseMode()
{
  if(thisWorldObject != null)
  {
    if(objectStatus[thisWorldObject.id].curID == -1)
    {
      let lastFragment = findLastFragment(thisWorldObject)

      if(lastFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = lastFragment.id
      }
    }

    obtainReviseFragmentsInBothSides(thisWorldObject, anotherWorldMaxPresent)
  }
}

export function leaveReviseMode()
{
  for(let i = 0; i < anotherWorldObjects.length; i += 1)
  {
    deleteObject(anotherWorldObjects[i])
  }

  for(let i = 0; i < anotherWorldLines.length; i += 1)
  {
    deleteObject(anotherWorldLines[i])
  }

  curTarget = null

  anotherWorldFragments = []
  anotherWorldObjects = []
  anotherWorldLines = []

  anotherWorldFocusIndex = -1

  handler.core.visible = false
}

export function enterAnimationMode()
{
  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  viewAnimationStatusAt(worldFrame)
}

export function leaveAnimationMode()
{

}
export function vDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    void main()
    {
       vUv = uv;
       vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
       gl_Position = projectionMatrix * mvPosition;
    }
  `
}

export function fDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    uniform sampler2D tEmissive;
    uniform vec4 borderColor;
    uniform float borderSize;
    uniform bool borderTest;

    float lerp(float v1, float v2, float a)
    {
      return v1 + (v2 - v1) * a;
    }

    void main()
    {
      if(!borderTest)
      {
        gl_FragColor = vec4(borderColor);

        return;
      }

      bool isBorder = false;
      vec2 newUv = vec2(vUv.x * (1.0 + borderSize * 2.0) - borderSize, vUv.y * (1.0 + borderSize * 2.0) - borderSize);

      vec2[] points = vec2[8]
      (
        vec2(newUv.x, clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(newUv.x, clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0))
      );

      for(int i = 0; i <= 8; i += 1)
      {
        if(texture2D(tEmissive, points[i]).a != 0.0)
        {
          isBorder = true;
          break;
        }
      }

      if(isBorder)
      {
        gl_FragColor = vec4(borderColor);
      }
      else
      {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
      }
    }
  `
}

var deleteText = []

var handlerSize = 0.3125
var handler

var curPressedMouse = [false, false, false]

var plane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0)
var pressedMousePos = new THREE.Vector2()
var mousePos = new THREE.Vector2()
var currentlyPressedKeys = []
var hasPressedKeys = []

var cameraDirection
var cameraStartDistance
var curTarget
var curHandlerTarget
var ifCoreMove = false
var isCopying = false
var isBoneMove = false

var autoRotationStartObject
var autoRotationStartFragment

/**
 * current selected object's copy when entering other mode
 */
var thisWorldObject

/**
 * max node traverse for both sides in revise mode
 */
var anotherWorldMaxPresent = 4

/**
 *  the data for fragments for other modes
 */
var anotherWorldFragments = []

/**
 *  temperary objects for other modes
 */
var anotherWorldObjects = []

/**
 *  visible lines to help create animation for other modes
 */
export var anotherWorldLines = []

/**
 *  current mid index for editing in other modes
 */
export var anotherWorldFocusIndex = -1

export var curEasePointTarget

export var cameraMovementFactor = 1.0 / 10.0
export var cameraRotationFactor = 1.0 / 10.0

export function handleKeyDown(event)
{
  currentlyPressedKeys[event.keyCode] = true
  hasPressedKeys[event.keyCode] = true

  console.log(event.keyCode);

  //1
  if(event.keyCode == 49)
  {
    worldFrame = Math.max(0, worldFrame - 10)

    addHelpText("Move world fragment 10 Back")
  }

  //2
  if(event.keyCode == 50)
  {
    worldFrame += 10

    addHelpText("Move world fragment 10 Forward")
  }

  //p
  if(event.keyCode == 80)
  {
    threeD = !threeD
    switchTo3DView(threeD)

    addHelpText("View switched")
  }

  //s
  if(event.keyCode == 83)
  {
    saveJson()

    addHelpText("Saved data")
  }

  //l
  if(event.keyCode == 76)
  {
    loadJson(slLoader.value)

    addHelpText("Loaded data")
  }

  //tab
  if(event.keyCode == 9)
  {
    if(mode == "Revise")
    {
      curTarget = thisWorldObject

      handler.core.visible = true
      handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)

      addHelpText("Select the current original object")
    }
  }

  //ctrl + c
  if(event.keyCode == 67)
  {
    if(mode == "Standard" && currentlyPressedKeys[17])
    {
      copiedObject = curTarget

      isCopying = true

      addHelpText("Copied one object")
    }
  }

  //i
  if (event.keyCode == 73)
  {
    if (mode == "Revise")
    {
      if (curTarget != null)
      {
        if (autoRotationStartFragment == null)
        {
          autoRotationStartObject = curTarget
          autoRotationStartFragment = findObjectExistsFragment(curTarget, anotherWorldFragments, 1)
          addHelpText("Set/Reset Object auto rotation start frame")
        }
        else
        {
          if (curTarget == autoRotationStartObject)
          {
            addHelpText("Auto rotation created failed because still in the same frame")
          }
          else
          {
            let sequenceFragments = traverseAnimationFragments(autoRotationStartFragment, findObjectExistsFragment(curTarget, anotherWorldFragments, 2))
            console.log(autoRotationStartFragment);
            console.log(findObjectExistsFragment(curTarget, anotherWorldFragments, 2));
            setRotationMode(sequenceFragments, "Object")

            autoRotationStartObject = null
            autoRotationStartFragment = null
            addHelpText("Object auto rotation (re)set finished")
          }
        }
      }
      else
      {
        addHelpText("No object have been selected")
      }
    }
  }

  //u
  if (event.keyCode == 85)
  {
    if (mode == "Revise")
    {
      if (curTarget != null)
      {
        if (autoRotationStartFragment == null)
        {
          autoRotationStartObject = curTarget
          autoRotationStartFragment = findObjectExistsFragment(curTarget, anotherWorldFragments)
          addHelpText("Set/Reset Vertices auto rotation start frame")
        }
        else
        {
          if (curTarget == autoRotationStartObject)
          {
            addHelpText("Auto rotation created failed because still in the same frame")
          }
          else
          {
            let sequenceFragments = traverseAnimationFragments(autoRotationStartFragment, findObjectExistsFragment(curTarget, anotherWorldFragments))
            setRotationMode(sequenceFragments, "Vertex")

            autoRotationStartObject = null
            autoRotationStartFragment = null
            addHelpText("Vertices auto rotation (re)set finished")
          }
        }
      }
      else
      {
        addHelpText("No object have been selected")
      }
    }
  }

  //ctrl + v
  if(event.keyCode == 86)
  {
    if(mode == "Standard" && currentlyPressedKeys[17])
    {
      if(!isCopying)
      {
        return
      }

      let newObj = cloneAll(copiedObject)

      objectInfo[objectInfo.length] =
      {
        "object": newObj
      }

      objectStatus[newObj.id] =
      {
        "curID": -1
      }

      scene.add(newObj)
      curTarget = newObj

      isCopying = true

      addHelpText("Pasted one object")
    }
    else
    {
      if(curTarget != null)
      {
        handler.core.visible = true
      }

      reEnterMode("Standard")
      mode = "Standard"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Standard mode")
    }
  }

  //r
  if(event.keyCode == 82)
  {
    addHelpText("Switch to Revise mode")

    reEnterMode("Revise")
    mode = "Revise"
    systemMode.text.innerHTML = mode
  }

  //left Arrow
  if(event.keyCode == 37)
  {
    if(mode == "Revise")
    {
      anotherWorldMaxPresent = 4 //Math.max(2, anotherWorldMaxPresent - 2)
      let curIndexCopy = anotherWorldFocusIndex

      leaveReviseMode()
      enterReviseMode()

      anotherWorldFocusIndex = curIndexCopy
      handler.core.visible = true

      if(anotherWorldMaxPresent == 2)
      {
        addHelpText("Nodes Travel length Reached min value")
      }
      else
      {
        addHelpText("Decrease nodes travel length")
      }
    }
    else
    {
      addHelpText("Not in Revise Mode")
    }
  }

  //right Arrow
  if(event.keyCode == 39)
  {
    if(mode == "Revise")
    {
      anotherWorldMaxPresent = Math.min(anotherWorldFragments.length * 2, anotherWorldMaxPresent + 2)
      let curIndexCopy = anotherWorldFocusIndex

      leaveReviseMode()
      enterReviseMode()

      anotherWorldFocusIndex = curIndexCopy
      handler.core.visible = true

      if(anotherWorldMaxPresent == anotherWorldFragments.length * 2)
      {
        addHelpText("Nodes Travel length Reached max value")
      }
      else
      {
        addHelpText("Increase nodes travel length")
      }
    }
    else
    {
      addHelpText("Not in Revise Mode")
    }
  }

  //space
  if(event.keyCode == 32)
  {
    if(mode == "Standard" && !easeCurveInControl && curTarget != null)
    {
      if(animationBuffer[curTarget.id] == null)
      {
        animationBuffer[curTarget.id] = recordStartFrame(curTarget)
        objectStatus[curTarget.id].curID = -1

        addHelpText("Start Frame Created")
      }
      else
      {
        recordFrame(curTarget)
        recordFrame(curTarget)

        addHelpText("Continuely Created Frames")
      }
    }
  }

  //f : force create
  if (event.keyCode == 70)
  {
    if (mode == "Standard" && curTarget != null || mode == "Revise" && curTarget == thisWorldObject)
    {
      let curObject = null

      if (mode == "Standard")
      {
        curObject = curTarget
      }
      else
      {
        curObject = thisWorldObject
      }

      let preFragment = findLastFragment(curObject)

      if(preFragment != null && preFragment.startFrame + preFragment.duration == worldFrame)
      {
        addHelpText("Created failed: still in the same time line")
      }
      else if(preFragment != null)
      {
        animationFragments[animationFragments.length] =
        {
          "id": globalID++,
          "object": curObject,
          "objectInfo":
          {
            "name": "TEST0",
            "vertex": [],
            "posWeight": [],
            "rotationWeight": [],
            "scaleWeight": [],
            "alphaWeight": [],
          },

          "parentFragment": undefined,
          "childFragments": [],

          "startPosition": {"x": preFragment.endPosition.x, "y": preFragment.endPosition.y, "z": preFragment.endPosition.z},
          "endPosition": {"x": curObject.position.x, "y": curObject.position.y, "z": curObject.position.z},
          "startEulerAngle": {"x": preFragment.endEulerAngle.x, "y": preFragment.endEulerAngle.y, "z": preFragment.endEulerAngle.z},
          "endEulerAngle": {"x": curObject.rotation.x, "y": curObject.rotation.y, "z": curObject.rotation.z},
          "startScale": {"x": preFragment.endScale.x, "y": preFragment.endScale.y, "z": preFragment.endScale.z},
          "endScale": {"x": curObject.scale.x, "y": curObject.scale.y, "z": curObject.scale.z},
          "startAlpha": preFragment.endAlpha,
          "endAlpha": curObject.material.opacity,

          "activeBefore": 1,
          "activeAfter": 1,

          "callBefore": [],
          "callBack": [],

          "startFrame": preFragment.startFrame + preFragment.duration,
          "duration": worldFrame - (preFragment.startFrame + preFragment.duration),

          "posEasePoints": JSON.parse(JSON.stringify(easeArray)),
          "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

          "movementType": "Ballist",
          "ballisticMovementNext": undefined
        }

        if (preFragment.movementType == "Ballist")
        {
          preFragment.ballisticMovementNext = animationFragments[animationFragments.length - 1]
        }
      }
      else
      {
        recordFrame(curObject)

        //if created, set ballisticMovementNext
        if (animationBuffer[curObject.id] == null)
        {
          animationFragments[animationFragments.length - 1].movementType = "Ballist"
        }
      }
    }

    if(mode == "Revise")
    {
      leaveReviseMode()
      enterReviseMode()
      curTarget = thisWorldObject
    }
  }

  //a
  if(event.keyCode == 65)
  {
    if(mode != "Animation")
    {
      reEnterMode("Animation")
      mode = "Animation"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Animation mode")
    }
    else
    {
      mode = "Animation (Root)"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Animation (Root) mode")
    }
  }

  //e
  if (event.keyCode == 69)
  {
    if (mode == "Animation" || mode == "Animation (Root)")
    {
      if(curTarget != null)
      {
        let preFragment = findPreviousFragment(curTarget, worldFrame)

        if (preFragment != null)
        {
          if (mode == "Animation (Root)")
          {
            EditStartFrameFrom(findNextFragment(curTarget, worldFrame), 10)

            addHelpText("Increased 10 duration(" + (preFragment.duration + 10) + ") for current fragment with subsequence effect")
          }
          else
          {
            addHelpText("Increased 10 duration(" + (preFragment.duration + 10) + ") for current fragment")
          }

          preFragment.duration += 10

          viewAnimationStatusAt(worldFrame)

          if (handler.core.visible)
          {
            handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
          }
        }
        else
        {
          addHelpText("No fragment have found for this object")
        }
      }
      else
      {
        viewAnimationStatusAt(++worldFrame)
      }
    }
  }

  //q
  if (event.keyCode == 81)
  {
    if (mode == "Animation" || mode == "Animation (Root)")
    {
      if(curTarget != null)
      {
        let preFragment = findPreviousFragment(curTarget, worldFrame)

        if (preFragment != null)
        {
          //check if valid first
          if (preFragment.duration != 10)
          {
            if (mode == "Animation (Root)")
            {
              EditStartFrameFrom(findNextFragment(curTarget, worldFrame), -10)

              addHelpText("Decrease 10 duration(" + (preFragment.duration - 10) + ") for current fragment with subsequence effect")
            }
            else
            {
              addHelpText("Decrease 10 duration(" + (preFragment.duration - 10) + ") for current fragment")
            }
          }

          preFragment.duration = Math.max(10, preFragment.duration - 10)

          viewAnimationStatusAt(worldFrame)

          if (handler.core.visible)
          {
            handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
          }
        }
        else
        {
          addHelpText("No fragment have found for this object")
        }
      }
      else
      {
        worldFrame = Math.max(0, worldFrame - 1)
        viewAnimationStatusAt(worldFrame)
      }
    }
  }
}

export function handleKeyUp(event)
{
  //ctrl
  if(event.keyCode == 17)
  {
    if(isCopying)
    {
      isCopying = false
      currentlyPressedKeys[event.keyCode] = false

      return
    }

    if(curTarget == null)
    {
      if (mode == "Revise")
      {
        let status = insertFrame(mousePos, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)

        if (status == "OK")
        {
          addHelpText("Inserted frame in " + worldFrame)
        }
        else
        {
          addHelpText("Inserted frame error with " + status)
        }
      }
      else
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false
      }

      return
    }

    if(easeCurveInControl)
    {
      const rect = UIRenderer.domElement.getBoundingClientRect();
      let mouseXPosInUICamera = ((mousePos.x + 1) / 2 * window.innerWidth - parseInt(UICanvas.style.left)) / parseInt(UICanvas.style.width)
      let mouseYPosInUICamera = ((mousePos.y + 1) / 2 * window.innerHeight - (window.innerHeight + parseInt(UICanvas.style.top) - parseInt(UICanvas.style.height))) / parseInt(UICanvas.style.height)

      for(let i = 0; i < easePointsOnScreen.length; i += 1)
      {
        if(mouseXPosInUICamera < easePointsOnScreen[i].position.x)
        {
          let newPoint = newSphere(mouseXPosInUICamera, mouseYPosInUICamera, 0, 0.05)
          newPoint.material.emissive = new THREE.Color(1, 0, 0)
          UIScene.add(newPoint)

          easePointsOnScreen.splice(i, 0, newPoint);

          updateEaseCurve()
          break
        }
      }
    }
    else
    {
      recordFrame(curTarget)
    }
  }

  //backspace
  if(event.keyCode == 8)
  {
    if(easeCurveInControl)
    {
      if(curEasePointTarget != null)
      {
        for(let i = 0; i < easePointsOnScreen.length; i += 1)
        {
          if(easePointsOnScreen[i] == curEasePointTarget)
          {
            UIScene.remove(easePointsOnScreen[i])
            easePointsOnScreen[i].geometry.dispose()
            easePointsOnScreen[i].material.dispose()

            easePointsOnScreen.splice(i, 1)

            updateEaseCurve()
            break
          }
        }
      }
    }
    else
    {
      if(curTarget != null)
      {
        if (mode == "Standard")
        {
          let curFragment = findObjectCurFragment(curTarget)

          if (curFragment != null)
          {
            deleteFragmentIn(curFragment, animationFragments)
            addHelpText("Deleted current fragment")
          }
          else
          {
            deleteObject(curTarget)
            addHelpText("Deleted current object")
          }
        }
        else if (mode == "Revise")
        {
          for (let i = 0; i < anotherWorldObjects.length; i += 1)
          {
            if (curTarget == anotherWorldObjects[i])
            {
              deleteFrame(curTarget, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)
              addHelpText("Deleted selected frame")

              handler.core.visible = false

              break
            }

            if (i - 1 == anotherWorldObjects.length)
            {
              addHelpText("Bug")
            }
          }
        }
      }
    }
  }

  //q
  if(event.keyCode == 81)
  {
    if(mode == "Revise")
    {
      let preFragment = findPreviousFragment(thisWorldObject, findObjectCurFragment(thisWorldObject).startFrame)

      if(preFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = preFragment.id
        worldFrame = preFragment.startFrame

        leaveReviseMode()
        enterReviseMode()

        addHelpText("Back to the previous frame for this object")
      }
      else
      {
        addHelpText("No previous frame for this object")
      }
    }
    else if(mode == "Standard")
    {
      if(curTarget == null)
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false

        return
      }

      // try to find current fragment first
      let preFragment = findObjectCurFragment(curTarget)

      //if not find, check if start frame has recorded
      if(preFragment == null)
      {
        preFragment = animationBuffer[curTarget.id]

        //if find
        if(preFragment != null)
        {
          //if position is not equal, return to start frame
          if(!atOnePoint(curTarget, preFragment.position, new THREE.Vector3()))
          {
            curTarget.position.set(preFragment.position.x, preFragment.position.y, preFragment.position.z)
            handler.core.position.set(preFragment.position.x, preFragment.position.y, preFragment.position.z)
            worldFrame = preFragment.frame

            addHelpText("Back to the start frame")
          }
          //delete all cur animation info
          else
          {
            worldFrame = preFragment.frame
            animationBuffer[curTarget.id] = null

            addHelpText("The start frame clear finished")
          }
        }
        else
        {
          preFragment = findPreviousFragment(curTarget, worldFrame)

          //if find:
          //  back to the previous fragment's position
          //  set object status to previous fragment
          if(preFragment != null)
          {
            curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
            handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
            objectStatus[curTarget.id].curID = preFragment.id
            worldFrame = preFragment.startFrame

            addHelpText("Back to the previous fragment's start position")
          }
          else
          {
            addHelpText("No fragment have found for this object")
          }
        }
      }
      else if(atOnePoint(curTarget, preFragment.startPosition, preFragment.endPosition))
      {
        preFragment = findPreviousFragment(curTarget, preFragment.startFrame)

        //if find:
        //  back to the previous fragment's position
        //  set object status to previous fragment
        if(preFragment != null)
        {
          curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
          handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
          objectStatus[curTarget.id].curID = preFragment.id
          worldFrame = preFragment.startFrame

          addHelpText("Back to the previous fragment's start position")
        }
        else
        {
          addHelpText("Already in the first fragment")
        }
      }
      else
      {
        curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
        handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
        worldFrame = preFragment.startFrame

        addHelpText("Back to original position of this fragment")
      }
    }
  }

  //e
  if(event.keyCode == 69)
  {
    if(mode == "Revise")
    {
      let nextFragment = findNextFragment(thisWorldObject, findObjectCurFragment(thisWorldObject).startFrame + findObjectCurFragment(thisWorldObject).duration)

      if(nextFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = nextFragment.id
        worldFrame = nextFragment.startFrame

        leaveReviseMode()
        enterReviseMode()

        addHelpText("Go to the next frame for this object")
      }
      else
      {
        addHelpText("No next frame for this object")
      }
    }
    else if(mode == "Standard")
    {
      if(curTarget == null)
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false

        return
      }

      let curFrame = worldFrame
      let curFragment = findObjectCurFragment(curTarget)

      if(curFragment != null)
      {
        curFrame = curFragment.startFrame + curFragment.duration
      }

      // try to find next fragment
      let nextFragment = findNextFragment(curTarget, curFrame)

      //if find, go to next fragment, clear
      //current buffer data
      if(nextFragment != null)
      {
        curTarget.position.set(nextFragment.startPosition.x, nextFragment.startPosition.y, nextFragment.startPosition.z)
        handler.core.position.set(nextFragment.startPosition.x, nextFragment.startPosition.y, nextFragment.startPosition.z)
        objectStatus[curTarget.id].curID = nextFragment.id
        worldFrame = nextFragment.startFrame

        animationBuffer[curTarget.id] = null

        addHelpText("Go to next fragment's start position")
      }
      else
      {
        addHelpText("No next fragment for this object")
      }
    }
  }

  currentlyPressedKeys[event.keyCode] = false
}

export function handleMouseDown(event)
{
  curPressedMouse[event.button] = true

  if(checkExists(mousePos, sceneZone))
  {
    handleMouseDownScene(event)
  }
  else
  {
    handleMouseDownUI(event)
  }

  pressedMousePos.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)
}

export function handleMouseDownScene(event)
{
  if(event.button == 0)
  {
    ifCoreMove = false

    raycaster.setFromCamera(mousePos, camera);

    let sceneObjects = []
    let anotherObjects = []
    let handlerObjects = []

    for(let i = 0; i < objectInfo.length; i += 1)
    {
      sceneObjects[i] = objectInfo[i].object
    }

    if(mode == "Revise")
    {
      for(let i = 0; i < anotherWorldObjects.length; i += 1)
      {
        anotherObjects[i] = anotherWorldObjects[i]
      }
    }

    let intersectsObjects = raycaster.intersectObjects(sceneObjects);
    let intersectsAnotherObjects = raycaster.intersectObjects(anotherObjects)

    handlerObjects[handlerObjects.length] = handler.core
    handlerObjects[handlerObjects.length] = handler.xMove
    handlerObjects[handlerObjects.length] = handler.yMove
    handlerObjects[handlerObjects.length] = handler.zMove
    handlerObjects[handlerObjects.length] = handler.xRotate
    handlerObjects[handlerObjects.length] = handler.yRotate
    handlerObjects[handlerObjects.length] = handler.zRotate
    handlerObjects[handlerObjects.length] = handler.xScale
    handlerObjects[handlerObjects.length] = handler.yScale
    handlerObjects[handlerObjects.length] = handler.zScale

    let intersectshandlerObjects = raycaster.intersectObjects(handlerObjects);

    if(intersectsObjects.length > 0 || intersectshandlerObjects.length > 0 || intersectsAnotherObjects.length > 0)
    {
      if ( intersectshandlerObjects.length > 0 )
      {
        curHandlerTarget = intersectshandlerObjects[0].object

        ifCoreMove = curHandlerTarget == handler.core
      }
      else if ( intersectsObjects.length > 0 )
      {
        curTarget = intersectsObjects[0].object

        handler.core.position.set(intersectsObjects[0].object.position.x, intersectsObjects[0].object.position.y, intersectsObjects[0].object.position.z)
        handler.core.visible = true

        ifCoreMove = true

        if(mode == "Revise" && curTarget != thisWorldObject)
        {
          thisWorldObject = curTarget
          leaveReviseMode()
          enterReviseMode()
          curTarget = thisWorldObject

          handler.core.visible = true
        }
      }
      else if ( intersectsAnotherObjects.length > 0 )
      {
        curTarget = intersectsAnotherObjects[0].object

        handler.core.position.set(intersectsAnotherObjects[0].object.position.x, intersectsAnotherObjects[0].object.position.y, intersectsAnotherObjects[0].object.position.z)
        handler.core.visible = true

        ifCoreMove = true
      }

      cameraDirection = new THREE.Vector3(curTarget.position.x - camera.position.x, curTarget.position.y - camera.position.y, curTarget.position.z - camera.position.z).normalize()
    }
    else
    {
      curTarget = null
      curHandlerTarget = null

      handler.core.visible = false
    }
  }
  else if(event.button == 2)
  {
    if(cameraStartDistance == null)
    {
      if(curTarget != null)
      {
        cameraStartDistance = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z).distanceTo(curTarget.position)
      }
      else
      {
        cameraStartDistance = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z).distanceTo(new THREE.Vector3())
      }
    }
  }
}

export function handleMouseDownUI(event)
{
  if(event.button == 0)
  {
    let mouse = {}
    const rect = UIRenderer.domElement.getBoundingClientRect();
    mouse.x = ( ( event.clientX - rect.left ) / ( rect.right - rect.left ) ) * 2 - 1;
    mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

    raycaster.setFromCamera(mouse, UICamera);

    let sceneObjects = []

    for (let i = 1; i < easePointsOnScreen.length - 1; i += 1)
    {
      sceneObjects[i - 1] = easePointsOnScreen[i]
    }

    let intersectsObjects = raycaster.intersectObjects(sceneObjects);

    if(intersectsObjects.length > 0)
    {
      curEasePointTarget = intersectsObjects[0].object
    }
    else
    {
      curEasePointTarget = null
    }
  }
}

export function handleMouseMove(event)
{
  if(checkExists(mousePos, sceneZone))
  {
    handleMouseMoveScene(event)
  }
  else
  {
    handleMouseMoveUI(event)
  }

  if(!isAnimating && checkExists(mousePos, {width: "100", height: "65", x: "0", y: "35"}))
  {
    systemMode.text.style.opacity = 0.5
    systemHelp.text.style.opacity = 0.75
  }
  else
  {
    systemMode.text.style.opacity = 0
    systemHelp.text.style.opacity = 0
  }

  mousePos.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)
}

export function handleMouseMoveScene(event)
{
  let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  if(curPressedMouse[0])
  {
    //transform object
    if(curTarget == null)
    {
      return
    }

    //if in Animation mode, prevent
    //object's moving
    if((mode == "Animation" || mode == "Animation (Root)") && !isBoneMove)
    {
      return
    }

    //free move
    if(ifCoreMove)
    {
      //scaler
      if(hasPressedKeys[16])
      {
        let v1 = 1

        if((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x < 0)
        {
          v1 = -1
        }

        let curPosition = new THREE.Vector2((event.clientX / window.innerWidth) * 2 - 1, 0)
        let distance = 1 + curPosition.distanceTo(mousePos) * 0.8 * cameraDistance / 10 * v1

        curTarget.scale.set(curTarget.scale.x * distance, curTarget.scale.y * distance, curTarget.scale.z * distance)
      }
      //drag
      else
      {
        let intersects = new THREE.Vector3()
        let curPosition = new THREE.Vector2((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)

        raycaster.setFromCamera(mousePos, camera)
        raycaster.ray.intersectPlane(plane, intersects)

        let distance = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

        raycaster.setFromCamera(curPosition, camera)
        raycaster.ray.intersectPlane(plane, intersects)

        distance = new THREE.Vector3(intersects.x - distance.x, intersects.y - distance.y, intersects.z - distance.z)

        curTarget.position.add(distance)
        handler.core.position.add(distance)
      }
    }
    //move
    else if(curHandlerTarget == handler.xMove || curHandlerTarget == handler.yMove || curHandlerTarget == handler.zMove)
    {
      if(curHandlerTarget == handler.xMove)
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.z > 0)
        {
          v1 = -1
        }

        if(cameraDirection.x < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.x)), 0, 0)
        let distance2 = new THREE.Vector3((-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.z)), 0, 0)

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
      else if(curHandlerTarget == handler.yMove)
      {
        let distance1 = new THREE.Vector3(0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance * 0.05, 0)
        let distance2 = new THREE.Vector3(0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance * lerp1(0.5, 0.05, Math.abs(cameraDirection.y)), 0, 0)

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
      else
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.x < 0)
        {
          v1 = -1
        }

        if(cameraDirection.z < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(0, 0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.z)))
        let distance2 = new THREE.Vector3(0, 0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.x)))

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
    }
    //rotate
    else if(curHandlerTarget == handler.xRotate || curHandlerTarget == handler.yRotate || curHandlerTarget == handler.zRotate)
    {
      let rotationFactor = 15
      let v1 = 1

      if(cameraDirection.z < 0)
      {
        v1 = -1
      }

      let distance = ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * v1

      if(curHandlerTarget == handler.xRotate)
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(1, 0, 0), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
      else if(curHandlerTarget == handler.yRotate)
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
      else
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 0, 1), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
    }
    //scale
    else
    {
      let scaleFactor = 10

      if(curHandlerTarget == handler.xScale)
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.z > 0)
        {
          v1 = -1
        }

        if(cameraDirection.x < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * scaleFactor * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.x)), 0, 0)
        let distance2 = new THREE.Vector3((-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * scaleFactor * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.z)), 0, 0)

        curTarget.scale.add(distance1).add(distance2)
      }
      else if(curHandlerTarget == handler.yScale)
      {
        let distance1 = new THREE.Vector3(0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * scaleFactor * 0.05, 0)
        let distance2 = new THREE.Vector3(0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * scaleFactor * lerp1(0.5, 0.05, Math.abs(cameraDirection.y)), 0, 0)

        curTarget.scale.add(distance1).add(distance2)
      }
      else
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.x < 0)
        {
          v1 = -1
        }

        if(cameraDirection.z < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(0, 0, ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * scaleFactor * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.z)))
        let distance2 = new THREE.Vector3(0, 0, (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * scaleFactor * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.x)))

        curTarget.scale.add(distance1).add(distance2)
      }
    }

    if(mode == "Revise")
    {
      updateObjectsInFrame(curTarget, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)
    }
  }
  else if(curPressedMouse[1])
  {
    let distance1 = ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * cameraDistance / 10 * 5
    let distance2 = (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * cameraDistance / 10 * 5

    camera.translateX(-distance1)
    camera.translateY(-distance2)
  }
  else if(curPressedMouse[2])
  {
    if(!threeD)
    {
      return
    }

    let distance1 = ((event.clientX / window.innerWidth) * 2 - 1 - mousePos.x) * 250
    let distance2 = (-(event.clientY / window.innerHeight) * 2 + 1 - mousePos.y) * 250

    camera.translateZ(-cameraStartDistance)
    camera.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), toRadians(-distance1));
    camera.translateZ(cameraStartDistance)

    camera.translateZ(-cameraStartDistance)
    camera.rotateX(toRadians(distance2));
    camera.rotation.x = clamp(camera.rotation.x, -Math.PI / 2, Math.PI / 2)
    camera.translateZ(cameraStartDistance)

    let dir = camera.getWorldDirection(new THREE.Vector3())
    plane.normal = new THREE.Vector3(dir.x, dir.y, dir.z)
  }
}

export function handleMouseMoveUI(event)
{
  if(curPressedMouse[0])
  {
    if(curEasePointTarget != null)
    {
      let intersects = new THREE.Vector3()
      let curPosition = new THREE.Vector2((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)

      raycaster.setFromCamera(mousePos, UICamera)
      raycaster.ray.intersectPlane(plane, intersects)

      let distance = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

      raycaster.setFromCamera(curPosition, UICamera)
      raycaster.ray.intersectPlane(plane, intersects)

      let xDistance = (intersects.x - distance.x) * (renderer.getSize(new THREE.Vector2()).x / parseInt(UICanvas.style.width))
      let yDistance = (intersects.y - distance.y) * (renderer.getSize(new THREE.Vector2()).y / parseInt(UICanvas.style.height))
      distance = new THREE.Vector3(xDistance, yDistance, 0)

      curEasePointTarget.position.add(distance)

      curEasePointTarget.position.x = clamp(curEasePointTarget.position.x, 0, 1)
      curEasePointTarget.position.y = clamp(curEasePointTarget.position.y, 0, 1)

      updateEaseCurve()
    }
  }
}

export function handleMouseUp(event)
{
  curPressedMouse[event.button] = false

  if(checkExists(mousePos, sceneZone))
  {
    handleMouseUpScene(event)
  }
  else
  {
    handleMouseUpUI(event)
  }
}

export function handleMouseUpScene(event)
{
  hasPressedKeys[16] = false
  cameraStartDistance = null
}

export function handleMouseUpUI(event)
{

}

export function handleMouseWheel(event)
{
  if(checkExists(pressedMousePos, sceneZone))
  {
    handleMouseWheelScene(event)
  }
  else
  {
    handleMouseWheelUI(event)
  }
}

export function handleMouseWheelScene(event)
{
  let translateZValue = 2.5
  let cameraDistance = Math.abs(camera.position.z)

  if(curTarget != null)
  {
    if(!threeD)
    {
      cameraDistance = Math.abs(camera.position.z - curTarget.position.z)
    }
    else
    {
      cameraDistance = camera.position.distanceTo(curTarget.position)
    }
  }

  if(event.wheelDelta != null)
  {
    if(event.wheelDelta > 0 && translateZValue < cameraDistance)
    {
      camera.translateZ(-translateZValue)
    }
    else if(event.wheelDelta < 0 && cameraDistance < 50 - translateZValue)
    {
      camera.translateZ(translateZValue)
    }
  }
  else if(event.detail != null)
  {
    if(event.detail > 0 && cameraDistance < 50 - translateZValue)
    {
      camera.translateZ(translateZValue)
    }
    else if(event.detail < 0 && translateZValue < cameraDistance)
    {
      camera.translateZ(-translateZValue)
    }
  }

  cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  handler.core.scale.set(cameraDistance / 8, cameraDistance / 8, cameraDistance / 8)

  if(!threeD)
  {
    let cameraDistance2 = Math.abs(camera.position.z)

    if(curTarget != null)
    {
      cameraDistance2 = Math.abs(camera.position.z - curTarget.position.z)
    }

    let height = cameraDistance2
    let width = cameraDistance2 * window.innerWidth / window.innerHeight

    camera.left = width / -2
    camera.right = width / 2
    camera.top = height / 2
    camera.bottom = height / -2

    camera.updateProjectionMatrix()
  }
}

export function handleMouseWheelUI(event)
{

}

export function handleKeys()
{

}

export function onWindowResize()
 {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}
export var GeneralHelpText =
"1: Decrease WorldFrame 10\n" +
"2: Increase WorldFrame 10\n" +
"p: Switch to 2D/3D\n" +
"s/l: Save/Load data\n" +
"v: Enter Standard Mode\n" +
"r: Enter Revise Mode\n";

export var StandardHelpText =
"Mouse: Control camera/Control Object\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Create one (start/end) frame\n" +
"Ctrl + c/v: Copy/Paste one object\n" +
"space: Create one continuely frame\n" +
"backspace: Delete current fragment\n" +
"backspace: Delete current object (no fragment)\n" +
"f: Create/Start a curve movement frame\n" +
"q: Back to the previous frame\n" +
"e: Go to the next frame\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Enter Animation Mode";

export var ReviseHelpText =
"Mouse: Control camera/Control Object/Edit Fragment\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Create one (start/end) frame\n" +
"backspace: Delete current frame\n" +
"f: Start a curve movement fragment\n" +
"q: Back to previous frame\n" +
"e: Go to next frame\n" +
"i: Set/Reset auto-movement start/end frame\n" +
"Left Arrow: Decrease Node Travelling 1\n" +
"Right Arrow: Increase Node Travelling 1\n" +
"\n" +
"If no object have been selected:\n" +
"Ctrl: Insert one (start/end) frame\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Enter Animation Mode";

export var AnimationHelpText =
"Mouse:\n" +
"Control camera/Choose Object\n" +
"Edit Object's Weight/Rotation Point\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Add one Rotation Point\n" +
"backspace: Delete current fragment\n" +
"q: Decrease fragment's duration 1\n" +
"e: Increase fragment's duration 1\n" +
"\n" +
"If no object have been selected:\n" +
"q: Decrease frame 1 and Set the animation status\n" +
"e: Increase frame 1 and Set the animation status\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Switch between Animation/Animation (Root) Mode";

export var globalID = 0;

export var canvas;
export var renderer;
export var scene;
export var camera;
export var target;
export var composer;
export var composer2
export var raycaster;
export var sceneZone =
{
  width: "75",
  height: "100",
  x: "0",
  y: "0"
}

export var obj1
export var borderEffect
export var slLoader
export var copiedObject

export var objectInfo = []
export var animationFragments = []

export var threeD = false

export var UICanvas
export var UIRenderer
export var UIScene
export var UICamera

export var curTimeText
export var systemInfo = []
export var systemMode = {}
export var systemHelp = {}
export var mode = "Standard"

export var easeCurve
export var easeArray
export var easeCurveInControl
export var easePointsOnScreen = []

export var worldFrame = 0
export var isAnimating = false

export function main()
{
  init()
  initUI()

  switchTo3DView(false)

  let curTimeControl = newButton(90, 80, 10, 10, "World Frame", "", 1, 0, 0, 1, document.getElementById("root"))
  curTimeText = newText(0, 0, worldFrame, curTimeControl)

  let frameText = newText(0, 0, 0, newImage(0, 0, 5, 5, "", 0, 0, 0, 0, document.getElementById("root")))
  frameText.style.opacity = 0.5

  let startTime = new Date()
  let counter = 0

  var loop = function()
  {
    if(counter % 60 == 0)
    {
      document.getElementById(frameText.id ).innerHTML = "FPS: " + (60000 / (new Date().getTime() - startTime.getTime())).toFixed(1)

      startTime = new Date()
    }

    counter += 1

    document.getElementById(curTimeText.id).innerHTML = worldFrame

    renderer.clear()
    renderer.render(scene, camera);

    UIRenderer.render(UIScene, UICamera)
    renderHelpText()

    handleKeys();
    requestAnimationFrame(loop);

    if (counter == 30)
    {
      createSequence("a")

      macroStartRecording("c")
      let macroID = globalID - 1

      rect(0, 0, 0, 1, 1, 1)
      let cur2 = objectInfo[objectInfo.length - 1].object
      let cur2ID = objectInfo[objectInfo.length - 1].id
      createKeyFrame()
      position(cur2ID, -2, 2, 0)
      createKeyFrame()
      setKeyframeDuration(globalID - 1, 100)
      position(cur2ID, 2, -2, 0)
      createKeyFrame()
      setKeyframeDuration(globalID - 1, 30)
      macroStopRecording()

      macroStartRecording("b")
      circle(0, 0, 0, 0.5)
      let cur = objectInfo[objectInfo.length - 1].object
      createKeyFrame()
      cur.position.set(-2, -2, 0)
      createKeyFrame()
      setKeyframeDuration(globalID - 1, 100)
      cur.position.set(2, 2, 0)
      createKeyFrame()
      setKeyframeDuration(globalID - 1, 30)
      macroStopRecording()

      playSequence()
    }
  };

  loop();
}

export function init()
{
  canvas = document.getElementById('canvas');
  UICanvas = document.getElementById('UICanvas');
  UICanvas.onmouseover = function()
  {
    easeCurveInControl = true
  }
  UICanvas.onmouseout = function()
  {
    easeCurveInControl = false
  }

  canvas.style.position = "relative"
  UICanvas.style.position = "relative"

  scene = new THREE.Scene();
  UIScene = new THREE.Scene()

  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.01, 3000);
  camera.position.set(0, 6, 5);
  camera.rotation.set(0, 0, 0, "YZX")
  camera.rotateX(toRadians(-30))

  let size = 1
  UICamera = new THREE.OrthographicCamera(size / -2, size / 2, size / 2, size / -2, 0.01, 3000)
  UICamera.position.set(0.5, 0.5, 10)
  UICamera.rotation.set(0, 0, 0, "YZX")

  renderer = new THREE.WebGLRenderer( { canvas : canvas, context : canvas.getContext( 'webgl2') });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.autoClear = false
  renderer.antialias = true

  UIRenderer = new THREE.WebGLRenderer( { canvas : UICanvas, context : UICanvas.getContext( 'webgl2') });
  UIRenderer.setPixelRatio( window.devicePixelRatio );
  UIRenderer.setSize( window.innerWidth * 0.1, window.innerWidth * 0.1, false);

  raycaster = new THREE.Raycaster();

  initBasicScene()

  window.addEventListener('resize', onWindowResize, false );
  document.onkeydown = handleKeyDown;
  document.onkeyup = handleKeyUp;
  document.onmousedown = handleMouseDown;
  document.onmousemove = handleMouseMove;
  document.onmouseup = handleMouseUp;
  document.onmousewheel = handleMouseWheel;
  if (document.addEventListener)
  {
    document.addEventListener('DOMMouseScroll', handleMouseWheel, false);
  }

  // load texture map
  let loader = new THREE.TextureLoader();
  let texture = loader.load("b.png");

  texture.magFilter = THREE.NearestFilter;
  texture.minFilter = THREE.NearestFilter;

  let imageNames = ["AllWhite.png","AllWhite.png","AllWhite.png","AllWhite.png","AllWhite.png","AllWhite.png"];
  let ourCubeMap = new THREE.CubeTextureLoader().load( imageNames );
  ourCubeMap.repeat.set(100, 100);

  UIScene.background = ourCubeMap;

  obj1 = newPlane(0, 0, 0, 1, 1, null, new THREE.Color(1, 0, 0))
  let obj2 = newCube(1, 1, 0, 1, 1, 1, null, new THREE.Color(0, 1, 0))
  let obj3 = newPlane(0, 0, 0, 1, 1, null, new THREE.Color(0, 0, 0))

  objectInfo[objectInfo.length] =
  {
    "object": obj2,
    "id": globalID++
  }

  scene.add(obj2)
  addBorder(obj2, 0, 1, 0, 1, 0.05, false)

  let arr = [{"x": 0, "y": 0}, {"x": 0.5, "y": 0.5}, {"x": 1, "y": 1}]
  setCurveCanvasSize(window.innerWidth - 200, window.innerHeight * 0.5, 100)
  drawEaseCurveOnScreen(arr)
}

export function initHandler()
{
  let core = newSphere(0, 0, 0, handlerSize * 0.3, null, new THREE.Color(0, 0, 0))
  core.material.depthTest = false
  core.renderOrder = 2

  let xMove = newCone(2, 0, 0, handlerSize * 0.25, handlerSize, null, new THREE.Color(0.6, 0, 0), core)
  let yMove = newCone(0, 2, 0, handlerSize * 0.25, handlerSize, null, new THREE.Color(0, 0.6, 0), core)
  let zMove = newCone(0, 0, 2, handlerSize * 0.25, handlerSize, null, new THREE.Color(0, 0, 0.6), core)
  xMove.material.emissive = new THREE.Color(0.4, 0, 0)
  yMove.material.emissive = new THREE.Color(0, 0.4, 0)
  zMove.material.emissive = new THREE.Color(0, 0, 0.4)

  let xRotate = newSphere(1.7, 0, 0, handlerSize * 0.25, null, new THREE.Color(0.6, 0, 0), core)
  let yRotate = newSphere(0, 1.7, 0, handlerSize * 0.25, null, new THREE.Color(0, 0.6, 0), core)
  let zRotate = newSphere(0, 0, 1.7, handlerSize * 0.25, null, new THREE.Color(0, 0, 0.6), core)
  xRotate.material.emissive = new THREE.Color(0.4, 0, 0)
  yRotate.material.emissive = new THREE.Color(0, 0.4, 0)
  zRotate.material.emissive = new THREE.Color(0, 0, 0.4)

  let xLine = newCylinder(1, 0, 0, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  let yLine = newCylinder(0, 1, 0, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  let zLine = newCylinder(0, 0, 1, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  xLine.material.emissive = new THREE.Color(0.6, 0, 0)
  yLine.material.emissive = new THREE.Color(0, 0.6, 0)
  zLine.material.emissive = new THREE.Color(0, 0, 0.6)

  let scaleCore = newCube(0, 0, 0, handlerSize * 0.6, handlerSize * 0.6, handlerSize * 0.6, null, new THREE.Color(0, 0, 0), core)

  let faceLine = newCylinder(1, 0, 0, 0.01, 2, null, new THREE.Color(0, 0, 0), scaleCore)
  faceLine.material.emissive = new THREE.Color(0, 0, 0)

  let xScale = newCube(1.47, 0, 0, handlerSize * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0.6, 0, 0), core)
  let yScale = newCube(0, 1.47, 0, handlerSize  * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0, 0.6, 0), core)
  let zScale = newCube(0, 0, 1.47, handlerSize * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0, 0, 0.6), core)
  xScale.material.emissive = new THREE.Color(0.4, 0, 0)
  yScale.material.emissive = new THREE.Color(0, 0.4, 0)
  zScale.material.emissive = new THREE.Color(0, 0, 0.4)

  xMove.rotation.set(0, 0, toRadians(-90), "XYZ")
  yMove.rotation.set(0, 0, 0, "XYZ")
  zMove.rotation.set(toRadians(90), 0, 0, "XYZ")

  xRotate.rotation.set(0, 0, toRadians(-90), "XYZ")
  yRotate.rotation.set(0, 0, 0, "XYZ")
  zRotate.rotation.set(toRadians(90), 0, 0, "XYZ")

  xScale.rotation.set(0, 0, toRadians(-90), "XYZ")
  yScale.rotation.set(0, 0, 0, "XYZ")
  zScale.rotation.set(toRadians(90), 0, 0, "XYZ")

  xLine.rotation.set(0, 0, toRadians(-90), "XYZ")
  zLine.rotation.set(toRadians(90), 0, 0, "XYZ")

  faceLine.rotation.set(0, 0, toRadians(-90), "XYZ")
  for(let i = 0; i < core.children.length; i += 1)
  {
    core.children[i].material.depthTest = false
    core.children[i].renderOrder = 2
  }

  for(let i = 0; i < scaleCore.children.length; i += 1)
  {
    scaleCore.children[i].material.depthTest = false
    scaleCore.children[i].renderOrder = 2
  }

  xLine.material.depthTest = false
  xLine.renderOrder = 1
  yLine.material.depthTest = false
  yLine.renderOrder = 1
  zLine.material.depthTest = false
  zLine.renderOrder = 1

  faceLine.material.depthTest = false
  faceLine.renderOrder = 1

  handler =
  {
    "core": core,
    "xMove": xMove,
    "yMove": yMove,
    "zMove": zMove,

    "xRotate": xRotate,
    "yRotate": yRotate,
    "zRotate": zRotate,

    "scaleCore": scaleCore,

    "xScale": xScale,
    "yScale": yScale,
    "zScale": zScale,

    "xLine": xLine,
    "yLine": yLine,
    "zLine": zLine
  }

  scene.add(handler.core)
}

export function switchTo3DView(mode)
{
  if(mode)
  {
    handler.zMove.translateZ(-10000)
    handler.zRotate.translateZ(-10000)
    handler.zScale.translateZ(-10000)
    handler.zLine.translateZ(-10000)

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.01, 3000)
    camera.position.set(0, 6, 10);
    camera.rotation.set(0, 0, 0, "YZX")
    camera.rotateX(toRadians(-30))
  }
  else
  {
    handler.zMove.translateZ(10000)
    handler.zRotate.translateZ(10000)
    handler.zScale.translateZ(10000)
    handler.zLine.translateZ(10000)

    let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

    let height = cameraDistance
    let width = cameraDistance * window.innerWidth / window.innerHeight
    camera = new THREE.OrthographicCamera(width / -2, width / 2, height / 2, height / -2, 0.01, 3000)

    camera.position.set(0, 0, 10)
    camera.rotation.set(0, 0, 0, "YZX")
  }

  let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  handler.core.scale.set(cameraDistance / 10, cameraDistance / 10, cameraDistance / 10)
}

export function setCurveCanvasSize(x, y, size)
{
  UICanvas.style.left = x + "px"
  UICanvas.style.top = (-window.innerHeight + y) + "px"
  UICanvas.style.height = size + "px"
  UICanvas.style.width = size + "px"
}

export function drawEaseCurveOnScreen(points)
{
  let curvePoints = []

  for(let i = 0; i < points.length; i += 1)
  {
    curvePoints[i] = new THREE.Vector3(points[i].x, points[i].y, 0)

    easePointsOnScreen[i] = newSphere(points[i].x, points[i].y, 0, 0.05)
    easePointsOnScreen[i].material.emissive = new THREE.Color(1, 0, 0)
    UIScene.add(easePointsOnScreen[i])
  }

  let curve = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")

  let curPoints = curve.getPoints( 50 );
  let geometry = new THREE.BufferGeometry().setFromPoints( curPoints );
  let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );

  easeCurve = new THREE.Line( geometry, material );
  UIScene.add(easeCurve)

  easeArray = points
}

export function updateEaseCurve()
{
  let curvePoints = []
  easeArray = []

  for(let i = 0; i < easePointsOnScreen.length; i += 1)
  {
    easeArray[i] = {}
    easeArray[i].x = easePointsOnScreen[i].position.x
    easeArray[i].y = easePointsOnScreen[i].position.y

    curvePoints[i] = new THREE.Vector3(easePointsOnScreen[i].position.x, easePointsOnScreen[i].position.y, 0)
  }

  let curve = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")
  let curPoints = curve.getPoints( 50 );

  for(let i = 0; i < curPoints.length; i += 1)
  {
    easeCurve.geometry.attributes.position.array[i * 3] = curPoints[i].x
    easeCurve.geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
    easeCurve.geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
  }

  easeCurve.geometry.attributes.position.needsUpdate = true
}

export function deleteEaseCurveOnScreen()
{
  UIScene.remove(easeCurve)
  easeCurve.geometry.dispose()
  easeCurve.material.dispose()

  for(let i = 0; i < easePointsOnScreen.length; i += 1)
  {
    UIScene.remove(easePointsOnScreen[i])
    easePointsOnScreen[i].geometry.dispose()
    easePointsOnScreen[i].material.dispose()
  }

  easePointsOnScreen = []
  easeCurve = null
}

export function initUI()
{
  let animate = newButton(90, 90, 10, 10, "Animate", "", 1, 0, 0, 1, document.getElementById("root"))
  addOnClickedListener(animate, function()
  {
    if(isAnimating)return

    worldFrame = 0
    isAnimating = true

    clearAnimationData(animationFragments)
    initAnimationLoop(animationFragments)
    animateAllFragments(animationFragments)
  })
   let create = newButton(80,80,10,10, "create","",1,0,0,1,document.getElementById("root"))
   addOnClickedListener(create,function(){
           //circle(-0.87,0.87,0)
           //rect(0,0,0)
           //line(3,3)
           //line2(1,1,0,2,2,0)
           empty(-1,-2,0)
           group()
   })
  window.oncontextmenu = function(e)
  {
    e.preventDefault()
  }

  slLoader = newUI("textarea", document.getElementById("root"))
  changeUIStatus(slLoader, 80, 10, 20, 20)

  systemMode.pic = newImage(2, 90, 30, 10, "", 0, 0, 0, 0, document.getElementById("root"))
  systemMode.text = newText(0, 0, mode, systemMode.pic)
  systemMode.text.style.fontSize = "50px"
  systemMode.text.style.opacity = 0

  systemHelp.pic = newImage(1, 25, 20, 20, "", 0, 0, 0, 0, document.getElementById("root"))
  systemHelp.text = newText(0, 0, "", systemHelp.pic)
  systemHelp.text.innerText = StandardHelpText
  systemHelp.text.style.fontSize = "14px"
  systemHelp.text.style.color = "green"
  systemHelp.text.style.opacity = 0

  // let v1 = new THREE.Vector3(0, 2, 2)
  // let v2 = new THREE.Vector3(0, 5, 5)
  //
  // let line = newLine(v1, v2, new THREE.Color(0, 0, 0))
  // scene.add(line)
  //
  // let curve = new THREE.LineCurve3(v1, v2)
  // let t = curve.getTangentAt(0.25)
  // console.log(t);
  //
  // let teste = newCube(0.5, 2.5, 0, 2, 1, 1, null, new THREE.Color(1, 0, 0))
  // scene.add(teste)
  //
  // let test = newCube(0, 0, 0, 2, 1, 1, null, new THREE.Color(1, 0, 0))
  // scene.add(test)
  //
  // let rotationCosAngle = new THREE.Vector3().crossVectors(new THREE.Vector3(1, 0, 0), t)
  // console.log(rotationCosAngle);
  //
  // if (rotationCosAngle.x != 0)
  // {
  //   test.rotation.x =  Math.acos(rotationCosAngle.x)
  //
  //
  // }
  //
  // if (rotationCosAngle.y != 0)
  // {
  //   test.rotation.y = Math.PI / 2  - Math.acos(rotationCosAngle.y)
  //
  //   // if (t.y > 0)
  //   // {
  //   //   test.rotation.y = Math.PI - test.rotation.y
  //   // }
  // }
  //
  // if (rotationCosAngle.z != 0)
  // {
  //   test.rotation.z = Math.PI / 2 - Math.acos(rotationCosAngle.z)
  //
  //   if (t.x < 0)
  //   {
  //     test.rotation.z = Math.PI - test.rotation.z
  //   }
  // }
  //
  // console.log(test.rotation);
  // //test.lookAt(t.x, t.y, t.z)
  // //test.rotateY(toRadians(90))
  // console.log(test.rotation);
  //
  //
  //
  // test.position.set(lerp3(v1, v2, 0.5).x, lerp3(v1, v2, 0.5).y, lerp3(v1, v2, 0.5).z)
  // // for (var i = 0; i < test.geometry.vertices.length; i++)
  // // {
  // //   test.geometry.vertices[i].applyAxisAngle(new THREE.Vector3(0, 1, 0), (t.y))
  // //   test.geometry.vertices[i].applyAxisAngle(new THREE.Vector3(1, 0, 0), (t.x))
  // //   test.geometry.vertices[i].applyAxisAngle(new THREE.Vector3(0, 0, 1), (t.z))
  // // }
  //
  // test.geometry.verticesNeedUpdate = true
  //
  // console.log(test.geometry);
  //
  //  test.scale.set(1, 1.5 ,1)
  //
  // console.log(test.geometry.vertices);
}
export function lerp1(v1, v2, a)
{
  return v1 + (v2 - v1) * a
}

export function lerp2(v1, v2, a)
{
  let v3 = {}
  v3.x = v1.x * (1 - a) + v2.x * a
  v3.y = v1.y * (1 - a) + v2.y * a

  return v3
}

export function lerp3(v1, v2, a)
{
  let v3 = {}
  v3.x = v1.x * (1 - a) + v2.x * a
  v3.y = v1.y * (1 - a) + v2.y * a
  v3.z = v1.z * (1 - a) + v2.z * a

  return v3
}

export function clamp(v, min, max)
{
  if(v < min)
  {
    v = min
  }

  if(v > max)
  {
    v = max
  }

  return v
}

export function toRadians(degrees)
{
	return degrees * Math.PI / 180
}

export function toDegrees(radians)
{
	return radians * 180 / Math.PI
}
export function newPlane(x, y, z, xS, yS, texture, color, parent)
{
  let obj = new THREE.PlaneGeometry(xS, yS);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCube(x, y, z, xS, yS, zS, texture, color, parent)
{
  let obj = new THREE.CubeGeometry(xS, yS, zS);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCone(x, y, z, r, h, texture, color, parent)
{
  let obj = new THREE.ConeGeometry(r, h, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newSphere(x, y, z, r, texture, color, parent)
{
  var obj = new THREE.SphereGeometry(r, 48, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCylinder(x, y, z, r, h, texture, color, parent)
{
  var obj = new THREE.CylinderGeometry(r, r, h, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newLine(v1, v2, color, parent)
{
  const curve = new THREE.LineCurve3(v1, v2)
  const points = curve.getPoints(2);

  const geometry = new THREE.BufferGeometry().setFromPoints( points );
  const material = new THREE.LineBasicMaterial( { color : color } );
  const mesh = new THREE.Line( geometry, material );

  if(parent != null)
  {
    parent.add(mesh)
  }

  return mesh;
}

export function deleteObject(mesh)
{
  for(let i = 0; i < mesh.children.length; i += 1)
  {
    deleteObject(mesh.children[i])
  }

  if(mesh.parent != null)
  {
    mesh.parent.remove(mesh)
  }
  else
  {
    scene.remove(mesh)
  }

  mesh.geometry.dispose()
  mesh.material.dispose()
}

export function cloneAll(object)
{
  let newObj = object.clone()
  newObj.material = object.material.clone()

  return newObj
}

export function addBorder(object, r, g, b, a, size, borderTest)
{
  var outlineMaterial = new THREE.ShaderMaterial( { side: THREE.BackSide } );
  var outlineMesh = new THREE.Mesh( object.geometry, outlineMaterial );
  outlineMesh.scale.multiplyScalar(1 + size);
  object.add(outlineMesh)

  outlineMaterial.transparent = true

  outlineMaterial.uniforms =
  {
    tEmissive: {type: "texture2D", value: object.material.map},
    borderColor: {type: "vec4", value: new THREE.Vector4(r, g, b, a)},
    borderSize: {type: "float", value: size * 0.5},
    borderTest: {type: "bool", value: borderTest}
  },

  outlineMaterial.vertexShader = vDeleteTexAlpha(),
  outlineMaterial.fragmentShader = fDeleteTexAlpha()

  if(a != 1)
  {
    outlineMaterial.opecity = a
  }

  if(object.geometry.type == "PlaneGeometry")
  {
    outlineMaterial.side = THREE.FrontSide
    outlineMesh.position.z -= 0.001
  }

  for (let i = 0; i < objectInfo.length; i++)
  {
    if(objectInfo[i].object == object)
    {
      objectInfo[i].borderObject = outlineMesh
    }
  }

  return outlineMesh
}

export function setBorderTest(borderObject, active)
{
  borderObject.material.uniforms.borderTest.value = active
}

export function setObjectProperty(mesh, x, y, z, texture, color, parent)
{
  objectStatus[mesh.id] =
  {
    "curID": -1
  }

  if(texture != null)
  {
    mesh.material.map = texture
  }

  if(color != null)
  {
    mesh.material.color = color
  }

  mesh.position.set(x, y, z)
  mesh.rotation.set(0, 0, 0, "YZX")
  mesh.material.transparent = true

  if(parent != null)
  {
    parent.add(mesh)
  }
}
export function newUI(type, parent)
{
  if(type == null)
  {
    type = "div";
  }

  let ui = document.createElement(type);
  ui.id = globalID++;

  ui.style.position = "absolute";

  if(parent != null)
  {
    parent.appendChild(ui);
  }

  return ui;
}

export function changeUIStatus(ui, x, y, width, height)
{
  ui.style.left = x + "%";
  ui.style.top = y + "%";
  ui.style.width = width + "%";
  ui.style.height = height + "%";
}

export function newImage(x, y, width, height, pic, r, g, b, a, parent)
{
  let ui = newUI();
  ui.style.left = x + "%";
  ui.style.top = y + "%";
  ui.style.width = width + "%";
  ui.style.height = height + "%";
  ui.style.backgroundColor = "rgba(" + r * 255 + "," + g * 255 + "," + b * 255 + "," + a + ")";
  ui.style.backgroundImage = pic;
  ui.style.backgroundSize = "100% 100%";
  ui.style.backgroundRepeat = "no-repeat";
  ui.style.backgroundBlendMode = "Overlay";

  parent.appendChild(ui);

  return ui;
}

export function newText(x, y, what, parent)
{
  let ui = newUI();
  ui.style.position = "relative";
  ui.style.left = x + "%";
  ui.style.top = y + "%";

  let text = document.createTextNode(what);
  ui.appendChild(text);

  parent.appendChild(ui);

  return ui;
}

export function newButton(x, y, width, height, what, pic, r, g, b, a, parent, mode)
{
  let ui = newUI("button");
  ui.style.left = x + "%";
  ui.style.top = y + "%";
  ui.style.width = width + "%";
  ui.style.height = height + "%";
  ui.style.backgroundColor = "rgba(" + r * 255 + "," + g * 255 + "," + b * 255 + "," + a + ")";
  ui.style.backgroundImage = pic;
  ui.style.backgroundSize = "100% 100%";
  ui.style.backgroundRepeat = "no-repeat";
  ui.style.backgroundBlendMode = "Overlay";

  ui.onmouseout = function()
  {
    ui.style.cursor = "auto";
  };

  ui.onmouseover = function()
  {
    ui.style.cursor = "pointer";
  };

  parent.appendChild(ui);

  let text = newText(0, 0, what, ui)

  if(mode == 1)
  {
    return [ui, text];
  }
  else
  {
    return ui;
  }
}

export function destroy(ui)
{
  ui.parentNode.removeChild(ui);
}

export function addOnClickedListener(ui, callBack)
{
  ui.onclick = function()
  {
    callBack();
  }
}
export function load(path, objSelf, callBack)
{
  var loader = new THREE.OBJLoader();

  loader.load(path, function(object)
  {
    objSelf[0] = object;

    if(object.material == null)
    {
      object.material = new THREE.MeshPhongMaterial( { color: 0xb6b311, specular: 0x222222, shininess: 50} );
    }

    object.material.transparent = true

    if(callBack != null)
    {
      callBack()
    }
  },
  function()
  {

  },
  function(e)
  {
    console.error( e );
  });
}

export function checkExists(mousePos, zone)
{
  if((mousePos.x + 1) / 2 < zone.x / 100)
  {
    return false
  }

  if((mousePos.x + 1) / 2 > (zone.x + zone.width) / 100)
  {
    return false
  }

  if((mousePos.y + 1) / 2 < zone.y / 100)
  {
    return false
  }

  if((mousePos.y + 1) / 2 > (zone.y + zone.height) / 100)
  {
    return false
  }

  return true
}

export function setObjectsVisibilityInScene(visibility, scene)
{
  for(let i = 0; i < objectInfo.length; i += 1)
  {
    objectInfo[i].object.visible = visibility
  }
}

export function initBasicScene()
{
  initHandler()

  let light = new THREE.SpotLight( new THREE.Color(0.627, 0.627, 0.627) );
  light.position.set(0, 2, 15);
  light.castShadow = true;
  light.penumbra = 0.384;
  light.intensity = 4;
  light.shadow.mapSize.width = 100;
  light.shadow.mapSize.height = 100;
  light.shadow.camera.near = 0.5;
  light.shadow.camera.far = 30;
  scene.add( light );
}

export function clearSceneObjects()
{
  for(let i = 0; i < objectInfo.length; i += 1)
  {
    scene.remove(objectInfo[i].object)
    deleteObject(objectInfo[i].object)
  }

  objectInfo = []
}

export function saveJson()
{
  clearAnimationData(animationFragments)

  let savingFragments = JSON.parse(JSON.stringify(animationFragments))
  toBasicStructure(animationFragments, savingFragments)
  slLoader.value = JSON.stringify(savingFragments, null, 4)
}

export function loadJson(jsonString)
{
  clearSceneObjects()
  globalID = 0

  animationFragments = JSON.parse(jsonString)

  let curObjects = []

  //init list first
  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(curObjects[animationFragments[i].objectInfo.id] == null)
    {
      if(animationFragments[i].objectInfo.type == "PlaneGeometry")
      {
        animationFragments[i].object = newPlane(0, 0, 0, 1, 1)
      }
      else if (animationFragments[i].objectInfo.type == "SphereGeometry")
      {
        animationFragments[i].object = newSphere(0, 0, 0, 1)
      }
      else if (animationFragments[i].objectInfo.type == "BoxGeometry")
      {
        animationFragments[i].object = newCube(0, 0, 0, 1, 1, 1)
      }

      if(animationFragments[i].objectInfo.texture != null)
      {
        animationFragments[i].object.material.map = animationFragments[i].objectInfo.texture
      }

      if(animationFragments[i].objectInfo.color != null)
      {
        animationFragments[i].object.material.color = new THREE.Color(animationFragments[i].objectInfo.color.x, animationFragments[i].objectInfo.color.y, animationFragments[i].objectInfo.color.z)
      }

      curObjects[animationFragments[i].objectInfo.id] = animationFragments[i].object

      scene.add(animationFragments[i].object)
      objectInfo[objectInfo.length] =
      {
        "object": animationFragments[i].object
      }
    }
    else
    {
      animationFragments[i].object = curObjects[animationFragments[i].objectInfo.id]

      objectStatus[animationFragments[i].object.id].curID = animationFragments[i].id
      console.log("F = " + animationFragments[i].id);
    }

    globalID = Math.max(globalID, animationFragments[i].id + 1)
  }

  //link fragments
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if(animationFragments[i].ballisticMovementNext != null)
    {
      animationFragments[i].ballisticMovementNext = animationFragments[animationFragments[i].ballisticMovementNext]
    }

    if(animationFragments[i].autoRotationNext != null)
    {
      animationFragments[i].autoRotationNext = animationFragments[animationFragments[i].autoRotationNext]
    }

    if(animationFragments[i].parentFragment != null)
    {
      animationFragments[i].parentFragment = animationFragments[animationFragments[i].parentFragment]
    }


    for (let j = 0; j < animationFragments[i].childFragments.length; j += 1)
    {
      animationFragments[i].childFragments[j] = animationFragments[animationFragments[i].childFragments[j]]
    }
  }
}

export function toBasicStructure(fragments, dstFragments)
{
  for(let i = 0; i < fragments.length; i += 1)
  {
    if(fragments[i].ballisticMovementNext != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].ballisticMovementNext == fragments[j])
        {
          dstFragments[i].ballisticMovementNext = j
          break
        }
      }
    }

    if(fragments[i].autoRotationNext != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].autoRotationNext == fragments[j])
        {
          dstFragments[i].autoRotationNext = j
          break
        }
      }
    }

    if(fragments[i].childFragments.length > 0)
    {
      for(let j = 0; j < fragments[i].childFragments.length; j += 1)
      {
        for(let k = 0; k < fragments.length; k += 1)
        {
          if(fragments[i].childFragments[j] == fragments[k])
          {
            dstFragments[i].childFragments[j] = k
            break
          }
        }
      }
    }

    if(fragments[i].parent != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].parent == fragments[j])
        {
          dstFragments[i].parent = j
          break
        }
      }
    }
  }

  for(let i = 0; i < dstFragments.length; i += 1)
  {
    dstFragments[i].objectInfo.id = fragments[i].object.id
    dstFragments[i].objectInfo.type = fragments[i].object.geometry.type
    dstFragments[i].objectInfo.color = {"x": fragments[i].object.material.color.r, "y": fragments[i].object.material.color.g, "z": fragments[i].object.material.color.b}

    dstFragments[i].object = undefined
  }
}

export function atOnePoint(obj, vec3a, vec3b)
{
  return vec3a.x == obj.position.x && vec3a.y == obj.position.y && vec3a.z == obj.position.z ||
    vec3b.x == obj.position.x && vec3b.y == obj.position.y && vec3b.z == obj.position.z
}

export function addHelpText(content)
{
  let curData = {}

  curData.pic = newImage(0, 0, 20, 10, "", 0, 0, 0, 0, document.getElementById("root"))
  curData.text = newText(0, 0, content, curData.pic)
  curData.text.style.color = "red"
  curData.duration = 3

  systemInfo.splice(0, 0, curData)
}

export function physicallyEqual(fragment1, fragment2)
{
  let bias = 0.01

  return Math.abs(fragment1.endPosition.x - fragment2.startPosition.x) < bias &&
    Math.abs(fragment1.endPosition.y - fragment2.startPosition.y) < bias &&
    Math.abs(fragment1.endPosition.z - fragment2.startPosition.z) < bias
    // &&
    // Math.abs(fragment1.endEulerAngle.x - fragment2.startEulerAngle.x) < bias &&
    // Math.abs(fragment1.endEulerAngle.y - fragment2.startEulerAngle.y) < bias &&
    // Math.abs(fragment1.endEulerAngle.z - fragment2.startEulerAngle.z) < bias &&
    // Math.abs(fragment1.endScale.x - fragment2.endScale.x) < bias &&
    // Math.abs(fragment1.endScale.y - fragment2.endScale.y) < bias &&
    // Math.abs(fragment1.endScale.z - fragment2.endScale.z) < bias
}

export function vector3Equals(v1, v2)
{
  return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z
}

export function findObjectExistsFragment(object, fragments, mode)
{
  let startFirst = function()
  {
    for (let i = 0; i < fragments.length; i += 1)
    {
      if (vector3Equals(object.position, fragments[i].startPosition) )
      {
        return fragments[i]
      }
    }
  }

  let endFirst = function()
  {
    for (let i = 0; i < fragments.length; i += 1)
    {
      if (vector3Equals(object.position, fragments[i].endPosition) )
      {
        return fragments[i]
      }
    }
  }

  if (mode == 1)
  {
    let curFragment = startFirst()

    if (curFragment != null)
    {
      return curFragment
    }
  }

  return endFirst()
}

export function obtainReviseFragmentsInBothSides(object, count)
{
  let cur = findObjectCurFragment(object)
  let midIndex = -1

  //no fragment for this object
  if (cur == null)
  {
    return
  }

  // left
  while(cur != null)
  {
    let pre = findPreviousFragment(object, cur.startFrame)

    if(pre == null || !physicallyEqual(pre, cur))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = pre
    cur = pre
  }

  //revert
  for(let i = 0; i < anotherWorldFragments.length / 2; i += 1)
  {
    let copy = anotherWorldFragments[i]
    anotherWorldFragments[i] = anotherWorldFragments[anotherWorldFragments.length - i - 1]
    anotherWorldFragments[anotherWorldFragments.length - i - 1] = copy
  }

  cur = findObjectCurFragment(object)

  // middle
  if(cur != null)
  {
    midIndex = anotherWorldFragments.length
    anotherWorldFocusIndex = midIndex
    anotherWorldFragments[anotherWorldFragments.length] = cur
  }

  // right
  while(cur != null)
  {
    let next = findNextFragment(object, cur.startFrame + cur.duration)

    if(next == null || !physicallyEqual(cur, next))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = next
    cur = next
  }

  let startIndex = Math.max(0, midIndex - parseInt(count / 2))
  let endIndex = Math.min(anotherWorldFragments.length, midIndex + Math.ceil(count / 2))

  //console.log("org:" + startIndex + " !!! " + midIndex + " !!! " + endIndex + " !!! " + anotherWorldMaxPresent);

  //check if previous continuely curve movement
  for (let i = midIndex; i >= 0 && i >= midIndex - anotherWorldMaxPresent / 2; i -= 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      startIndex = Math.min(startIndex, i)
    }
  }

  for (let i = midIndex; i < anotherWorldFragments.length && i <= midIndex + anotherWorldMaxPresent / 2; i += 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      endIndex = i + 2
    }
  }

  //change max present
  if (startIndex != midIndex - parseInt(count / 2) || endIndex != midIndex + parseInt(count / 2))
  {
    anotherWorldMaxPresent = Math.max(endIndex - midIndex, midIndex - startIndex) * 2
    //console.log(startIndex + " !!! " + midIndex + " !!! " + endIndex + " !!! " + anotherWorldMaxPresent);
  }

  for(let i = startIndex; i < endIndex; i += 1)
  {
    if (i < 0 || i >= anotherWorldFragments.length)
    {
      continue
    }

    let cloneObject = cloneAll(object)

    cloneObject.position.set(anotherWorldFragments[i].startPosition.x, anotherWorldFragments[i].startPosition.y, anotherWorldFragments[i].startPosition.z)

    if (cloneObject.rotationType == "Object")
    {

    }
    else if (cloneObject.rotationType == "Vertex")
    {

    }
    else
    {
      cloneObject.rotation.set(anotherWorldFragments[i].startEulerAngle.x, anotherWorldFragments[i].startEulerAngle.y, anotherWorldFragments[i].startEulerAngle.z)
    }

    cloneObject.scale.set(anotherWorldFragments[i].startScale.x, anotherWorldFragments[i].startScale.y, anotherWorldFragments[i].startScale.z)

    cloneObject.material.opacity = 0.5
    cloneObject.visible = true

    anotherWorldObjects[anotherWorldObjects.length] = cloneObject
    scene.add(cloneObject)

    //DELETE Border
    for (let j = 0; j < cloneObject.children.length; j += 1)
    {
      if(cloneObject.children[j].material.side == THREE.BackSide)
      {
        deleteObject(cloneObject.children[j])
      }
    }

    if(i == midIndex)
    {
      addBorder(cloneObject, 1, 0, 0, 0.5, 0.05, false)
    }

    if(i == endIndex - 1 || i == anotherWorldFragments.length - 1)
    {
      let cloneObject = cloneAll(object)

      //DELETE Border
      for (let j = 0; j < cloneObject.children.length; j += 1)
      {
        if(cloneObject.children[j].material.side == THREE.BackSide)
        {
          deleteObject(cloneObject.children[j])
        }
      }

      cloneObject.position.set(anotherWorldFragments[i].endPosition.x, anotherWorldFragments[i].endPosition.y, anotherWorldFragments[i].endPosition.z)
      cloneObject.rotation.set(anotherWorldFragments[i].endEulerAngle.x, anotherWorldFragments[i].endEulerAngle.y, anotherWorldFragments[i].endEulerAngle.z, "YZX")
      cloneObject.scale.set(anotherWorldFragments[i].endScale.x, anotherWorldFragments[i].endScale.y, anotherWorldFragments[i].endScale.z)

      cloneObject.material.opacity = 0.5
      cloneObject.visible = true

      anotherWorldObjects[anotherWorldObjects.length] = cloneObject
      scene.add(cloneObject)
    }
  }

  for(let i = 0; i < anotherWorldObjects.length - 1; i += 1)
  {
    if(anotherWorldFragments[Math.max(0, startIndex) + i].ballisticMovementNext == null)
    {
      let line = new THREE.LineCurve3(anotherWorldObjects[i].position, anotherWorldObjects[i + 1].position)

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      anotherWorldLines[i] = curveObject
      scene.add(curveObject)
    }
    else
    {
      let curvePoints = []

      while (i < anotherWorldObjects.length - 1)
      {
        let curPos = new THREE.Vector3()
        curPos.x = anotherWorldFragments[startIndex + i].startPosition.x
        curPos.y = anotherWorldFragments[startIndex + i].startPosition.y
        curPos.z = anotherWorldFragments[startIndex + i].startPosition.z
        curvePoints[curvePoints.length] = curPos

        if(anotherWorldFragments[startIndex + i].ballisticMovementNext == null)
        {
          curPos = new THREE.Vector3()
          curPos.x = anotherWorldFragments[startIndex + i].endPosition.x
          curPos.y = anotherWorldFragments[startIndex + i].endPosition.y
          curPos.z = anotherWorldFragments[startIndex + i].endPosition.z
          curvePoints[curvePoints.length] = curPos

          break
        }

        i += 1
      }

      let line = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      for(let j = i; j >= 0; j -= 1)
      {
        if(anotherWorldLines[j] != null)
        {
          break
        }

        anotherWorldLines[j] = curveObject
      }

      scene.add(curveObject)
    }
  }
}

export function updateObjectsInFrame(object, curves, tObjects, fragments)
{
  if(object == thisWorldObject)
  {
    return
  }

  let curIndex = -1
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))

  for(let i = 0; i < tObjects.length; i += 1)
  {
    if(object == tObjects[i])
    {
      curIndex = i

      break
    }
  }

  let curveMovementPoints = []
  let isEntered = false
  let updated = false

  console.log(curIndex + " " + prefix + " " + anotherWorldMaxPresent + " " + anotherWorldFocusIndex);

  //obtain the curve movements points that close to the mid point
  for(let i = 0; i < curves.length; i += 1)
  {
    //check if this is the last element
    if (fragments[prefix + i].ballisticMovementNext != null || curveMovementPoints.length > 0 && fragments[prefix + i].ballisticMovementNext == null)
    {
      curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
        fragments[prefix + i].startPosition.x,
        fragments[prefix + i].startPosition.y,
        fragments[prefix + i].startPosition.z
      )
    }

    if (Math.min(curves.length - 1, curIndex) == i && curveMovementPoints.length > 0)
    {
      isEntered = true
    }

    if (fragments[prefix + i].ballisticMovementNext == null && curveMovementPoints.length > 0)
    {
      if (isEntered)
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + i].endPosition.x,
          fragments[prefix + i].endPosition.y,
          fragments[prefix + i].endPosition.z
        )
      }
      else
      {
        curveMovementPoints = []
      }
    }
  }

  if(0 <= prefix + curIndex - 1)
  {
    fragments[prefix + curIndex - 1].endPosition.x = object.position.x
    fragments[prefix + curIndex - 1].endPosition.y = object.position.y
    fragments[prefix + curIndex - 1].endPosition.z = object.position.z

    fragments[prefix + curIndex - 1].endEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex - 1].endEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex - 1].endEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex - 1].endScale.x = object.scale.x
    fragments[prefix + curIndex - 1].endScale.y = object.scale.y
    fragments[prefix + curIndex - 1].endScale.z = object.scale.z

    fragments[prefix + curIndex - 1].endAlpha.x = object.scale.x
    fragments[prefix + curIndex - 1].endAlpha.y = object.scale.y
    fragments[prefix + curIndex - 1].endAlpha.z = object.scale.z

    //pre
    if(0 <= curIndex - 1)
    {
      let curve
      let curPoints

      if(fragments[prefix + curIndex - 1].movementType != "Ballist")
      {
        curve = new THREE.LineCurve3(tObjects[curIndex - 1].position, object.position)
        curPoints = curve.getPoints( 50 );
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
        curPoints = curve.getPoints((curveMovementPoints.length - 1) * 50)

        updated = true
      }

      curves[curIndex - 1].geometry.attributes.position.count = curPoints.length
      curves[curIndex - 1].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex - 1].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex - 1].geometry.attributes.position.needsUpdate = true
    }
  }

  //next
  //prevent curve movement double update
  if (prefix + curIndex < fragments.length || curveMovementPoints.length > 0 && !updated)
  {
    fragments[prefix + curIndex].startPosition.x = object.position.x
    fragments[prefix + curIndex].startPosition.y = object.position.y
    fragments[prefix + curIndex].startPosition.z = object.position.z

    fragments[prefix + curIndex].startEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex].startEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex].startEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex].startScale.x = object.scale.x
    fragments[prefix + curIndex].startScale.y = object.scale.y
    fragments[prefix + curIndex].startScale.z = object.scale.z

    fragments[prefix + curIndex].startAlpha.x = object.scale.x
    fragments[prefix + curIndex].startAlpha.y = object.scale.y
    fragments[prefix + curIndex].startAlpha.z = object.scale.z

    if(curIndex < anotherWorldLines.length)
    {
      let curve
      let curPoints

      if(fragments[prefix + curIndex].movementType != "Ballist")
      {
        curve = new THREE.LineCurve3(object.position, tObjects[curIndex + 1].position)
        curPoints = curve.getPoints( 50 );
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
        curPoints = curve.getPoints((curveMovementPoints.length - 1) * 50)
      }

      curves[curIndex].geometry.attributes.position.count = curPoints.length
      curves[curIndex].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex].geometry.attributes.position.needsUpdate = true
    }
  }
}

/**
 * View a scene at specific frame, note that initAnimationLoop()
 * should be called first
 */
export function viewAnimationStatusAt(curFrame)
{
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if (animationFragments[i].startFrame <= curFrame && curFrame < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      for (let j = 0; i < animationFragments[i].callBefore.length; j += 1)
      {
        animationFragments[i].callBefore[i]()
      }

      if (curFrame == animationFragments[i].startFrame + animationFragments[i].duration - 1)
      {
        for (let j = 0; i < animationFragments[i].callBack.length; j += 1)
        {
          animationFragments[i].callBack[i]()
        }
      }

      setFragmentStatusAt(animationFragments[i], curFrame - animationFragments[i].startFrame)
    }
  }
}

export function EditStartFrameFrom(fragment, durationChangeValue)
{
  if (fragment == null)
  {
    return
  }

  EditStartFrameFrom(findNextFragment(fragment.object, fragment.startFrame + fragment.duration), durationChangeValue)
  fragment.startFrame += durationChangeValue
}

export function deleteFragmentIn(fragment, fragments)
{
  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i] == fragment)
    {
      fragments.splice(i, 1)

      break
    }
  }
}

export function editHelpText(mode)
{
  if(mode == "Standard")
  {
    systemHelp.text.innerText = StandardHelpText
  }
  else if(mode == "Revise")
  {
    systemHelp.text.innerText = ReviseHelpText
  }
  else if (mode == "Animation" || mode == "Animation (Root)")
  {
    systemHelp.text.innerText = AnimationHelpText
  }
}

/**
 * point : [in] Vector2 : NewKeyframe's location
 * curves : [in] Object3D Array : Lines / Easing curves
 * tObjects : [in/out] Object3D Array: Temperory objects
 * fragments : [in/out] AnimationFragments Json Array : fragments to insert a new fragment
 */
export function insertFrame(point, curves, tObjects, fragments)
{
  let min=Number.MAX_VALUE
  let index = -1
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))

  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].startFrame < worldFrame && worldFrame < fragments[i].startFrame + fragments[i].duration)
    {
      index = i + prefix

      break
    }
  }

  if(index != -1)
  {
    let intersects = new THREE.Vector3()

    raycaster.setFromCamera(point, camera)
    raycaster.ray.intersectPlane(plane, intersects)
    let point3D = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

    let newObject = cloneAll(thisWorldObject)
    newObject.position.set(point3D.x, point3D.y, point3D.z)
    newObject.material.visible = true
    newObject.material.opacity = 0.5
    newObject.material.transparent = true

    let newFragment =
    {
      "id": globalID++,
      "object": thisWorldObject,
      "objectInfo":
      {
        "name": fragments[index].objectInfo.name,
        "vertex": [],
        "posWeight": [],
        "rotationWeight": [],
        "scaleWeight": [],
        "alphaWeight": [],

        // "border":
        // {
        //   show = 1,
        //   color = {"x": 1, "y": 1, "z": 1},
        //   size = 0.05,
        //   borderTest = -1
        // }
      },

      "parentFragment": undefined,
      "childFragments": [],

      "startPosition": {"x": newObject.position.x, "y": newObject.position.y, "z": newObject.position.z},
      "endPosition": {"x": fragments[index].endPosition.x, "y": fragments[index].endPosition.y, "z": fragments[index].endPosition.z},
      "startEulerAngle": null,
      "endEulerAngle": {"x": fragments[index].endEulerAngle.x, "y": fragments[index].endEulerAngle.y, "z": fragments[index].endEulerAngle.z},
      "startScale": null,
      "endScale": {"x": fragments[index].endScale.x, "y": fragments[index].endScale.y, "z": fragments[index].endScale.z},
      "startAlpha": null,
      "endAlpha": fragments[index].endAlpha,

      "activeBefore": 1,
      "activeAfter": 1,

      "callBefore": [],
      "callBack": [],

      "startFrame": worldFrame,
      "duration": fragments[index].startFrame + fragments[index].duration - worldFrame,

      "posEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

      "movementType": fragments[index].movementType,
      "ballisticMovementNext": fragments[index].ballisticMovementNext,

      "rotationType": fragments[index].rotationType,
      "autoRotationNext": fragments[index].autoRotationNext
    }

    let insertWidth = (worldFrame - fragments[index].startFrame) / fragments[index].duration

    //update two closest frames' info
    if (fragments[index].ballisticMovementNext != undefined || fragments[index - 1] != null && fragments[index - 1].ballisticMovementNext != undefined)
    {
      fragments[index].ballisticMovementNext = newFragment
    }

    if (fragments[index].autoRotationNext != undefined || fragments[index - 1] != null && fragments[index - 1].autoRotationNext != undefined)
    {
      fragments[index].autoRotationNext = newFragment
    }

    let easeCurvesToUpdate = [fragments[index].posEasePoints, fragments[index].rotationEasePoints, fragments[index].scaleEasePoints, fragments[index].alphaEasePoints]
    let easeCurvesToChange = [newFragment.posEasePoints, newFragment.rotationEasePoints, newFragment.scaleEasePoints, newFragment.alphaEasePoints]

    for(let i = 0; i < 4; i += 1)
    {
      let curCurvePosition = []

      for(let j = 0; j < easeCurvesToUpdate[i].length; j += 1)
      {
        curCurvePosition[j] = new THREE.Vector3(easeCurvesToUpdate[i][j].x, easeCurvesToUpdate[i][j].y, 0)
      }

      let curve = new THREE.CatmullRomCurve3(curCurvePosition, false, "chordal")
      let insertHeight = curve.getPointAt(insertWidth).y

      if (i == 1)
      {
        fragments[index].endEulerAngle = lerp3(fragments[index].startEulerAngle, fragments[index].endEulerAngle, insertHeight)
        newFragment.startEulerAngle = lerp3(fragments[index].startEulerAngle, fragments[index].endEulerAngle, insertHeight)
      }
      else if(i == 2)
      {
        fragments[index].endScale = lerp3(fragments[index].startScale, fragments[index].endScale, insertHeight)
        newFragment.startScale = lerp3(fragments[index].startScale, fragments[index].endScale, insertHeight)
      }
      else if (i == 3)
      {
        fragments[index].endAlpha = lerp1(fragments[index].startAlpha, fragments[index].endAlpha, insertHeight)
        newFragment.startAlpha = lerp1(fragments[index].startAlpha, fragments[index].endAlpha, insertHeight)
      }

      let leftCurve = []
      let rightCurve = []

      for (var j = 0; j < easeCurvesToUpdate[i].length; j++)
      {
        if (easeCurvesToUpdate[i][j].x < insertWidth)
        {
          leftCurve[leftCurve.length] = easeCurvesToUpdate[i][j]
        }
        else if (easeCurvesToUpdate[i][j].x > insertWidth)
        {
          rightCurve[rightCurve.length] = easeCurvesToUpdate[i][j]
        }
      }

      leftCurve[leftCurve.length] = {"x": insertWidth, "y": insertHeight}
      rightCurve.splice(0, 0, {"x": insertWidth, "y": insertHeight});

      //check validibility
      for (let j = 0; j < leftCurve.length; j += 1)
      {
        if (leftCurve[j].y < 0 || insertHeight < leftCurve[j].y)
        {
          deleteObject(newObject)

          return "Invalid fragment because an irregular interpolation curve"
        }
      }

      for (let j = 0; j < rightCurve.length; j += 1)
      {
        if (rightCurve[j].y < insertHeight || 1 < rightCurve[j].y)
        {
          deleteObject(newObject)

          return "Invalid fragment because an irregular interpolation curve"
        }
      }

      //normalize left
      for (var j = 0; j < leftCurve.length; j += 1)
      {
        leftCurve[j].x = Math.round(leftCurve[j].x / insertWidth * 1000) / 1000
        leftCurve[j].y = Math.round(leftCurve[j].y / insertHeight * 1000) / 1000
      }

      //normalize right
      for (var j = 0; j < rightCurve.length; j += 1)
      {
        rightCurve[j].x = Math.round((rightCurve[j].x - insertWidth) / (1 - insertWidth) * 1000) / 1000
        rightCurve[j].y = Math.round((rightCurve[j].y - insertHeight) / (1 - insertHeight) * 1000) / 1000
      }

      easeCurvesToUpdate[i] = leftCurve
      easeCurvesToChange[i] = rightCurve
    }

    //update left curve Object
    //if curve movement
    if (fragments[index].ballisticMovementNext != undefined || fragments[index - 1] != null && fragments[index - 1].ballisticMovementNext != undefined)
    {
      let curveMovementPoints = []

      //record current frame's position
      for (let j = 0; j < curves.length; j += 1)
      {
        if (curves[j] == curves[index - prefix])
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            fragments[prefix + j].startPosition.x,
            fragments[prefix + j].startPosition.y,
            fragments[prefix + j].startPosition.z
          )
        }

        //mid
        if (j == index - prefix)
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            newFragment.startPosition.x,
            newFragment.startPosition.y,
            newFragment.startPosition.z
          )
        }

        //not or last
        if (curveMovementPoints.length > 0 && curves[j] != curves[index - prefix] || j == curves.length - 1)
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            fragments[prefix + j].endPosition.x,
            fragments[prefix + j].endPosition.y,
            fragments[prefix + j].endPosition.z
          )

          break
        }
      }

      let curCurve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
      let curPoints = curCurve.getPoints((curveMovementPoints.length - 1) * 50)

      curves[index - prefix].geometry.attributes.position.count = curPoints.length
      curves[index - prefix].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[index - prefix].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[index - prefix].geometry.attributes.position.needsUpdate = true

      curves.splice(index - prefix + 1, 0, curves[index - prefix])
    }
    //normal line movement
    else
    {
      let curCurve = new THREE.LineCurve3(tObjects[index - prefix].position, newObject.position)
      let curPoints = curCurve.getPoints(50)

      curves[index - prefix].geometry.attributes.position.count = curPoints.length
      curves[index - prefix].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[index - prefix].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[index - prefix].geometry.attributes.position.needsUpdate = true

      let newCurve = new THREE.LineCurve3(newObject.position, new THREE.Vector3(newFragment.endPosition.x, newFragment.endPosition.y, newFragment.endPosition.z))
      let points = newCurve.getPoints( 50 );
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      scene.add(curveObject)
      curves.splice(index - prefix + 1, 0, curveObject)
    }

    //update basic info
    fragments[index].endPosition.x = newObject.position.x
    fragments[index].endPosition.y = newObject.position.y
    fragments[index].endPosition.z = newObject.position.z
    fragments[index].duration = worldFrame - fragments[index].startFrame

    anotherWorldMaxPresent += 2

    if (index < anotherWorldFocusIndex)
    {
        anotherWorldMaxPresent += 1
    }

    //insert new info
    tObjects.splice(index - prefix + 1, 0, newObject)
    fragments.splice(index + 1, 0, newFragment)
    scene.add(newObject)
    animationFragments[animationFragments.length] = newFragment

    return "OK"
  }

  return "No fragment have found for this object"
}

export function deleteFrame(object, curves, tObjects, fragments)
{
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))
  let curIndex = -1

  let curveMovementStartIndex = -1

  for (let i = 0; i < tObjects.length; i += 1)
  {
    if(object == tObjects[i])
    {
      curIndex = i + prefix
      break
    }
  }

  //first object
  if(curIndex - prefix == 0)
  {
    if (fragments[curIndex].ballisticMovementNext != null)
    {
      curveMovementStartIndex = 0
    }
    else
    {
      deleteObject(curves[0])
    }

    //if current fragment is this fragment, change to the
    //next one
    if (fragments.length > 1 && fragments[curIndex] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex + 1].id
    }
    else
    {
      objectStatus[thisWorldObject.id].curID = -1
    }

    deleteObject(tObjects[0])
    deleteFragmentIn(fragments[curIndex], animationFragments)

    tObjects.splice(0, 1)
    curves.splice(0, 1)
    fragments.splice(curIndex, 1)
  }
  //last object
  else if (curIndex - prefix == tObjects.length - 1)
  {
    if (fragments[curIndex - 2] != null && fragments[curIndex - 2].ballisticMovementNext != null)
    {
      fragments[curIndex - 2].ballisticMovementNext = null

      curveMovementStartIndex = curIndex - 2
    }
    else
    {
      deleteObject(curves[curIndex - prefix - 1])
    }

    //if current fragment is this fragment, change to the
    //previous one
    if (fragments[curIndex - 2] != null && fragments[curIndex - 1] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex - 2].id
    }
    else
    {
      objectStatus[thisWorldObject.id].curID = -1
    }

    deleteObject(tObjects[curIndex - prefix])
    deleteFragmentIn(fragments[curIndex - 1], animationFragments)

    tObjects.splice(curIndex - prefix, 1)
    curves.splice(curIndex - prefix - 1, 1)
    fragments.splice(curIndex - 1, 1)
  }
  //mid
  else
  {
    if (fragments[curIndex - 1].ballisticMovementNext != null)
    {
      fragments[curIndex - 1].ballisticMovementNext = fragments[curIndex + 1]
      fragments[curIndex - 1].autoRotationNext = fragments[curIndex + 1]

      curveMovementStartIndex = curIndex - 1
    }
    //line reconnection
    else
    {
      let curCurve = new THREE.LineCurve3(tObjects[curIndex - prefix - 1].position, tObjects[curIndex - prefix + 1].position)
      let curPoints = curCurve.getPoints(50)

      curves[curIndex - prefix - 1].geometry.attributes.position.count = curPoints.length
      curves[curIndex - prefix - 1].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex - prefix - 1].geometry.attributes.position.needsUpdate = true

      deleteObject(curves[curIndex - prefix])
    }

    //adjust easing
    let easeCurvesToUpdate = [fragments[curIndex - 1].posEasePoints, fragments[curIndex - 1].rotationEasePoints, fragments[curIndex - 1].scaleEasePoints, fragments[curIndex - 1].alphaEasePoints]
    let easeCurvesToDelete = [fragments[curIndex].posEasePoints, fragments[curIndex].rotationEasePoints, fragments[curIndex].scaleEasePoints, fragments[curIndex].alphaEasePoints]
    let insertWidth = fragments[curIndex].duration / (fragments[curIndex].duration + fragments[curIndex - 1].duration)

    for(let i = 0; i < 4; i += 1)
    {
      for (let j = 0; j < easeCurvesToUpdate[i].length; j += 1)
      {
        easeCurvesToUpdate[i][j].x = lerp1(0, insertWidth, easeCurvesToUpdate[i][j].x)
        easeCurvesToUpdate[i][j].y = lerp1(0, insertWidth, easeCurvesToUpdate[i][j].y)
      }

      for (let j = 1; j < easeCurvesToDelete[i].length; j += 1)
      {
        easeCurvesToUpdate[i][easeCurvesToUpdate[i].length] =
        {
          "x": lerp1(insertWidth, 1, easeCurvesToDelete[i][j].x),
          "y": lerp1(insertWidth, 1, easeCurvesToDelete[i][j].y)
        }
      }
    }

    fragments[curIndex - 1].endPosition = fragments[curIndex].endPosition
    fragments[curIndex - 1].endEulerAngle = fragments[curIndex].endEulerAngle
    fragments[curIndex - 1].endScale = fragments[curIndex].endScale
    fragments[curIndex - 1].endAlpha = fragments[curIndex].endAlpha
    fragments[curIndex - 1].duration += fragments[curIndex].duration

    //if current fragment is this fragment, change to the
    //previous one
    if (fragments[curIndex] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex - 1].id
    }

    deleteObject(tObjects[curIndex - prefix])
    deleteFragmentIn(fragments[curIndex], animationFragments)

    tObjects.splice(curIndex - prefix, 1)
    curves.splice(curIndex - prefix, 1)
    fragments.splice(curIndex, 1)
  }

  //clear final object if nothing left
  if (fragments.length == 0)
  {
    deleteObject(tObjects[0])
  }

  //handle curve movement changes
  if (curveMovementStartIndex != -1)
  {
    let curveMovementPoints = []

    //record current frame's position
    for (let j = 0; j < curves.length; j += 1)
    {
      if (curves[j] == curves[curveMovementStartIndex])
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + j].startPosition.x,
          fragments[prefix + j].startPosition.y,
          fragments[prefix + j].startPosition.z
        )
      }

      //not or last
      if (curveMovementPoints.length > 0 && curves[j] != curves[curveMovementStartIndex] || j == curves.length - 1)
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + j].endPosition.x,
          fragments[prefix + j].endPosition.y,
          fragments[prefix + j].endPosition.z
        )

        break
      }
    }

    let curCurve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
    let curPoints = curCurve.getPoints((curveMovementPoints.length - 1) * 50)

    curves[curveMovementStartIndex].geometry.attributes.position.count = curPoints.length
    curves[curveMovementStartIndex].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

    for(let i = 0; i < curPoints.length; i += 1)
    {
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3] = curPoints[i].x
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
    }

    curves[curveMovementStartIndex].geometry.attributes.position.needsUpdate = true
  }
}

export function traverseAnimationFragments(from, to)
{
  let sequenceFragments = [from]
  let cur = findPreviousFragment(from.object, from.startFrame)

  while (cur != null)
  {
    sequenceFragments[sequenceFragments.length] = cur

    if (cur == to)
    {
      return sequenceFragments
    }

    cur = findPreviousFragment(cur.object, cur.startFrame)
  }

  sequenceFragments = [from]
  cur = findNextFragment(from.object, from.startFrame + from.duration)

  while (cur != null)
  {
    sequenceFragments[sequenceFragments.length] = cur

    if (cur == to)
    {
      console.log(sequenceFragments);
      return sequenceFragments
    }

    cur = findNextFragment(cur.object, cur.startFrame + from.duration)
  }

  return [from]
}

export function setRotationMode(fragments, mode)
{
  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].rotationType == mode)
    {
      fragments[i].rotationType = "Normal"
      fragments[i].autoRotationNext = undefined
    }
    else
    {
      fragments[i].rotationType = mode
      fragments[i].autoRotationNext = fragments[i + 1]
    }
  }
}

export function generateAutoRotationCurve(fragments)
{

  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].ballisticMovementNext != null)
    {
      fragments[i].autoRotationCurve = fragments[i].movementCurve
    }
    else
    {
      let startPosition = new THREE.Vector3(
        fragments[i].startPosition.x,
        fragments[i].startPosition.y,
        fragments[i].startPosition.z
      )
      let endPosition = new THREE.Vector3(
        fragments[i].endPosition.x,
        fragments[i].endPosition.y,
        fragments[i].endPosition.z
      )

      let curve = new THREE.LineCurve3(startPosition, endPosition)

      fragments[i].autoRotationCurve = curve
    }
  }

}

export function animation_objectBaseRotationCurve(curFragment)
{

}

export function animation_vertexBaseRotationCurve(curFragment)
{

}

export var sequences = []
export var variables = []
export var curSequenceName
export var curMacroName
export var curAnimationFragment

export var commandAnimating = false

export function circle(x, y, z, r)
{
    let obj = newSphere(x, y, z, r, null, new THREE.Color(0, 1, 1))
    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }

    scene.add(obj)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        circle(x,y,z,r)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        circle(x,y,z,r)
      }
    }
}
//rect [<x> <y> [z]] [<w> <h>]
export function rect(x, y, z, w, h, l)
{
  let obj = newCube(x, y, z, w, h, l, null, new THREE.Color(0, 1, 1))

  objectInfo[objectInfo.length] =
  {
    "object": obj,
    "id": globalID++
  }

  scene.add(obj)

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      rect(x, y, z, w, h, l)
    }
  }

  if (!commandAnimating && curAnimationFragment != null)
  {
    curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
    {
      rect(x, y, z, w, h, l)
    }
  }
}
//line [<x> <y> [z]] [length]
export function line(x, y, z, length)
{
    let v1 = new THREE.Vector3(x + -length / 2, y + -length / 2, z + -length / 2)
    let v2 = new THREE.Vector3(x + length / 2, y + length / 2, z + length / 2)

    let obj = newLine(v1, v2, new THREE.Color(0, 0, 0))

    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }

    scene.add(obj)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        line(x, y, z, length)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        line(x, y, z, length)
      }
    }
}
//line [<x> <y> [z] / <object_id>] [<x> <y> [z] / <object_id>] creates a new line between given objects or points
export function line2(x,y,z,x2,y2,z2)
{
    let v1=new THREE.Vector3(x,y,z)
    let v2=new THREE.Vector3(x2,y2,z2)
    let obj = newLine(v1,v2,new THREE.Color(0, 0, 0))
    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }
    scene.add(obj)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        line2(x,y,z,x2,y2,z2)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        line2(x,y,z,x2,y2,z2)
      }
    }
}
//empty [<x> <y> [z]] creates an "empty object" at the given position for use with other objects (like creating lines to the empty, etc.)
export function empty(x,y,z)
{
    let a = new THREE.Group()
    a.position.set(x,y,z)
    scene.add(a)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        empty(x,y,z)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        empty(x,y,z)
      }
    }
}
//group <object_id> <object_id> ... groups infinitely many objects together; this creates a unique object_id for the group; grouping groups together ungroups previous groups and creates a union of the groups; individual objects can still be manipulated by invoking their ID
export function group(ids){
     var group = new THREE.Group();
     objectInfo[objectInfo.length] =
     {
          "object": group,
          "id": globalID++
     }
     for (let i = 0; i < ids.length; i += 1)
        {
          for (var j = 0; j < objectInfo.length; j += 1)
          {
            if (objectInfo[j].id == ids[i])
            {
              group.add(objectInfo[j].object)
            }
          }
     }

     scene.add(group)

     if (!commandAnimating && curSequenceName != null && curMacroName != null)
     {
       sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
       {
         group(ids)
       }
     }

     if (!commandAnimating && curAnimationFragment != null)
     {
       curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
       {
         group(ids)
       }
     }
}
//ungroup <object_id> alias for delete <object_id>
export function ungroup(object_id){
    for (var i = 0; i < objectInfo.length; i++)
    {
        if (objectInfo[i].id == id)
        {
          deleteObject(objectInfo[i].object)
          objectInfo.splice(i, 1)
        }
    }
    scene.add(group)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        ungroup(object_id)
      }
    }

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        ungroup(object_id)
      }
    }
}
//Altering objects
//Altering objects
//position <object_id> <x> <y> [z]
export function position(id,x,y,z){
    data(id).position.set(x,y,z)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        position(id,x,y,z)
      }
    }
}
//size <object_id> <w> <h>
export function size(id,w,h){
    data(id).scale.set(w,h,data(id).position.scale.z)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        size(id,w,h)
      }
    }
}
//scale <object_id> <scale>
export function scale(id,sc){
    data(id).scale.set(sc,sc,sc)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        scale(id,sc)
      }
    }
}
//scale <object_id> [x_scale] [y_scale] [z_scale]
export function scale2(id,x_scale,y_scale,z_scale){
    data(id).scale.set(x_scale,y_scale,z_scale)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        scale2(id,x_scale,y_scale,z_scale)
      }
    }
}
//rotation <object_id> <degrees_or_radians> <vector> rotates the object about the given vector
export function rotation(id,dor,v)
{
  let curObject = data(id)

  let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(v.x, v.y, v.z), toRadians(dor));
  let q2 = new THREE.Quaternion().copy(curObject.quaternion);
  curObject.quaternion.copy(q).multiply(q2);

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      rotation(id,dor,v)
    }
  }
}
//rotation <object_id> <degrees_or_radians> <x/y/z> rotates the object about the chosen axis (x, y, or z)
export function rotation2(id,dor,w)
{
  let curObject = data(id)

  if (w == "x")
  {
    curObject.rotateX(toRadians(x))
  }
  else if (w == "y")
  {
    curObject.rotateY(toRadians(y))
  }
  else
  {
    curObject.rotateZ(toRadians(z))
  }

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      rotation2(id,x,y,z)
    }
  }
}
//fill <object_id> <alpha-hex>
export function fill(id,alh){
    data(id).material.color = new THREE.Color(alh)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        fill(id,alh)
      }
    }
}
//fill <object_id> <r> <g> <b> [a]
export function fill2(id,r,g,b,a){
    data(id).material.color=new THREE.Color(r,g,b)
    data(id).material.opacity = a

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        fill2(id,r,g,b,a)
      }
    }
}
//border <object_id> <width> <alpha-hex>
export function border(id,w,h){
   var t=h.toString();
   var r=parseInt(t.substring(1,3),16)
   var g=parseInt(t.substring(3,5),16)
   var b=parseInt(t.substring(5,7),16)
   r=r/255
   g=g/255
   b=b/255
   addBorder(data(id),r,g,b,1,w)

   if (!commandAnimating && curSequenceName != null && curMacroName != null)
   {
     sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
     {
       border(id,w,h)
     }
   }
}
//border <object_id> <width> <r> <g> <b> [a]
export function border2(object,w,r,g,b,a){
    addBorder(object,r,g,b,a,w)

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        border2(object,w,r,g,b,a)
      }
    }
}
//reset <object_id> [keyframe_id] resets the object to its inital state or to its state at given keyframe
export function reset(objID, keyframeID)
{
    let curObject = data(objID)
    let targetFragment = null

    if(id2 != null)
    {
      targetFragment = findFragment(keyframeID)
    }
    else
    {
      let cur = findObjectCurFragment(curObject, 9999999)

      while (true)
      {
        let pre = findObjectCurFragment(curObject, cur.startFrame)

        if (pre == null)
        {
          break
        }

        cur = pre
      }

      targetFragment = cur
    }

    curObject[i].position.x = curFragment.startPosition.x
    curObject[i].position.y = curFragment.startPosition.y
    curObject[i].position.z = curFragment.startPosition.z

    curObject[i].rotation.x = curFragment.startEulerAngle.x
    curObject[i].rotation.y = curFragment.startEulerAngle.y
    curObject[i].rotation.z = curFragment.startEulerAngle.z

    curObject[i].scale.x = curFragment.startScale.x
    curObject[i].scale.y = curFragment.startScale.y
    curObject[i].scale.z = curFragment.startScale.z

    curObject[i].material.opacity = curFragment.startAlpha

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        reset(objID, keyframeID)
      }
    }
}
//hide <object_id> hides the object completely, indepedent of alpha values
export function hide(id){
    data[i].visible=false

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        hide(id)
      }
    }
}
//show <object_id> shows the object again
export function show(id){
    data[i].visible=true

    if (!commandAnimating && curSequenceName != null && curMacroName != null)
    {
      sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
      {
        show(id)
      }
    }
}

//Animating
//keyframe create creates a new keyframe and adds all objects and their states to it
export function createKeyFrame()
{
  for (var i = 0; i < objectInfo.length; i++)
  {
    if (animationBuffer[objectInfo[i].object.id] == null)
    {
      let start = recordStartFrame(objectInfo[i].object)
      worldFrame += 1
      let end = recordEndFrame(objectInfo[i].object)

      recordAnimationFrame(start, end)
      recordFrame(objectInfo[i].object)
    }
    else
    {
      let curFrame = recordStartFrame(objectInfo[i].object)
      animationBuffer[objectInfo[i].object.id].frame = worldFrame

      recordAnimationFrame(animationBuffer[objectInfo[i].object.id], curFrame)
      animationBuffer[objectInfo[i].object.id] = curFrame
    }
  }

  curAnimationFragment = animationFragments[animationFragments.length - 1]

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = createKeyFrame
  }
}
//keyframe duration <keyframe_id> <duration> sets the keyframe's duration in milliseconds
export function setKeyframeDuration(id, duration)
{
  let curFragment = data(id)
  curFragment.duration = duration
  worldFrame = curFragment.startFrame + duration

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      setKeyframeDuration(id, duration)
    }
  }
}

//keyframe clear <keyframe_id> clears the given keyframe of all data except its duration and position in the sequence (essentially reverts it to an empty keyframe, a copy of the previous one)
export function clearKeyFrame(id)
{
  deleteContent(id)

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      clearKeyFrame(id)
    }
  }
}
//keyframe goto <keyframe_id> sets the visual state of the animation to the given keyframe's state
export function goToKeyFrame(id)
{
  worldFrame = data(id).startFrame
  enterAnimationMode()
}

//keyframe return effectively a macro for keyframe goto for the last created keyframe
export function returnKeyframe()
{
  let lastKeyFrameDuration = 0

  for (var i = 0; i < animationFragments.length; i++)
  {
    if(lastKeyFrameDuration < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      lastKeyFrameDuration = animationFragments[i].startFrame + animationFragments[i].duration
    }
  }

  worldFrame = lastKeyFrameDuration
  enterAnimationMode()
}
//keyframe preview <keyframe_id> plays the animation from the keyframe preceding the given keyframe to the given keyframe
export function previewKeyframe(id)
{
  let lastKeyFrameDuration = 0
  let lastKeyFrameID = -1

  for (var i = 0; i < animationFragments.length; i++)
  {
    if(lastKeyFrameDuration < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      lastKeyFrameDuration = animationFragments[i].startFrame + animationFragments[i].duration
      lastKeyFrameID = animationFragments[i].id
    }
  }

  playKeyFrameBetween(id, lastKeyFrameID)
}

//keyframe preview <keyframe_id> <keyframe_id> plays the animations between the two keyframes
export function playKeyFrameBetween(id1, id2)
{
  let curFrame = data[id1].startFrame
  let frame = data[id2].startFrame

  worldFrame = curFrame
  enterAnimationMode()

  var loop = function()
  {
    if (curFrame == frame + 1)
    {
      commandAnimating = false

      return
    }

    viewAnimationStatusAt(curFrame)

    requestAnimationFrame(loop)
  };

  commandAnimating = true
  loop()
}
//macro record [name] starts recording a new macro; commands from this point on will be recorded and added to this macro
export function macroStartRecording(name)
{
  animationFragments = []
  objectInfo = []

  worldFrame = 0

  sequences[curSequenceName].macros[name] =
  {
    "id": globalID++,
    "name": name,
    "commands": [],
    "animationFragments": animationFragments,
    "objectInfo": objectInfo
  }

  curMacroName = name
}
//macro stop stops recording the macro
export function macroStopRecording()
{
  animationFragments = []
  objectInfo = []

  curMacroName = null
  curAnimationFragment = null
}
//macro [name_or_id] executes an existing macro; they can also be executed like a command by invoking their name
export function exec_enter_macro(name_or_id)
{
  let curMacro = null

  if (sequences[curSequenceName].macros[name_or_id] != null)
  {
    curMacro = sequences[curSequenceName].macros[name_or_id]
  }
  else
  {
    curMacro = data[name_or_id]
  }

  curMacroName = curMacro.name
  animationFragments = curMacro.animationFragments
  objectInfo = curMacro.objectInfo

  worldFrame = 0
  isAnimating = true
  commandAnimating = true

  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  animateAllFragments(animationFragments)
}
//sequence create [animation_name] creates a new animation; commands from this point on are part of the animation
export function createSequence(name)
{
  sequences[name] =
  {
    "id": globalID++,
    "name": name,
    "macros": {}
  }

  curSequenceName = name
}
//sequence duration <duration> sets the duration of the animation in milliseconds
export function setSequenceDuration(duration)
{
  let animationFragmentsCopy = []
  let curDuration = 0

  for (let macroKey in sequences[curSequenceName].macros)
  {
    animationFragmentsCopy = animationFragmentsCopy.concat(sequences[curSequenceName].macros[macroKey].animationFragments)
  }

  for (var i = 0; i < animationFragmentsCopy.length; i++)
  {
    curDuration += animationFragmentsCopy[i].duration
  }

  for (var i = 0; i < animationFragmentsCopy.length; i++)
  {
    animationFragmentsCopy[i].duration *= (duration / curDuration)
  }
}

//sequence play plays the animation from beginning to end
export function playSequence()
{
  animationFragments = []
  objectInfo = []

  for (let macroKey in sequences[curSequenceName].macros)
  {
    animationFragments = animationFragments.concat(sequences[curSequenceName].macros[macroKey].animationFragments)
    objectInfo = objectInfo.concat(sequences[curSequenceName].macros[macroKey].objectInfo)
  }

  worldFrame = 0
  isAnimating = true
  commandAnimating = true

  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  animateAllFragments(animationFragments)
}
//save export commands to text file available to download; this command is never recorded
export function save()
{

}

//unility
//variable create <name> <value> create a variable to be used by name in place of actual values from this point on
export function variableCreate(name, v)
{
  variables[name] =
  {
    "data": v,
    "id": globalID++
  }

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      variableCreate(name, v)
    }
  }
}
//variable set <name> <value> change the value of an existing variable
export function variableSet(name, v)
{
  if (variables[name] == null)
  {
    return
  }

  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      variableSet(name, v)
    }
  }

  variables[name].data = v
}
//data <id> returns a map of the given object/keyframe/macro/variable data to be used in variables or simply for display;
//values are by reference, but readonly
export function data(id)
{
  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      data(id)
    }
  }

  for (let sequenceKey in sequences)
  {
    for (let macroKey in sequences[sequenceKey].macros)
    {
      if(sequences[sequenceKey].macros[macroKey].id == id)
      {
        return sequences[sequenceKey].macros[macroKey]
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].objectInfo.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].objectInfo[i].id == id)
        {
          return sequences[sequenceKey].macros[macroKey].objectInfo[i].object
        }
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].animationFragments.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].animationFragments[i].id == id)
        {
          return sequences[sequenceKey].macros[macroKey].animationFragments[i]
        }
      }
    }
  }

  for (let key in variables)
  {
    if(variables[key].id == id)
    {
      return variables[key]
    }
  }
}
//delete <object_id>
export function deleteContent(id)
{
  if (!commandAnimating && curSequenceName != null && curMacroName != null)
  {
    sequences[curSequenceName].macros[curMacroName].commands[sequences[curSequenceName].macros[curMacroName].commands.length] = function()
    {
      deleteContent(id)
    }
  }

  for (let sequenceKey in sequences)
  {
    for (let macroKey in sequences[sequenceKey].macros)
    {
      if(sequences[sequenceKey].macros[macroKey].id == id)
      {
        sequences[sequenceKey].macros[macroKey] = undefined

        return
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].objectInfo.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].objectInfo[i].id == id)
        {
          deleteObject(sequences[sequenceKey].macros[macroKey].objectInfo[i].object)
          sequences[sequenceKey].macros[macroKey].objectInfo.splice(i, 1)

          return
        }
      }

      for (var i = 0; i < sequences[sequenceKey].macros[macroKey].animationFragments.length; i++)
      {
        if(sequences[sequenceKey].macros[macroKey].animationFragments[i].id == id)
        {
          sequences[sequenceKey].macros[macroKey].animationFragments.splice(i, 1)

          return
        }
      }
    }
  }

  for (let key in variables)
  {
    if(variables[key].id == id)
    {
      variables[key] = undefined

      return
    }
  }
}
//help lists all existing commands and short descriptions; this command is never recorded
export function help()
{
  //TODO
}
//man <command> our equivalent of man pages for our commands; this command is never recorded
export function man()
{
  //TODO
}



main()
