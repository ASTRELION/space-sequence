import React from 'react';
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <footer>
      <div className="py-12 md:py-16">
        <div className="max-w-6xl mx-auto px-4 sm:px-6">

          {/* Top area: Blocks */}
          <div className="grid md:grid-cols-12 gap-8 lg:gap-20 mb-8 md:mb-12">

            {/* 1st block */}
         <div className="md:col-span-4 lg:col-span-5">
              <div className="mb-2">
              <Link to="/" className="block" aria-label="Cruip">
            <img
            src={require('../images/icon_wbg.jpg')}
            alt="Open"
            width={120}
            height={100} />
            </Link>
              </div>

            </div>

            {/* 2nd, 3rd and 4th blocks */}
            <div className="md:col-span-8 lg:col-span-7 grid sm:grid-cols-3 gap-8">

              {/* 3rd block */}
              <div className="text-sm">
                <h6 className="text-gray-200 font-medium mb-1">Resources</h6>
                <ul>
                  <li className="mb-1">
                    <a href="" className="text-gray-400 hover:text-gray-100 transition duration-150 ease-in-out">Git Project Repository</a>
                  </li>
                  <li className="mb-1">
                    <a href="" className="text-gray-400 hover:text-gray-100 transition duration-150 ease-in-out">Project Site</a>
                  </li>
                </ul>
              </div>
            </div>

          </div>

          {/* Bottom area */}
          <div className="md:flex md:items-center md:justify-between">

             Social links
            <ul className="flex mb-4 md:order-1 md:ml-4 md:mb-0">
            <li className="ml-4">
                  <a href="">

                </a>
              </li>
            </ul>

            {/* Copyrights note */}
            <div className="text-gray-400 text-sm mr-4">&copy; 2020 Open PRO. All rights reserved.</div>

          </div>

        </div>
      </div>
    </footer>
  );
}

export default Footer;
