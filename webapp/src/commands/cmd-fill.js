import * as Command from "../animation/Compound.js"
import ParseArgument from "./util.js"

export default function makeCommand(program)
{
    program
        .command("fill <object> <r> <g> <b> <a>")
        .description("Fill an object with a color")
        .action((obj, r, g, b, a) => {
            Command.fill2(ParseArgument(obj, "id"), r, g, b, a);
        })
}
