import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("sequence <subcmd> [arg1]")
        .description("Sequence command")
        .action((subCmd, arg1) => {
            switch (subCmd.toLowerCase())
            {
                case "create":
                    Command.createSequence(arg1);
                    break;

                case "play":
                    Command.playSequence(arg1);
                    break;

                case "duration":
                    Command.setSequenceDuration(arg1);
                    break;
            }
        })
}
