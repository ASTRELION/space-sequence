import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("reset <object> [keyframe]")
        .description("Reset an object to its initial state or a keyframes state")
        .action((obj, keyframe) => {
            Command.reset(obj, keyframe);
        })
}