import * as Command from "../animation/Compound.js"
import ParseArgument from "./util.js"

export default function makeCommand(program)
{
    program
        .command("line [x1] [y1] [z1] [x2] [y2] [z2]")
        .description("Create a line between two points")
        .action((x1, y1, z1, x2, y2, z2) => {
            Command.line2(
              parseFloat(ParseArgument(x1, "x")),
              parseFloat(ParseArgument(y1, "y")),
              parseFloat(ParseArgument(z1, "z")),
              parseFloat(ParseArgument(x2, "x")),
              parseFloat(ParseArgument(y2, "y")),
              parseFloat(ParseArgument(z2, "z"))
            );
        })
}
