import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("ungroup <object>")
        .description("Ungroup an existing group")
        .action((obj) => {
            Command.ungroup(obj);
        })
}