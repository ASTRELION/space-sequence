import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("data <object>")
        .description("Returns the data of an object")
        .action((obj) => {
            Command.data(obj);
        })
}