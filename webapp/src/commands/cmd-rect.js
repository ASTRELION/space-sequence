import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("rect [x] [y] [z] [w] [h] [l]")
        .description("Create a new rectangle object")
        .action((x, y, z, w, h, l) => {
            Command.rect(x, y, z, w, h, l);
        })
}