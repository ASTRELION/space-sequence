import * as Command from "../animation/Compound.js"

/*
Parses given `arg` as `param`.

$ references the last created object.
ParseArgument("$+5", "x") returns the last created object's x value + 5.
ParseArgument("$2+5", "x") returns the 2nd to last created object's x value + 5.
ParseArgument("$") returns the last created object
ParseArgument("$2") returns the second to last object created.
$1 is equivalent to $

& references the first created object.
ParseArgument("&+5") returns the first object's x value + 5.
*/
export default function ParseArgument(arg, param)
{
    arg = String(arg)
        .trim()
        .replace(" ", "")
        .toLowerCase();

    if (param)
    {
        param = String(param)
            .replace(/\s+/g, " ")
            .trim()
            .toLowerCase();
    }

    if (arg.startsWith("$")) // latest object
    {
        let numString = arg.substring(1, arg.length); // $2+5 -> 2+5
        return ParseRelativeEndArg(numString, param);

    }
    else if (arg.startsWith("&")) // earliest object
    {
        let numString = arg.substring(1, arg.length); // $2+5 -> 2+5
        return ParseRelativeStartArg(numString, param);
    }
    else if (arg.startsWith("#")) // latest frame
    {
        let numString = arg.substring(1, arg.length);
        return ParseRelativeFrameEndArg(numString, param);
    }
    else if (arg.startsWith("@")) // earliest frame
    {
        let numString = arg.substring(1, arg.length);
        return ParseRelativeFrameStartArg(numString, param);
    }
    else if (arg.startsWith("!")) // latest objectInfo
    {
        let numString = arg.substring(1, arg.length);
        return ParseRelativeObjectInfoEndArg(numString, param);
    }
    else if (arg.startsWith("~")) // earliest objectInfo
    {
        let numString = arg.substring(1, arg.length);
        return ParseRelativeObjectInfoStartArg(numString, param);
    }

    return arg;
}

// Finds object relative to the end of the object array
function ParseRelativeEndArg(numString, param)
{
    let obj = null

    if (numString.includes("+"))
    {
        let numArray = numString.split("+"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[Command.objectInfo.length - ParseIndex(numArray)].object.position;
        return param ? obj[param] + parseFloat(numArray[1]): obj;
    }
    else if (numString.includes("-"))
    {
        let numArray = numString.split("-"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[Command.objectInfo.length - ParseIndex(numArray)].object.position;
        return param ? obj[param] - parseFloat(numArray[1]): obj;
    }
    else if (numString.includes("*"))
    {
        let numArray = numString.split("*"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[Command.objectInfo.length - ParseIndex(numArray)].object.position;
        return param ? obj[param] * parseFloat(numArray[1]): obj;
    }
    else if (numString.includes("/"))
    {
        let numArray = numString.split("/"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[Command.objectInfo.length - ParseIndex(numArray)].object.position;
        return param ? obj[param] / parseFloat(numArray[1]): obj;
    }
    else
    {
      obj = Command.objectInfo[Command.objectInfo.length - (numString != "" ? parseInt(numString) : 1)].object.position;
      return param ? obj[param]: obj;
    }
}

// Finds object relative to the beginning of the object array
function ParseRelativeStartArg(numString, param)
{
    let obj = null;

    if (numString.includes("+"))
    {
        let numArray = numString.split("+"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[ParseIndex(numArray) - 1].object.position;
        return param ? obj[param] + parseFloat(numArray[1]): obj;
    }
    else if (numString.includes("-"))
    {
        let numArray = numString.split("-"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[ParseIndex(numArray) - 1].object.position;
        return param ? obj[param] - parseFloat(numArray[1]): obj;
    }
    else if (numString.includes("*"))
    {
        let numArray = numString.split("*"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[ParseIndex(numArray) - 1].object.position;
        return param ? obj[param] * parseFloat(numArray[1]): obj;
    }
    else if (numString.includes("/"))
    {
        let numArray = numString.split("/"); // 2+5 -> [2, 5]
        obj = Command.objectInfo[ParseIndex(numArray) - 1].object.position;
        return param ? obj[param] / parseFloat(numArray[1]): obj;
    }
    else
    {
      obj = Command.objectInfo[numString != "" ? parseInt(numString) : 0].object.position;
      return param ? obj[param]: obj;
    }
}

//-----------------------------------------------
//The parser of animation fragments

function ParseRelativeFrameEndArg(numString, param)
{
    let fragment = Command.animationFragments[Command.animationFragments.length - (numString != "" ? parseInt(numString) : 1)];
    return fragment[param];
}

function ParseRelativeFrameStartArg(numString, param)
{
    let fragment = Command.animationFragments[numString != "" ? parseInt(numString) : 0];
    return fragment[param];
}

function ParseRelativeObjectInfoEndArg(numString, param)
{
    let obj = Command.objectInfo[Command.objectInfo.length - (numString != "" ? parseInt(numString) : 1)];
    return obj[param];
}

function ParseRelativeObjectInfoStartArg(numString, param)
{
    let obj = Command.objectInfo[numString != "" ? parseInt(numString) : 0];
    return obj[param];
}

function ParseIndex(numArray)
{
    let dI = numArray[0] != "" ? numArray[0] : 1;

    if (dI < 1)
    {
        dI = 1;
    }

    return dI;
}
