import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("save")
        .description("Export command history to a file")
        .action(() => {
            Command.save();
        })
}