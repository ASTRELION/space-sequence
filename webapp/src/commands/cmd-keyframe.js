import { command } from "commander";
import * as Command from "../animation/Compound.js"
import ParseArgument from "./util.js"

export default function makeCommand(program)
{
    program
        .command("keyframe <subcmd> [arg1] [arg2]")
        .description("Keyframe command")
        .action((subCmd, arg1, arg2) => {
            switch(subCmd.toLowerCase())
            {
                case "create":
                    Command.createKeyFrame();
                    break;

                case "duration":
                    Command.setKeyframeDuration(ParseArgument(arg1, "id"), parseInt(arg2));
                    break;

                case "clear":
                    Command.clearKeyFrame(ParseArgument(arg1, "id"));
                    break;

                case "return":
                    Command.returnKeyframe();
                    break;

                case "goto":
                    Command.goToKeyFrame(ParseArgument(arg1, "id"));
                    break;

                case "preview":
                    Command.previewKeyframe(ParseArgument(parseInt(arg1)));
                    break;
            }
        })
}
