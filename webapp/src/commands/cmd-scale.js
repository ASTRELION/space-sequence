import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("scale <object> [dx] [dy] [dz]")
        .description("Scale an object by given deltas")
        .action((obj, dx, dy, dz) => {
            Command.scale2(obj, parseFloat(dx), parseFloat(dy), parseFloat(dz));
        })
}
