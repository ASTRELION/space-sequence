import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("group <objects...>")
        .description("Group objects together")
        .action((objs) => {
            Command.group(objs)
        })
}