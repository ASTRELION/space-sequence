import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("variable <subcmd> [arg1] [arg2]")
        .description("Variable command")
        .action((subCmd, arg1, arg2) => {
            switch (subCmd.toLowerCase())
            {
                case "create":
                    Command.variableCreate(arg1, arg2);
                    break;

                case "set":
                    Command.variableSet(arg1, arg2);
                    break;
            }
        })
}