import border from "./cmd-border.js"
import circle from "./cmd-circle.js"
import data from "./cmd-data.js"
import deletecmd from "./cmd-delete.js"
import empty from "./cmd-empty.js"
import fill from "./cmd-fill.js"
import group from "./cmd-group.js"
import hide from "./cmd-hide.js"
import keyframe from "./cmd-keyframe.js"
import line from "./cmd-line.js"
import macro from "./cmd-macro.js"
import position from "./cmd-position.js"
import rect from "./cmd-rect.js"
import reset from "./cmd-reset.js"
import rotation from "./cmd-rotation.js"
import save from "./cmd-save.js"
import scale from "./cmd-scale.js"
import sequence from "./cmd-sequence.js"
import show from "./cmd-show.js"
import size from "./cmd-size.js"
import template from "./cmd-template.js"
import ungroup from "./cmd-ungroup.js"
import variable from "./cmd-variable.js"
import light from "./cmd-light.js"
import { Command } from "commander"

const program = new Command();
// register commands
border(program);
circle(program);
data(program);
deletecmd(program);
empty(program);
fill(program);
group(program);
hide(program);
keyframe(program);
line(program);
macro(program);
position(program);
rect(program);
reset(program);
rotation(program);
save(program);
scale(program);
sequence(program);
show(program);
size(program);
template(program);
ungroup(program);
variable(program);

light(program);

export default function ParseCommand(commandString)
{
    // make input prettier
    let commandIssued = commandString
        .replace(/\s+/g, " ")
        .trim()
        .toLowerCase();
    console.log("Issued: " + commandIssued.split(" "));

    // parse & run
    program.parse(
        commandIssued.split(" "),
        { from: "user" }
    );
}
