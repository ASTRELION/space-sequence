import * as Command from "../animation/Compound.js"
import ParseArgument from "./util.js"

export default function makeCommand(program)
{
    program
        .command("border <object> <size> <r> <g> <b> <a> <borderTest>")
        .description("Adjust border of an object")
        .action((obj, size, r, g, b, a, borderTest) => {
            Command.border2(ParseArgument(obj, "id"), parseFloat(size), parseFloat(r), parseFloat(g), parseFloat(b), parseFloat(a), borderTest == "1" ? true : false);
        });
}
