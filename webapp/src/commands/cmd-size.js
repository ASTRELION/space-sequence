import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("size <object> <w> <h>")
        .description("Set the size of an object")
        .action((obj, w, h) => {
            Command.size(obj, w, h);
        })
}