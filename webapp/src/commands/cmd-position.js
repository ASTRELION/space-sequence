import * as Command from "../animation/Compound.js"
import ParseArgument from "./util.js"

export default function makeCommand(program)
{
    program
        .command("position <object> <x> <y> <z>")
        .description("Change the position of an object")
        .action((obj, x, y, z) => {
            Command.position(
              parseFloat(ParseArgument(obj, "id")),
              parseFloat(ParseArgument(x, "x")),
              parseFloat(ParseArgument(y, "y")),
              parseFloat(ParseArgument(z, "z"))
            );
        })
}
