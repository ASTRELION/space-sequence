import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("hide <object>")
        .description("Hide an object")
        .action((obj) => {
            Command.hide(obj);
        })
}