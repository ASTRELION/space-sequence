import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("empty [x] [y] [z]")
        .description("Create a new empty object")
        .action((x, y, z) => {
            Command.empty(x, y, z);
        })
}