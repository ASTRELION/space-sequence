import * as Command from "../animation/Compound.js"
import ParseArgument from "./util.js"

export default function makeCommand(program)
{
    program
        .command("delete <object> <delay>")
        .description("Delete an object or other data")
        .action((obj, delay) => {
            Command.deleteContent(ParseArgument(obj, "id"), parseInt(delay));
        })
}
