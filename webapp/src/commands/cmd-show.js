import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("show <object>")
        .description("Show an object (un-hide)")
        .action((obj) => {
            Command.show(obj);
        })
}