import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("macro <subcmd> [arg1]")
        .description("Macro command")
        .action((subCmd, arg1) => {
            switch (subCmd.toLowerCase())
            {
                case "record":
                    Command.macroStartRecording(arg1);
                    break;

                case "stop":
                    Command.macroStopRecording();
                    break;

                default:
                    Command.exec_enter_macro(subCmd)

            }
        })
}
