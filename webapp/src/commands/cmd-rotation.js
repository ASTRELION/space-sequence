import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("rotation <object> <angle> <vector>")
        .description("Rotate an object about the given line")
        .action((obj, angle, vector) => {
            Command.rotation(obj, angle, vector);
        })
}