import * as Command from "../animation/Compound.js"

export default function makeCommand(program)
{
    program
        .command("defaultlight")
        .description("Hide an object")
        .action(() => {
          Command.light.visible = !Command.light.visible
        })
}
