import * as Command from "../animation/Compound.js"
import ParseArgument from "./util.js"

export default function makeCommand(program)
{
    program
        .command("circle [x] [y] [z] [r]")
        .description("Create a new circle object")
        .action((x, y, z, r) => {
            console.log(ParseArgument(x, ""));

            Command.circle(
                parseFloat(ParseArgument(x, "x")),
                parseFloat(ParseArgument(y, "y")),
                parseFloat(ParseArgument(z, "z")),
                parseFloat(ParseArgument(r, "r"))
            );
        })
}
