import * as THREE from "./three.module.js"
import {NURBSCurve} from "./NURBSCurve.js"
import {SVGLoader} from "./SVGLoader.js"
import ParseCommand from '../commands/cmd';

/**
 * record the start fragment
 * record the whole fragment if the start has already existed
 */
export var animationBuffer = []

/**
 * the id for current operating fragment
 * (update when left/right arrow or create new whole fragment)
 */
export var objectStatus = []

export var uploadedObjectURL = []

export var onPage

export function initAnimationLoop(data)
{
  for(let i = 0; i < data.length; i += 1)
  {
    let positionPoints = []
    let rotationPoints = []
    let scalePoints = []
    let alphaPoints = []

    for(let j = 0; j < data[i].posEasePoints.length; j += 1)
    {
      positionPoints[j] = new THREE.Vector3(data[i].posEasePoints[j].x, data[i].posEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].rotationEasePoints.length; j += 1)
    {
      rotationPoints[j] = new THREE.Vector3(data[i].rotationEasePoints[j].x, data[i].rotationEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].scaleEasePoints.length; j += 1)
    {
      scalePoints[j] = new THREE.Vector3(data[i].scaleEasePoints[j].x, data[i].scaleEasePoints[j].y, 0)
    }

    for(let j = 0; j < data[i].alphaEasePoints.length; j += 1)
    {
      alphaPoints[j] = new THREE.Vector3(data[i].alphaEasePoints[j].x, data[i].alphaEasePoints[j].y, 0)
    }

    data[i].positionCurve = new THREE.CatmullRomCurve3(positionPoints, false, "chordal")
    data[i].rotationCurve = new THREE.CatmullRomCurve3(rotationPoints, false, "chordal")
    data[i].scaleCurve = new THREE.CatmullRomCurve3(scalePoints, false, "chordal")
    data[i].alphaCurve = new THREE.CatmullRomCurve3(alphaPoints, false, "chordal")

    data[i].object.rotation.x = toRadians(data[i].startEulerAngle.x)
    data[i].object.rotation.y = toRadians(data[i].startEulerAngle.y)
    data[i].object.rotation.z = toRadians(data[i].startEulerAngle.z)

    if(data[i].movementCurve == null)
    {
      let movementPoints = []
      let cur = data[i]

      while(true)
      {
        movementPoints[movementPoints.length] = new THREE.Vector3(cur.startPosition.x, cur.startPosition.y, cur.startPosition.z)

        if(cur.ballisticMovementNext != null)
        {
          cur = cur.ballisticMovementNext
        }
        else
        {
          movementPoints[movementPoints.length] = new THREE.Vector3(cur.endPosition.x, cur.endPosition.y, cur.endPosition.z)

          break
        }
      }

      let curve = new THREE.CatmullRomCurve3(movementPoints, false, "chordal")
      // let curve = new THREE.QuadraticBezierCurve3(movementPoints[0], movementPoints[1], movementPoints[2])
      cur = data[i]

      for(let j = 0; j < movementPoints.length - 1; j += 1)
      {
        cur.movementCurve = curve
        cur.movementCurveIndex = j
        cur.movementCurveLength = movementPoints.length - 1

        cur = cur.ballisticMovementNext
      }
    }

    if (data[i].rotationType == "Object" && data[i].autoRotationCurve == null)
    {
      let sequenceFragments = []
      let cur = data[i]

      while(cur != null)
      {
        sequenceFragments[sequenceFragments.length] = cur
        cur = cur.autoRotationNext
      }

      generateAutoRotationCurve(sequenceFragments)
    }
  }
}

export function animateAllFragments(data)
{
  let animationLength = data.length

  for(let i = 0; i < animationLength; i += 1)
  {
    if(data[i].activeBefore != 1)
    {
      setAlpha(0, data[i])
    }
  }

  systemMode.text.style.opacity = 0
  systemHelp.text.style.opacity = 0

  let finalAnimationTime = 9999999

  console.log(data);
  handler.core.visible = false

  var loop = function()
  {
    worldFrame += 1
    systemFrame.text.innerHTML = worldFrame

    if(worldFrame >= finalAnimationTime)
    {
      isAnimating = false
      commandAnimating = false

      return
    }

    for(let i = 0; i < data.length; i += 1)
    {
      if(data[i].animated == null && data[i].startFrame <= worldFrame)
      {
        animateNow(data[i])

        data[i].animated = true
        animationLength -= 1

        if(animationLength == 0)
        {
          finalAnimationTime = data[i].startFrame + data[i].duration
        }
      }
    }

    requestAnimationFrame(loop)
  }

  loop()
}

export function animateNow(fragment)
{
  let curFrame = 0

  for(let i = 0; i < fragment.childFragments.length; i += 1)
  {
    animateNow(fragment.childFragments[i])
  }

  for (let i = 0; i < fragment.callBefore.length; i += 1)
  {
    fragment.callBefore[i]()
  }

  var animationLoop = function()
  {
    if(curFrame == fragment.duration + 1)
    {
      if(fragment.activeAfter != 1)
      {
        setAlpha(0, fragment)
      }

      for (let i = 0; i < fragment.callBack.length; i += 1)
      {
        fragment.callBack[i]()
      }

      return
    }

    setFragmentStatusAt(fragment, curFrame)

    curFrame += 1

    requestAnimationFrame(animationLoop)
  };

  animationLoop()
}

export function setFragmentStatusAt(fragment, curFrame)
{
  if (fragment.duration == 0)
  {
    return
  }

  let curStartPortition = fragment.movementCurveIndex / fragment.movementCurveLength
  let curTotalPortition = 1 / fragment.movementCurveLength

  let curPosition = fragment.movementCurve.getPointAt(curStartPortition + curTotalPortition * clamp(fragment.positionCurve.getPointAt(curFrame / fragment.duration).y, 0, 1))
  let curScale = lerp3(fragment.startScale, fragment.endScale, fragment.scaleCurve.getPointAt(curFrame / fragment.duration).y)
  let curAlpha = lerp1(fragment.startAlpha, fragment.endAlpha, fragment.alphaCurve.getPointAt(curFrame / fragment.duration).y)

  if (fragment.rotationType == "Object")
  {
    let curRotation = fragment.autoRotationCurve.getTangentAt(lerp1(curStartPortition, curStartPortition + curTotalPortition, curFrame / fragment.duration))
    fragment.object.position.set(0, 0, 0)
    fragment.object.lookAt(curRotation.x, curRotation.y, curRotation.z)
    fragment.object.rotateY(toRadians(90))
  }
  else if (fragment.rotationType == "Vertex")
  {

  }
  else
  {
    let curRotation = lerp3(fragment.startEulerAngle, fragment.endEulerAngle, fragment.rotationCurve.getPointAt(curFrame / fragment.duration).y)
    fragment.object.rotation.set(0, 0, 0, "YZX")
    fragment.object.rotateY(curRotation.y)
    fragment.object.rotateZ(curRotation.z)
    fragment.object.rotateX(curRotation.x)
  }

  fragment.object.position.set(curPosition.x, curPosition.y, curPosition.z)
  fragment.object.scale.set(curScale.x, curScale.y, curScale.z)
  setAlpha(curAlpha, fragment)
}

export function clearAnimationData(data)
{
  for(let i = 0; i < data.length; i += 1)
  {
    data[i].positionCurve = undefined
    data[i].rotationCurve = undefined
    data[i].scaleCurve = undefined
    data[i].alphaCurve = undefined

    data[i].movementCurve = undefined
    data[i].movementCurveIndex = undefined
    data[i].movementCurveLength = undefined

    data[i].autoRotationCurve = undefined
    data[i].autoRotationCurveIndex = undefined
    data[i].autoRotationCurveLength = undefined

    data[i].animated = undefined
  }
}

export function recordFrame(object)
{
  if(animationBuffer[object.id] != null && animationBuffer[object.id].frame != worldFrame)
  {
    recordAnimationFrame(animationBuffer[object.id], recordEndFrame(object))

    animationBuffer[object.id] = null
    objectStatus[object.id].curID = animationFragments[animationFragments.length - 1].id

    addHelpText("Fragment Create Completed")
  }
  else if (animationBuffer[object.id] != null && animationBuffer[object.id].frame == worldFrame)
  {
    addHelpText("Created failed: still in the same time line")
  }
  else
  {
    animationBuffer[object.id] = recordStartFrame(object)
    objectStatus[object.id].curID = -1

    addHelpText("Start Frame Created")
  }
}

export function recordStartFrame(object)
{
  let startInfo =
  {
    "object": object,
    "objectInfo":
    {
      "name": object.name,
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": []
    },

    "position": {"x": object.position.x, "y": object.position.y, "z": object.position.z},
    "rotation": {"x": object.rotation.x, "y": object.rotation.y, "z": object.rotation.z},
    "scale": {"x": object.scale.x, "y": object.scale.y, "z": object.scale.z},
    "alpha": object.material.opacity,

    "frame": worldFrame,

    "callBack": []
  }

  return startInfo
}

export function recordEndFrame(object)
{
  let endInfo =
  {
    "object": object,
    "objectInfo":
    {
      "name": object.name,
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": []
    },

    "childFragments": [],

    "position": {"x": object.position.x, "y": object.position.y, "z": object.position.z},
    "rotation": {"x": object.rotation.x, "y": object.rotation.y, "z": object.rotation.z},
    "scale": {"x": object.scale.x, "y": object.scale.y, "z": object.scale.z},
    "alpha": object.material.opacity,

    "frame": worldFrame,

    "callBack": []
  }

  return endInfo
}

export function recordAnimationFrame(startInfo, endInfo)
{
  animationFragments[animationFragments.length] =
  {
    "id": globalID++,
    "object": startInfo.object,
    "objectInfo":
    {
      "name": startInfo.object.name,
      "vertex": [],
      "posWeight": [],
      "rotationWeight": [],
      "scaleWeight": [],
      "alphaWeight": [],

      // "border":
      // {
      //   show = 1,
      //   color = {"x": 1, "y": 1, "z": 1},
      //   size = 0.05,
      //   borderTest = -1
      // }
    },

    "parentFragment": undefined,
    "childFragments": [],

    "startPosition": startInfo.position,
    "endPosition": endInfo.position,
    "startEulerAngle": startInfo.rotation,
    "endEulerAngle": endInfo.rotation,
    "startScale": startInfo.scale,
    "endScale": endInfo.scale,
    "startAlpha": startInfo.alpha,
    "endAlpha": endInfo.alpha,

    "activeBefore": 1,
    "activeAfter": 1,

    "callBefore": startInfo.callBack,
    "callBack": endInfo.callBack,

    "startFrame": startInfo.frame,
    "duration": endInfo.frame - startInfo.frame,

    //advanced elements
    //TODO
    "posEasePoints": JSON.parse(JSON.stringify(easeArray)),
    "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
    "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
    "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

    "ballisticMovementNext": undefined
  }

  generateIDList()

  if(mode == "Revise")
  {
    leaveReviseMode()
    enterReviseMode()
    curTarget = thisWorldObject
  }
}

export function setAlpha(opacity, fragment)
{
  let parentOpacity = 1

  if(fragment.parentFragment != null)
  {
    parentOpacity = fragment.parentFragment.object.material.opacity
  }

  fragment.object.material.opacity = parentOpacity * opacity

  for(let i = 0; i < fragment.childFragments.length; i += 1)
  {
    setAlpha(fragment.childFragments[i], opacity)
  }
}

export function setObjectOpacity(obj, a)
{
  obj.material.opacity = a

  obj.traverse
  (
    function(child)
    {
      if (child.isMesh)
      {
        child.material.transparent = true
        child.material.opacity = a
      }
    }
  )
}

export function findLastFragment(object)
{
  let preFrame = -99999999
  let preIndex = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(object == animationFragments[i].object && preFrame <= animationFragments[i].startFrame + animationFragments[i].duration)
    {
      preFrame = animationFragments[i].startFrame + animationFragments[i].duration
      preIndex = i
    }
  }

  if(preIndex == -1)
  {
    return null
  }
  else
  {
    return animationFragments[preIndex]
  }
}

export function findPreviousFragment(object, curFrame)
{
  let distance = 99999999
  let index = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    let curDistance = curFrame - animationFragments[i].startFrame

    if(object == animationFragments[i].object && curDistance > 0 && curDistance < distance)
    {
      distance = curDistance
      index = i
    }
  }

  if(index != -1)
  {
    return animationFragments[index]
  }
  else
  {
    return null
  }
}

export function findNextFragment(object, curFrame)
{
  let distance = -99999999
  let index = -1

  for(let i = 0; i < animationFragments.length; i += 1)
  {
    let curDistance = curFrame - animationFragments[i].startFrame

    if(object == animationFragments[i].object && curDistance <= 0 && curDistance > distance)
    {
      distance = curDistance
      index = i
    }
  }

  if(index != -1)
  {
    return animationFragments[index]
  }
  else
  {
    return null
  }
}

export function findObjectCurFragment(object)
{
  if(objectStatus[object.id].curID != -1)
  {
    return findFragment(objectStatus[object.id].curID)
  }

  return null
}

export function findFragment(id)
{
  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(id == animationFragments[i].id)
    {
      return animationFragments[i]
    }
  }

  return null
}

export function renderHelpText()
{
  for(let i = 0; i < systemInfo.length; i += 1)
  {
    if(systemInfo[i].duration < 0)
    {
      destroy(systemInfo[i].pic)

      systemInfo.splice(i, 1)
      i -= 1

      continue
    }

    changeUIStatus(systemInfo[i].pic, 0, 5 + 2.5 * i + document.documentElement.scrollTop / window.innerHeight * 100, 50, 10)

    if(systemInfo[i].duration < 0.5)
    {
      systemInfo[i].text.style.opacity = systemInfo[i].duration * 2
    }

    systemInfo[i].duration -= 0.0167
  }
}

export function reEnterMode(target)
{
  let modeIndex = 0
  let targetIndex = 0
  let modeStringArr = ["Standard", "Revise", "Animation", "Animation (Root)"]
  let modeEnterFunctionArr = [enterStandardMode, enterReviseMode, enterAnimationMode, enterAnimationMode]
  let modeLeaveFunctionArr = [leaveStandardMode, leaveReviseMode, leaveAnimationMode, leaveAnimationMode]

  for (let i = 0; i < modeStringArr.length; i += 1)
  {
    if (mode == modeStringArr[i])
    {
        modeIndex = i
    }

    if (target == modeStringArr[i])
    {
        targetIndex = i
    }
  }

  modeLeaveFunctionArr[modeIndex]()
  modeEnterFunctionArr[targetIndex]()

  editHelpText(target)
}

export function enterStandardMode()
{
  if(thisWorldObject != null)
  {
    curTarget = thisWorldObject
    curTarget.visible = true
  }

  thisWorldObject = null
}

export function leaveStandardMode()
{
  if(curTarget != null)
  {
    thisWorldObject = curTarget
  }
}

export function enterReviseMode()
{
  if(thisWorldObject != null)
  {
    if(objectStatus[thisWorldObject.id].curID == -1)
    {
      let lastFragment = findLastFragment(thisWorldObject)

      if(lastFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = lastFragment.id
      }
    }

    obtainReviseFragmentsInBothSides(thisWorldObject, anotherWorldMaxPresent)
  }
}

export function leaveReviseMode()
{
  for(let i = 0; i < anotherWorldObjects.length; i += 1)
  {
    deleteObject(anotherWorldObjects[i])
  }

  for(let i = 0; i < anotherWorldLines.length; i += 1)
  {
    deleteObject(anotherWorldLines[i])
  }

  curTarget = null

  anotherWorldFragments = []
  anotherWorldObjects = []
  anotherWorldLines = []

  anotherWorldFocusIndex = -1

  handler.core.visible = false
}

export function enterAnimationMode()
{
  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  viewAnimationStatusAt(worldFrame)
}

export function leaveAnimationMode()
{

}
export function vDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    void main()
    {
       vUv = uv;
       vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
       gl_Position = projectionMatrix * mvPosition;
    }
  `
}

export function fDeleteTexAlpha()
{
  return `
    varying vec2 vUv;

    uniform sampler2D tEmissive;
    uniform vec4 borderColor;
    uniform float borderSize;
    uniform bool borderTest;

    float lerp(float v1, float v2, float a)
    {
      return v1 + (v2 - v1) * a;
    }

    void main()
    {
      if(!borderTest)
      {
        gl_FragColor = vec4(borderColor);

        return;
      }

      bool isBorder = false;
      vec2 newUv = vec2(vUv.x * (1.0 + borderSize * 2.0) - borderSize, vUv.y * (1.0 + borderSize * 2.0) - borderSize);

      vec2[] points = vec2[8]
      (
        vec2(newUv.x, clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(newUv.x, clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), newUv.y),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x + borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y - borderSize, 0.0, 1.0)),
        vec2(clamp(newUv.x - borderSize, 0.0, 1.0), clamp(newUv.y + borderSize, 0.0, 1.0))
      );

      for(int i = 0; i <= 8; i += 1)
      {
        if(texture2D(tEmissive, points[i]).a != 0.0)
        {
          isBorder = true;
          break;
        }
      }

      if(isBorder)
      {
        gl_FragColor = vec4(borderColor);
      }
      else
      {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
      }
    }
  `
}

var handlerSize = 0.3125
var handler

var curPressedMouse = [false, false, false]

var plane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0)
var UIPlane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0)
var pressedMousePos = new THREE.Vector2()
var mousePos = new THREE.Vector2()
var UIMousePos = new THREE.Vector2()
var currentlyPressedKeys = []
var hasPressedKeys = []

var cameraDirection
var cameraStartDistance
var curTarget
var curHandlerTarget
var ifCoreMove = false
var isCopying = false

var autoRotationStartObject
var autoRotationStartFragment

/**
 * current selected object's copy when entering other mode
 */
var thisWorldObject

/**
 * max node traverse for both sides in revise mode
 */
var anotherWorldMaxPresent = 4

/**
 *  the data for fragments for other modes
 */
var anotherWorldFragments = []

/**
 *  temperary objects for other modes
 */
var anotherWorldObjects = []

/**
 *  visible lines to help create animation for other modes
 */
export var anotherWorldLines = []

/**
 *  current mid index for editing in other modes
 */
export var anotherWorldFocusIndex = -1

export var curEasePointTarget

export var cameraMovementFactor = 1.0 / 10.0
export var cameraRotationFactor = 1.0 / 10.0

export function handleKeyDown(event)
{
  if(!checkExists(mousePos, sceneZone) && !checkExists(UIMousePos, UIZone))
  {
    return
  }

  currentlyPressedKeys[event.keyCode] = true
  hasPressedKeys[event.keyCode] = true

  console.log(event.keyCode);

  //enter
  if (event.keyCode == "13")
  {
    if(isAnimating)
    {
      addHelpText("Already animating")

      return
    }

    if (animationFragments.length == 0)
    {
      addHelpText("No keyframe have been created")

      return
    }

    worldFrame = 0
    isAnimating = true

    clearAnimationData(animationFragments)
    initAnimationLoop(animationFragments)
    animateAllFragments(animationFragments)

    addHelpText("Start animating")
  }

  //5
  if (event.keyCode == 53)
  {
    showSystemHelp = !showSystemHelp

    if (showSystemHelp)
    {
      addHelpText("Show Help Text")
      systemHelp.text.style.opacity = 0.75
    }
    else
    {
      addHelpText("Hide Help Text")
      systemHelp.text.style.opacity = 0
    }
  }

  //1
  if(event.keyCode == 49)
  {
    worldFrame = Math.max(0, worldFrame - 10)

    addHelpText("Move world fragment 10 Back")
  }

  //2
  if(event.keyCode == 50)
  {
    worldFrame += 10

    addHelpText("Move world fragment 10 Forward")
  }

  //p
  if(event.keyCode == 80)
  {
    threeD = !threeD
    switchTo3DView(threeD)

    addHelpText("View switched")
  }

  //s
  if(event.keyCode == 83)
  {
    saveJson()

    addHelpText("Saved data")
  }

  //l
  if(event.keyCode == 76)
  {
    loadJson(slLoader.value)

    addHelpText("Loaded data")
  }

  //tab
  if(event.keyCode == 9)
  {
    if(mode == "Revise")
    {
      curTarget = thisWorldObject

      handler.core.visible = true
      handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)

      addHelpText("Select the current original object")
    }
  }

  //ctrl + c
  if(event.keyCode == 67)
  {
    if(mode == "Standard" && currentlyPressedKeys[17])
    {
      copiedObject = curTarget

      isCopying = true

      addHelpText("Copied one object")
    }
  }

  //i
  if (event.keyCode == 73)
  {
    if (mode == "Revise")
    {
      if (curTarget != null)
      {
        if (autoRotationStartFragment == null)
        {
          autoRotationStartObject = curTarget
          autoRotationStartFragment = findObjectExistsFragment(curTarget, anotherWorldFragments, 1)
          addHelpText("Set/Reset Object auto rotation start frame")
        }
        else
        {
          if (curTarget == autoRotationStartObject)
          {
            addHelpText("Auto rotation created failed because still in the same frame")
          }
          else
          {
            let sequenceFragments = traverseAnimationFragments(autoRotationStartFragment, findObjectExistsFragment(curTarget, anotherWorldFragments, 2))
            setRotationMode(sequenceFragments, "Object")

            autoRotationStartObject = null
            autoRotationStartFragment = null
            addHelpText("Object auto rotation (re)set finished")
          }
        }
      }
      else
      {
        addHelpText("No object have been selected")
      }
    }
  }

  //u
  if (event.keyCode == 85)
  {
    if (mode == "Revise")
    {
      if (curTarget != null)
      {
        if (autoRotationStartFragment == null)
        {
          autoRotationStartObject = curTarget
          autoRotationStartFragment = findObjectExistsFragment(curTarget, anotherWorldFragments)
          addHelpText("Set/Reset Vertices auto rotation start frame")
        }
        else
        {
          if (curTarget == autoRotationStartObject)
          {
            addHelpText("Auto rotation created failed because still in the same frame")
          }
          else
          {
            let sequenceFragments = traverseAnimationFragments(autoRotationStartFragment, findObjectExistsFragment(curTarget, anotherWorldFragments))
            setRotationMode(sequenceFragments, "Vertex")

            autoRotationStartObject = null
            autoRotationStartFragment = null
            addHelpText("Vertices auto rotation (re)set finished")
          }
        }
      }
      else
      {
        addHelpText("No object have been selected")
      }
    }
  }

  //ctrl + v
  if(event.keyCode == 86)
  {
    if(mode == "Standard" && currentlyPressedKeys[17])
    {
      if(!isCopying)
      {
        return
      }

      let newObj = cloneAll(copiedObject)

      objectInfo[objectInfo.length] =
      {
        "object": newObj,
        "id": globalID++
      }

      newObj.traverse( function ( child )
      {
        if ( child.isMesh )
        {
          child.castShadow = true;
          child.receiveShadow = true;

          if(child.material == null)
          {
            child.material = new THREE.MeshPhongMaterial( { color: 0xb6b311, specular: 0x222222, shininess: 50} );
          }

          child.material.transparent = true

          objectInfo[objectInfo.length] =
          {
            "object": child,
            "id": globalID++
          }
        }
      });

      objectStatus[newObj.id] =
      {
        "curID": -1
      }

      scene.add(newObj)
      curTarget = newObj

      isCopying = true

      addHelpText("Pasted one object")
      generateIDList()
    }
    else
    {
      if(curTarget != null)
      {
        handler.core.visible = true
      }

      reEnterMode("Standard")
      mode = "Standard"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Standard mode")
    }
  }

  //r
  if(event.keyCode == 82)
  {
    addHelpText("Switch to Revise mode")

    reEnterMode("Revise")
    mode = "Revise"
    systemMode.text.innerHTML = mode
  }

  //left Arrow
  if(event.keyCode == 37)
  {
    if(mode == "Revise")
    {
      anotherWorldMaxPresent = 4 //Math.max(2, anotherWorldMaxPresent - 2)
      let curIndexCopy = anotherWorldFocusIndex

      leaveReviseMode()
      enterReviseMode()

      anotherWorldFocusIndex = curIndexCopy
      handler.core.visible = true

      if(anotherWorldMaxPresent == 2)
      {
        addHelpText("Nodes Travel length Reached min value")
      }
      else
      {
        addHelpText("Decrease nodes travel length")
      }
    }
    else
    {
      addHelpText("Not in Revise Mode")
    }
  }

  //right Arrow
  if(event.keyCode == 39)
  {
    if(mode == "Revise")
    {
      anotherWorldMaxPresent = Math.min(anotherWorldFragments.length * 2, anotherWorldMaxPresent + 2)
      let curIndexCopy = anotherWorldFocusIndex

      leaveReviseMode()
      enterReviseMode()

      anotherWorldFocusIndex = curIndexCopy
      handler.core.visible = true

      if(anotherWorldMaxPresent == anotherWorldFragments.length * 2)
      {
        addHelpText("Nodes Travel length Reached max value")
      }
      else
      {
        addHelpText("Increase nodes travel length")
      }
    }
    else
    {
      addHelpText("Not in Revise Mode")
    }
  }

  //space
  if(event.keyCode == 32)
  {
    if(mode == "Standard" && !easeCurveInControl && curTarget != null)
    {
      if(animationBuffer[curTarget.id] == null)
      {
        animationBuffer[curTarget.id] = recordStartFrame(curTarget)
        objectStatus[curTarget.id].curID = -1

        addHelpText("Start Frame Created")
      }
      else
      {
        recordFrame(curTarget)
        recordFrame(curTarget)

        addHelpText("Continuely Created Frames")
      }

      generateIDList()
    }
  }

  //f : force create
  if (event.keyCode == 70)
  {
    if (mode == "Standard" && curTarget != null || mode == "Revise" && curTarget == thisWorldObject)
    {
      let curObject = null

      if (mode == "Standard")
      {
        curObject = curTarget
      }
      else
      {
        curObject = thisWorldObject
      }

      let preFragment = findLastFragment(curObject)

      if(preFragment != null && preFragment.startFrame + preFragment.duration == worldFrame)
      {
        addHelpText("Created failed: still in the same time line")
      }
      else if(preFragment != null)
      {
        animationFragments[animationFragments.length] =
        {
          "id": globalID++,
          "object": curObject,
          "objectInfo":
          {
            "name": curObject.name,
            "vertex": [],
            "posWeight": [],
            "rotationWeight": [],
            "scaleWeight": [],
            "alphaWeight": [],
          },

          "parentFragment": undefined,
          "childFragments": [],

          "startPosition": {"x": preFragment.endPosition.x, "y": preFragment.endPosition.y, "z": preFragment.endPosition.z},
          "endPosition": {"x": curObject.position.x, "y": curObject.position.y, "z": curObject.position.z},
          "startEulerAngle": {"x": preFragment.endEulerAngle.x, "y": preFragment.endEulerAngle.y, "z": preFragment.endEulerAngle.z},
          "endEulerAngle": {"x": curObject.rotation.x, "y": curObject.rotation.y, "z": curObject.rotation.z},
          "startScale": {"x": preFragment.endScale.x, "y": preFragment.endScale.y, "z": preFragment.endScale.z},
          "endScale": {"x": curObject.scale.x, "y": curObject.scale.y, "z": curObject.scale.z},
          "startAlpha": preFragment.endAlpha,
          "endAlpha": curObject.material.opacity,

          "activeBefore": 1,
          "activeAfter": 1,

          "callBefore": [],
          "callBack": [],

          "startFrame": preFragment.startFrame + preFragment.duration,
          "duration": worldFrame - (preFragment.startFrame + preFragment.duration),

          "posEasePoints": JSON.parse(JSON.stringify(easeArray)),
          "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
          "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

          "movementType": "Ballist",
          "ballisticMovementNext": undefined
        }

        if (preFragment.movementType == "Ballist")
        {
          preFragment.ballisticMovementNext = animationFragments[animationFragments.length - 1]
        }
      }
      else
      {
        recordFrame(curObject)
        generateIDList()

        //if created, set ballisticMovementNext
        if (animationBuffer[curObject.id] == null)
        {
          animationFragments[animationFragments.length - 1].movementType = "Ballist"
        }
      }

      generateIDList()
    }

    if(mode == "Revise")
    {
      leaveReviseMode()
      enterReviseMode()
      curTarget = thisWorldObject
    }
  }

  //a
  if(event.keyCode == 65)
  {
    if(mode != "Animation")
    {
      reEnterMode("Animation")
      mode = "Animation"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Animation mode")
    }
    else
    {
      mode = "Animation (Root)"
      systemMode.text.innerHTML = mode

      addHelpText("Switch to Animation (Root) mode")
    }
  }

  //e
  if (event.keyCode == 69)
  {
    if (mode == "Animation" || mode == "Animation (Root)")
    {
      if(curTarget != null)
      {
        let preFragment = findPreviousFragment(curTarget, worldFrame)

        if (preFragment != null)
        {
          if (mode == "Animation (Root)")
          {
            EditStartFrameFrom(findNextFragment(curTarget, worldFrame), 10)

            addHelpText("Increased 10 duration(" + (preFragment.duration + 10) + ") for current fragment with subsequence effect")
          }
          else
          {
            addHelpText("Increased 10 duration(" + (preFragment.duration + 10) + ") for current fragment")
          }

          preFragment.duration += 10

          viewAnimationStatusAt(worldFrame)

          if (handler.core.visible)
          {
            handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
          }
        }
        else
        {
          addHelpText("No fragment have found for this object")
        }
      }
      else
      {
        viewAnimationStatusAt(++worldFrame)
      }
    }

    generateIDList()
  }

  //q
  if (event.keyCode == 81)
  {
    if (mode == "Animation" || mode == "Animation (Root)")
    {
      if(curTarget != null)
      {
        let preFragment = findPreviousFragment(curTarget, worldFrame)

        if (preFragment != null)
        {
          //check if valid first
          if (preFragment.duration != 10)
          {
            if (mode == "Animation (Root)")
            {
              EditStartFrameFrom(findNextFragment(curTarget, worldFrame), -10)

              addHelpText("Decrease 10 duration(" + (preFragment.duration - 10) + ") for current fragment with subsequence effect")
            }
            else
            {
              addHelpText("Decrease 10 duration(" + (preFragment.duration - 10) + ") for current fragment")
            }
          }

          preFragment.duration = Math.max(10, preFragment.duration - 10)

          viewAnimationStatusAt(worldFrame)

          if (handler.core.visible)
          {
            handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
          }
        }
        else
        {
          addHelpText("No fragment have found for this object")
        }
      }
      else
      {
        worldFrame = Math.max(0, worldFrame - 1)
        viewAnimationStatusAt(worldFrame)
      }
    }

    generateIDList()
  }
}

export function handleKeyUp(event)
{
  if(!checkExists(mousePos) && !checkExists(UIMousePos))
  {
    return
  }

  //ctrl
  if(event.keyCode == 17)
  {
    if(-1 <= UIMousePos.x && UIMousePos.x < -0.33 && -1 <= UIMousePos.y && UIMousePos.y < -0.33)
    {
      let mouseXPosInUICamera = (-1 - UIMousePos.x) / (-1 - -0.33)
      let mouseYPosInUICamera = (-1 - UIMousePos.y) / (-1 - -0.33)

      for(let i = 0; i < easePointsOnScreen.length; i += 1)
      {
        if(mouseXPosInUICamera < easePointsOnScreen[i].position.x)
        {
          let newPoint = newSphere(mouseXPosInUICamera, mouseYPosInUICamera, 0, 0.05)
          newPoint.material.emissive = new THREE.Color(1, 0, 0)
          UIScene.add(newPoint)

          easePointsOnScreen.splice(i, 0, newPoint);

          updateEaseCurve()
          break
        }
      }
    }
    else
    {
      if(isCopying)
      {
        isCopying = false
        currentlyPressedKeys[event.keyCode] = false

        return
      }

      if(curTarget == null)
      {
        if (mode == "Revise")
        {
          let status = insertFrame(mousePos, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)

          if (status == "OK")
          {
            addHelpText("Inserted frame in " + worldFrame)
          }
          else
          {
            addHelpText("Inserted frame error with " + status)
          }
        }
        else
        {
          addHelpText("No object selected")
          currentlyPressedKeys[event.keyCode] = false
        }

        generateIDList()

        return
      }

      recordFrame(curTarget)
    }

    generateIDList()
  }

  // //t
  // if (event.keyCode == 84)
  // {
  //   if (mode == "Standard")
  //   {
  //     if (animationBuffer[camera.id] == null)
  //     {
  //       objectStatus[camera.id] = {"curID": -1}
  //     }
  //
  //     recordFrame(camera)
  //   }
  // }
  //
  // //g
  // if (event.keyCode == 71)
  // {
  //   if (mode == "Standard")
  //   {
  //     if(animationBuffer[camera.id] == null)
  //     {
  //       animationBuffer[camera.id] = recordStartFrame(camera)
  //       objectStatus[camera.id] = {"curID": -1}
  //
  //       addHelpText("Camera Start Frame Created")
  //     }
  //     else
  //     {
  //       recordFrame(camera)
  //       recordFrame(camera)
  //
  //       addHelpText("Camera Continuely Created Frames")
  //     }
  //   }
  //   else if (mode == "Revise")
  //   {
  //     leaveReviseMode()
  //
  //     thisWorldObject = camera
  //
  //     enterReviseMode()
  //   }
  // }

  //backspace
  if(event.keyCode == 8)
  {
    if(easeCurveInControl)
    {
      if(curEasePointTarget != null)
      {
        for(let i = 0; i < easePointsOnScreen.length; i += 1)
        {
          if(easePointsOnScreen[i] == curEasePointTarget)
          {
            UIScene.remove(easePointsOnScreen[i])
            easePointsOnScreen[i].geometry.dispose()
            easePointsOnScreen[i].material.dispose()

            easePointsOnScreen.splice(i, 1)

            updateEaseCurve()
            break
          }
        }
      }
    }
    else
    {
      if(curTarget != null)
      {
        if (mode == "Standard")
        {
          let curFragment = findObjectCurFragment(curTarget)

          if (curFragment != null)
          {
            deleteFragmentIn(curFragment, animationFragments)
            addHelpText("Deleted current fragment")
          }
          else
          {
            deleteObject(curTarget)
            handler.core.visible = false
            curTarget = null

            addHelpText("Deleted current object")
          }
        }
        else if (mode == "Revise")
        {
          for (let i = 0; i < anotherWorldObjects.length; i += 1)
          {
            if (curTarget == anotherWorldObjects[i])
            {
              deleteFrame(curTarget, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)
              addHelpText("Deleted selected frame")

              handler.core.visible = false

              break
            }

            if (i - 1 == anotherWorldObjects.length)
            {
              addHelpText("Bug")
            }
          }
        }

        generateIDList()
      }
    }
  }

  //q
  if(event.keyCode == 81)
  {
    if(mode == "Revise")
    {
      let preFragment = findPreviousFragment(thisWorldObject, findObjectCurFragment(thisWorldObject).startFrame)

      if(preFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = preFragment.id
        worldFrame = preFragment.startFrame

        leaveReviseMode()
        enterReviseMode()

        addHelpText("Back to the previous frame for this object")
      }
      else
      {
        addHelpText("No previous frame for this object")
      }
    }
    else if(mode == "Standard")
    {
      if(curTarget == null)
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false

        return
      }

      // try to find current fragment first
      let preFragment = findObjectCurFragment(curTarget)

      //if not find, check if start frame has recorded
      if(preFragment == null)
      {
        preFragment = animationBuffer[curTarget.id]

        //if find
        if(preFragment != null)
        {
          //if position is not equal, return to start frame
          if(!atOnePoint(curTarget, preFragment.position, new THREE.Vector3()))
          {
            curTarget.position.set(preFragment.position.x, preFragment.position.y, preFragment.position.z)
            handler.core.position.set(preFragment.position.x, preFragment.position.y, preFragment.position.z)
            worldFrame = preFragment.frame

            addHelpText("Back to the start frame")
          }
          //delete all cur animation info
          else
          {
            worldFrame = preFragment.frame
            animationBuffer[curTarget.id] = null

            addHelpText("The start frame clear finished")
          }
        }
        else
        {
          preFragment = findPreviousFragment(curTarget, worldFrame)

          //if find:
          //  back to the previous fragment's position
          //  set object status to previous fragment
          if(preFragment != null)
          {
            curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
            handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
            objectStatus[curTarget.id].curID = preFragment.id
            worldFrame = preFragment.startFrame

            addHelpText("Back to the previous fragment's start position")
          }
          else
          {
            addHelpText("No fragment have found for this object")
          }
        }
      }
      else if(atOnePoint(curTarget, preFragment.startPosition, preFragment.endPosition))
      {
        preFragment = findPreviousFragment(curTarget, preFragment.startFrame)

        //if find:
        //  back to the previous fragment's position
        //  set object status to previous fragment
        if(preFragment != null)
        {
          curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
          handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
          objectStatus[curTarget.id].curID = preFragment.id
          worldFrame = preFragment.startFrame

          addHelpText("Back to the previous fragment's start position")
        }
        else
        {
          addHelpText("Already in the first fragment")
        }
      }
      else
      {
        curTarget.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
        handler.core.position.set(preFragment.startPosition.x, preFragment.startPosition.y, preFragment.startPosition.z)
        worldFrame = preFragment.startFrame

        addHelpText("Back to original position of this fragment")
      }
    }
  }

  //e
  if(event.keyCode == 69)
  {
    if(mode == "Revise")
    {
      let nextFragment = findNextFragment(thisWorldObject, findObjectCurFragment(thisWorldObject).startFrame + findObjectCurFragment(thisWorldObject).duration)

      if(nextFragment != null)
      {
        objectStatus[thisWorldObject.id].curID = nextFragment.id
        worldFrame = nextFragment.startFrame

        leaveReviseMode()
        enterReviseMode()

        addHelpText("Go to the next frame for this object")
      }
      else
      {
        addHelpText("No next frame for this object")
      }
    }
    else if(mode == "Standard")
    {
      if(curTarget == null)
      {
        addHelpText("No object selected")
        currentlyPressedKeys[event.keyCode] = false

        return
      }

      let curFrame = worldFrame
      let curFragment = findObjectCurFragment(curTarget)

      if(curFragment != null)
      {
        curFrame = curFragment.startFrame + curFragment.duration
      }

      // try to find next fragment
      let nextFragment = findNextFragment(curTarget, curFrame)

      //if find, go to next fragment, clear
      //current buffer data
      if(nextFragment != null)
      {
        curTarget.position.set(nextFragment.startPosition.x, nextFragment.startPosition.y, nextFragment.startPosition.z)
        handler.core.position.set(nextFragment.startPosition.x, nextFragment.startPosition.y, nextFragment.startPosition.z)
        objectStatus[curTarget.id].curID = nextFragment.id
        worldFrame = nextFragment.startFrame

        animationBuffer[curTarget.id] = null

        addHelpText("Go to next fragment's start position")
      }
      else
      {
        addHelpText("No next fragment for this object")
      }
    }
  }

  currentlyPressedKeys[event.keyCode] = false
}

export function handleMouseDown(event)
{
  curPressedMouse[event.button] = true

  if(checkExists(mousePos, sceneZone))
  {
    handleMouseDownScene(event)
  }
  else
  {
    handleMouseDownUI(event)
  }

  pressedMousePos.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1)
}

export function handleMouseDownScene(event)
{
  if(event.button == 0)
  {
    ifCoreMove = false

    raycaster.setFromCamera(mousePos, camera);

    let sceneObjects = []
    let anotherObjects = []
    let handlerObjects = []

    for(let i = 0; i < objectInfo.length; i += 1)
    {
      sceneObjects[i] = objectInfo[i].object
    }

    if(mode == "Revise")
    {
      for(let i = 0; i < anotherWorldObjects.length; i += 1)
      {
        anotherObjects[i] = anotherWorldObjects[i]
      }
    }

    let intersectsObjects = raycaster.intersectObjects(sceneObjects);
    let anotherObjectsVV = []

    for (var i = 0; i < anotherObjects.length; i++)
    {
      anotherObjects[i].traverse
      (
        function(child)
        {
          if (child.isMesh)
          {
            anotherObjectsVV[anotherObjectsVV.length] = child
          }
        }
      )
    }

    let intersectsAnotherObjects = raycaster.intersectObjects(anotherObjectsVV)

    handlerObjects[handlerObjects.length] = handler.core
    handlerObjects[handlerObjects.length] = handler.xMove
    handlerObjects[handlerObjects.length] = handler.yMove
    handlerObjects[handlerObjects.length] = handler.zMove
    handlerObjects[handlerObjects.length] = handler.xRotate
    handlerObjects[handlerObjects.length] = handler.yRotate
    handlerObjects[handlerObjects.length] = handler.zRotate
    handlerObjects[handlerObjects.length] = handler.xScale
    handlerObjects[handlerObjects.length] = handler.yScale
    handlerObjects[handlerObjects.length] = handler.zScale

    let intersectshandlerObjects = raycaster.intersectObjects(handlerObjects);

    if(intersectsObjects.length > 0 || intersectshandlerObjects.length > 0 || intersectsAnotherObjects.length > 0)
    {
      if ( intersectshandlerObjects.length > 0 )
      {
        curHandlerTarget = intersectshandlerObjects[0].object

        ifCoreMove = curHandlerTarget == handler.core
      }
      else if ( intersectsObjects.length > 0 )
      {
        curTarget = getParentObject(intersectsObjects[0].object)

        handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
        handler.core.visible = true

        ifCoreMove = true

        if(mode == "Revise" && curTarget != thisWorldObject)
        {
          thisWorldObject = curTarget
          leaveReviseMode()
          enterReviseMode()
          curTarget = thisWorldObject

          handler.core.visible = true
        }
      }
      else if ( intersectsAnotherObjects.length > 0 )
      {
        curTarget = getParentObject(intersectsAnotherObjects[0].object)

        handler.core.position.set(curTarget.position.x, curTarget.position.y, curTarget.position.z)
        handler.core.visible = true

        ifCoreMove = true
      }

      cameraDirection = new THREE.Vector3(curTarget.position.x - camera.position.x, curTarget.position.y - camera.position.y, curTarget.position.z - camera.position.z).normalize()
    }
    else
    {
      curTarget = null
      curHandlerTarget = null

      handler.core.visible = false
    }
  }
  else if(event.button == 2)
  {
    if(cameraStartDistance == null)
    {
      if(curTarget != null)
      {
        cameraStartDistance = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z).distanceTo(curTarget.position)
      }
      else
      {
        cameraStartDistance = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z).distanceTo(new THREE.Vector3())
      }
    }
  }
}

export function handleMouseDownUI(event)
{
  if(event.button == 0)
  {
    let mouse = {}
    const rect = UIRenderer.domElement.getBoundingClientRect();
    mouse.x = ( ( event.clientX - rect.left ) / ( rect.right - rect.left ) ) * 2 - 1;
    mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

    raycaster.setFromCamera(mouse, UICamera);

    let sceneObjects = []

    for (let i = 1; i < easePointsOnScreen.length - 1; i += 1)
    {
      sceneObjects[i - 1] = easePointsOnScreen[i]
    }

    let intersectsObjects = raycaster.intersectObjects(sceneObjects);

    if(intersectsObjects.length > 0)
    {
      curEasePointTarget = intersectsObjects[0].object
    }
    else
    {
      curEasePointTarget = null
    }
  }
}

export var showSystemHelp = true

export function handleMouseMove(event)
{
  document.documentElement.style.overflow = '';

  if(checkExists(mousePos))
  {
    document.documentElement.style.overflow = 'hidden';
    handleMouseMoveScene(event)
  }
  else if(checkExists(UIMousePos))
  {
    handleMouseMoveUI(event)
  }

  if(!isAnimating && checkExists(mousePos, sceneZone))
  {
    systemMode.text.style.opacity = 0.5
    systemFrame.text.style.opacity = 0.5

    if (showSystemHelp)
    {
      systemHelp.text.style.opacity = 0.75
    }
  }
  else
  {
    systemMode.text.style.opacity = 0
    systemHelp.text.style.opacity = 0
    systemFrame.text.style.opacity = 0
  }

  mousePos.set((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1, -((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1))
  UIMousePos.set((event.clientX - parseInt(UICanvas.style.left) + document.documentElement.scrollLeft) / parseInt(UICanvas.style.width) * 2 - 1, -((event.clientY - parseInt(UICanvas.style.top) + document.documentElement.scrollTop) / parseInt(UICanvas.style.height) * 2 - 1))
}

export function handleMouseMoveScene(event)
{
  let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  if(curPressedMouse[0])
  {
    //transform object
    if(curTarget == null)
    {
      return
    }

    //if in Animation mode, prevent
    //object's moving
    if((mode == "Animation" || mode == "Animation (Root)"))
    {
      return
    }

    //free move
    if(ifCoreMove)
    {
      //scaler
      if(hasPressedKeys[16])
      {
        let v1 = 1

        if((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x < 0)
        {
          v1 = -1
        }

        let curPosition = new THREE.Vector2((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1, 0)
        let distance = 1 + curPosition.distanceTo(mousePos) * 0.5 * cameraDistance / 10 * v1

        curTarget.scale.set(curTarget.scale.x * distance, curTarget.scale.y * distance, curTarget.scale.z * distance)
      }
      //drag
      else
      {
        let intersects = new THREE.Vector3()
        let curPosition = new THREE.Vector2(
          (event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1,
          -((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1)
        )

        raycaster.setFromCamera(mousePos, camera)
        raycaster.ray.intersectPlane(plane, intersects)

        let distance = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

        raycaster.setFromCamera(curPosition, camera)
        raycaster.ray.intersectPlane(plane, intersects)

        distance = new THREE.Vector3(intersects.x - distance.x, intersects.y - distance.y, intersects.z - distance.z)

        curTarget.position.add(distance)
        handler.core.position.add(distance)
      }
    }
    //move
    else if(curHandlerTarget == handler.xMove || curHandlerTarget == handler.yMove || curHandlerTarget == handler.zMove)
    {
      if(curHandlerTarget == handler.xMove)
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.z > 0)
        {
          v1 = -1
        }

        if(cameraDirection.x < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * cameraDistance * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.x)), 0, 0)
        let distance2 = new THREE.Vector3((-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * cameraDistance * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.z)), 0, 0)

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
      else if(curHandlerTarget == handler.yMove)
      {
        let distance1 = new THREE.Vector3(0, ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * cameraDistance * 0.05, 0)
        let distance2 = new THREE.Vector3(0, (-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * cameraDistance * lerp1(0.5, 0.05, Math.abs(cameraDirection.y)), 0, 0)

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
      else
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.x < 0)
        {
          v1 = -1
        }

        if(cameraDirection.z < 0)
        {
          v2 = -1
        }

        let distance1 = new THREE.Vector3(0, 0, ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * cameraDistance * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.z)))
        let distance2 = new THREE.Vector3(0, 0, (-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * cameraDistance * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.x)))

        curTarget.position.add(distance1).add(distance2)
        handler.core.position.add(distance1).add(distance2)
      }
    }
    //rotate
    else if(curHandlerTarget == handler.xRotate || curHandlerTarget == handler.yRotate || curHandlerTarget == handler.zRotate)
    {
      let rotationFactor = 15
      let v1 = 1

      if(cameraDirection.z < 0)
      {
        v1 = -1
      }

      let distance = ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * v1

      if(curHandlerTarget == handler.xRotate)
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(1, 0, 0), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
      else if(curHandlerTarget == handler.yRotate)
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
      else
      {
        let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 0, 1), distance * rotationFactor);
        let q2 = new THREE.Quaternion().copy(curTarget.quaternion);
        curTarget.quaternion.copy(q).multiply(q2);
        handler.scaleCore.quaternion.copy(q).multiply(q2);
      }
    }
    //scale
    else
    {
      let scaleFactor = 10

      if(curHandlerTarget == handler.xScale)
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.z > 0)
        {
          v1 = -1
        }

        if(cameraDirection.x < 0)
        {
          v2 = -1
        }

        let distance1 = ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * scaleFactor * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.x))
        let distance2 = (-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * scaleFactor * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.z))

        curTarget.scale.x *= (1 + distance1) * (1 + distance2)
      }
      else if(curHandlerTarget == handler.yScale)
      {
        let distance1 = ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * scaleFactor * 0.05
        let distance2 = (-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * scaleFactor * lerp1(0.5, 0.05, Math.abs(cameraDirection.y))

        curTarget.scale.y *= (1 + distance1) * (1 + distance2)
      }
      else
      {
        let v1 = 1
        let v2 = 1

        if(cameraDirection.x < 0)
        {
          v1 = -1
        }

        if(cameraDirection.z < 0)
        {
          v2 = -1
        }

        let distance1 = ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * scaleFactor * lerp1(v1, 0.05 * v1, Math.abs(cameraDirection.z))
        let distance2 = (-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * scaleFactor * lerp1(v2, 0.05 * v2, Math.abs(cameraDirection.x))

        curTarget.scale.z *= (1 + distance1) * (1 + distance2)
      }
    }

    if(mode == "Revise")
    {
      updateObjectsInFrame(curTarget, anotherWorldLines, anotherWorldObjects, anotherWorldFragments)
    }
  }
  else if(curPressedMouse[1])
  {
    let distance1 = ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * cameraDistance / 10 * 5
    let distance2 = (-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * cameraDistance / 10 * 5

    camera.translateX(-distance1)
    camera.translateY(-distance2)
  }
  else if(curPressedMouse[2])
  {
    if(!threeD)
    {
      return
    }

    let distance1 = ((event.clientX - parseInt(canvas.style.left) + document.documentElement.scrollLeft) / parseInt(canvas.style.width) * 2 - 1 - mousePos.x) * 250
    let distance2 = (-((event.clientY - parseInt(canvas.style.top) + document.documentElement.scrollTop) / parseInt(canvas.style.height) * 2 - 1) - mousePos.y) * 250

    camera.translateZ(-cameraStartDistance)
    camera.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), toRadians(-distance1));
    camera.translateZ(cameraStartDistance)

    camera.translateZ(-cameraStartDistance)
    camera.rotateX(toRadians(distance2));
    camera.rotation.x = clamp(camera.rotation.x, -Math.PI / 2, Math.PI / 2)
    camera.translateZ(cameraStartDistance)

    let dir = camera.getWorldDirection(new THREE.Vector3())
    plane.normal = new THREE.Vector3(dir.x, dir.y, dir.z)
  }

  let dir = camera.getWorldDirection(new THREE.Vector3())

  light.position.set(camera.position.x, camera.position.y, camera.position.z)
  light.rotation.set(dir.x, dir.y, dir.z, "YZX")
}

export function handleMouseMoveUI(event)
{
  if(curPressedMouse[0])
  {
    //RGB picker
    if (UIMousePos.y > 0)
    {
      function getUVW(vertexPostion, vUv, i)
      {
        let j = i + 1;

        if(i == 2)
        {
          j = 0;
        }

        let v1 = new THREE.Vector3(Math.abs(vertexPostion[j].x - vertexPostion[i].x), Math.abs(vertexPostion[j].y - vertexPostion[i].y), 0.0);
        let v2 = new THREE.Vector3(Math.abs(vUv.x - vertexPostion[i].x), Math.abs(vUv.y - vertexPostion[i].y), 0);
        return new THREE.Vector3().crossVectors(v1, v2).length() / 2.0;
      }

      function findRGB(vUv)
      {
        let n = new THREE.Vector3(0.0, 0.0, 0.0);
        let r = new THREE.Vector3(1.0, 0.0, 0.0);
        let g = new THREE.Vector3(0.0, 1.0, 0.0);
        let b = new THREE.Vector3(0.0, 0.0, 1.0);

        let vertexPostion;
        let vertexColor;

        if(vUv.x < vUv.y)
        {
          vertexPostion = [
            new THREE.Vector2(0.0, 1.5),
            new THREE.Vector2(3.0, 1.5),
            new THREE.Vector2(0.0, 0.0)
          ];

          vertexColor = [r, n, g];
        }
        else
        {
          vertexPostion = [
            new THREE.Vector2(3.0, 0.0),
            new THREE.Vector2(3.0, 1.5),
            new THREE.Vector2(0.0, 0.0)
          ];

          vertexColor = [r, b, g];
        }

        let u = getUVW(vertexPostion, vUv, 0);
        let v = getUVW(vertexPostion, vUv, 1);
        let w = getUVW(vertexPostion, vUv, 2);

        let rResult = u * vertexColor[0].x + v * vertexColor[1].x + w * vertexColor[2].x;
        let gResult = u * vertexColor[0].y + v * vertexColor[1].y + w * vertexColor[2].y;
        let bResult = u * vertexColor[0].z + v * vertexColor[1].z + w * vertexColor[2].z;

        return new THREE.Color(rResult, gResult, bResult);
      }

      if (curTarget != null)
      {
        let curColor = findRGB(new THREE.Vector2((UIMousePos.x + 1) / 2 * 3, UIMousePos.y * 1.5))

        if (curTarget.isMesh)
        {
          curTarget.material.color = curColor
        }

        curTarget.traverse(function(child)
        {
          if (child.isMesh)
          {
            child.material.color = curColor
          }
        })
      }
      else
      {
        addHelpText("No Object selected")
      }
    }
    //A pickere
    else if (-0.15 <= UIMousePos.y && UIMousePos.y <= -0.05)
    {
      if (curTarget != null && curTarget.isMesh)
      {
        setObjectOpacity(curTarget, (UIMousePos.x + 1) / 2)
      }
      else
      {
        addHelpText("No Object selected")
      }
    }
    else if(curEasePointTarget != null)
    {
      let intersects = new THREE.Vector3()
      let curPosition = new THREE.Vector2(
        (event.clientX - parseInt(UICanvas.style.left) + document.documentElement.scrollLeft) / parseInt(UICanvas.style.width) * 2 - 1,
        -((event.clientY - parseInt(UICanvas.style.top) + document.documentElement.scrollTop) / parseInt(UICanvas.style.height) * 2 - 1)
      )

      raycaster.setFromCamera(UIMousePos, UICamera)
      raycaster.ray.intersectPlane(UIPlane, intersects)

      let distance = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

      raycaster.setFromCamera(curPosition, UICamera)
      raycaster.ray.intersectPlane(UIPlane, intersects)

      let xDistance = (intersects.x - distance.x) * (UIRenderer.getSize(new THREE.Vector2()).x / parseInt(UICanvas.style.width))
      let yDistance = (intersects.y - distance.y) * (UIRenderer.getSize(new THREE.Vector2()).y / parseInt(UICanvas.style.height))
      distance = new THREE.Vector3(xDistance, yDistance, 0)

      curEasePointTarget.position.add(distance)

      curEasePointTarget.position.x = clamp(curEasePointTarget.position.x, 0, 1)
      curEasePointTarget.position.y = clamp(curEasePointTarget.position.y, 0, 1)

      updateEaseCurve()
    }
  }
}

export function handleMouseUp(event)
{
  curPressedMouse[event.button] = false

  if(checkExists(mousePos, sceneZone))
  {
    handleMouseUpScene(event)
  }
  else
  {
    handleMouseUpUI(event)
  }
}

export function handleMouseUpScene(event)
{
  hasPressedKeys[16] = false
  cameraStartDistance = null
}

export function handleMouseUpUI(event)
{

}

export function handleMouseWheel(event)
{
  if(checkExists(mousePos, sceneZone))
  {
    handleMouseWheelScene(event)
  }
  else
  {
    handleMouseWheelUI(event)
  }
}

export function handleMouseWheelScene(event)
{
  let translateZValue = 2.5
  let cameraDistance = Math.abs(camera.position.z)

  if(curTarget != null)
  {
    if(!threeD)
    {
      cameraDistance = Math.abs(camera.position.z - curTarget.position.z)
    }
    else
    {
      cameraDistance = camera.position.distanceTo(curTarget.position)
    }
  }

  if(event.wheelDelta != null)
  {
    if(event.wheelDelta > 0 && translateZValue < cameraDistance)
    {
      camera.translateZ(-translateZValue)
    }
    else if(event.wheelDelta < 0 && cameraDistance < 1500 - translateZValue)
    {
      camera.translateZ(translateZValue)
    }
  }
  else if(event.detail != null)
  {
    if(event.detail > 0 && cameraDistance < 1500 - translateZValue)
    {
      camera.translateZ(translateZValue)
    }
    else if(event.detail < 0 && translateZValue < cameraDistance)
    {
      camera.translateZ(-translateZValue)
    }
  }

  cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  handler.core.scale.set(cameraDistance / 8, cameraDistance / 8, cameraDistance / 8)

  if(!threeD)
  {
    let cameraDistance2 = Math.abs(camera.position.z)

    if(curTarget != null)
    {
      cameraDistance2 = Math.abs(camera.position.z - curTarget.position.z)
    }

    let height = cameraDistance2
    let width = cameraDistance2 * parseInt(canvas.style.width) / parseInt(canvas.style.height)

    camera.left = width / -2
    camera.right = width / 2
    camera.top = height / 2
    camera.bottom = height / -2

    camera.updateProjectionMatrix()
  }

  let dir = camera.getWorldDirection(new THREE.Vector3())

  light.position.set(camera.position.x, camera.position.y, camera.position.z)
  light.rotation.set(dir.x, dir.y, dir.z, "YZX")
}

export function handleMouseWheelUI(event)
{

}

export function handleKeys()
{

}

export function destroyAll()
{
  if (!onPage)
  {
    return
  }

  onPage = false

  copiedObject = undefined

  animationBuffer = []
  objectStatus = []
  objectInfo = []
  animationFragments = []

  threeD = false

  for (let i = 0; i < scene.children.length; i += 1)
  {
    deleteObject(scene.children[i])
  }

  renderer.dispose()
  scene.dispose()
  setCanvasSize(0, 0, 0, 0)

  for (let i = 0; i < UIScene.children.length; i += 1)
  {
    deleteObject(UIScene.children[i])
  }

  UIRenderer.dispose()
  UIScene.dispose()
  setCurveCanvasSize(0, 0, 0, 0)

  anotherWorldLines = []
  anotherWorldObjects = []
  anotherWorldFragments = []
  anotherWorldFocusIndex = -1
  anotherWorldMaxPresent = 4

  curTimeText = undefined
  systemInfo = []
  systemMode = {}
  systemHelp = {}
  systemFrame = {}
  mode = "Standard"

  easeCurve = undefined
  easeArray = undefined
  easeCurveInControl = false
  easePointsOnScreen = []

  worldFrame = 0
  globalID = 0
  isAnimating = false

  sequences = []
  variables = []
  curSequenceName = undefined
  curMacroName = undefined
  curAnimationFragment = undefined

  commandAnimating = false

  window.addEventListener('resize', undefined, false );
  document.addEventListener('DOMMouseScroll', undefined, false);
  document.onkeydown = undefined;
  document.onkeyup = undefined;
  document.onmousedown = undefined;
  document.onmousemove = undefined;
  document.onmouseup = undefined;
  document.onmousewheel = undefined;

  document.getElementById("canvasRoot").replaceChildren()
}

export function onWindowResize()
{
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}
export var GeneralHelpText =
"1: Decrease WorldFrame 10\n" +
"2: Increase WorldFrame 10\n" +
"Enter: Start current animation\n" +
"p: Switch to 2D/3D\n" +
"s/l: Save/Load data\n" +
"v: Enter Standard Mode\n" +
"r: Enter Revise Mode\n";

export var StandardHelpText =
"Mouse: Control camera/Control Object\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Create one (start/end) frame\n" +
"Ctrl + c/v: Copy/Paste one object\n" +
"Space: Create one continuely frame\n" +
"Backspace: Delete current fragment\n" +
"Backspace: Delete current object (no fragment)\n" +
"f: Create/Start a curve movement frame\n" +
"q: Back to the previous frame\n" +
"e: Go to the next frame\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Enter Animation Mode";

export var ReviseHelpText =
"Mouse: Control camera/Control Object/\nEdit Fragment\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Create one (start/end) frame\n" +
"Backspace: Delete current frame\n" +
"f: Start a curve movement fragment\n" +
"q: Back to previous frame\n" +
"e: Go to next frame\n" +
"i: Set/Reset auto-movement start/end frame\n" +
"Left Arrow: Decrease Node Travelling 1\n" +
"Right Arrow: Increase Node Travelling 1\n" +
"\n" +
"If no object have been selected:\n" +
"Ctrl: Insert one (start/end) frame\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Enter Animation Mode";

export var AnimationHelpText =
"Mouse:\n" +
"Control camera/Choose Object\n" +
"Edit Object's Weight/Rotation Point\n" +
"\n" +
"If any object has been selected:\n" +
"Ctrl: Add one Rotation Point\n" +
"Backspace: Delete current fragment\n" +
"q: Decrease fragment's duration 1\n" +
"e: Increase fragment's duration 1\n" +
"\n" +
"If no object have been selected:\n" +
"q: Decrease frame 1 and Set the animation status\n" +
"e: Increase frame 1 and Set the animation status\n" +
"\n" +
"\n" +
GeneralHelpText +
"a: Switch in Animation/Animation (Root) Mode";

export var globalID = 0;

export var canvas;
export var renderer;
export var scene;
export var camera;
export var cameraObject;
export var raycaster;
export var sceneZone
export var UIZone

export var obj1
export var borderEffect
export var slLoader
export var copiedObject

export var objectInfo = []
export var animationFragments = []

export var threeD = false

export var UICanvas
export var UIRenderer
export var UIScene
export var UICamera

export var curTimeText
export var systemInfo = []
export var systemMode = {}
export var systemHelp = {}
export var systemFrame = {}
export var mode = "Standard"

export var easeCurve
export var easeArray
export var easeCurveInControl
export var easePointsOnScreen = []

export var worldFrame = 0
export var isAnimating = false


export function main()
{
  onPage = true

  init()
  initUI()

  switchTo3DView(false)

  let frameText = newText(0, 0, 0, newImage(0, 0, 5, 5, "", 0, 0, 0, 0, document.getElementById("canvasRoot")))
  frameText.style.opacity = 0.5

  let startTime = new Date()
  let counter = 0

  var loop = function()
  {
    if (!onPage)
    {
      return
    }

    if(counter % 60 == 0)
    {
      frameText.innerHTML = "FPS: " + (60000 / (new Date().getTime() - startTime.getTime())).toFixed(1)

      startTime = new Date()
    }

    counter += 1

    systemFrame.text.innerHTML = worldFrame
    frameText.style.top = document.documentElement.scrollTop + "px"

    renderer.clear()
    renderer.render(scene, camera);

    UIRenderer.render(UIScene, UICamera)
    renderHelpText()

    handleKeys();
    requestAnimationFrame(loop);

    if (counter == 30)
    {
      // createSequence("a")
      //
      // macroStartRecording("c")
      // let macroID = globalID - 1
      //
      // rect(0, 0, 0, 1, 1, 1)
      // let cur2 = objectInfo[objectInfo.length - 1].object
      // let cur2ID = objectInfo[objectInfo.length - 1].id
      // createKeyFrame()
      // position(cur2ID, -2, 2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 100)
      // position(cur2ID, 2, -2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 30)
      // macroStopRecording()
      //
      // macroStartRecording("b")
      // circle(0, 0, 0, 0.5)
      // let cur = objectInfo[objectInfo.length - 1].object
      // createKeyFrame()
      // cur.position.set(-2, -2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 100)
      // cur.position.set(2, 2, 0)
      // createKeyFrame()
      // setKeyframeDuration(globalID - 1, 30)
      // macroStopRecording()
      //
      // playSequence()
    }
  };

  loop();
}

export function init()
{
  canvas = document.getElementById('canvas');
  UICanvas = document.getElementById('UICanvas');
  UICanvas.onmouseover = function()
  {
    easeCurveInControl = true
  }
  UICanvas.onmouseout = function()
  {
    easeCurveInControl = false
  }

  canvas.style.position = "absolute"
  UICanvas.style.position = "absolute"

  scene = new THREE.Scene();
  UIScene = new THREE.Scene()

  camera = new THREE.PerspectiveCamera(45, parseInt(canvas.style.width) / parseInt(canvas.style.height), 0.01, 3000);
  camera.position.set(0, 6, 5);
  camera.rotation.set(0, 0, 0, "YZX")
  camera.rotateX(toRadians(-30))
  camera.material = {"opacity" : 1}

  let size = 3
  UICamera = new THREE.OrthographicCamera(size / -2, size / 2, size / 2, size / -2, 0.01, 3000)
  UICamera.position.set(1.5, 1.5, 10)
  UICamera.rotation.set(0, 0, 0, "YZX")

  renderer = new THREE.WebGLRenderer( { canvas : canvas, context : canvas.getContext( 'webgl2') });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.autoClear = false
  renderer.antialias = true

  UIRenderer = new THREE.WebGLRenderer( { canvas : UICanvas, context : UICanvas.getContext( 'webgl2') });
  UIRenderer.setPixelRatio( window.devicePixelRatio );
  UIRenderer.setSize(300, 300, false);

  raycaster = new THREE.Raycaster();

  initBasicScene()
  generateColorPickerShader()

  window.addEventListener('resize', onWindowResize, false );
  document.onkeydown = handleKeyDown;
  document.onkeyup = handleKeyUp;
  document.onmousedown = handleMouseDown;
  document.onmousemove = handleMouseMove;
  document.onmouseup = handleMouseUp;
  document.onmousewheel = handleMouseWheel;
  if (document.addEventListener)
  {
    document.addEventListener('DOMMouseScroll', handleMouseWheel, false);
  }

  // load texture map
  let loader = new THREE.TextureLoader();
  let texture = loader.load("b.png");

  texture.magFilter = THREE.NearestFilter;
  texture.minFilter = THREE.NearestFilter;

  obj1 = newPlane(0, 0, 0, 1, 1, null, new THREE.Color(1, 0, 0))
  let obj2 = newCube(1, 1, 0, 1, 1, 1, null, new THREE.Color(0, 1, 0))
  let obj3 = newPlane(0, 0, 0, 1, 1, null, new THREE.Color(0, 0, 0))

  objectInfo[objectInfo.length] =
  {
    "object": obj2,
    "id": globalID++
  }

  scene.add(obj2)
  addBorder(obj2, 1, 1, 0, 1, 0.05, true)

  setCanvasSize((window.innerWidth - 880) / 2 - 27, 828 + 87, 895, 571)

  let arr = [{"x": 0, "y": 0}, {"x": 0.5, "y": 0.5}, {"x": 1, "y": 1}]
  setCurveCanvasSize((window.innerWidth - 880) / 2 - 27 + 895 + 15, 828 + 87 + 250, 300, 300)
  drawEaseCurveOnScreen(arr)

  sceneZone =
  {
    width: parseInt(canvas.style.width) / window.innerWidth * 100,
    height: parseInt(canvas.style.height) / window.innerHeight * 100,
    x: parseInt(canvas.style.left) / window.innerWidth * 100,
    y: parseInt(canvas.style.top) / window.innerHeight * 100
  }

  UIZone =
  {
    width: parseInt(UICanvas.style.width) / window.innerWidth * 100,
    height: parseInt(UICanvas.style.height) / window.innerHeight * 100,
    x: parseInt(UICanvas.style.left) / window.innerWidth * 100,
    y: parseInt(UICanvas.style.top) / window.innerHeight * 100
  }
}

export function initHandler()
{
  let core = newSphere(0, 0, 0, handlerSize * 0.3, null, new THREE.Color(0, 0, 0))
  core.material.depthTest = false
  core.renderOrder = 2

  let xMove = newCone(2, 0, 0, handlerSize * 0.25, handlerSize, null, new THREE.Color(0.6, 0, 0), core)
  let yMove = newCone(0, 2, 0, handlerSize * 0.25, handlerSize, null, new THREE.Color(0, 0.6, 0), core)
  let zMove = newCone(0, 0, 2, handlerSize * 0.25, handlerSize, null, new THREE.Color(0, 0, 0.6), core)
  xMove.material.emissive = new THREE.Color(0.4, 0, 0)
  yMove.material.emissive = new THREE.Color(0, 0.4, 0)
  zMove.material.emissive = new THREE.Color(0, 0, 0.4)

  let xRotate = newSphere(1.7, 0, 0, handlerSize * 0.25, null, new THREE.Color(0.6, 0, 0), core)
  let yRotate = newSphere(0, 1.7, 0, handlerSize * 0.25, null, new THREE.Color(0, 0.6, 0), core)
  let zRotate = newSphere(0, 0, 1.7, handlerSize * 0.25, null, new THREE.Color(0, 0, 0.6), core)
  xRotate.material.emissive = new THREE.Color(0.4, 0, 0)
  yRotate.material.emissive = new THREE.Color(0, 0.4, 0)
  zRotate.material.emissive = new THREE.Color(0, 0, 0.4)

  let xLine = newCylinder(1, 0, 0, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  let yLine = newCylinder(0, 1, 0, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  let zLine = newCylinder(0, 0, 1, 0.005, 2, null, new THREE.Color(0, 0, 0), core)
  xLine.material.emissive = new THREE.Color(0.6, 0, 0)
  yLine.material.emissive = new THREE.Color(0, 0.6, 0)
  zLine.material.emissive = new THREE.Color(0, 0, 0.6)

  let scaleCore = newCube(0, 0, 0, handlerSize * 0.6, handlerSize * 0.6, handlerSize * 0.6, null, new THREE.Color(0, 0, 0), core)

  let faceLine = newCylinder(1, 0, 0, 0.01, 2, null, new THREE.Color(0, 0, 0), scaleCore)
  faceLine.material.emissive = new THREE.Color(0, 0, 0)

  let xScale = newCube(1.47, 0, 0, handlerSize * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0.6, 0, 0), core)
  let yScale = newCube(0, 1.47, 0, handlerSize  * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0, 0.6, 0), core)
  let zScale = newCube(0, 0, 1.47, handlerSize * 0.4, handlerSize * 0.4, handlerSize * 0.4, null, new THREE.Color(0, 0, 0.6), core)
  xScale.material.emissive = new THREE.Color(0.4, 0, 0)
  yScale.material.emissive = new THREE.Color(0, 0.4, 0)
  zScale.material.emissive = new THREE.Color(0, 0, 0.4)

  xMove.rotation.set(0, 0, toRadians(-90), "XYZ")
  yMove.rotation.set(0, 0, 0, "XYZ")
  zMove.rotation.set(toRadians(90), 0, 0, "XYZ")

  xRotate.rotation.set(0, 0, toRadians(-90), "XYZ")
  yRotate.rotation.set(0, 0, 0, "XYZ")
  zRotate.rotation.set(toRadians(90), 0, 0, "XYZ")

  xScale.rotation.set(0, 0, toRadians(-90), "XYZ")
  yScale.rotation.set(0, 0, 0, "XYZ")
  zScale.rotation.set(toRadians(90), 0, 0, "XYZ")

  xLine.rotation.set(0, 0, toRadians(-90), "XYZ")
  zLine.rotation.set(toRadians(90), 0, 0, "XYZ")

  faceLine.rotation.set(0, 0, toRadians(-90), "XYZ")
  for(let i = 0; i < core.children.length; i += 1)
  {
    core.children[i].material.depthTest = false
    core.children[i].renderOrder = 2
  }

  for(let i = 0; i < scaleCore.children.length; i += 1)
  {
    scaleCore.children[i].material.depthTest = false
    scaleCore.children[i].renderOrder = 2
  }

  xLine.material.depthTest = false
  xLine.renderOrder = 1
  yLine.material.depthTest = false
  yLine.renderOrder = 1
  zLine.material.depthTest = false
  zLine.renderOrder = 1

  faceLine.material.depthTest = false
  faceLine.renderOrder = 1

  handler =
  {
    "core": core,
    "xMove": xMove,
    "yMove": yMove,
    "zMove": zMove,

    "xRotate": xRotate,
    "yRotate": yRotate,
    "zRotate": zRotate,

    "scaleCore": scaleCore,

    "xScale": xScale,
    "yScale": yScale,
    "zScale": zScale,

    "xLine": xLine,
    "yLine": yLine,
    "zLine": zLine
  }

  scene.add(handler.core)
  handler.core.visible = false
}

export function switchTo3DView(mode)
{
  if(mode)
  {
    handler.zMove.translateZ(-10000)
    handler.zRotate.translateZ(-10000)
    handler.zScale.translateZ(-10000)
    handler.zLine.translateZ(-10000)

    camera = new THREE.PerspectiveCamera(45, parseInt(canvas.style.width) / parseInt(canvas.style.height), 0.01, 3000)
    camera.position.set(0, 6, 10);
    camera.rotation.set(0, 0, 0, "YZX")
    camera.rotateX(toRadians(-30))

    camera.material = {"opacity" : 1}
  }
  else
  {
    handler.zMove.translateZ(10000)
    handler.zRotate.translateZ(10000)
    handler.zScale.translateZ(10000)
    handler.zLine.translateZ(10000)

    let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

    let height = cameraDistance
    let width = cameraDistance * parseInt(canvas.style.width) / parseInt(canvas.style.height)
    camera = new THREE.OrthographicCamera(width / -2, width / 2, height / 2, height / -2, 0.01, 3000)

    camera.position.set(0, 0, 10)
    camera.rotation.set(0, 0, 0, "YZX")

    camera.material = {"opacity" : 1}
  }

  let cameraDistance = camera.position.distanceTo(new THREE.Vector3())

  if(curTarget != null)
  {
    cameraDistance = camera.position.distanceTo(curTarget.position)
  }

  handler.core.scale.set(cameraDistance / 10, cameraDistance / 10, cameraDistance / 10)
}

export function setCurveCanvasSize(x, y, width, height)
{
  UICanvas.style.left = x + "px"
  UICanvas.style.top = (-window.innerHeight + y) + "px"
  UICanvas.style.height = height + "px"
  UICanvas.style.width = width + "px"
}

export function setCanvasSize(x, y, width, height)
{
  canvas.style.left = x + "px"
  canvas.style.top = (-window.innerHeight + y) + "px"
  canvas.style.height = height + "px"
  canvas.style.width = width + "px"
}

export function drawEaseCurveOnScreen(points)
{
  let curvePoints = []

  for(let i = 0; i < points.length; i += 1)
  {
    curvePoints[i] = new THREE.Vector3(points[i].x, points[i].y, 0)

    easePointsOnScreen[i] = newSphere(points[i].x, points[i].y, 0, 0.05)
    easePointsOnScreen[i].material.emissive = new THREE.Color(1, 0, 0)
    UIScene.add(easePointsOnScreen[i])
  }

  let curve = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")

  let curPoints = curve.getPoints( 50 );
  let geometry = new THREE.BufferGeometry().setFromPoints( curPoints );
  let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );

  easeCurve = new THREE.Line( geometry, material );
  UIScene.add(easeCurve)

  easeArray = points
}

export function updateEaseCurve()
{
  let curvePoints = []
  easeArray = []

  for(let i = 0; i < easePointsOnScreen.length; i += 1)
  {
    easeArray[i] = {}
    easeArray[i].x = easePointsOnScreen[i].position.x
    easeArray[i].y = easePointsOnScreen[i].position.y

    curvePoints[i] = new THREE.Vector3(easePointsOnScreen[i].position.x, easePointsOnScreen[i].position.y, 0)
  }

  let curve = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")
  let curPoints = curve.getPoints( 50 );

  for(let i = 0; i < curPoints.length; i += 1)
  {
    easeCurve.geometry.attributes.position.array[i * 3] = curPoints[i].x
    easeCurve.geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
    easeCurve.geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
  }

  easeCurve.geometry.attributes.position.needsUpdate = true

  if (curTarget != null)
  {
    let preFragment = findPreviousFragment(curTarget, worldFrame)

    if (preFragment == null)
    {
      preFragment = findPreviousFragment(curTarget, worldFrame + 1)
    }

    if (preFragment != null)
    {
      preFragment.posEasePoints = JSON.parse(JSON.stringify(easeArray))
    }
  }
}

export function deleteEaseCurveOnScreen()
{
  UIScene.remove(easeCurve)
  easeCurve.geometry.dispose()
  easeCurve.material.dispose()

  for(let i = 0; i < easePointsOnScreen.length; i += 1)
  {
    UIScene.remove(easePointsOnScreen[i])
    easePointsOnScreen[i].geometry.dispose()
    easePointsOnScreen[i].material.dispose()
  }

  easePointsOnScreen = []
  easeCurve = null
}

export function initUI()
{
  window.oncontextmenu = function(e)
  {
    e.preventDefault()
  }

  slLoader = newUI("textarea", document.getElementById("canvasRoot"))
  slLoader.placeholder = "Save/Load Content"
  changeUIStatus(slLoader, 79, 30, 19, 20)

  systemMode.pic = newImage(20, 90, 30, 10, "", 0, 0, 0, 0, document.getElementById("canvasRoot"))
  systemMode.text = newText(0, 0, mode, systemMode.pic)
  systemMode.text.style.fontSize = "35px"
  systemMode.text.style.color = "black"
  systemMode.text.style.opacity = 0

  systemHelp.pic = newImage(20, 30, 20, 20, "", 0, 0, 0, 0, document.getElementById("canvasRoot"))
  systemHelp.text = newText(0, 0, "", systemHelp.pic)
  systemHelp.text.innerText = StandardHelpText
  systemHelp.text.style.fontSize = "12px"
  systemHelp.text.style.color = "green"
  systemHelp.text.style.opacity = 0

  systemFrame.pic = newImage(20, 21, 30, 10, "", 0, 0, 0, 0, document.getElementById("canvasRoot"))
  systemFrame.text = newText(0, 0, "0", systemFrame.pic)
  systemFrame.text.style.fontSize = "35px"
  systemFrame.text.style.color = "black"
  systemFrame.text.style.opacity = 0

  let loadObjectButton = newUI("input", document.getElementById("canvasRoot"))
  loadObjectButton.type = "file"
  loadObjectButton.multiple = "multiple"
  loadObjectButton.accept = ".fbx,.svg,.png"
  changeUIStatus(loadObjectButton, 80, 24, 20, 4)
  loadObjectButton.onchange = function(content)
  {
    for (let i = 0; i < loadObjectButton.files.length; i += 1)
    {
      let name = loadObjectButton.files[i].name
      let fileType = name.substring(name.length - 3, name.length)

      let url = URL.createObjectURL(loadObjectButton.files[i]);
      uploadedObjectURL[uploadedObjectURL.length] =
      {
        "name": name,
        "url": url
      }

      if (fileType == "fbx")
      {
        load(url, null, name)

        addHelpText("Loaded object file")
      }
      else if (fileType == "svg")
      {
        loadSVG(url, null, name)

        addHelpText("Loaded SVG file")
      }
      else if (fileType == "png")
      {
        let loader = new THREE.TextureLoader();
        let texture = loader.load(url);

        let importObj = newPlane(0, 0, 0, 1, 1, texture, new THREE.Color(1, 1, 1))
        importObj.name = name

        scene.add(importObj)
        objectInfo[objectInfo.length] =
        {
          "object": importObj,
          "id": globalID++
        }

        addHelpText("Loaded image file")

        generateIDList()
      }
      else
      {
        addHelpText("Unsupported file type")
      }
    }
  }

  let colorPickerText = newText(0, 0, "Color picker", newImage(79.2, 55, 15, 5, "", 0, 0, 0, 0, document.getElementById("canvasRoot")))
  colorPickerText.style.opacity = 0.5

  let alphaText = newText(0, 0, "Alpha", newImage(79.2, 75, 15, 5, "", 0, 0, 0, 0, document.getElementById("canvasRoot")))
  alphaText.style.opacity = 0.5

  let movementCurveText = newText(0, 0, "Movement", newImage(80, 86, 15, 5, "", 0, 0, 0, 0, document.getElementById("canvasRoot")))
  movementCurveText.style.opacity = 0.3

  generateIDList()
}

export function lerp1(v1, v2, a)
{
  return v1 + (v2 - v1) * a
}

export function lerp2(v1, v2, a)
{
  let v3 = {}
  v3.x = v1.x * (1 - a) + v2.x * a
  v3.y = v1.y * (1 - a) + v2.y * a

  return v3
}

export function lerp3(v1, v2, a)
{
  let v3 = {}
  v3.x = v1.x * (1 - a) + v2.x * a
  v3.y = v1.y * (1 - a) + v2.y * a
  v3.z = v1.z * (1 - a) + v2.z * a

  return v3
}

export function clamp(v, min, max)
{
  if(v < min)
  {
    v = min
  }

  if(v > max)
  {
    v = max
  }

  return v
}

export function toRadians(degrees)
{
	return degrees * Math.PI / 180
}

export function toDegrees(radians)
{
	return radians * 180 / Math.PI
}

export function newPlane(x, y, z, xS, yS, texture, color, parent)
{
  let obj = new THREE.PlaneGeometry(xS, yS);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCube(x, y, z, xS, yS, zS, texture, color, parent)
{
  let obj = new THREE.BoxGeometry(xS, yS, zS);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCone(x, y, z, r, h, texture, color, parent)
{
  let obj = new THREE.ConeGeometry(r, h, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newSphere(x, y, z, r, texture, color, parent)
{
  var obj = new THREE.SphereGeometry(r, 48, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newCylinder(x, y, z, r, h, texture, color, parent)
{
  var obj = new THREE.CylinderGeometry(r, r, h, 48);

  let material = new THREE.MeshStandardMaterial();
  let mesh = new THREE.Mesh(obj, material);

  setObjectProperty(mesh, x, y, z, texture, color, parent)

  return mesh;
}

export function newLine(v1, v2, color, parent)
{
  const curve = new THREE.LineCurve3(v1, v2)
  const points = curve.getPoints(2);

  const geometry = new THREE.BufferGeometry().setFromPoints( points );
  const material = new THREE.LineBasicMaterial( { color : color } );
  const mesh = new THREE.Line( geometry, material );

  if(parent != null)
  {
    parent.add(mesh)
  }

  return mesh;
}

export function deleteObject(mesh)
{
  for(let i = 0; i < mesh.children.length; i += 1)
  {
    deleteObject(mesh.children[i])
  }

  if(mesh.parent != null)
  {
    mesh.parent.remove(mesh)
  }
  else
  {
    scene.remove(mesh)
  }

  if (mesh.type == "Mesh")
  {
    mesh.geometry.dispose()
    mesh.material.dispose()
  }

  for (let i = 0; i < objectInfo.length; i += 1)
  {
    if (objectInfo[i].object == mesh)
    {
      objectInfo.splice(i, 1)

      break
    }
  }
}

export function cloneAll(object)
{
  let newObj = object.clone()
  newObj.material = object.material.clone()

  newObj.traverse
  (
    function(child)
    {
      if (child.isMesh)
      {
        child.material = child.material.clone()
        child.material.transparent = true
      }
    }
  )

  return newObj
}

export function addBorder(object, r, g, b, a, size, borderTest)
{
  if (object.isMesh)
  {
    var outlineMaterial = new THREE.ShaderMaterial( { side: THREE.BackSide } );
    var outlineMesh = new THREE.Mesh( object.geometry, outlineMaterial );
    outlineMesh.scale.multiplyScalar(1 + size);
    object.add(outlineMesh)

    outlineMaterial.transparent = true

    outlineMaterial.uniforms =
    {
      tEmissive: {type: "texture2D", value: object.material.map},
      borderColor: {type: "vec4", value: new THREE.Vector4(r, g, b, a)},
      borderSize: {type: "float", value: size * 0.5},
      borderTest: {type: "bool", value: borderTest}
    }

    outlineMaterial.vertexShader = vDeleteTexAlpha()
    outlineMaterial.fragmentShader = fDeleteTexAlpha()

    if(a != 1)
    {
      outlineMaterial.opecity = a
    }

    if(object.geometry.type == "PlaneGeometry")
    {
      outlineMaterial.side = THREE.FrontSide
      outlineMesh.position.z -= 0.001
    }

    for (let i = 0; i < objectInfo.length; i++)
    {
      if(objectInfo[i].object == object)
      {
        objectInfo[i].borderObject = outlineMesh
      }
    }
  }
  else
  {
    for (var i = 0; i < object.children.length; i++) {
      addBorder(object.children[i], r, g, b, a, size, borderTest)
    }
  }
}

export function setBorderTest(borderObject, active)
{
  borderObject.material.uniforms.borderTest.value = active
}

export function setObjectProperty(mesh, x, y, z, texture, color, parent)
{
  objectStatus[mesh.id] =
  {
    "curID": -1
  }

  if(texture != null)
  {
    mesh.material.map = texture
  }

  if(color != null)
  {
    mesh.material.color = color
  }

  mesh.position.set(x, y, z)
  mesh.rotation.set(0, 0, 0, "YZX")
  mesh.material.transparent = true

  if(parent != null)
  {
    parent.add(mesh)
  }
}
export function newUI(type, parent)
{
  if(type == null)
  {
    type = "div";
  }

  let ui = document.createElement(type);
  ui.id = globalID++;

  ui.style.position = "absolute";

  if(parent != null)
  {
    parent.appendChild(ui);
  }

  return ui;
}

export function changeUIStatus(ui, x, y, width, height)
{
  ui.style.left = (x / 100 * window.innerWidth) + "px";
  ui.style.top = (y / 100 * window.innerHeight) + "px";
  ui.style.width = (width / 100 * window.innerWidth) + "px";
  ui.style.height = (height / 100 * window.innerHeight) + "px";
}

export function newImage(x, y, width, height, pic, r, g, b, a, parent)
{
  let ui = newUI();
  changeUIStatus(ui, x, y, width, height)
  ui.style.backgroundColor = "rgba(" + r * 255 + "," + g * 255 + "," + b * 255 + "," + a + ")";
  ui.style.backgroundImage = pic;
  ui.style.backgroundSize = "100% 100%";
  ui.style.backgroundRepeat = "no-repeat";
  ui.style.backgroundBlendMode = "Overlay";

  parent.appendChild(ui);

  return ui;
}

export function newText(x, y, what, parent)
{
  let ui = newUI();
  ui.style.position = "relative";
  ui.style.left = x + "%";
  ui.style.top = y + "%";

  let text = document.createTextNode(what);
  ui.appendChild(text);

  parent.appendChild(ui);

  return ui;
}

export function newButton(x, y, width, height, what, pic, r, g, b, a, parent, mode)
{
  let ui = newUI("button");
  changeUIStatus(ui, x, y, width, height)
  ui.style.backgroundColor = "rgba(" + r * 255 + "," + g * 255 + "," + b * 255 + "," + a + ")";
  ui.style.backgroundImage = pic;
  ui.style.backgroundSize = "100% 100%";
  ui.style.backgroundRepeat = "no-repeat";
  ui.style.backgroundBlendMode = "Overlay";

  ui.onmouseout = function()
  {
    ui.style.cursor = "auto";
  };

  ui.onmouseover = function()
  {
    ui.style.cursor = "pointer";
  };

  parent.appendChild(ui);

  let text = newText(0, 0, what, ui)

  if(mode == 1)
  {
    return [ui, text];
  }
  else
  {
    return ui;
  }
}

export function destroy(ui)
{
  ui.parentNode.removeChild(ui);
}

export function addOnClickedListener(ui, callBack)
{
  ui.onclick = function()
  {
    callBack();
  }
}

export function load(path, objSelf, fileName, callBack)
{
  var loader = new FBXLoader();

  loader.load(path, function(object)
  {
    if (objSelf != null)
    {
      objSelf[0] = object;
    }

    if(object.material == null)
    {
      object.material = new THREE.MeshPhongMaterial( { color: 0xb6b311, specular: 0x222222, shininess: 50} );
    }

    object.material.transparent = true
    object.name = fileName

    scene.add(object)
    objectStatus[object.id] =
    {
      "curID": -1
    }
    object.scale.set(0.1, 0.1, 0.1)

    object.traverse( function ( child )
    {
      if ( child.isMesh )
      {
        child.castShadow = true;
        child.receiveShadow = true;

        objectInfo[objectInfo.length] =
        {
          "object": child,
          "id": globalID++
        }
      }
    });

    if(callBack != null)
    {
      callBack()
    }

    generateIDList()
  },
  function()
  {

  },
  function(e)
  {
    console.error( e );
  });
}

export function loadSVG(url, objSelf, fileName, callBack)
{
  const loader = new SVGLoader();

  loader.load( url, function ( data )
  {
    const paths = data.paths;
		const group = new THREE.Group();
    group.scale.set(0.01, 0.01, 0.01)
    group.rotateZ(toRadians(180))

    group.material = new THREE.MeshBasicMaterial();
    group.material.transparent = true



		for ( let i = 0; i < paths.length; i ++ )
    {
			const path = paths[ i ];

			const material = new THREE.MeshBasicMaterial( {
				color: path.color,
				side: THREE.DoubleSide,
				depthWrite: false
			} );

      material.transparent = true

			const shapes = path.toShapes(true);

			for ( let j = 0; j < shapes.length; j ++ )
      {

				const shape = shapes[ j ];
				const geometry = new THREE.ShapeGeometry( shape );
				const mesh = new THREE.Mesh( geometry, material );
				group.add( mesh );

        objectInfo[objectInfo.length] =
        {
          "object": mesh,
          "id": globalID++
        }
			}
		}

    if (objSelf != null)
    {
      objSelf[0] = group;
    }

		scene.add( group );

    objectInfo[objectInfo.length] =
    {
      "object": group,
      "id": globalID++
    }

    group.name = fileName

    objectStatus[group.id] =
    {
      "curID": -1
    }

    if(callBack != null)
    {
      callBack()
    }

    generateIDList()
  } );
}

export function checkExists(mousePos)
{
  return -1 <= mousePos.x && mousePos.x <= 1 && -1 <= mousePos.y && mousePos.y <= 1
}

export function setObjectsVisibilityInScene(visibility, scene)
{
  for(let i = 0; i < objectInfo.length; i += 1)
  {
    objectInfo[i].object.visible = visibility
  }
}

export var light

export function initBasicScene()
{
  initHandler()

  light = new THREE.SpotLight( new THREE.Color(0.588, 0.588, 0.588) );
  light.position.set(0, 2, 15);
  light.castShadow = true;
  light.penumbra = 0.848;
  light.angle = 2.8
  light.intensity = 1.4384;
  light.shadow.mapSize.width = 4096;
  light.shadow.mapSize.height = 4096;
  light.shadow.camera.near = 0.5;
  light.shadow.camera.far = 30;
  scene.add( light );
}

export function clearSceneObjects()
{
  while(objectInfo.length != 0)
  {
    deleteObject(objectInfo[0].object)
  }
}

export function saveJson()
{
  clearAnimationData(animationFragments)

  let savingFragments = JSON.parse(JSON.stringify(animationFragments))
  toBasicStructure(animationFragments, savingFragments)
  slLoader.value = JSON.stringify(savingFragments, null, 4)
}

export function loadJson(jsonString)
{
  clearSceneObjects()
  globalID = 0

  animationFragments = JSON.parse(jsonString)

  let curObjects = []

  //init list first
  for(let i = 0; i < animationFragments.length; i += 1)
  {
    if(curObjects[animationFragments[i].objectInfo.id] == null)
    {
      let isImportedFile = false

      for (var j = 0; j < uploadedObjectURL.length; j += 1)
      {
        if(uploadedObjectURL[j].name == animationFragments[i].objectInfo.name)
        {
          let name = uploadedObjectURL[j].name
          let url = uploadedObjectURL[j].url
          let fileType = name.substring(name.length - 3, name.length)

          isImportedFile = true

          let obj = []

          if (fileType == "fbx")
          {
            load(url, obj, name, function()
            {
              animationFragments[i].object = obj[0]
            })
          }
          else if (fileType == "svg")
          {
            loadSVG(url, obj, name, function()
            {
              animationFragments[i].object = obj[0]
            })
          }
          else if (fileType == "png")
          {
            let loader = new THREE.TextureLoader();
            let texture = loader.load(url);

            let importObj = newPlane(0, 0, 0, 1, 1, texture, new THREE.Color(1, 1, 1))
            importObj.name = name

            scene.add(importObj)
            objectInfo[objectInfo.length] =
            {
              "object": importObj,
              "id": globalID++
            }

            animationFragments[i].object = importObj

            generateIDList()
          }
          else
          {
            addHelpText("Loading file with " + name)
          }

          break
        }
      }

      if (isImportedFile)
      {
        curObjects[animationFragments[i].objectInfo.id] = animationFragments[i].object

        continue
      }
      else if(animationFragments[i].objectInfo.type == "PlaneGeometry")
      {
        animationFragments[i].object = newPlane(0, 0, 0, 1, 1)
      }
      else if (animationFragments[i].objectInfo.type == "SphereGeometry")
      {
        animationFragments[i].object = newSphere(0, 0, 0, 1)
      }
      else if (animationFragments[i].objectInfo.type == "BoxGeometry")
      {
        animationFragments[i].object = newCube(0, 0, 0, 1, 1, 1)
      }

      if(animationFragments[i].objectInfo.texture != null)
      {
        animationFragments[i].object.material.map = animationFragments[i].objectInfo.texture
      }

      if(animationFragments[i].objectInfo.color != null)
      {
        animationFragments[i].object.material.color = new THREE.Color(animationFragments[i].objectInfo.color.x, animationFragments[i].objectInfo.color.y, animationFragments[i].objectInfo.color.z)
      }

      curObjects[animationFragments[i].objectInfo.id] = animationFragments[i].object

      scene.add(animationFragments[i].object)
      objectInfo[objectInfo.length] =
      {
        "object": animationFragments[i].object,
        "id": globalID
      }
    }
    else
    {
      animationFragments[i].object = curObjects[animationFragments[i].objectInfo.id]

      objectStatus[animationFragments[i].object.id].curID = animationFragments[i].id
    }

    globalID = Math.max(globalID, animationFragments[i].id + 1)
    generateIDList()
  }

  //link fragments
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if(animationFragments[i].ballisticMovementNext != null)
    {
      animationFragments[i].ballisticMovementNext = animationFragments[animationFragments[i].ballisticMovementNext]
    }

    if(animationFragments[i].autoRotationNext != null)
    {
      animationFragments[i].autoRotationNext = animationFragments[animationFragments[i].autoRotationNext]
    }

    if(animationFragments[i].parentFragment != null)
    {
      animationFragments[i].parentFragment = animationFragments[animationFragments[i].parentFragment]
    }


    for (let j = 0; j < animationFragments[i].childFragments.length; j += 1)
    {
      animationFragments[i].childFragments[j] = animationFragments[animationFragments[i].childFragments[j]]
    }
  }
}

export function toBasicStructure(fragments, dstFragments)
{
  for(let i = 0; i < fragments.length; i += 1)
  {
    if(fragments[i].ballisticMovementNext != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].ballisticMovementNext == fragments[j])
        {
          dstFragments[i].ballisticMovementNext = j
          break
        }
      }
    }

    if(fragments[i].autoRotationNext != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].autoRotationNext == fragments[j])
        {
          dstFragments[i].autoRotationNext = j
          break
        }
      }
    }

    if(fragments[i].childFragments.length > 0)
    {
      for(let j = 0; j < fragments[i].childFragments.length; j += 1)
      {
        for(let k = 0; k < fragments.length; k += 1)
        {
          if(fragments[i].childFragments[j] == fragments[k])
          {
            dstFragments[i].childFragments[j] = k
            break
          }
        }
      }
    }

    if(fragments[i].parent != null)
    {
      for(let j = 0; j < fragments.length; j += 1)
      {
        if(fragments[i].parent == fragments[j])
        {
          dstFragments[i].parent = j
          break
        }
      }
    }
  }

  for(let i = 0; i < dstFragments.length; i += 1)
  {
    if (dstFragments[i].objectInfo.name != "" && !dstFragments[i].object.isMesh)
    {
      dstFragments[i].objectInfo.id = fragments[i].object.id
    }
    else
    {
      dstFragments[i].objectInfo.id = fragments[i].object.id
      dstFragments[i].objectInfo.type = fragments[i].object.geometry.type
      dstFragments[i].objectInfo.color = {"x": fragments[i].object.material.color.r, "y": fragments[i].object.material.color.g, "z": fragments[i].object.material.color.b}
    }

    dstFragments[i].object = undefined
  }
}

export function atOnePoint(obj, vec3a, vec3b)
{
  return vec3a.x == obj.position.x && vec3a.y == obj.position.y && vec3a.z == obj.position.z ||
    vec3b.x == obj.position.x && vec3b.y == obj.position.y && vec3b.z == obj.position.z
}

export function addHelpText(content)
{
  let curData = {}

  curData.pic = newImage(0, 0, 20, 10, "", 0, 0, 0, 0, document.getElementById("canvasRoot"))
  curData.text = newText(0, 0, content, curData.pic)
  curData.text.style.color = "red"
  curData.duration = 3

  systemInfo.splice(0, 0, curData)
}

export function physicallyEqual(fragment1, fragment2)
{
  let bias = 0.01

  return Math.abs(fragment1.endPosition.x - fragment2.startPosition.x) < bias &&
    Math.abs(fragment1.endPosition.y - fragment2.startPosition.y) < bias &&
    Math.abs(fragment1.endPosition.z - fragment2.startPosition.z) < bias
    // &&
    // Math.abs(fragment1.endEulerAngle.x - fragment2.startEulerAngle.x) < bias &&
    // Math.abs(fragment1.endEulerAngle.y - fragment2.startEulerAngle.y) < bias &&
    // Math.abs(fragment1.endEulerAngle.z - fragment2.startEulerAngle.z) < bias &&
    // Math.abs(fragment1.endScale.x - fragment2.endScale.x) < bias &&
    // Math.abs(fragment1.endScale.y - fragment2.endScale.y) < bias &&
    // Math.abs(fragment1.endScale.z - fragment2.endScale.z) < bias
}

export function vector3Equals(v1, v2)
{
  return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z
}

export function findObjectExistsFragment(object, fragments, mode)
{
  let startFirst = function()
  {
    for (let i = 0; i < fragments.length; i += 1)
    {
      if (vector3Equals(object.position, fragments[i].startPosition) )
      {
        return fragments[i]
      }
    }
  }

  let endFirst = function()
  {
    for (let i = 0; i < fragments.length; i += 1)
    {
      if (vector3Equals(object.position, fragments[i].endPosition) )
      {
        return fragments[i]
      }
    }
  }

  if (mode == 1)
  {
    let curFragment = startFirst()

    if (curFragment != null)
    {
      return curFragment
    }
  }

  return endFirst()
}

export function obtainReviseFragmentsInBothSides(object, count)
{
  let cur = findObjectCurFragment(object)
  let midIndex = -1

  //no fragment for this object
  if (cur == null)
  {
    return
  }

  // left
  while(cur != null)
  {
    let pre = findPreviousFragment(object, cur.startFrame)

    if(pre == null || !physicallyEqual(pre, cur))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = pre
    cur = pre
  }

  //revert
  for(let i = 0; i < anotherWorldFragments.length / 2; i += 1)
  {
    let copy = anotherWorldFragments[i]
    anotherWorldFragments[i] = anotherWorldFragments[anotherWorldFragments.length - i - 1]
    anotherWorldFragments[anotherWorldFragments.length - i - 1] = copy
  }

  cur = findObjectCurFragment(object)

  // middle
  if(cur != null)
  {
    midIndex = anotherWorldFragments.length
    anotherWorldFocusIndex = midIndex
    anotherWorldFragments[anotherWorldFragments.length] = cur
  }

  // right
  while(cur != null)
  {
    let next = findNextFragment(object, cur.startFrame + cur.duration)

    if(next == null || !physicallyEqual(cur, next))
    {
      break
    }

    anotherWorldFragments[anotherWorldFragments.length] = next
    cur = next
  }

  let startIndex = Math.max(0, midIndex - parseInt(count / 2))
  let endIndex = Math.min(anotherWorldFragments.length, midIndex + Math.ceil(count / 2))

  //console.log("org:" + startIndex + " !!! " + midIndex + " !!! " + endIndex + " !!! " + anotherWorldMaxPresent);

  //check if previous continuely curve movement
  for (let i = midIndex; i >= 0 && i >= midIndex - anotherWorldMaxPresent / 2; i -= 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      startIndex = Math.min(startIndex, i)
    }
  }

  for (let i = midIndex; i < anotherWorldFragments.length && i <= midIndex + anotherWorldMaxPresent / 2; i += 1)
  {
    if (anotherWorldFragments[i].ballisticMovementNext != null)
    {
      endIndex = i + 2
    }
  }

  //change max present
  if (startIndex != midIndex - parseInt(count / 2) || endIndex != midIndex + parseInt(count / 2))
  {
    anotherWorldMaxPresent = Math.max(endIndex - midIndex, midIndex - startIndex) * 2
    //console.log(startIndex + " !!! " + midIndex + " !!! " + endIndex + " !!! " + anotherWorldMaxPresent);
  }

  for(let i = startIndex; i < endIndex; i += 1)
  {
    if (i < 0 || i >= anotherWorldFragments.length)
    {
      continue
    }

    let cloneObject = cloneAll(object)

    cloneObject.position.set(anotherWorldFragments[i].startPosition.x, anotherWorldFragments[i].startPosition.y, anotherWorldFragments[i].startPosition.z)

    if (cloneObject.rotationType == "Object")
    {

    }
    else if (cloneObject.rotationType == "Vertex")
    {

    }
    else
    {
      cloneObject.rotation.set(anotherWorldFragments[i].startEulerAngle.x, anotherWorldFragments[i].startEulerAngle.y, anotherWorldFragments[i].startEulerAngle.z)
    }

    cloneObject.scale.set(anotherWorldFragments[i].startScale.x, anotherWorldFragments[i].startScale.y, anotherWorldFragments[i].startScale.z)

    setObjectOpacity(cloneObject, 0.5)
    cloneObject.visible = true

    anotherWorldObjects[anotherWorldObjects.length] = cloneObject
    scene.add(cloneObject)

    //DELETE Border
    for (let j = 0; j < cloneObject.children.length; j += 1)
    {
      if(cloneObject.children[j].isMesh && cloneObject.children[j].material.side == THREE.BackSide)
      {
        deleteObject(cloneObject.children[j])
      }
    }

    if(i == midIndex)
    {
      addBorder(cloneObject, 1, 0, 0, 0.5, 0.05, false)
    }

    if(i == endIndex - 1 || i == anotherWorldFragments.length - 1)
    {
      let cloneObject = cloneAll(object)

      //DELETE Border
      for (let j = 0; j < cloneObject.children.length; j += 1)
      {
        if(cloneObject.children[j].isMesh && cloneObject.children[j].material.side == THREE.BackSide)
        {
          deleteObject(cloneObject.children[j])
        }
      }

      cloneObject.position.set(anotherWorldFragments[i].endPosition.x, anotherWorldFragments[i].endPosition.y, anotherWorldFragments[i].endPosition.z)
      cloneObject.rotation.set(anotherWorldFragments[i].endEulerAngle.x, anotherWorldFragments[i].endEulerAngle.y, anotherWorldFragments[i].endEulerAngle.z, "YZX")
      cloneObject.scale.set(anotherWorldFragments[i].endScale.x, anotherWorldFragments[i].endScale.y, anotherWorldFragments[i].endScale.z)

      setObjectOpacity(cloneObject, 0.5)
      cloneObject.visible = true

      anotherWorldObjects[anotherWorldObjects.length] = cloneObject
      scene.add(cloneObject)
    }
  }

  for(let i = 0; i < anotherWorldObjects.length - 1; i += 1)
  {
    if(anotherWorldFragments[Math.max(0, startIndex) + i].ballisticMovementNext == null)
    {
      let line = new THREE.LineCurve3(anotherWorldObjects[i].position, anotherWorldObjects[i + 1].position)

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      anotherWorldLines[i] = curveObject
      scene.add(curveObject)
    }
    else
    {
      let curvePoints = []

      while (i < anotherWorldObjects.length - 1)
      {
        let curPos = new THREE.Vector3()
        curPos.x = anotherWorldFragments[startIndex + i].startPosition.x
        curPos.y = anotherWorldFragments[startIndex + i].startPosition.y
        curPos.z = anotherWorldFragments[startIndex + i].startPosition.z
        curvePoints[curvePoints.length] = curPos

        if(anotherWorldFragments[startIndex + i].ballisticMovementNext == null)
        {
          curPos = new THREE.Vector3()
          curPos.x = anotherWorldFragments[startIndex + i].endPosition.x
          curPos.y = anotherWorldFragments[startIndex + i].endPosition.y
          curPos.z = anotherWorldFragments[startIndex + i].endPosition.z
          curvePoints[curvePoints.length] = curPos

          break
        }

        i += 1
      }

      let line = new THREE.CatmullRomCurve3(curvePoints, false, "chordal")

      let points = line.getPoints(50);
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      curveObject.material.transparent = true
      curveObject.material.opacity = 0.5

      for(let j = i; j >= 0; j -= 1)
      {
        if(anotherWorldLines[j] != null)
        {
          break
        }

        anotherWorldLines[j] = curveObject
      }

      scene.add(curveObject)
    }
  }
}

export function updateObjectsInFrame(object, curves, tObjects, fragments)
{
  if(object == thisWorldObject)
  {
    return
  }

  let curIndex = -1
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))

  for(let i = 0; i < tObjects.length; i += 1)
  {
    if(object == tObjects[i])
    {
      curIndex = i

      break
    }
  }

  let curveMovementPoints = []
  let isEntered = false
  let updated = false

  //console.log(curIndex + " " + prefix + " " + anotherWorldMaxPresent + " " + anotherWorldFocusIndex);

  //obtain the curve movements points that close to the mid point
  for(let i = 0; i < curves.length; i += 1)
  {
    //check if this is the last element
    if (fragments[prefix + i].ballisticMovementNext != null || curveMovementPoints.length > 0 && fragments[prefix + i].ballisticMovementNext == null)
    {
      curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
        fragments[prefix + i].startPosition.x,
        fragments[prefix + i].startPosition.y,
        fragments[prefix + i].startPosition.z
      )
    }

    if (Math.min(curves.length - 1, curIndex) == i && curveMovementPoints.length > 0)
    {
      isEntered = true
    }

    if (fragments[prefix + i].ballisticMovementNext == null && curveMovementPoints.length > 0)
    {
      if (isEntered)
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + i].endPosition.x,
          fragments[prefix + i].endPosition.y,
          fragments[prefix + i].endPosition.z
        )
      }
      else
      {
        curveMovementPoints = []
      }
    }
  }

  if(0 <= prefix + curIndex - 1)
  {
    fragments[prefix + curIndex - 1].endPosition.x = object.position.x
    fragments[prefix + curIndex - 1].endPosition.y = object.position.y
    fragments[prefix + curIndex - 1].endPosition.z = object.position.z

    fragments[prefix + curIndex - 1].endEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex - 1].endEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex - 1].endEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex - 1].endScale.x = object.scale.x
    fragments[prefix + curIndex - 1].endScale.y = object.scale.y
    fragments[prefix + curIndex - 1].endScale.z = object.scale.z

    fragments[prefix + curIndex - 1].endAlpha = object.material.opacity

    //pre
    if(0 <= curIndex - 1)
    {
      let curve
      let curPoints

      if(fragments[prefix + curIndex - 1].movementType != "Ballist")
      {
        curve = new THREE.LineCurve3(tObjects[curIndex - 1].position, object.position)
        curPoints = curve.getPoints( 50 );
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
        curPoints = curve.getPoints((curveMovementPoints.length - 1) * 50)

        updated = true
      }

      curves[curIndex - 1].geometry.attributes.position.count = curPoints.length
      curves[curIndex - 1].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex - 1].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex - 1].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex - 1].geometry.attributes.position.needsUpdate = true
    }
  }

  //next
  //prevent curve movement double update
  if (prefix + curIndex < fragments.length || curveMovementPoints.length > 0 && !updated)
  {
    fragments[prefix + curIndex].startPosition.x = object.position.x
    fragments[prefix + curIndex].startPosition.y = object.position.y
    fragments[prefix + curIndex].startPosition.z = object.position.z

    fragments[prefix + curIndex].startEulerAngle.x = object.rotation.x
    fragments[prefix + curIndex].startEulerAngle.y = object.rotation.y
    fragments[prefix + curIndex].startEulerAngle.z = object.rotation.z

    fragments[prefix + curIndex].startScale.x = object.scale.x
    fragments[prefix + curIndex].startScale.y = object.scale.y
    fragments[prefix + curIndex].startScale.z = object.scale.z

    fragments[prefix + curIndex].startAlpha = object.material.opacity

    if(curIndex < anotherWorldLines.length)
    {
      let curve
      let curPoints

      if(fragments[prefix + curIndex].movementType != "Ballist")
      {
        curve = new THREE.LineCurve3(object.position, tObjects[curIndex + 1].position)
        curPoints = curve.getPoints( 50 );
      }
      else
      {
        curve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
        curPoints = curve.getPoints((curveMovementPoints.length - 1) * 50)
      }

      curves[curIndex].geometry.attributes.position.count = curPoints.length
      curves[curIndex].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex].geometry.attributes.position.needsUpdate = true
    }
  }
}

/**
 * View a scene at specific frame, note that initAnimationLoop()
 * should be called first
 */
export function viewAnimationStatusAt(curFrame)
{
  for (let i = 0; i < animationFragments.length; i += 1)
  {
    if (animationFragments[i].startFrame <= curFrame && curFrame < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      for (let j = 0; i < animationFragments[i].callBefore.length; j += 1)
      {
        animationFragments[i].callBefore[i]()
      }

      if (curFrame == animationFragments[i].startFrame + animationFragments[i].duration - 1)
      {
        for (let j = 0; i < animationFragments[i].callBack.length; j += 1)
        {
          animationFragments[i].callBack[i]()
        }
      }

      setFragmentStatusAt(animationFragments[i], curFrame - animationFragments[i].startFrame)
    }
  }
}

export function EditStartFrameFrom(fragment, durationChangeValue)
{
  if (fragment == null)
  {
    return
  }

  EditStartFrameFrom(findNextFragment(fragment.object, fragment.startFrame + fragment.duration), durationChangeValue)
  fragment.startFrame += durationChangeValue
}

export function deleteFragmentIn(fragment, fragments)
{
  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i] == fragment)
    {
      fragments.splice(i, 1)

      break
    }
  }
}

export function editHelpText(mode)
{
  if(mode == "Standard")
  {
    systemHelp.text.innerText = StandardHelpText
  }
  else if(mode == "Revise")
  {
    systemHelp.text.innerText = ReviseHelpText
  }
  else if (mode == "Animation" || mode == "Animation (Root)")
  {
    systemHelp.text.innerText = AnimationHelpText
  }
}

/**
 * point : [in] Vector2 : NewKeyframe's location
 * curves : [in] Object3D Array : Lines / Easing curves
 * tObjects : [in/out] Object3D Array: Temperory objects
 * fragments : [in/out] AnimationFragments Json Array : fragments to insert a new fragment
 */
export function insertFrame(point, curves, tObjects, fragments)
{
  let min=Number.MAX_VALUE
  let index = -1
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))

  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].startFrame < worldFrame && worldFrame < fragments[i].startFrame + fragments[i].duration)
    {
      index = i + prefix

      break
    }
  }

  if(index != -1)
  {
    let intersects = new THREE.Vector3()

    raycaster.setFromCamera(point, camera)
    raycaster.ray.intersectPlane(plane, intersects)
    let point3D = new THREE.Vector3(intersects.x, intersects.y, intersects.z)

    let newObject = cloneAll(thisWorldObject)
    newObject.position.set(point3D.x, point3D.y, point3D.z)
    newObject.material.visible = true
    setObjectOpacity(newObject, 0.5)
    newObject.material.transparent = true

    let newFragment =
    {
      "id": globalID++,
      "object": thisWorldObject,
      "objectInfo":
      {
        "name": fragments[index].object.name,
        "vertex": [],
        "posWeight": [],
        "rotationWeight": [],
        "scaleWeight": [],
        "alphaWeight": [],
      },

      "parentFragment": undefined,
      "childFragments": [],

      "startPosition": {"x": newObject.position.x, "y": newObject.position.y, "z": newObject.position.z},
      "endPosition": {"x": fragments[index].endPosition.x, "y": fragments[index].endPosition.y, "z": fragments[index].endPosition.z},
      "startEulerAngle": null,
      "endEulerAngle": {"x": fragments[index].endEulerAngle.x, "y": fragments[index].endEulerAngle.y, "z": fragments[index].endEulerAngle.z},
      "startScale": null,
      "endScale": {"x": fragments[index].endScale.x, "y": fragments[index].endScale.y, "z": fragments[index].endScale.z},
      "startAlpha": null,
      "endAlpha": fragments[index].endAlpha,

      "activeBefore": 1,
      "activeAfter": 1,

      "callBefore": [],
      "callBack": [],

      "startFrame": worldFrame,
      "duration": fragments[index].startFrame + fragments[index].duration - worldFrame,

      "posEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "rotationEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "scaleEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],
      "alphaEasePoints": [{"x": 0, "y": 0}, {"x": 1, "y": 1}],

      "movementType": fragments[index].movementType,
      "ballisticMovementNext": fragments[index].ballisticMovementNext,

      "rotationType": fragments[index].rotationType,
      "autoRotationNext": fragments[index].autoRotationNext
    }

    let insertWidth = (worldFrame - fragments[index].startFrame) / fragments[index].duration

    //update two closest frames' info
    if (fragments[index].ballisticMovementNext != undefined || fragments[index - 1] != null && fragments[index - 1].ballisticMovementNext != undefined)
    {
      fragments[index].ballisticMovementNext = newFragment
    }

    if (fragments[index].autoRotationNext != undefined || fragments[index - 1] != null && fragments[index - 1].autoRotationNext != undefined)
    {
      fragments[index].autoRotationNext = newFragment
    }

    let easeCurvesToUpdate = [fragments[index].posEasePoints, fragments[index].rotationEasePoints, fragments[index].scaleEasePoints, fragments[index].alphaEasePoints]
    let easeCurvesToChange = [newFragment.posEasePoints, newFragment.rotationEasePoints, newFragment.scaleEasePoints, newFragment.alphaEasePoints]

    for(let i = 0; i < 4; i += 1)
    {
      let curCurvePosition = []

      for(let j = 0; j < easeCurvesToUpdate[i].length; j += 1)
      {
        curCurvePosition[j] = new THREE.Vector3(easeCurvesToUpdate[i][j].x, easeCurvesToUpdate[i][j].y, 0)
      }

      let curve = new THREE.CatmullRomCurve3(curCurvePosition, false, "chordal")
      let insertHeight = curve.getPointAt(insertWidth).y

      if (i == 1)
      {
        fragments[index].endEulerAngle = lerp3(fragments[index].startEulerAngle, fragments[index].endEulerAngle, insertHeight)
        newFragment.startEulerAngle = lerp3(fragments[index].startEulerAngle, fragments[index].endEulerAngle, insertHeight)
      }
      else if(i == 2)
      {
        fragments[index].endScale = lerp3(fragments[index].startScale, fragments[index].endScale, insertHeight)
        newFragment.startScale = lerp3(fragments[index].startScale, fragments[index].endScale, insertHeight)
      }
      else if (i == 3)
      {
        fragments[index].endAlpha = lerp1(fragments[index].startAlpha, fragments[index].endAlpha, insertHeight)
        newFragment.startAlpha = lerp1(fragments[index].startAlpha, fragments[index].endAlpha, insertHeight)
      }

      let leftCurve = []
      let rightCurve = []

      for (var j = 0; j < easeCurvesToUpdate[i].length; j++)
      {
        if (easeCurvesToUpdate[i][j].x < insertWidth)
        {
          leftCurve[leftCurve.length] = easeCurvesToUpdate[i][j]
        }
        else if (easeCurvesToUpdate[i][j].x > insertWidth)
        {
          rightCurve[rightCurve.length] = easeCurvesToUpdate[i][j]
        }
      }

      leftCurve[leftCurve.length] = {"x": insertWidth, "y": insertHeight}
      rightCurve.splice(0, 0, {"x": insertWidth, "y": insertHeight});

      //check validibility
      for (let j = 0; j < leftCurve.length; j += 1)
      {
        if (leftCurve[j].y < 0 || insertHeight < leftCurve[j].y)
        {
          deleteObject(newObject)

          return "Invalid fragment because an irregular interpolation curve"
        }
      }

      for (let j = 0; j < rightCurve.length; j += 1)
      {
        if (rightCurve[j].y < insertHeight || 1 < rightCurve[j].y)
        {
          deleteObject(newObject)

          return "Invalid fragment because an irregular interpolation curve"
        }
      }

      //normalize left
      for (var j = 0; j < leftCurve.length; j += 1)
      {
        leftCurve[j].x = Math.round(leftCurve[j].x / insertWidth * 1000) / 1000
        leftCurve[j].y = Math.round(leftCurve[j].y / insertHeight * 1000) / 1000
      }

      //normalize right
      for (var j = 0; j < rightCurve.length; j += 1)
      {
        rightCurve[j].x = Math.round((rightCurve[j].x - insertWidth) / (1 - insertWidth) * 1000) / 1000
        rightCurve[j].y = Math.round((rightCurve[j].y - insertHeight) / (1 - insertHeight) * 1000) / 1000
      }

      easeCurvesToUpdate[i] = leftCurve
      easeCurvesToChange[i] = rightCurve
    }

    //update left curve Object
    //if curve movement
    if (fragments[index].ballisticMovementNext != undefined || fragments[index - 1] != null && fragments[index - 1].ballisticMovementNext != undefined)
    {
      let curveMovementPoints = []

      //record current frame's position
      for (let j = 0; j < curves.length; j += 1)
      {
        if (curves[j] == curves[index - prefix])
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            fragments[prefix + j].startPosition.x,
            fragments[prefix + j].startPosition.y,
            fragments[prefix + j].startPosition.z
          )
        }

        //mid
        if (j == index - prefix)
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            newFragment.startPosition.x,
            newFragment.startPosition.y,
            newFragment.startPosition.z
          )
        }

        //not or last
        if (curveMovementPoints.length > 0 && curves[j] != curves[index - prefix] || j == curves.length - 1)
        {
          curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
            fragments[prefix + j].endPosition.x,
            fragments[prefix + j].endPosition.y,
            fragments[prefix + j].endPosition.z
          )

          break
        }
      }

      let curCurve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
      let curPoints = curCurve.getPoints((curveMovementPoints.length - 1) * 50)

      curves[index - prefix].geometry.attributes.position.count = curPoints.length
      curves[index - prefix].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[index - prefix].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[index - prefix].geometry.attributes.position.needsUpdate = true

      curves.splice(index - prefix + 1, 0, curves[index - prefix])
    }
    //normal line movement
    else
    {
      let curCurve = new THREE.LineCurve3(tObjects[index - prefix].position, newObject.position)
      let curPoints = curCurve.getPoints(50)

      curves[index - prefix].geometry.attributes.position.count = curPoints.length
      curves[index - prefix].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[index - prefix].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[index - prefix].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[index - prefix].geometry.attributes.position.needsUpdate = true

      let newCurve = new THREE.LineCurve3(newObject.position, new THREE.Vector3(newFragment.endPosition.x, newFragment.endPosition.y, newFragment.endPosition.z))
      let points = newCurve.getPoints( 50 );
      let geometry = new THREE.BufferGeometry().setFromPoints( points );
      let material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
      let curveObject = new THREE.Line( geometry, material );

      scene.add(curveObject)
      curves.splice(index - prefix + 1, 0, curveObject)
    }

    //update basic info
    fragments[index].endPosition.x = newObject.position.x
    fragments[index].endPosition.y = newObject.position.y
    fragments[index].endPosition.z = newObject.position.z
    fragments[index].duration = worldFrame - fragments[index].startFrame

    anotherWorldMaxPresent += 2

    if (index < anotherWorldFocusIndex)
    {
        anotherWorldMaxPresent += 1
    }

    //insert new info
    tObjects.splice(index - prefix + 1, 0, newObject)
    fragments.splice(index + 1, 0, newFragment)
    scene.add(newObject)
    animationFragments[animationFragments.length] = newFragment

    return "OK"
  }

  return "No fragment have found for this object"
}

export function deleteFrame(object, curves, tObjects, fragments)
{
  let prefix = Math.max(0, anotherWorldFocusIndex - parseInt(anotherWorldMaxPresent / 2))
  let curIndex = -1

  let curveMovementStartIndex = -1

  for (let i = 0; i < tObjects.length; i += 1)
  {
    if(object == tObjects[i])
    {
      curIndex = i + prefix
      break
    }
  }

  //first object
  if(curIndex - prefix == 0)
  {
    if (fragments[curIndex].ballisticMovementNext != null)
    {
      curveMovementStartIndex = 0
    }
    else
    {
      deleteObject(curves[0])
    }

    //if current fragment is this fragment, change to the
    //next one
    if (fragments.length > 1 && fragments[curIndex] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex + 1].id
    }
    else
    {
      objectStatus[thisWorldObject.id].curID = -1
    }

    deleteObject(tObjects[0])
    deleteFragmentIn(fragments[curIndex], animationFragments)

    tObjects.splice(0, 1)
    curves.splice(0, 1)
    fragments.splice(curIndex, 1)
  }
  //last object
  else if (curIndex - prefix == tObjects.length - 1)
  {
    if (fragments[curIndex - 2] != null && fragments[curIndex - 2].ballisticMovementNext != null)
    {
      fragments[curIndex - 2].ballisticMovementNext = null

      curveMovementStartIndex = curIndex - 2
    }
    else
    {
      deleteObject(curves[curIndex - prefix - 1])
    }

    //if current fragment is this fragment, change to the
    //previous one
    if (fragments[curIndex - 2] != null && fragments[curIndex - 1] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex - 2].id
    }
    else
    {
      objectStatus[thisWorldObject.id].curID = -1
    }

    deleteObject(tObjects[curIndex - prefix])
    deleteFragmentIn(fragments[curIndex - 1], animationFragments)

    tObjects.splice(curIndex - prefix, 1)
    curves.splice(curIndex - prefix - 1, 1)
    fragments.splice(curIndex - 1, 1)
  }
  //mid
  else
  {
    if (fragments[curIndex - 1].ballisticMovementNext != null)
    {
      fragments[curIndex - 1].ballisticMovementNext = fragments[curIndex + 1]
      fragments[curIndex - 1].autoRotationNext = fragments[curIndex + 1]

      curveMovementStartIndex = curIndex - 1
    }
    //line reconnection
    else
    {
      let curCurve = new THREE.LineCurve3(tObjects[curIndex - prefix - 1].position, tObjects[curIndex - prefix + 1].position)
      let curPoints = curCurve.getPoints(50)

      curves[curIndex - prefix - 1].geometry.attributes.position.count = curPoints.length
      curves[curIndex - prefix - 1].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

      for(let i = 0; i < curPoints.length; i += 1)
      {
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3] = curPoints[i].x
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
        curves[curIndex - prefix - 1].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
      }

      curves[curIndex - prefix - 1].geometry.attributes.position.needsUpdate = true

      deleteObject(curves[curIndex - prefix])
    }

    //adjust easing
    let easeCurvesToUpdate = [fragments[curIndex - 1].posEasePoints, fragments[curIndex - 1].rotationEasePoints, fragments[curIndex - 1].scaleEasePoints, fragments[curIndex - 1].alphaEasePoints]
    let easeCurvesToDelete = [fragments[curIndex].posEasePoints, fragments[curIndex].rotationEasePoints, fragments[curIndex].scaleEasePoints, fragments[curIndex].alphaEasePoints]
    let insertWidth = fragments[curIndex].duration / (fragments[curIndex].duration + fragments[curIndex - 1].duration)

    for(let i = 0; i < 4; i += 1)
    {
      for (let j = 0; j < easeCurvesToUpdate[i].length; j += 1)
      {
        easeCurvesToUpdate[i][j].x = lerp1(0, insertWidth, easeCurvesToUpdate[i][j].x)
        easeCurvesToUpdate[i][j].y = lerp1(0, insertWidth, easeCurvesToUpdate[i][j].y)
      }

      for (let j = 1; j < easeCurvesToDelete[i].length; j += 1)
      {
        easeCurvesToUpdate[i][easeCurvesToUpdate[i].length] =
        {
          "x": lerp1(insertWidth, 1, easeCurvesToDelete[i][j].x),
          "y": lerp1(insertWidth, 1, easeCurvesToDelete[i][j].y)
        }
      }
    }

    fragments[curIndex - 1].endPosition = fragments[curIndex].endPosition
    fragments[curIndex - 1].endEulerAngle = fragments[curIndex].endEulerAngle
    fragments[curIndex - 1].endScale = fragments[curIndex].endScale
    fragments[curIndex - 1].endAlpha = fragments[curIndex].endAlpha
    fragments[curIndex - 1].duration += fragments[curIndex].duration

    //if current fragment is this fragment, change to the
    //previous one
    if (fragments[curIndex] == findObjectCurFragment(thisWorldObject))
    {
      objectStatus[thisWorldObject.id].curID = fragments[curIndex - 1].id
    }

    deleteObject(tObjects[curIndex - prefix])
    deleteFragmentIn(fragments[curIndex], animationFragments)

    tObjects.splice(curIndex - prefix, 1)
    curves.splice(curIndex - prefix, 1)
    fragments.splice(curIndex, 1)
  }

  //clear final object if nothing left
  if (fragments.length == 0)
  {
    deleteObject(tObjects[0])
  }

  //handle curve movement changes
  if (curveMovementStartIndex != -1)
  {
    let curveMovementPoints = []

    //record current frame's position
    for (let j = 0; j < curves.length; j += 1)
    {
      if (curves[j] == curves[curveMovementStartIndex])
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + j].startPosition.x,
          fragments[prefix + j].startPosition.y,
          fragments[prefix + j].startPosition.z
        )
      }

      //not or last
      if (curveMovementPoints.length > 0 && curves[j] != curves[curveMovementStartIndex] || j == curves.length - 1)
      {
        curveMovementPoints[curveMovementPoints.length] = new THREE.Vector3(
          fragments[prefix + j].endPosition.x,
          fragments[prefix + j].endPosition.y,
          fragments[prefix + j].endPosition.z
        )

        break
      }
    }

    let curCurve = new THREE.CatmullRomCurve3(curveMovementPoints, false, "chordal")
    let curPoints = curCurve.getPoints((curveMovementPoints.length - 1) * 50)

    curves[curveMovementStartIndex].geometry.attributes.position.count = curPoints.length
    curves[curveMovementStartIndex].geometry.attributes.position.array = new Float32Array(curPoints.length * 3)

    for(let i = 0; i < curPoints.length; i += 1)
    {
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3] = curPoints[i].x
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3 + 1] = curPoints[i].y
      curves[curveMovementStartIndex].geometry.attributes.position.array[i * 3 + 2] = curPoints[i].z
    }

    curves[curveMovementStartIndex].geometry.attributes.position.needsUpdate = true
  }
}

export function traverseAnimationFragments(from, to)
{
  let sequenceFragments = [from]
  let cur = findPreviousFragment(from.object, from.startFrame)

  while (cur != null)
  {
    sequenceFragments[sequenceFragments.length] = cur

    if (cur == to)
    {
      return sequenceFragments
    }

    cur = findPreviousFragment(cur.object, cur.startFrame)
  }

  sequenceFragments = [from]
  cur = findNextFragment(from.object, from.startFrame + from.duration)

  while (cur != null)
  {
    sequenceFragments[sequenceFragments.length] = cur

    if (cur == to)
    {
      return sequenceFragments
    }

    cur = findNextFragment(cur.object, cur.startFrame + from.duration)
  }

  return [from]
}

export function generateColorPickerShader()
{
  let vRGBShader = `
    varying vec2 vUv;

    void main()
    {
       vUv = uv;
       vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
       gl_Position = projectionMatrix * mvPosition;
    }
  `

  let fRGBShader = `
    varying vec2 vUv;

    float getUVW(vec2[3] vertexPostion, vec2 vUv, int i)
    {
      int j = i + 1;

      if(i == 2)
      {
        j = 0;
      }

      vec3 v1 = vec3(abs(vertexPostion[j] - vertexPostion[i]), 0.0);
      vec3 v2 = vec3(abs(vUv - vertexPostion[i]), 0);
      return length(cross(v1, v2)) / 2.0;
    }

    vec3 findRGB(float x, float y)
    {
      vec3 n = vec3(0.0, 0.0, 0.0);
      vec3 r = vec3(1.0, 0.0, 0.0);
      vec3 g = vec3(0.0, 1.0, 0.0);
      vec3 b = vec3(0.0, 0.0, 1.0);

      vec2 vertexPostion[3];
      vec3 vertexColor[3];

      if(x < y)
      {
        vertexPostion = vec2[]
        (
          vec2(0.0, 1.0),
          vec2(1.0, 1.0),
          vec2(0.0, 0.0)
        );

        vertexColor = vec3[](r, n, g);
      }
      else
      {
        vertexPostion = vec2[]
        (
          vec2(1.0, 0.0),
          vec2(1.0, 1.0),
          vec2(0.0, 0.0)
        );

        vertexColor = vec3[](r, b, g);
      }

      float u = getUVW(vertexPostion, vUv, 0);
      float v = getUVW(vertexPostion, vUv, 1);
      float w = getUVW(vertexPostion, vUv, 2);

      float rResult = u * vertexColor[0].x + v * vertexColor[1].x + w * vertexColor[2].x;
      float gResult = u * vertexColor[0].y + v * vertexColor[1].y + w * vertexColor[2].y;
      float bResult = u * vertexColor[0].z + v * vertexColor[1].z + w * vertexColor[2].z;

      return vec3(rResult, gResult, bResult);
    }

    void main()
    {
      gl_FragColor = vec4(findRGB(vUv.x, vUv.y), 1.0);
    }
  `

  let fAShader = `
    varying vec2 vUv;

    vec3 lerp(vec3 v1, vec3 v2, float a)
    {
      return v1 + (v2 - v1) * a;
    }

    void main()
    {
      gl_FragColor = vec4(1.0, 1.0, 1.0, lerp(vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 1.0), vUv.x));
    }
  `

  let geometry = new THREE.PlaneGeometry(3, 1.5);
  let material = new THREE.ShaderMaterial();
  let mesh = new THREE.Mesh( geometry, material );
  mesh.position.set(1.5, 2.25, -1)

  material.vertexShader = vRGBShader
  material.fragmentShader = fRGBShader

  UIScene.add(mesh)

  let aGeometry = new THREE.PlaneGeometry(3, 0.15);
  let aMaterial = new THREE.ShaderMaterial();
  let aMesh = new THREE.Mesh( aGeometry, aMaterial );
  aMesh.position.set(1.5, 1.35, -1)

  aMaterial.vertexShader = vRGBShader
  aMaterial.fragmentShader = fAShader
  aMaterial.transparent = true

  UIScene.add(aMesh)
}

export var tableLayout

export function generateIDList()
{
  if (tableLayout != null)
  {
    tableLayout.replaceChildren()
  }

  tableLayout = newUI("td", document.getElementById("canvasRoot"))
  tableLayout.style.overflow = "scroll"
  changeUIStatus(tableLayout, 0.25, 37, 18, 55)

  let IDTable = newUI("table", tableLayout)

  let title = newUI("tr", IDTable)
  title.style.position = null
  title.style.fontSize = 30 + "px"
  title.innerHTML = "Content IDs"

  let curContent = buildObject_FragmentList()

  for (let i = 0; i < curContent.length; i += 1)
  {
    let object = newUI("tr", IDTable)
    object.style.position = null
    object.innerHTML = voidSpace(6) + "|-- " + curContent[i].objectInfo.id + " : Object "

    for (let j = 0; j < curContent[i].fragments.length; j += 1)
    {
      let object = newUI("tr", IDTable)
      object.style.position = null
      object.innerHTML = voidSpace(12) + "|-- " + curContent[i].fragments[j].id + ": Keyframe: " +
            (curContent[i].fragments[j].startFrame + curContent[i].fragments[j].duration)
    }
  }
}

export function voidSpace(num)
{
  let string = ""

  for (var i = 0; i < num; i++)
  {
    string += "&nbsp;"
  }

  return string
}

export function buildObject_FragmentList()
{
  let curValidObjects = []

  for (let i = 0; i < objectInfo.length; i++)
  {
    if (objectInfo[i].object.parent == scene)
    {
      curValidObjects[curValidObjects.length] =
      {
        "objectInfo": objectInfo[i],
        "fragments": []
      }
    }
  }

  for (let i = 0; i < animationFragments.length; i += 1)
  {
    for (let j = 0; j < curValidObjects.length; j += 1)
    {
      if (animationFragments[i].object == curValidObjects[j].objectInfo.object)
      {
        curValidObjects[j].fragments[curValidObjects[j].fragments.length] = animationFragments[i]
      }
    }
  }

  for (let i = 0; i < curValidObjects.length; i += 1)
  {
    sortObjectFragmentsWithStartTime(curValidObjects[i].fragments)
  }

  return curValidObjects
}

export function sortObjectFragmentsWithStartTime(fragments)
{
  for (let i = 0; i < fragments.length; i += 1)
  {
    for (let j = i + 1; j < fragments.length; j += 1)
    {
      if (fragments[i].startFrame > fragments[j].startFrame)
      {
        let copy = fragments[i]
        fragments[i] = fragments[j]
        fragments[j] = copy
      }
    }
  }
}

export function setRotationMode(fragments, mode)
{
  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].rotationType == mode)
    {
      fragments[i].rotationType = "Normal"
      fragments[i].autoRotationNext = undefined
    }
    else
    {
      fragments[i].rotationType = mode
      fragments[i].autoRotationNext = fragments[i + 1]
    }
  }
}

export function generateAutoRotationCurve(fragments)
{

  for (let i = 0; i < fragments.length; i += 1)
  {
    if (fragments[i].ballisticMovementNext != null)
    {
      fragments[i].autoRotationCurve = fragments[i].movementCurve
    }
    else
    {
      let startPosition = new THREE.Vector3(
        fragments[i].startPosition.x,
        fragments[i].startPosition.y,
        fragments[i].startPosition.z
      )
      let endPosition = new THREE.Vector3(
        fragments[i].endPosition.x,
        fragments[i].endPosition.y,
        fragments[i].endPosition.z
      )

      let curve = new THREE.LineCurve3(startPosition, endPosition)

      fragments[i].autoRotationCurve = curve
    }
  }

}

export function animation_objectBaseRotationCurve(curFragment)
{

}

export function animation_vertexBaseRotationCurve(curFragment)
{

}

export function getParentObject(childObject)
{
  if (childObject.parent == scene)
  {
    return childObject
  }

  return getParentObject(childObject.parent)
}


export var sequences = []
export var variables = []
export var curSequenceName
export var curMacroName
export var curAnimationFragment

export var commandAnimating = false

export function circle(x, y, z, r)
{
    let obj = newSphere(x, y, z, r, null, new THREE.Color(0, 1, 1))

    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }

    scene.add(obj)

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        circle(x,y,z,r)
      }
    }

    generateIDList()
}
//rect [<x> <y> [z]] [<w> <h>]
export function rect(x, y, z, w, h, l)
{
  let obj = newCube(x, y, z, w, h, l, null, new THREE.Color(0, 1, 1))

  objectInfo[objectInfo.length] =
  {
    "object": obj,
    "id": globalID++
  }

  scene.add(obj)

  if (!commandAnimating && curAnimationFragment != null)
  {
    curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
    {
      rect(x, y, z, w, h, l)
    }
  }
}
//line [<x> <y> [z]] [length]
export function line(x, y, z, length)
{
    let v1 = new THREE.Vector3(x + -length / 2, y + -length / 2, z + -length / 2)
    let v2 = new THREE.Vector3(x + length / 2, y + length / 2, z + length / 2)

    let obj = newLine(v1, v2, new THREE.Color(0, 0, 0))

    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }

    objectStatus[obj.id] =
    {
      "curID": -1
    }

    scene.add(obj)

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        line(x, y, z, length)
      }
    }
}
//line [<x> <y> [z] / <object_id>] [<x> <y> [z] / <object_id>] creates a new line between given objects or points
export function line2(x,y,z,x2,y2,z2)
{
    let v1=new THREE.Vector3(x,y,z)
    let v2=new THREE.Vector3(x2,y2,z2)
    let obj = newLine(v1,v2,new THREE.Color(0, 0, 0))
    objectInfo[objectInfo.length] =
    {
      "object": obj,
      "id": globalID++
    }
    objectStatus[obj.id] =
    {
      "curID": -1
    }

    scene.add(obj)

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        line2(x,y,z,x2,y2,z2)
      }
    }
}
//empty [<x> <y> [z]] creates an "empty object" at the given position for use with other objects (like creating lines to the empty, etc.)
export function empty(x,y,z)
{
    let a = new THREE.Group()
    a.position.set(x,y,z)
    scene.add(a)

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        empty(x,y,z)
      }
    }
}
//group <object_id> <object_id> ... groups infinitely many objects together; this creates a unique object_id for the group; grouping groups together ungroups previous groups and creates a union of the groups; individual objects can still be manipulated by invoking their ID
export function group(ids){
     var group = new THREE.Group();
     objectInfo[objectInfo.length] =
     {
          "object": group,
          "id": globalID++
     }
     for (let i = 0; i < ids.length; i += 1)
        {
          for (var j = 0; j < objectInfo.length; j += 1)
          {
            if (objectInfo[j].id == ids[i])
            {
              group.add(objectInfo[j].object)
            }
          }
     }

     scene.add(group)

     if (!commandAnimating && curAnimationFragment != null)
     {
       curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
       {
         group(ids)
       }
     }
}
//ungroup <object_id> alias for delete <object_id>
export function ungroup(id){
    for (var i = 0; i < objectInfo.length; i++)
    {
        if (objectInfo[i].id == id)
        {
          deleteObject(objectInfo[i].object)
          objectInfo.splice(i, 1)
        }
    }
    scene.add(group)

    if (!commandAnimating && curAnimationFragment != null)
    {
      curAnimationFragment.callBefore[curAnimationFragment.callBefore.length] = function()
      {
        ungroup(id)
      }
    }
}
//Altering objects
//Altering objects
//position <object_id> <x> <y> [z]
export function position(id,x,y,z){
  console.log(id);
    data(id).position.set(x,y,z)
}
//size <object_id> <w> <h>
export function size(id,w,h){
    data(id).scale.set(w,h,data(id).position.scale.z)
}
//scale <object_id> <scale>
export function scale(id,sc){
    data(id).scale.set(sc,sc,sc)
}
//scale <object_id> [x_scale] [y_scale] [z_scale]
export function scale2(id,x_scale,y_scale,z_scale){
    data(id).scale.set(x_scale,y_scale,z_scale)
}
//rotation <object_id> <degrees_or_radians> <vector> rotates the object about the given vector
export function rotation(id,dor,v)
{
  let curObject = data(id)

  let q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(v.x, v.y, v.z), toRadians(dor));
  let q2 = new THREE.Quaternion().copy(curObject.quaternion);
  curObject.quaternion.copy(q).multiply(q2);
}
//rotation <object_id> <degrees_or_radians> <x/y/z> rotates the object about the chosen axis (x, y, or z)
export function rotation2(id,dor,v)
{
  let curObject = data(id)

  if (v == "x")
  {
    curObject.rotateX(toRadians(v.x))
  }
  else if (v == "y")
  {
    curObject.rotateY(toRadians(v.y))
  }
  else
  {
    curObject.rotateZ(toRadians(v.z))
  }
}
//fill <object_id> <alpha-hex>
export function fill(id,alh){
    data(id).material.emissive = new THREE.Color(alh)
}
//fill <object_id> <r> <g> <b> [a]
export function fill2(id,r,g,b,a){
    data(id).material.emissive = new THREE.Color(r,g,b)
    data(id).material.opacity = a
}
//border <object_id> <width> <alpha-hex>
export function border(id,w,h){
   var t=h.toString();
   var r=parseInt(t.substring(1,3),16)
   var g=parseInt(t.substring(3,5),16)
   var b=parseInt(t.substring(5,7),16)
   r=r/255
   g=g/255
   b=b/255
   addBorder(data(id),r,g,b,1,w)
}
//border <object_id> <width> <r> <g> <b> [a]
export function border2(id,size,r,g,b,a, borderTest){
    addBorder(data(id),r,g,b,a, size, borderTest)
}
//reset <object_id> [keyframe_id] resets the object to its inital state or to its state at given keyframe
export function reset(objID, keyframeID)
{
    let curObject = data(objID)
    let targetFragment = null

    if(objID != null)
    {
      targetFragment = findFragment(keyframeID)
    }
    else
    {
      let cur = findObjectCurFragment(curObject, 9999999)

      while (true)
      {
        let pre = findObjectCurFragment(curObject, cur.startFrame)

        if (pre == null)
        {
          break
        }

        cur = pre
      }

      targetFragment = cur
    }

    curObject.position.x = targetFragment.startPosition.x
    curObject.position.y = targetFragment.startPosition.y
    curObject.position.z = targetFragment.startPosition.z

    curObject.rotation.x = targetFragment.startEulerAngle.x
    curObject.rotation.y = targetFragment.startEulerAngle.y
    curObject.rotation.z = targetFragment.startEulerAngle.z

    curObject.scale.x = targetFragment.startScale.x
    curObject.scale.y = targetFragment.startScale.y
    curObject.scale.z = targetFragment.startScale.z

    curObject.material.opacity = targetFragment.startAlpha
}
//hide <object_id> hides the object completely, indepedent of alpha values
export function hide(id){
    data(id).visible=false
}
//show <object_id> shows the object again
export function show(id){
    data(id).visible=true
}

//Animating
//keyframe create creates a new keyframe and adds all objects and their states to it
export function createKeyFrame()
{
  for (var i = 0; i < objectInfo.length; i++)
  {
    if (animationBuffer[objectInfo[i].object.id] == null)
    {
      let start = recordStartFrame(objectInfo[i].object)
      let end = recordEndFrame(objectInfo[i].object)

      recordAnimationFrame(start, end)
      recordFrame(objectInfo[i].object)
    }
    else
    {
      let curFrame = recordStartFrame(objectInfo[i].object)
      animationBuffer[objectInfo[i].object.id].frame = worldFrame

      recordAnimationFrame(animationBuffer[objectInfo[i].object.id], curFrame)
      animationBuffer[objectInfo[i].object.id] = curFrame
    }

    animationFragments[animationFragments.length - 1].startFrame = 0
    animationFragments[animationFragments.length - 1].duration = 0

    generateIDList()
  }

  curAnimationFragment = animationFragments[animationFragments.length - 1]
}
//keyframe duration <keyframe_id> <duration> sets the keyframe's duration in milliseconds
export function setKeyframeDuration(id, duration)
{
  let curDuration = 0
  let curFragment = data(id)
  let preFragment = findPreviousFragment(curFragment.object, curFragment.startFrame)

  if (preFragment != null)
  {
    curFragment.duration = preFragment.startFrame + preFragment.duration + duration
  }
  else
  {
    curFragment.duration = duration
  }

  console.log(animationFragments);

  generateIDList()
}

//keyframe clear <keyframe_id> clears the given keyframe of all data except its duration and position in the sequence (essentially reverts it to an empty keyframe, a copy of the previous one)
export function clearKeyFrame(id)
{
  deleteContent(id)

  generateIDList()
}
//keyframe goto <keyframe_id> sets the visual state of the animation to the given keyframe's state
export function goToKeyFrame(id)
{
  worldFrame = data(id).startFrame
  enterAnimationMode()
}

//keyframe return effectively a macro for keyframe goto for the last created keyframe
export function returnKeyframe()
{
  let lastKeyFrameDuration = 0

  for (var i = 0; i < animationFragments.length; i++)
  {
    if(lastKeyFrameDuration < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      lastKeyFrameDuration = animationFragments[i].startFrame + animationFragments[i].duration
    }
  }

  worldFrame = lastKeyFrameDuration
  enterAnimationMode()
}
//keyframe preview <keyframe_id> plays the animation from the keyframe preceding the given keyframe to the given keyframe
export function previewKeyframe(id)
{
  let lastKeyFrameDuration = 0
  let lastKeyFrameID = -1

  for (var i = 0; i < animationFragments.length; i++)
  {
    if(lastKeyFrameDuration < animationFragments[i].startFrame + animationFragments[i].duration)
    {
      lastKeyFrameDuration = animationFragments[i].startFrame + animationFragments[i].duration
      lastKeyFrameID = animationFragments[i].id
    }
  }

  playKeyFrameBetween(id, lastKeyFrameID)
}

//keyframe preview <keyframe_id> <keyframe_id> plays the animations between the two keyframes
export function playKeyFrameBetween(id1, id2)
{
  let curFrame = data[id1].startFrame
  let frame = data[id2].startFrame

  worldFrame = curFrame
  enterAnimationMode()

  var loop = function()
  {
    if (curFrame == frame + 1)
    {
      commandAnimating = false

      return
    }

    viewAnimationStatusAt(curFrame)

    requestAnimationFrame(loop)
  };

  commandAnimating = true
  loop()
}
//macro record [name] starts recording a new macro; commands from this point on will be recorded and added to this macro
export function macroStartRecording(name)
{
  if (sequences[curSequenceName].macros[name] == null)
  {
    sequences[curSequenceName].macros[name] =
    {
      "id": globalID++,
      "name": name,
      "commands": [],
      "animationFragments": animationFragments,
      "objectInfo": objectInfo
    }
  }

  curMacroName = name
}
//macro stop stops recording the macro
export function macroStopRecording()
{
  curMacroName = null
  curAnimationFragment = null
}
//macro [name_or_id] executes an existing macro; they can also be executed like a command by invoking their name
export function exec_enter_macro(name_or_id)
{
  let curMacro = null

  animationFragments = []

  if (sequences[curSequenceName].macros[name_or_id] != null)
  {
    curMacro = sequences[curSequenceName].macros[name_or_id]
  }
  else
  {
    curMacro = data(name_or_id)
  }

  curMacroName = curMacro.name

  for (let i = 0; i < curMacro.commands.length; i++)
  {
    ParseCommand(curMacro.commands[i])
  }

  generateIDList()

  console.log(curMacro.commands);

  worldFrame = 0
  isAnimating = true
  commandAnimating = true

  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  animateAllFragments(animationFragments)
}
//sequence create [animation_name] creates a new animation; commands from this point on are part of the animation
export function createSequence(name)
{
  if (sequences[name] == null)
  {
    animationFragments = []
    objectInfo = []

    sequences[name] =
    {
      "id": globalID++,
      "name": name,
      "macros": {},
      "animationFragments": animationFragments,
      "objectInfo": objectInfo
    }
  }

  curSequenceName = name
}
//sequence duration <duration> sets the duration of the animation in milliseconds
export function setSequenceDuration(duration)
{
  let curDuration = 0

  for (var i = 0; i < animationFragments.length; i++)
  {
    curDuration += animationFragments[i].duration
  }

  for (var i = 0; i < animationFragments.length; i++)
  {
    animationFragments[i].duration *= (duration / curDuration)
  }
}

//sequence play plays the animation from beginning to end
export function playSequence(curSequenceName)
{
  animationFragments = sequences[curSequenceName].animationFragments
  objectInfo = sequences[curSequenceName].objectInfo

  worldFrame = 0
  isAnimating = true
  commandAnimating = true

  clearAnimationData(animationFragments)
  initAnimationLoop(animationFragments)
  animateAllFragments(animationFragments)
}
//save export commands to text file available to download; this command is never recorded
export function save()
{

}

//unility
//variable create <name> <value> create a variable to be used by name in place of actual values from this point on
export function variableCreate(name, v)
{
  variables[name] =
  {
    "data": v,
    "id": globalID++
  }
}
//variable set <name> <value> change the value of an existing variable
export function variableSet(name, v)
{
  if (variables[name] == null)
  {
    return
  }

  variables[name].data = v
}
//data <id> returns a map of the given object/keyframe/macro/variable data to be used in variables or simply for display;
//values are by reference, but readonly
export function data(id)
{
  for (var i = 0; i < objectInfo.length; i++)
  {
    if(objectInfo[i].id == id)
    {
      return objectInfo[i].object
    }
  }

  for (var i = 0; i < animationFragments.length; i++)
  {
    if(animationFragments[i].id == id)
    {
      return animationFragments[i]
    }
  }

  for (let sequenceKey in sequences)
  {
    for (let macroKey in sequences[sequenceKey].macros)
    {
      if(sequences[sequenceKey].macros[macroKey].id == id)
      {
        return sequences[sequenceKey].macros[macroKey]
      }
    }
  }

  for (let key in variables)
  {
    if(variables[key].id == id)
    {
      return variables[key]
    }
  }
}
//delete <object_id>
export function deleteContent(id, delay)
{
  let curFrame = 0

  let loop = function()
  {
    if (curFrame == delay)
    {
      for (var i = 0; i < objectInfo.length; i++)
      {
        if(objectInfo[i].id == id)
        {
          deleteObject(objectInfo[i].object)
          objectInfo.splice(i, 1)
        }
      }

      for (var i = 0; i < animationFragments.length; i++)
      {
        if(animationFragments[i].id == id)
        {
          animationFragments.splice(i, 1)

          return
        }
      }

      for (let sequenceKey in sequences)
      {
        for (let macroKey in sequences[sequenceKey].macros)
        {
          if(sequences[sequenceKey].macros[macroKey].id == id)
          {
            sequences[sequenceKey].macros[macroKey] = undefined

            return
          }
        }
      }

      for (let key in variables)
      {
        if(variables[key].id == id)
        {
          variables[key] = undefined

          return
        }
      }

      generateIDList()
    }

    curFrame += 1
    requestAnimationFrame(loop)
  }

  loop()
}
//help lists all existing commands and short descriptions; this command is never recorded
export function help()
{
  //TODO
}
//man <command> our equivalent of man pages for our commands; this command is never recorded
export function man()
{
  //TODO
}

/**
 * @author Kyle-Larson https://github.com/Kyle-Larson
 * @author Takahiro https://github.com/takahirox
 * @author Lewy Blue https://github.com/looeee
 *
 * Loader loads FBX file and generates Group representing FBX scene.
 * Requires FBX file to be >= 7.0 and in ASCII or >= 6400 in Binary format
 * Versions lower than this may load but will probably have errors
 *
 * Needs Support:
 *  Morph normals / blend shape normals
 *
 * FBX format references:
 * 	https://wiki.blender.org/index.php/User:Mont29/Foundation/FBX_File_Structure
 * 	http://help.autodesk.com/view/FBX/2017/ENU/?guid=__cpp_ref_index_html (C++ SDK reference)
 *
 * 	Binary format specification:
 *		https://code.blender.org/2013/08/fbx-binary-file-format-specification/
 */


var FBXLoader = ( function () {

	var fbxTree;
	var connections;
	var sceneGraph;

	function FBXLoader( manager ) {

		this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

	}

	FBXLoader.prototype = {

		constructor: FBXLoader,

		crossOrigin: 'anonymous',

		load: function ( url, onLoad, onProgress, onError ) {

			var self = this;

			var path = ( self.path === undefined ) ? THREE.LoaderUtils.extractUrlBase( url ) : self.path;

			var loader = new THREE.FileLoader( this.manager );
			loader.setPath( self.path );
			loader.setResponseType( 'arraybuffer' );

			loader.load( url, function ( buffer ) {

				try {
					onLoad( self.parse( buffer, path ) );

				} catch ( error ) {

					setTimeout( function () {

						if ( onError ) onError( error );

						self.manager.itemError( url );

					}, 0 );

				}

			}, onProgress, onError );

		},

		setPath: function ( value ) {

			this.path = value;
			return this;

		},

		setResourcePath: function ( value ) {

			this.resourcePath = value;
			return this;

		},

		setCrossOrigin: function ( value ) {

			this.crossOrigin = value;
			return this;

		},

		parse: function ( FBXBuffer, path ) {

			if ( isFbxFormatBinary( FBXBuffer ) ) {

				fbxTree = new BinaryParser().parse( FBXBuffer );

			} else {

				var FBXText = convertArrayBufferToString( FBXBuffer );

				if ( ! isFbxFormatASCII( FBXText ) ) {

					throw new Error( 'THREE.FBXLoader: Unknown format.' );

				}

				if ( getFbxVersion( FBXText ) < 7000 ) {

					throw new Error( 'THREE.FBXLoader: FBX version not supported, FileVersion: ' + getFbxVersion( FBXText ) );

				}

				fbxTree = new TextParser().parse( FBXText );

			}

			// console.log( fbxTree );

			var textureLoader = new THREE.TextureLoader( this.manager ).setPath( this.resourcePath || path ).setCrossOrigin( this.crossOrigin );

			return new FBXTreeParser( textureLoader ).parse( fbxTree );

		}

	};

	// Parse the FBXTree object returned by the BinaryParser or TextParser and return a THREE.Group
	function FBXTreeParser( textureLoader ) {

		this.textureLoader = textureLoader;

	}

	FBXTreeParser.prototype = {

		constructor: FBXTreeParser,

		parse: function () {

			connections = this.parseConnections();

			var images = this.parseImages();
			var textures = this.parseTextures( images );
			var materials = this.parseMaterials( textures );
			var deformers = this.parseDeformers();
			var geometryMap = new GeometryParser().parse( deformers );

			this.parseScene( deformers, geometryMap, materials );

			return sceneGraph;

		},

		// Parses FBXTree.Connections which holds parent-child connections between objects (e.g. material -> texture, model->geometry )
		// and details the connection type
		parseConnections: function () {

			var connectionMap = new Map();

			if ( 'Connections' in fbxTree ) {

				var rawConnections = fbxTree.Connections.connections;

				rawConnections.forEach( function ( rawConnection ) {

					var fromID = rawConnection[ 0 ];
					var toID = rawConnection[ 1 ];
					var relationship = rawConnection[ 2 ];

					if ( ! connectionMap.has( fromID ) ) {

						connectionMap.set( fromID, {
							parents: [],
							children: []
						} );

					}

					var parentRelationship = { ID: toID, relationship: relationship };
					connectionMap.get( fromID ).parents.push( parentRelationship );

					if ( ! connectionMap.has( toID ) ) {

						connectionMap.set( toID, {
							parents: [],
							children: []
						} );

					}

					var childRelationship = { ID: fromID, relationship: relationship };
					connectionMap.get( toID ).children.push( childRelationship );

				} );

			}

			return connectionMap;

		},

		// Parse FBXTree.Objects.Video for embedded image data
		// These images are connected to textures in FBXTree.Objects.Textures
		// via FBXTree.Connections.
		parseImages: function () {

			var images = {};
			var blobs = {};

			if ( 'Video' in fbxTree.Objects ) {

				var videoNodes = fbxTree.Objects.Video;

				for ( var nodeID in videoNodes ) {

					var videoNode = videoNodes[ nodeID ];

					var id = parseInt( nodeID );

					images[ id ] = videoNode.RelativeFilename || videoNode.Filename;

					// raw image data is in videoNode.Content
					if ( 'Content' in videoNode ) {

						var arrayBufferContent = ( videoNode.Content instanceof ArrayBuffer ) && ( videoNode.Content.byteLength > 0 );
						var base64Content = ( typeof videoNode.Content === 'string' ) && ( videoNode.Content !== '' );

						if ( arrayBufferContent || base64Content ) {

							var image = this.parseImage( videoNodes[ nodeID ] );

							blobs[ videoNode.RelativeFilename || videoNode.Filename ] = image;

						}

					}

				}

			}

			for ( var id in images ) {

				var filename = images[ id ];

				if ( blobs[ filename ] !== undefined ) images[ id ] = blobs[ filename ];
				else images[ id ] = images[ id ].split( '\\' ).pop();

			}

			return images;

		},

		// Parse embedded image data in FBXTree.Video.Content
		parseImage: function ( videoNode ) {

			var content = videoNode.Content;
			var fileName = videoNode.RelativeFilename || videoNode.Filename;
			var extension = fileName.slice( fileName.lastIndexOf( '.' ) + 1 ).toLowerCase();

			var type;

			switch ( extension ) {

				case 'bmp':

					type = 'image/bmp';
					break;

				case 'jpg':
				case 'jpeg':

					type = 'image/jpeg';
					break;

				case 'png':

					type = 'image/png';
					break;

				case 'tif':

					type = 'image/tiff';
					break;

				case 'tga':

					if ( THREE.Loader.Handlers.get( '.tga' ) === null ) {

						console.warn( 'FBXLoader: TGA loader not found, skipping ', fileName );

					}

					type = 'image/tga';
					break;

				default:

					console.warn( 'FBXLoader: Image type "' + extension + '" is not supported.' );
					return;

			}

			if ( typeof content === 'string' ) { // ASCII format

				return 'data:' + type + ';base64,' + content;

			} else { // Binary Format

				var array = new Uint8Array( content );
				return window.URL.createObjectURL( new Blob( [ array ], { type: type } ) );

			}

		},

		// Parse nodes in FBXTree.Objects.Texture
		// These contain details such as UV scaling, cropping, rotation etc and are connected
		// to images in FBXTree.Objects.Video
		parseTextures: function ( images ) {

			var textureMap = new Map();

			if ( 'Texture' in fbxTree.Objects ) {

				var textureNodes = fbxTree.Objects.Texture;
				for ( var nodeID in textureNodes ) {

					var texture = this.parseTexture( textureNodes[ nodeID ], images );
					textureMap.set( parseInt( nodeID ), texture );

				}

			}

			return textureMap;

		},

		// Parse individual node in FBXTree.Objects.Texture
		parseTexture: function ( textureNode, images ) {

			var texture = this.loadTexture( textureNode, images );

			texture.ID = textureNode.id;

			texture.name = textureNode.attrName;

			var wrapModeU = textureNode.WrapModeU;
			var wrapModeV = textureNode.WrapModeV;

			var valueU = wrapModeU !== undefined ? wrapModeU.value : 0;
			var valueV = wrapModeV !== undefined ? wrapModeV.value : 0;

			// http://download.autodesk.com/us/fbx/SDKdocs/FBX_SDK_Help/files/fbxsdkref/class_k_fbx_texture.html#889640e63e2e681259ea81061b85143a
			// 0: repeat(default), 1: clamp

			texture.wrapS = valueU === 0 ? THREE.RepeatWrapping : THREE.ClampToEdgeWrapping;
			texture.wrapT = valueV === 0 ? THREE.RepeatWrapping : THREE.ClampToEdgeWrapping;

			if ( 'Scaling' in textureNode ) {

				var values = textureNode.Scaling.value;

				texture.repeat.x = values[ 0 ];
				texture.repeat.y = values[ 1 ];

			}

			return texture;

		},

		// load a texture specified as a blob or data URI, or via an external URL using THREE.TextureLoader
		loadTexture: function ( textureNode, images ) {

			var fileName;

			var currentPath = this.textureLoader.path;

			var children = connections.get( textureNode.id ).children;

			if ( children !== undefined && children.length > 0 && images[ children[ 0 ].ID ] !== undefined ) {

				fileName = images[ children[ 0 ].ID ];

				if ( fileName.indexOf( 'blob:' ) === 0 || fileName.indexOf( 'data:' ) === 0 ) {

					this.textureLoader.setPath( undefined );

				}

			}

			var texture;

			var extension = textureNode.FileName.slice( - 3 ).toLowerCase();

			if ( extension === 'tga' ) {

				var loader = THREE.Loader.Handlers.get( '.tga' );

				if ( loader === null ) {

					console.warn( 'FBXLoader: TGA loader not found, creating placeholder texture for', textureNode.RelativeFilename );
					texture = new THREE.Texture();

				} else {

					texture = loader.load( fileName );

				}

			} else if ( extension === 'psd' ) {

				console.warn( 'FBXLoader: PSD textures are not supported, creating placeholder texture for', textureNode.RelativeFilename );
				texture = new THREE.Texture();

			} else {

				texture = this.textureLoader.load( fileName );

			}

			this.textureLoader.setPath( currentPath );

			return texture;

		},

		// Parse nodes in FBXTree.Objects.Material
		parseMaterials: function ( textureMap ) {

			var materialMap = new Map();

			if ( 'Material' in fbxTree.Objects ) {

				var materialNodes = fbxTree.Objects.Material;

				for ( var nodeID in materialNodes ) {

					var material = this.parseMaterial( materialNodes[ nodeID ], textureMap );

					if ( material !== null ) materialMap.set( parseInt( nodeID ), material );

				}

			}

			return materialMap;

		},

		// Parse single node in FBXTree.Objects.Material
		// Materials are connected to texture maps in FBXTree.Objects.Textures
		// FBX format currently only supports Lambert and Phong shading models
		parseMaterial: function ( materialNode, textureMap ) {

			var ID = materialNode.id;
			var name = materialNode.attrName;
			var type = materialNode.ShadingModel;

			// Case where FBX wraps shading model in property object.
			if ( typeof type === 'object' ) {

				type = type.value;

			}

			// Ignore unused materials which don't have any connections.
			if ( ! connections.has( ID ) ) return null;

			var parameters = this.parseParameters( materialNode, textureMap, ID );

			var material;

			switch ( type.toLowerCase() ) {

				case 'phong':
					material = new THREE.MeshPhongMaterial();
					break;
				case 'lambert':
					material = new THREE.MeshLambertMaterial();
					break;
				default:
					console.warn( 'THREE.FBXLoader: unknown material type "%s". Defaulting to MeshPhongMaterial.', type );
					material = new THREE.MeshPhongMaterial();
					break;

			}

			material.setValues( parameters );
			material.name = name;

			return material;

		},

		// Parse FBX material and return parameters suitable for a three.js material
		// Also parse the texture map and return any textures associated with the material
		parseParameters: function ( materialNode, textureMap, ID ) {

			var parameters = {};

			if ( materialNode.BumpFactor ) {

				parameters.bumpScale = materialNode.BumpFactor.value;

			}
			if ( materialNode.Diffuse ) {

				parameters.color = new THREE.Color().fromArray( materialNode.Diffuse.value );

			} else if ( materialNode.DiffuseColor && materialNode.DiffuseColor.type === 'Color' ) {

				// The blender exporter exports diffuse here instead of in materialNode.Diffuse
				parameters.color = new THREE.Color().fromArray( materialNode.DiffuseColor.value );

			}

			if ( materialNode.DisplacementFactor ) {

				parameters.displacementScale = materialNode.DisplacementFactor.value;

			}

			if ( materialNode.Emissive ) {

				parameters.emissive = new THREE.Color().fromArray( materialNode.Emissive.value );

			} else if ( materialNode.EmissiveColor && materialNode.EmissiveColor.type === 'Color' ) {

				// The blender exporter exports emissive color here instead of in materialNode.Emissive
				parameters.emissive = new THREE.Color().fromArray( materialNode.EmissiveColor.value );

			}

			if ( materialNode.EmissiveFactor ) {

				parameters.emissiveIntensity = parseFloat( materialNode.EmissiveFactor.value );

			}

			if ( materialNode.Opacity ) {

				parameters.opacity = parseFloat( materialNode.Opacity.value );

			}

			if ( parameters.opacity < 1.0 ) {

				parameters.transparent = true;

			}

			if ( materialNode.ReflectionFactor ) {

				parameters.reflectivity = materialNode.ReflectionFactor.value;

			}

			if ( materialNode.Shininess ) {

				parameters.shininess = materialNode.Shininess.value;

			}

			if ( materialNode.Specular ) {

				parameters.specular = new THREE.Color().fromArray( materialNode.Specular.value );

			} else if ( materialNode.SpecularColor && materialNode.SpecularColor.type === 'Color' ) {

				// The blender exporter exports specular color here instead of in materialNode.Specular
				parameters.specular = new THREE.Color().fromArray( materialNode.SpecularColor.value );

			}

			var self = this;
			connections.get( ID ).children.forEach( function ( child ) {

				var type = child.relationship;

				switch ( type ) {

					case 'Bump':
						parameters.bumpMap = self.getTexture( textureMap, child.ID );
						break;

					case 'Maya|TEX_ao_map':
						parameters.aoMap = self.getTexture( textureMap, child.ID );
						break;

					case 'DiffuseColor':
					case 'Maya|TEX_color_map':
						parameters.map = self.getTexture( textureMap, child.ID );
						parameters.map.encoding = THREE.sRGBEncoding;
						break;

					case 'DisplacementColor':
						parameters.displacementMap = self.getTexture( textureMap, child.ID );
						break;

					case 'EmissiveColor':
						parameters.emissiveMap = self.getTexture( textureMap, child.ID );
						parameters.emissiveMap.encoding = THREE.sRGBEncoding;
						break;

					case 'NormalMap':
					case 'Maya|TEX_normal_map':
						parameters.normalMap = self.getTexture( textureMap, child.ID );
						break;

					case 'ReflectionColor':
						parameters.envMap = self.getTexture( textureMap, child.ID );
						parameters.envMap.mapping = THREE.EquirectangularReflectionMapping;
						parameters.envMap.encoding = THREE.sRGBEncoding;
						break;

					case 'SpecularColor':
						parameters.specularMap = self.getTexture( textureMap, child.ID );
						parameters.specularMap.encoding = THREE.sRGBEncoding;
						break;

					case 'TransparentColor':
						parameters.alphaMap = self.getTexture( textureMap, child.ID );
						parameters.transparent = true;
						break;

					case 'AmbientColor':
					case 'ShininessExponent': // AKA glossiness map
					case 'SpecularFactor': // AKA specularLevel
					case 'VectorDisplacementColor': // NOTE: Seems to be a copy of DisplacementColor
					default:
						console.warn( 'THREE.FBXLoader: %s map is not supported in three.js, skipping texture.', type );
						break;

				}

			} );

			return parameters;

		},

		// get a texture from the textureMap for use by a material.
		getTexture: function ( textureMap, id ) {

			// if the texture is a layered texture, just use the first layer and issue a warning
			if ( 'LayeredTexture' in fbxTree.Objects && id in fbxTree.Objects.LayeredTexture ) {

				console.warn( 'THREE.FBXLoader: layered textures are not supported in three.js. Discarding all but first layer.' );
				id = connections.get( id ).children[ 0 ].ID;

			}

			return textureMap.get( id );

		},

		// Parse nodes in FBXTree.Objects.Deformer
		// Deformer node can contain skinning or Vertex Cache animation data, however only skinning is supported here
		// Generates map of Skeleton-like objects for use later when generating and binding skeletons.
		parseDeformers: function () {

			var skeletons = {};
			var morphTargets = {};

			if ( 'Deformer' in fbxTree.Objects ) {

				var DeformerNodes = fbxTree.Objects.Deformer;

				for ( var nodeID in DeformerNodes ) {

					var deformerNode = DeformerNodes[ nodeID ];

					var relationships = connections.get( parseInt( nodeID ) );

					if ( deformerNode.attrType === 'Skin' ) {

						var skeleton = this.parseSkeleton( relationships, DeformerNodes );
						skeleton.ID = nodeID;

						if ( relationships.parents.length > 1 ) console.warn( 'THREE.FBXLoader: skeleton attached to more than one geometry is not supported.' );
						skeleton.geometryID = relationships.parents[ 0 ].ID;

						skeletons[ nodeID ] = skeleton;

					} else if ( deformerNode.attrType === 'BlendShape' ) {

						var morphTarget = {
							id: nodeID,
						};

						morphTarget.rawTargets = this.parseMorphTargets( relationships, DeformerNodes );
						morphTarget.id = nodeID;

						if ( relationships.parents.length > 1 ) console.warn( 'THREE.FBXLoader: morph target attached to more than one geometry is not supported.' );

						morphTargets[ nodeID ] = morphTarget;

					}

				}

			}

			return {

				skeletons: skeletons,
				morphTargets: morphTargets,

			};

		},

		// Parse single nodes in FBXTree.Objects.Deformer
		// The top level skeleton node has type 'Skin' and sub nodes have type 'Cluster'
		// Each skin node represents a skeleton and each cluster node represents a bone
		parseSkeleton: function ( relationships, deformerNodes ) {

			var rawBones = [];

			relationships.children.forEach( function ( child ) {

				var boneNode = deformerNodes[ child.ID ];

				if ( boneNode.attrType !== 'Cluster' ) return;

				var rawBone = {

					ID: child.ID,
					indices: [],
					weights: [],
					transformLink: new THREE.Matrix4().fromArray( boneNode.TransformLink.a ),
					// transform: new THREE.Matrix4().fromArray( boneNode.Transform.a ),
					// linkMode: boneNode.Mode,

				};

				if ( 'Indexes' in boneNode ) {

					rawBone.indices = boneNode.Indexes.a;
					rawBone.weights = boneNode.Weights.a;

				}

				rawBones.push( rawBone );

			} );

			return {

				rawBones: rawBones,
				bones: []

			};

		},

		// The top level morph deformer node has type "BlendShape" and sub nodes have type "BlendShapeChannel"
		parseMorphTargets: function ( relationships, deformerNodes ) {

			var rawMorphTargets = [];

			for ( var i = 0; i < relationships.children.length; i ++ ) {

				var child = relationships.children[ i ];

				var morphTargetNode = deformerNodes[ child.ID ];

				var rawMorphTarget = {

					name: morphTargetNode.attrName,
					initialWeight: morphTargetNode.DeformPercent,
					id: morphTargetNode.id,
					fullWeights: morphTargetNode.FullWeights.a

				};

				if ( morphTargetNode.attrType !== 'BlendShapeChannel' ) return;

				rawMorphTarget.geoID = connections.get( parseInt( child.ID ) ).children.filter( function ( child ) {

					return child.relationship === undefined;

				} )[ 0 ].ID;

				rawMorphTargets.push( rawMorphTarget );

			}

			return rawMorphTargets;

		},

		// create the main THREE.Group() to be returned by the loader
		parseScene: function ( deformers, geometryMap, materialMap ) {

			sceneGraph = new THREE.Group();

			var modelMap = this.parseModels( deformers.skeletons, geometryMap, materialMap );

			var modelNodes = fbxTree.Objects.Model;

			var self = this;
			modelMap.forEach( function ( model ) {

				var modelNode = modelNodes[ model.ID ];
				self.setLookAtProperties( model, modelNode );

				var parentConnections = connections.get( model.ID ).parents;

				parentConnections.forEach( function ( connection ) {

					var parent = modelMap.get( connection.ID );
					if ( parent !== undefined ) parent.add( model );

				} );

				if ( model.parent === null ) {

					sceneGraph.add( model );

				}


			} );

			this.bindSkeleton( deformers.skeletons, geometryMap, modelMap );

			this.createAmbientLight();

			this.setupMorphMaterials();

			sceneGraph.traverse( function ( node ) {

				if ( node.userData.transformData ) {

					if ( node.parent ) node.userData.transformData.parentMatrixWorld = node.parent.matrix;

					var transform = generateTransform( node.userData.transformData );

					node.applyMatrix( transform );

				}

			} );

			var animations = new AnimationParser().parse();

			// if all the models where already combined in a single group, just return that
			if ( sceneGraph.children.length === 1 && sceneGraph.children[ 0 ].isGroup ) {

				sceneGraph.children[ 0 ].animations = animations;
				sceneGraph = sceneGraph.children[ 0 ];

			}

			sceneGraph.animations = animations;

		},

		// parse nodes in FBXTree.Objects.Model
		parseModels: function ( skeletons, geometryMap, materialMap ) {

			var modelMap = new Map();
			var modelNodes = fbxTree.Objects.Model;

			for ( var nodeID in modelNodes ) {

				var id = parseInt( nodeID );
				var node = modelNodes[ nodeID ];
				var relationships = connections.get( id );

				var model = this.buildSkeleton( relationships, skeletons, id, node.attrName );

				if ( ! model ) {

					switch ( node.attrType ) {

						case 'Camera':
							model = this.createCamera( relationships );
							break;
						case 'Light':
							model = this.createLight( relationships );
							break;
						case 'Mesh':
							model = this.createMesh( relationships, geometryMap, materialMap );
							break;
						case 'NurbsCurve':
							model = this.createCurve( relationships, geometryMap );
							break;
						case 'LimbNode':
						case 'Root':
							model = new THREE.Bone();
							break;
						case 'Null':
						default:
							model = new THREE.Group();
							break;

					}

					model.name = THREE.PropertyBinding.sanitizeNodeName( node.attrName );
					model.ID = id;

				}

				this.getTransformData( model, node );
				modelMap.set( id, model );

			}

			return modelMap;

		},

		buildSkeleton: function ( relationships, skeletons, id, name ) {

			var bone = null;

			relationships.parents.forEach( function ( parent ) {

				for ( var ID in skeletons ) {

					var skeleton = skeletons[ ID ];

					skeleton.rawBones.forEach( function ( rawBone, i ) {

						if ( rawBone.ID === parent.ID ) {

							var subBone = bone;
							bone = new THREE.Bone();

							bone.matrixWorld.copy( rawBone.transformLink );

							// set name and id here - otherwise in cases where "subBone" is created it will not have a name / id
							bone.name = THREE.PropertyBinding.sanitizeNodeName( name );
							bone.ID = id;

							skeleton.bones[ i ] = bone;

							// In cases where a bone is shared between multiple meshes
							// duplicate the bone here and and it as a child of the first bone
							if ( subBone !== null ) {

								bone.add( subBone );

							}

						}

					} );

				}

			} );

			return bone;

		},

		// create a THREE.PerspectiveCamera or THREE.OrthographicCamera
		createCamera: function ( relationships ) {

			var model;
			var cameraAttribute;

			relationships.children.forEach( function ( child ) {

				var attr = fbxTree.Objects.NodeAttribute[ child.ID ];

				if ( attr !== undefined ) {

					cameraAttribute = attr;

				}

			} );

			if ( cameraAttribute === undefined ) {

				model = new THREE.Object3D();

			} else {

				var type = 0;
				if ( cameraAttribute.CameraProjectionType !== undefined && cameraAttribute.CameraProjectionType.value === 1 ) {

					type = 1;

				}

				var nearClippingPlane = 1;
				if ( cameraAttribute.NearPlane !== undefined ) {

					nearClippingPlane = cameraAttribute.NearPlane.value / 1000;

				}

				var farClippingPlane = 1000;
				if ( cameraAttribute.FarPlane !== undefined ) {

					farClippingPlane = cameraAttribute.FarPlane.value / 1000;

				}


				var width = window.innerWidth;
				var height = window.innerHeight;

				if ( cameraAttribute.AspectWidth !== undefined && cameraAttribute.AspectHeight !== undefined ) {

					width = cameraAttribute.AspectWidth.value;
					height = cameraAttribute.AspectHeight.value;

				}

				var aspect = width / height;

				var fov = 45;
				if ( cameraAttribute.FieldOfView !== undefined ) {

					fov = cameraAttribute.FieldOfView.value;

				}

				var focalLength = cameraAttribute.FocalLength ? cameraAttribute.FocalLength.value : null;

				switch ( type ) {

					case 0: // Perspective
						model = new THREE.PerspectiveCamera( fov, aspect, nearClippingPlane, farClippingPlane );
						if ( focalLength !== null ) model.setFocalLength( focalLength );
						break;

					case 1: // Orthographic
						model = new THREE.OrthographicCamera( - width / 2, width / 2, height / 2, - height / 2, nearClippingPlane, farClippingPlane );
						break;

					default:
						console.warn( 'THREE.FBXLoader: Unknown camera type ' + type + '.' );
						model = new THREE.Object3D();
						break;

				}

			}

			return model;

		},

		// Create a THREE.DirectionalLight, THREE.PointLight or THREE.SpotLight
		createLight: function ( relationships ) {

			var model;
			var lightAttribute;

			relationships.children.forEach( function ( child ) {

				var attr = fbxTree.Objects.NodeAttribute[ child.ID ];

				if ( attr !== undefined ) {

					lightAttribute = attr;

				}

			} );

			if ( lightAttribute === undefined ) {

				model = new THREE.Object3D();

			} else {

				var type;

				// LightType can be undefined for Point lights
				if ( lightAttribute.LightType === undefined ) {

					type = 0;

				} else {

					type = lightAttribute.LightType.value;

				}

				var color = 0xffffff;

				if ( lightAttribute.Color !== undefined ) {

					color = new THREE.Color().fromArray( lightAttribute.Color.value );

				}

				var intensity = ( lightAttribute.Intensity === undefined ) ? 1 : lightAttribute.Intensity.value / 100;

				// light disabled
				if ( lightAttribute.CastLightOnObject !== undefined && lightAttribute.CastLightOnObject.value === 0 ) {

					intensity = 0;

				}

				var distance = 0;
				if ( lightAttribute.FarAttenuationEnd !== undefined ) {

					if ( lightAttribute.EnableFarAttenuation !== undefined && lightAttribute.EnableFarAttenuation.value === 0 ) {

						distance = 0;

					} else {

						distance = lightAttribute.FarAttenuationEnd.value;

					}

				}

				// TODO: could this be calculated linearly from FarAttenuationStart to FarAttenuationEnd?
				var decay = 1;

				switch ( type ) {

					case 0: // Point
						model = new THREE.PointLight( color, intensity, distance, decay );
						break;

					case 1: // Directional
						model = new THREE.DirectionalLight( color, intensity );
						break;

					case 2: // Spot
						var angle = Math.PI / 3;

						if ( lightAttribute.InnerAngle !== undefined ) {

							angle = THREE.Math.degToRad( lightAttribute.InnerAngle.value );

						}

						var penumbra = 0;
						if ( lightAttribute.OuterAngle !== undefined ) {

							// TODO: this is not correct - FBX calculates outer and inner angle in degrees
							// with OuterAngle > InnerAngle && OuterAngle <= Math.PI
							// while three.js uses a penumbra between (0, 1) to attenuate the inner angle
							penumbra = THREE.Math.degToRad( lightAttribute.OuterAngle.value );
							penumbra = Math.max( penumbra, 1 );

						}

						model = new THREE.SpotLight( color, intensity, distance, angle, penumbra, decay );
						break;

					default:
						console.warn( 'THREE.FBXLoader: Unknown light type ' + lightAttribute.LightType.value + ', defaulting to a THREE.PointLight.' );
						model = new THREE.PointLight( color, intensity );
						break;

				}

				if ( lightAttribute.CastShadows !== undefined && lightAttribute.CastShadows.value === 1 ) {

					model.castShadow = true;

				}

			}

			return model;

		},

		createMesh: function ( relationships, geometryMap, materialMap ) {

			var model;
			var geometry = null;
			var material = null;
			var materials = [];

			// get geometry and materials(s) from connections
			relationships.children.forEach( function ( child ) {

				if ( geometryMap.has( child.ID ) ) {

					geometry = geometryMap.get( child.ID );

				}

				if ( materialMap.has( child.ID ) ) {

					materials.push( materialMap.get( child.ID ) );

				}

			} );

			if ( materials.length > 1 ) {

				material = materials;

			} else if ( materials.length > 0 ) {

				material = materials[ 0 ];

			} else {

				material = new THREE.MeshPhongMaterial( { color: 0xcccccc } );
				materials.push( material );

			}

			if ( 'color' in geometry.attributes ) {

				materials.forEach( function ( material ) {

					material.vertexColors = THREE.VertexColors;

				} );

			}

			if ( geometry.FBX_Deformer ) {

				materials.forEach( function ( material ) {

					material.skinning = true;

				} );

				model = new THREE.SkinnedMesh( geometry, material );
				model.normalizeSkinWeights();

			} else {

				model = new THREE.Mesh( geometry, material );

			}

			return model;

		},

		createCurve: function ( relationships, geometryMap ) {

			var geometry = relationships.children.reduce( function ( geo, child ) {

				if ( geometryMap.has( child.ID ) ) geo = geometryMap.get( child.ID );

				return geo;

			}, null );

			// FBX does not list materials for Nurbs lines, so we'll just put our own in here.
			var material = new THREE.LineBasicMaterial( { color: 0x3300ff, linewidth: 1 } );
			return new THREE.Line( geometry, material );

		},

		// parse the model node for transform data
		getTransformData: function ( model, modelNode ) {

			var transformData = {};

			if ( 'InheritType' in modelNode ) transformData.inheritType = parseInt( modelNode.InheritType.value );

			if ( 'RotationOrder' in modelNode ) transformData.eulerOrder = getEulerOrder( modelNode.RotationOrder.value );
			else transformData.eulerOrder = 'ZYX';

			if ( 'Lcl_Translation' in modelNode ) transformData.translation = modelNode.Lcl_Translation.value;

			if ( 'PreRotation' in modelNode ) transformData.preRotation = modelNode.PreRotation.value;
			if ( 'Lcl_Rotation' in modelNode ) transformData.rotation = modelNode.Lcl_Rotation.value;
			if ( 'PostRotation' in modelNode ) transformData.postRotation = modelNode.PostRotation.value;

			if ( 'Lcl_Scaling' in modelNode ) transformData.scale = modelNode.Lcl_Scaling.value;

			if ( 'ScalingOffset' in modelNode ) transformData.scalingOffset = modelNode.ScalingOffset.value;
			if ( 'ScalingPivot' in modelNode ) transformData.scalingPivot = modelNode.ScalingPivot.value;

			if ( 'RotationOffset' in modelNode ) transformData.rotationOffset = modelNode.RotationOffset.value;
			if ( 'RotationPivot' in modelNode ) transformData.rotationPivot = modelNode.RotationPivot.value;

			model.userData.transformData = transformData;

		},

		setLookAtProperties: function ( model, modelNode ) {

			if ( 'LookAtProperty' in modelNode ) {

				var children = connections.get( model.ID ).children;

				children.forEach( function ( child ) {

					if ( child.relationship === 'LookAtProperty' ) {

						var lookAtTarget = fbxTree.Objects.Model[ child.ID ];

						if ( 'Lcl_Translation' in lookAtTarget ) {

							var pos = lookAtTarget.Lcl_Translation.value;

							// DirectionalLight, SpotLight
							if ( model.target !== undefined ) {

								model.target.position.fromArray( pos );
								sceneGraph.add( model.target );

							} else { // Cameras and other Object3Ds

								model.lookAt( new THREE.Vector3().fromArray( pos ) );

							}

						}

					}

				} );

			}

		},

		bindSkeleton: function ( skeletons, geometryMap, modelMap ) {

			var bindMatrices = this.parsePoseNodes();

			for ( var ID in skeletons ) {

				var skeleton = skeletons[ ID ];

				var parents = connections.get( parseInt( skeleton.ID ) ).parents;

				parents.forEach( function ( parent ) {

					if ( geometryMap.has( parent.ID ) ) {

						var geoID = parent.ID;
						var geoRelationships = connections.get( geoID );

						geoRelationships.parents.forEach( function ( geoConnParent ) {

							if ( modelMap.has( geoConnParent.ID ) ) {

								var model = modelMap.get( geoConnParent.ID );

								model.bind( new THREE.Skeleton( skeleton.bones ), bindMatrices[ geoConnParent.ID ] );

							}

						} );

					}

				} );

			}

		},

		parsePoseNodes: function () {

			var bindMatrices = {};

			if ( 'Pose' in fbxTree.Objects ) {

				var BindPoseNode = fbxTree.Objects.Pose;

				for ( var nodeID in BindPoseNode ) {

					if ( BindPoseNode[ nodeID ].attrType === 'BindPose' ) {

						var poseNodes = BindPoseNode[ nodeID ].PoseNode;

						if ( Array.isArray( poseNodes ) ) {

							poseNodes.forEach( function ( poseNode ) {

								bindMatrices[ poseNode.Node ] = new THREE.Matrix4().fromArray( poseNode.Matrix.a );

							} );

						} else {

							bindMatrices[ poseNodes.Node ] = new THREE.Matrix4().fromArray( poseNodes.Matrix.a );

						}

					}

				}

			}

			return bindMatrices;

		},

		// Parse ambient color in FBXTree.GlobalSettings - if it's not set to black (default), create an ambient light
		createAmbientLight: function () {

			if ( 'GlobalSettings' in fbxTree && 'AmbientColor' in fbxTree.GlobalSettings ) {

				var ambientColor = fbxTree.GlobalSettings.AmbientColor.value;
				var r = ambientColor[ 0 ];
				var g = ambientColor[ 1 ];
				var b = ambientColor[ 2 ];

				if ( r !== 0 || g !== 0 || b !== 0 ) {

					var color = new THREE.Color( r, g, b );
					sceneGraph.add( new THREE.AmbientLight( color, 1 ) );

				}

			}

		},

		setupMorphMaterials: function () {

			var self = this;
			sceneGraph.traverse( function ( child ) {

				if ( child.isMesh ) {

					if ( child.geometry.morphAttributes.position && child.geometry.morphAttributes.position.length ) {

						if ( Array.isArray( child.material ) ) {

							child.material.forEach( function ( material, i ) {

								self.setupMorphMaterial( child, material, i );

							} );

						} else {

							self.setupMorphMaterial( child, child.material );

						}

					}

				}

			} );

		},

		setupMorphMaterial: function ( child, material, index ) {

			var uuid = child.uuid;
			var matUuid = material.uuid;

			// if a geometry has morph targets, it cannot share the material with other geometries
			var sharedMat = false;

			sceneGraph.traverse( function ( node ) {

				if ( node.isMesh ) {

					if ( Array.isArray( node.material ) ) {

						node.material.forEach( function ( mat ) {

							if ( mat.uuid === matUuid && node.uuid !== uuid ) sharedMat = true;

						} );

					} else if ( node.material.uuid === matUuid && node.uuid !== uuid ) sharedMat = true;

				}

			} );

			if ( sharedMat === true ) {

				var clonedMat = material.clone();
				clonedMat.morphTargets = true;

				if ( index === undefined ) child.material = clonedMat;
				else child.material[ index ] = clonedMat;

			} else material.morphTargets = true;

		}

	};

	// parse Geometry data from FBXTree and return map of BufferGeometries
	function GeometryParser() {}

	GeometryParser.prototype = {

		constructor: GeometryParser,

		// Parse nodes in FBXTree.Objects.Geometry
		parse: function ( deformers ) {

			var geometryMap = new Map();

			if ( 'Geometry' in fbxTree.Objects ) {

				var geoNodes = fbxTree.Objects.Geometry;

				for ( var nodeID in geoNodes ) {

					var relationships = connections.get( parseInt( nodeID ) );
					var geo = this.parseGeometry( relationships, geoNodes[ nodeID ], deformers );

					geometryMap.set( parseInt( nodeID ), geo );

				}

			}

			return geometryMap;

		},

		// Parse single node in FBXTree.Objects.Geometry
		parseGeometry: function ( relationships, geoNode, deformers ) {

			switch ( geoNode.attrType ) {

				case 'Mesh':
					return this.parseMeshGeometry( relationships, geoNode, deformers );
					break;

				case 'NurbsCurve':
					return this.parseNurbsGeometry( geoNode );
					break;

			}

		},


		// Parse single node mesh geometry in FBXTree.Objects.Geometry
		parseMeshGeometry: function ( relationships, geoNode, deformers ) {

			var skeletons = deformers.skeletons;
			var morphTargets = deformers.morphTargets;

			var modelNodes = relationships.parents.map( function ( parent ) {

				return fbxTree.Objects.Model[ parent.ID ];

			} );

			// don't create geometry if it is not associated with any models
			if ( modelNodes.length === 0 ) return;

			var skeleton = relationships.children.reduce( function ( skeleton, child ) {

				if ( skeletons[ child.ID ] !== undefined ) skeleton = skeletons[ child.ID ];

				return skeleton;

			}, null );

			var morphTarget = relationships.children.reduce( function ( morphTarget, child ) {

				if ( morphTargets[ child.ID ] !== undefined ) morphTarget = morphTargets[ child.ID ];

				return morphTarget;

			}, null );

			// Assume one model and get the preRotation from that
			// if there is more than one model associated with the geometry this may cause problems
			var modelNode = modelNodes[ 0 ];

			var transformData = {};

			if ( 'RotationOrder' in modelNode ) transformData.eulerOrder = getEulerOrder( modelNode.RotationOrder.value );
			if ( 'InheritType' in modelNode ) transformData.inheritType = parseInt( modelNode.InheritType.value );

			if ( 'GeometricTranslation' in modelNode ) transformData.translation = modelNode.GeometricTranslation.value;
			if ( 'GeometricRotation' in modelNode ) transformData.rotation = modelNode.GeometricRotation.value;
			if ( 'GeometricScaling' in modelNode ) transformData.scale = modelNode.GeometricScaling.value;

			var transform = generateTransform( transformData );

			return this.genGeometry( geoNode, skeleton, morphTarget, transform );

		},

		// Generate a THREE.BufferGeometry from a node in FBXTree.Objects.Geometry
		genGeometry: function ( geoNode, skeleton, morphTarget, preTransform ) {

			var geo = new THREE.BufferGeometry();
			if ( geoNode.attrName ) geo.name = geoNode.attrName;

			var geoInfo = this.parseGeoNode( geoNode, skeleton );
			var buffers = this.genBuffers( geoInfo );

			var positionAttribute = new THREE.Float32BufferAttribute( buffers.vertex, 3 );

			preTransform.applyToBufferAttribute( positionAttribute );

			geo.addAttribute( 'position', positionAttribute );

			if ( buffers.colors.length > 0 ) {

				geo.addAttribute( 'color', new THREE.Float32BufferAttribute( buffers.colors, 3 ) );

			}

			if ( skeleton ) {

				geo.addAttribute( 'skinIndex', new THREE.Uint16BufferAttribute( buffers.weightsIndices, 4 ) );

				geo.addAttribute( 'skinWeight', new THREE.Float32BufferAttribute( buffers.vertexWeights, 4 ) );

				// used later to bind the skeleton to the model
				geo.FBX_Deformer = skeleton;

			}

			if ( buffers.normal.length > 0 ) {

				var normalAttribute = new THREE.Float32BufferAttribute( buffers.normal, 3 );

				var normalMatrix = new THREE.Matrix3().getNormalMatrix( preTransform );
				normalMatrix.applyToBufferAttribute( normalAttribute );

				geo.addAttribute( 'normal', normalAttribute );

			}

			buffers.uvs.forEach( function ( uvBuffer, i ) {

				// subsequent uv buffers are called 'uv1', 'uv2', ...
				var name = 'uv' + ( i + 1 ).toString();

				// the first uv buffer is just called 'uv'
				if ( i === 0 ) {

					name = 'uv';

				}

				geo.addAttribute( name, new THREE.Float32BufferAttribute( buffers.uvs[ i ], 2 ) );

			} );

			if ( geoInfo.material && geoInfo.material.mappingType !== 'AllSame' ) {

				// Convert the material indices of each vertex into rendering groups on the geometry.
				var prevMaterialIndex = buffers.materialIndex[ 0 ];
				var startIndex = 0;

				buffers.materialIndex.forEach( function ( currentIndex, i ) {

					if ( currentIndex !== prevMaterialIndex ) {

						geo.addGroup( startIndex, i - startIndex, prevMaterialIndex );

						prevMaterialIndex = currentIndex;
						startIndex = i;

					}

				} );

				// the loop above doesn't add the last group, do that here.
				if ( geo.groups.length > 0 ) {

					var lastGroup = geo.groups[ geo.groups.length - 1 ];
					var lastIndex = lastGroup.start + lastGroup.count;

					if ( lastIndex !== buffers.materialIndex.length ) {

						geo.addGroup( lastIndex, buffers.materialIndex.length - lastIndex, prevMaterialIndex );

					}

				}

				// case where there are multiple materials but the whole geometry is only
				// using one of them
				if ( geo.groups.length === 0 ) {

					geo.addGroup( 0, buffers.materialIndex.length, buffers.materialIndex[ 0 ] );

				}

			}

			this.addMorphTargets( geo, geoNode, morphTarget, preTransform );

			return geo;

		},

		parseGeoNode: function ( geoNode, skeleton ) {

			var geoInfo = {};

			geoInfo.vertexPositions = ( geoNode.Vertices !== undefined ) ? geoNode.Vertices.a : [];
			geoInfo.vertexIndices = ( geoNode.PolygonVertexIndex !== undefined ) ? geoNode.PolygonVertexIndex.a : [];

			if ( geoNode.LayerElementColor ) {

				geoInfo.color = this.parseVertexColors( geoNode.LayerElementColor[ 0 ] );

			}

			if ( geoNode.LayerElementMaterial ) {

				geoInfo.material = this.parseMaterialIndices( geoNode.LayerElementMaterial[ 0 ] );

			}

			if ( geoNode.LayerElementNormal ) {

				geoInfo.normal = this.parseNormals( geoNode.LayerElementNormal[ 0 ] );

			}

			if ( geoNode.LayerElementUV ) {

				geoInfo.uv = [];

				var i = 0;
				while ( geoNode.LayerElementUV[ i ] ) {

					geoInfo.uv.push( this.parseUVs( geoNode.LayerElementUV[ i ] ) );
					i ++;

				}

			}

			geoInfo.weightTable = {};

			if ( skeleton !== null ) {

				geoInfo.skeleton = skeleton;

				skeleton.rawBones.forEach( function ( rawBone, i ) {

					// loop over the bone's vertex indices and weights
					rawBone.indices.forEach( function ( index, j ) {

						if ( geoInfo.weightTable[ index ] === undefined ) geoInfo.weightTable[ index ] = [];

						geoInfo.weightTable[ index ].push( {

							id: i,
							weight: rawBone.weights[ j ],

						} );

					} );

				} );

			}

			return geoInfo;

		},

		genBuffers: function ( geoInfo ) {

			var buffers = {
				vertex: [],
				normal: [],
				colors: [],
				uvs: [],
				materialIndex: [],
				vertexWeights: [],
				weightsIndices: [],
			};

			var polygonIndex = 0;
			var faceLength = 0;
			var displayedWeightsWarning = false;

			// these will hold data for a single face
			var facePositionIndexes = [];
			var faceNormals = [];
			var faceColors = [];
			var faceUVs = [];
			var faceWeights = [];
			var faceWeightIndices = [];

			var self = this;
			geoInfo.vertexIndices.forEach( function ( vertexIndex, polygonVertexIndex ) {

				var endOfFace = false;

				// Face index and vertex index arrays are combined in a single array
				// A cube with quad faces looks like this:
				// PolygonVertexIndex: *24 {
				//  a: 0, 1, 3, -3, 2, 3, 5, -5, 4, 5, 7, -7, 6, 7, 1, -1, 1, 7, 5, -4, 6, 0, 2, -5
				//  }
				// Negative numbers mark the end of a face - first face here is 0, 1, 3, -3
				// to find index of last vertex bit shift the index: ^ - 1
				if ( vertexIndex < 0 ) {

					vertexIndex = vertexIndex ^ - 1; // equivalent to ( x * -1 ) - 1
					endOfFace = true;

				}

				var weightIndices = [];
				var weights = [];

				facePositionIndexes.push( vertexIndex * 3, vertexIndex * 3 + 1, vertexIndex * 3 + 2 );

				if ( geoInfo.color ) {

					var data = getData( polygonVertexIndex, polygonIndex, vertexIndex, geoInfo.color );

					faceColors.push( data[ 0 ], data[ 1 ], data[ 2 ] );

				}

				if ( geoInfo.skeleton ) {

					if ( geoInfo.weightTable[ vertexIndex ] !== undefined ) {

						geoInfo.weightTable[ vertexIndex ].forEach( function ( wt ) {

							weights.push( wt.weight );
							weightIndices.push( wt.id );

						} );


					}

					if ( weights.length > 4 ) {

						if ( ! displayedWeightsWarning ) {

							console.warn( 'THREE.FBXLoader: Vertex has more than 4 skinning weights assigned to vertex. Deleting additional weights.' );
							displayedWeightsWarning = true;

						}

						var wIndex = [ 0, 0, 0, 0 ];
						var Weight = [ 0, 0, 0, 0 ];

						weights.forEach( function ( weight, weightIndex ) {

							var currentWeight = weight;
							var currentIndex = weightIndices[ weightIndex ];

							Weight.forEach( function ( comparedWeight, comparedWeightIndex, comparedWeightArray ) {

								if ( currentWeight > comparedWeight ) {

									comparedWeightArray[ comparedWeightIndex ] = currentWeight;
									currentWeight = comparedWeight;

									var tmp = wIndex[ comparedWeightIndex ];
									wIndex[ comparedWeightIndex ] = currentIndex;
									currentIndex = tmp;

								}

							} );

						} );

						weightIndices = wIndex;
						weights = Weight;

					}

					// if the weight array is shorter than 4 pad with 0s
					while ( weights.length < 4 ) {

						weights.push( 0 );
						weightIndices.push( 0 );

					}

					for ( var i = 0; i < 4; ++ i ) {

						faceWeights.push( weights[ i ] );
						faceWeightIndices.push( weightIndices[ i ] );

					}

				}

				if ( geoInfo.normal ) {

					var data = getData( polygonVertexIndex, polygonIndex, vertexIndex, geoInfo.normal );

					faceNormals.push( data[ 0 ], data[ 1 ], data[ 2 ] );

				}

				if ( geoInfo.material && geoInfo.material.mappingType !== 'AllSame' ) {

					var materialIndex = getData( polygonVertexIndex, polygonIndex, vertexIndex, geoInfo.material )[ 0 ];

				}

				if ( geoInfo.uv ) {

					geoInfo.uv.forEach( function ( uv, i ) {

						var data = getData( polygonVertexIndex, polygonIndex, vertexIndex, uv );

						if ( faceUVs[ i ] === undefined ) {

							faceUVs[ i ] = [];

						}

						faceUVs[ i ].push( data[ 0 ] );
						faceUVs[ i ].push( data[ 1 ] );

					} );

				}

				faceLength ++;

				if ( endOfFace ) {

					self.genFace( buffers, geoInfo, facePositionIndexes, materialIndex, faceNormals, faceColors, faceUVs, faceWeights, faceWeightIndices, faceLength );

					polygonIndex ++;
					faceLength = 0;

					// reset arrays for the next face
					facePositionIndexes = [];
					faceNormals = [];
					faceColors = [];
					faceUVs = [];
					faceWeights = [];
					faceWeightIndices = [];

				}

			} );

			return buffers;

		},

		// Generate data for a single face in a geometry. If the face is a quad then split it into 2 tris
		genFace: function ( buffers, geoInfo, facePositionIndexes, materialIndex, faceNormals, faceColors, faceUVs, faceWeights, faceWeightIndices, faceLength ) {

			for ( var i = 2; i < faceLength; i ++ ) {

				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ 0 ] ] );
				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ 1 ] ] );
				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ 2 ] ] );

				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ ( i - 1 ) * 3 ] ] );
				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ ( i - 1 ) * 3 + 1 ] ] );
				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ ( i - 1 ) * 3 + 2 ] ] );

				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ i * 3 ] ] );
				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ i * 3 + 1 ] ] );
				buffers.vertex.push( geoInfo.vertexPositions[ facePositionIndexes[ i * 3 + 2 ] ] );

				if ( geoInfo.skeleton ) {

					buffers.vertexWeights.push( faceWeights[ 0 ] );
					buffers.vertexWeights.push( faceWeights[ 1 ] );
					buffers.vertexWeights.push( faceWeights[ 2 ] );
					buffers.vertexWeights.push( faceWeights[ 3 ] );

					buffers.vertexWeights.push( faceWeights[ ( i - 1 ) * 4 ] );
					buffers.vertexWeights.push( faceWeights[ ( i - 1 ) * 4 + 1 ] );
					buffers.vertexWeights.push( faceWeights[ ( i - 1 ) * 4 + 2 ] );
					buffers.vertexWeights.push( faceWeights[ ( i - 1 ) * 4 + 3 ] );

					buffers.vertexWeights.push( faceWeights[ i * 4 ] );
					buffers.vertexWeights.push( faceWeights[ i * 4 + 1 ] );
					buffers.vertexWeights.push( faceWeights[ i * 4 + 2 ] );
					buffers.vertexWeights.push( faceWeights[ i * 4 + 3 ] );

					buffers.weightsIndices.push( faceWeightIndices[ 0 ] );
					buffers.weightsIndices.push( faceWeightIndices[ 1 ] );
					buffers.weightsIndices.push( faceWeightIndices[ 2 ] );
					buffers.weightsIndices.push( faceWeightIndices[ 3 ] );

					buffers.weightsIndices.push( faceWeightIndices[ ( i - 1 ) * 4 ] );
					buffers.weightsIndices.push( faceWeightIndices[ ( i - 1 ) * 4 + 1 ] );
					buffers.weightsIndices.push( faceWeightIndices[ ( i - 1 ) * 4 + 2 ] );
					buffers.weightsIndices.push( faceWeightIndices[ ( i - 1 ) * 4 + 3 ] );

					buffers.weightsIndices.push( faceWeightIndices[ i * 4 ] );
					buffers.weightsIndices.push( faceWeightIndices[ i * 4 + 1 ] );
					buffers.weightsIndices.push( faceWeightIndices[ i * 4 + 2 ] );
					buffers.weightsIndices.push( faceWeightIndices[ i * 4 + 3 ] );

				}

				if ( geoInfo.color ) {

					buffers.colors.push( faceColors[ 0 ] );
					buffers.colors.push( faceColors[ 1 ] );
					buffers.colors.push( faceColors[ 2 ] );

					buffers.colors.push( faceColors[ ( i - 1 ) * 3 ] );
					buffers.colors.push( faceColors[ ( i - 1 ) * 3 + 1 ] );
					buffers.colors.push( faceColors[ ( i - 1 ) * 3 + 2 ] );

					buffers.colors.push( faceColors[ i * 3 ] );
					buffers.colors.push( faceColors[ i * 3 + 1 ] );
					buffers.colors.push( faceColors[ i * 3 + 2 ] );

				}

				if ( geoInfo.material && geoInfo.material.mappingType !== 'AllSame' ) {

					buffers.materialIndex.push( materialIndex );
					buffers.materialIndex.push( materialIndex );
					buffers.materialIndex.push( materialIndex );

				}

				if ( geoInfo.normal ) {

					buffers.normal.push( faceNormals[ 0 ] );
					buffers.normal.push( faceNormals[ 1 ] );
					buffers.normal.push( faceNormals[ 2 ] );

					buffers.normal.push( faceNormals[ ( i - 1 ) * 3 ] );
					buffers.normal.push( faceNormals[ ( i - 1 ) * 3 + 1 ] );
					buffers.normal.push( faceNormals[ ( i - 1 ) * 3 + 2 ] );

					buffers.normal.push( faceNormals[ i * 3 ] );
					buffers.normal.push( faceNormals[ i * 3 + 1 ] );
					buffers.normal.push( faceNormals[ i * 3 + 2 ] );

				}

				if ( geoInfo.uv ) {

					geoInfo.uv.forEach( function ( uv, j ) {

						if ( buffers.uvs[ j ] === undefined ) buffers.uvs[ j ] = [];

						buffers.uvs[ j ].push( faceUVs[ j ][ 0 ] );
						buffers.uvs[ j ].push( faceUVs[ j ][ 1 ] );

						buffers.uvs[ j ].push( faceUVs[ j ][ ( i - 1 ) * 2 ] );
						buffers.uvs[ j ].push( faceUVs[ j ][ ( i - 1 ) * 2 + 1 ] );

						buffers.uvs[ j ].push( faceUVs[ j ][ i * 2 ] );
						buffers.uvs[ j ].push( faceUVs[ j ][ i * 2 + 1 ] );

					} );

				}

			}

		},

		addMorphTargets: function ( parentGeo, parentGeoNode, morphTarget, preTransform ) {

			if ( morphTarget === null ) return;

			parentGeo.morphAttributes.position = [];
			// parentGeo.morphAttributes.normal = []; // not implemented

			var self = this;
			morphTarget.rawTargets.forEach( function ( rawTarget ) {

				var morphGeoNode = fbxTree.Objects.Geometry[ rawTarget.geoID ];

				if ( morphGeoNode !== undefined ) {

					self.genMorphGeometry( parentGeo, parentGeoNode, morphGeoNode, preTransform, rawTarget.name );

				}

			} );

		},

		// a morph geometry node is similar to a standard  node, and the node is also contained
		// in FBXTree.Objects.Geometry, however it can only have attributes for position, normal
		// and a special attribute Index defining which vertices of the original geometry are affected
		// Normal and position attributes only have data for the vertices that are affected by the morph
		genMorphGeometry: function ( parentGeo, parentGeoNode, morphGeoNode, preTransform, name ) {

			var morphGeo = new THREE.BufferGeometry();
			if ( morphGeoNode.attrName ) morphGeo.name = morphGeoNode.attrName;

			var vertexIndices = ( parentGeoNode.PolygonVertexIndex !== undefined ) ? parentGeoNode.PolygonVertexIndex.a : [];

			// make a copy of the parent's vertex positions
			var vertexPositions = ( parentGeoNode.Vertices !== undefined ) ? parentGeoNode.Vertices.a.slice() : [];

			var morphPositions = ( morphGeoNode.Vertices !== undefined ) ? morphGeoNode.Vertices.a : [];
			var indices = ( morphGeoNode.Indexes !== undefined ) ? morphGeoNode.Indexes.a : [];

			for ( var i = 0; i < indices.length; i ++ ) {

				var morphIndex = indices[ i ] * 3;

				// FBX format uses blend shapes rather than morph targets. This can be converted
				// by additively combining the blend shape positions with the original geometry's positions
				vertexPositions[ morphIndex ] += morphPositions[ i * 3 ];
				vertexPositions[ morphIndex + 1 ] += morphPositions[ i * 3 + 1 ];
				vertexPositions[ morphIndex + 2 ] += morphPositions[ i * 3 + 2 ];

			}

			// TODO: add morph normal support
			var morphGeoInfo = {
				vertexIndices: vertexIndices,
				vertexPositions: vertexPositions,
			};

			var morphBuffers = this.genBuffers( morphGeoInfo );

			var positionAttribute = new THREE.Float32BufferAttribute( morphBuffers.vertex, 3 );
			positionAttribute.name = name || morphGeoNode.attrName;

			preTransform.applyToBufferAttribute( positionAttribute );

			parentGeo.morphAttributes.position.push( positionAttribute );

		},

		// Parse normal from FBXTree.Objects.Geometry.LayerElementNormal if it exists
		parseNormals: function ( NormalNode ) {

			var mappingType = NormalNode.MappingInformationType;
			var referenceType = NormalNode.ReferenceInformationType;
			var buffer = NormalNode.Normals.a;
			var indexBuffer = [];
			if ( referenceType === 'IndexToDirect' ) {

				if ( 'NormalIndex' in NormalNode ) {

					indexBuffer = NormalNode.NormalIndex.a;

				} else if ( 'NormalsIndex' in NormalNode ) {

					indexBuffer = NormalNode.NormalsIndex.a;

				}

			}

			return {
				dataSize: 3,
				buffer: buffer,
				indices: indexBuffer,
				mappingType: mappingType,
				referenceType: referenceType
			};

		},

		// Parse UVs from FBXTree.Objects.Geometry.LayerElementUV if it exists
		parseUVs: function ( UVNode ) {

			var mappingType = UVNode.MappingInformationType;
			var referenceType = UVNode.ReferenceInformationType;
			var buffer = UVNode.UV.a;
			var indexBuffer = [];
			if ( referenceType === 'IndexToDirect' ) {

				indexBuffer = UVNode.UVIndex.a;

			}

			return {
				dataSize: 2,
				buffer: buffer,
				indices: indexBuffer,
				mappingType: mappingType,
				referenceType: referenceType
			};

		},

		// Parse Vertex Colors from FBXTree.Objects.Geometry.LayerElementColor if it exists
		parseVertexColors: function ( ColorNode ) {

			var mappingType = ColorNode.MappingInformationType;
			var referenceType = ColorNode.ReferenceInformationType;
			var buffer = ColorNode.Colors.a;
			var indexBuffer = [];
			if ( referenceType === 'IndexToDirect' ) {

				indexBuffer = ColorNode.ColorIndex.a;

			}

			return {
				dataSize: 4,
				buffer: buffer,
				indices: indexBuffer,
				mappingType: mappingType,
				referenceType: referenceType
			};

		},

		// Parse mapping and material data in FBXTree.Objects.Geometry.LayerElementMaterial if it exists
		parseMaterialIndices: function ( MaterialNode ) {

			var mappingType = MaterialNode.MappingInformationType;
			var referenceType = MaterialNode.ReferenceInformationType;

			if ( mappingType === 'NoMappingInformation' ) {

				return {
					dataSize: 1,
					buffer: [ 0 ],
					indices: [ 0 ],
					mappingType: 'AllSame',
					referenceType: referenceType
				};

			}

			var materialIndexBuffer = MaterialNode.Materials.a;

			// Since materials are stored as indices, there's a bit of a mismatch between FBX and what
			// we expect.So we create an intermediate buffer that points to the index in the buffer,
			// for conforming with the other functions we've written for other data.
			var materialIndices = [];

			for ( var i = 0; i < materialIndexBuffer.length; ++ i ) {

				materialIndices.push( i );

			}

			return {
				dataSize: 1,
				buffer: materialIndexBuffer,
				indices: materialIndices,
				mappingType: mappingType,
				referenceType: referenceType
			};

		},

		// Generate a NurbGeometry from a node in FBXTree.Objects.Geometry
		parseNurbsGeometry: function ( geoNode ) {

			if ( NURBSCurve === undefined ) {

				console.error( 'THREE.FBXLoader: The loader relies on THREE.NURBSCurve for any nurbs present in the model. Nurbs will show up as empty geometry.' );
				return new THREE.BufferGeometry();

			}

			var order = parseInt( geoNode.Order );

			if ( isNaN( order ) ) {

				console.error( 'THREE.FBXLoader: Invalid Order %s given for geometry ID: %s', geoNode.Order, geoNode.id );
				return new THREE.BufferGeometry();

			}

			var degree = order - 1;

			var knots = geoNode.KnotVector.a;
			var controlPoints = [];
			var pointsValues = geoNode.Points.a;

			for ( var i = 0, l = pointsValues.length; i < l; i += 4 ) {

				controlPoints.push( new THREE.Vector4().fromArray( pointsValues, i ) );

			}

			var startKnot, endKnot;

			if ( geoNode.Form === 'Closed' ) {

				controlPoints.push( controlPoints[ 0 ] );

			} else if ( geoNode.Form === 'Periodic' ) {

				startKnot = degree;
				endKnot = knots.length - 1 - startKnot;

				for ( var i = 0; i < degree; ++ i ) {

					controlPoints.push( controlPoints[ i ] );

				}

			}

			var curve = new NURBSCurve( degree, knots, controlPoints, startKnot, endKnot );
			var vertices = curve.getPoints( controlPoints.length * 7 );

			var positions = new Float32Array( vertices.length * 3 );

			vertices.forEach( function ( vertex, i ) {

				vertex.toArray( positions, i * 3 );

			} );

			var geometry = new THREE.BufferGeometry();
			geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );

			return geometry;

		},

	};

	// parse animation data from FBXTree
	function AnimationParser() {}

	AnimationParser.prototype = {

		constructor: AnimationParser,

		// take raw animation clips and turn them into three.js animation clips
		parse: function () {

			var animationClips = [];

			var rawClips = this.parseClips();

			if ( rawClips !== undefined ) {

				for ( var key in rawClips ) {

					var rawClip = rawClips[ key ];

					var clip = this.addClip( rawClip );

					animationClips.push( clip );

				}

			}

			return animationClips;

		},

		parseClips: function () {

			// since the actual transformation data is stored in FBXTree.Objects.AnimationCurve,
			// if this is undefined we can safely assume there are no animations
			if ( fbxTree.Objects.AnimationCurve === undefined ) return undefined;

			var curveNodesMap = this.parseAnimationCurveNodes();

			this.parseAnimationCurves( curveNodesMap );

			var layersMap = this.parseAnimationLayers( curveNodesMap );
			var rawClips = this.parseAnimStacks( layersMap );

			return rawClips;

		},

		// parse nodes in FBXTree.Objects.AnimationCurveNode
		// each AnimationCurveNode holds data for an animation transform for a model (e.g. left arm rotation )
		// and is referenced by an AnimationLayer
		parseAnimationCurveNodes: function () {

			var rawCurveNodes = fbxTree.Objects.AnimationCurveNode;

			var curveNodesMap = new Map();

			for ( var nodeID in rawCurveNodes ) {

				var rawCurveNode = rawCurveNodes[ nodeID ];

				if ( rawCurveNode.attrName.match( /S|R|T|DeformPercent/ ) !== null ) {

					var curveNode = {

						id: rawCurveNode.id,
						attr: rawCurveNode.attrName,
						curves: {},

					};

					curveNodesMap.set( curveNode.id, curveNode );

				}

			}

			return curveNodesMap;

		},

		// parse nodes in FBXTree.Objects.AnimationCurve and connect them up to
		// previously parsed AnimationCurveNodes. Each AnimationCurve holds data for a single animated
		// axis ( e.g. times and values of x rotation)
		parseAnimationCurves: function ( curveNodesMap ) {

			var rawCurves = fbxTree.Objects.AnimationCurve;

			// TODO: Many values are identical up to roundoff error, but won't be optimised
			// e.g. position times: [0, 0.4, 0. 8]
			// position values: [7.23538335023477e-7, 93.67518615722656, -0.9982695579528809, 7.23538335023477e-7, 93.67518615722656, -0.9982695579528809, 7.235384487103147e-7, 93.67520904541016, -0.9982695579528809]
			// clearly, this should be optimised to
			// times: [0], positions [7.23538335023477e-7, 93.67518615722656, -0.9982695579528809]
			// this shows up in nearly every FBX file, and generally time array is length > 100

			for ( var nodeID in rawCurves ) {

				var animationCurve = {

					id: rawCurves[ nodeID ].id,
					times: rawCurves[ nodeID ].KeyTime.a.map( convertFBXTimeToSeconds ),
					values: rawCurves[ nodeID ].KeyValueFloat.a,

				};

				var relationships = connections.get( animationCurve.id );

				if ( relationships !== undefined ) {

					var animationCurveID = relationships.parents[ 0 ].ID;
					var animationCurveRelationship = relationships.parents[ 0 ].relationship;

					if ( animationCurveRelationship.match( /X/ ) ) {

						curveNodesMap.get( animationCurveID ).curves[ 'x' ] = animationCurve;

					} else if ( animationCurveRelationship.match( /Y/ ) ) {

						curveNodesMap.get( animationCurveID ).curves[ 'y' ] = animationCurve;

					} else if ( animationCurveRelationship.match( /Z/ ) ) {

						curveNodesMap.get( animationCurveID ).curves[ 'z' ] = animationCurve;

					} else if ( animationCurveRelationship.match( /d|DeformPercent/ ) && curveNodesMap.has( animationCurveID ) ) {

						curveNodesMap.get( animationCurveID ).curves[ 'morph' ] = animationCurve;

					}

				}

			}

		},

		// parse nodes in FBXTree.Objects.AnimationLayer. Each layers holds references
		// to various AnimationCurveNodes and is referenced by an AnimationStack node
		// note: theoretically a stack can have multiple layers, however in practice there always seems to be one per stack
		parseAnimationLayers: function ( curveNodesMap ) {

			var rawLayers = fbxTree.Objects.AnimationLayer;

			var layersMap = new Map();

			for ( var nodeID in rawLayers ) {

				var layerCurveNodes = [];

				var connection = connections.get( parseInt( nodeID ) );

				if ( connection !== undefined ) {

					// all the animationCurveNodes used in the layer
					var children = connection.children;

					children.forEach( function ( child, i ) {

						if ( curveNodesMap.has( child.ID ) ) {

							var curveNode = curveNodesMap.get( child.ID );

							// check that the curves are defined for at least one axis, otherwise ignore the curveNode
							if ( curveNode.curves.x !== undefined || curveNode.curves.y !== undefined || curveNode.curves.z !== undefined ) {

								if ( layerCurveNodes[ i ] === undefined ) {

									var modelID = connections.get( child.ID ).parents.filter( function ( parent ) {

										return parent.relationship !== undefined;

									} )[ 0 ].ID;

									if ( modelID !== undefined ) {

										var rawModel = fbxTree.Objects.Model[ modelID.toString() ];

										var node = {

											modelName: THREE.PropertyBinding.sanitizeNodeName( rawModel.attrName ),
											ID: rawModel.id,
											initialPosition: [ 0, 0, 0 ],
											initialRotation: [ 0, 0, 0 ],
											initialScale: [ 1, 1, 1 ],

										};

										sceneGraph.traverse( function ( child ) {

											if ( child.ID === rawModel.id ) {

												node.transform = child.matrix;

												if ( child.userData.transformData ) node.eulerOrder = child.userData.transformData.eulerOrder;

											}

										} );

										if ( ! node.transform ) node.transform = new THREE.Matrix4();

										// if the animated model is pre rotated, we'll have to apply the pre rotations to every
										// animation value as well
										if ( 'PreRotation' in rawModel ) node.preRotation = rawModel.PreRotation.value;
										if ( 'PostRotation' in rawModel ) node.postRotation = rawModel.PostRotation.value;

										layerCurveNodes[ i ] = node;

									}

								}

								if ( layerCurveNodes[ i ] ) layerCurveNodes[ i ][ curveNode.attr ] = curveNode;

							} else if ( curveNode.curves.morph !== undefined ) {

								if ( layerCurveNodes[ i ] === undefined ) {

									var deformerID = connections.get( child.ID ).parents.filter( function ( parent ) {

										return parent.relationship !== undefined;

									} )[ 0 ].ID;

									var morpherID = connections.get( deformerID ).parents[ 0 ].ID;
									var geoID = connections.get( morpherID ).parents[ 0 ].ID;

									// assuming geometry is not used in more than one model
									var modelID = connections.get( geoID ).parents[ 0 ].ID;

									var rawModel = fbxTree.Objects.Model[ modelID ];

									var node = {

										modelName: THREE.PropertyBinding.sanitizeNodeName( rawModel.attrName ),
										morphName: fbxTree.Objects.Deformer[ deformerID ].attrName,

									};

									layerCurveNodes[ i ] = node;

								}

								layerCurveNodes[ i ][ curveNode.attr ] = curveNode;

							}

						}

					} );

					layersMap.set( parseInt( nodeID ), layerCurveNodes );

				}

			}

			return layersMap;

		},

		// parse nodes in FBXTree.Objects.AnimationStack. These are the top level node in the animation
		// hierarchy. Each Stack node will be used to create a THREE.AnimationClip
		parseAnimStacks: function ( layersMap ) {

			var rawStacks = fbxTree.Objects.AnimationStack;

			// connect the stacks (clips) up to the layers
			var rawClips = {};

			for ( var nodeID in rawStacks ) {

				var children = connections.get( parseInt( nodeID ) ).children;

				if ( children.length > 1 ) {

					// it seems like stacks will always be associated with a single layer. But just in case there are files
					// where there are multiple layers per stack, we'll display a warning
					console.warn( 'THREE.FBXLoader: Encountered an animation stack with multiple layers, this is currently not supported. Ignoring subsequent layers.' );

				}

				var layer = layersMap.get( children[ 0 ].ID );

				rawClips[ nodeID ] = {

					name: rawStacks[ nodeID ].attrName,
					layer: layer,

				};

			}

			return rawClips;

		},

		addClip: function ( rawClip ) {

			var tracks = [];

			var self = this;
			rawClip.layer.forEach( function ( rawTracks ) {

				tracks = tracks.concat( self.generateTracks( rawTracks ) );

			} );

			return new THREE.AnimationClip( rawClip.name, - 1, tracks );

		},

		generateTracks: function ( rawTracks ) {

			var tracks = [];

			var initialPosition = new THREE.Vector3();
			var initialRotation = new THREE.Quaternion();
			var initialScale = new THREE.Vector3();

			if ( rawTracks.transform ) rawTracks.transform.decompose( initialPosition, initialRotation, initialScale );

			initialPosition = initialPosition.toArray();
			initialRotation = new THREE.Euler().setFromQuaternion( initialRotation, rawTracks.eulerOrder ).toArray();
			initialScale = initialScale.toArray();

			if ( rawTracks.T !== undefined && Object.keys( rawTracks.T.curves ).length > 0 ) {

				var positionTrack = this.generateVectorTrack( rawTracks.modelName, rawTracks.T.curves, initialPosition, 'position' );
				if ( positionTrack !== undefined ) tracks.push( positionTrack );

			}

			if ( rawTracks.R !== undefined && Object.keys( rawTracks.R.curves ).length > 0 ) {

				var rotationTrack = this.generateRotationTrack( rawTracks.modelName, rawTracks.R.curves, initialRotation, rawTracks.preRotation, rawTracks.postRotation, rawTracks.eulerOrder );
				if ( rotationTrack !== undefined ) tracks.push( rotationTrack );

			}

			if ( rawTracks.S !== undefined && Object.keys( rawTracks.S.curves ).length > 0 ) {

				var scaleTrack = this.generateVectorTrack( rawTracks.modelName, rawTracks.S.curves, initialScale, 'scale' );
				if ( scaleTrack !== undefined ) tracks.push( scaleTrack );

			}

			if ( rawTracks.DeformPercent !== undefined ) {

				var morphTrack = this.generateMorphTrack( rawTracks );
				if ( morphTrack !== undefined ) tracks.push( morphTrack );

			}

			return tracks;

		},

		generateVectorTrack: function ( modelName, curves, initialValue, type ) {

			var times = this.getTimesForAllAxes( curves );
			var values = this.getKeyframeTrackValues( times, curves, initialValue );

			return new THREE.VectorKeyframeTrack( modelName + '.' + type, times, values );

		},

		generateRotationTrack: function ( modelName, curves, initialValue, preRotation, postRotation, eulerOrder ) {

			if ( curves.x !== undefined ) {

				this.interpolateRotations( curves.x );
				curves.x.values = curves.x.values.map( THREE.Math.degToRad );

			}
			if ( curves.y !== undefined ) {

				this.interpolateRotations( curves.y );
				curves.y.values = curves.y.values.map( THREE.Math.degToRad );

			}
			if ( curves.z !== undefined ) {

				this.interpolateRotations( curves.z );
				curves.z.values = curves.z.values.map( THREE.Math.degToRad );

			}

			var times = this.getTimesForAllAxes( curves );
			var values = this.getKeyframeTrackValues( times, curves, initialValue );

			if ( preRotation !== undefined ) {

				preRotation = preRotation.map( THREE.Math.degToRad );
				preRotation.push( eulerOrder );

				preRotation = new THREE.Euler().fromArray( preRotation );
				preRotation = new THREE.Quaternion().setFromEuler( preRotation );

			}

			if ( postRotation !== undefined ) {

				postRotation = postRotation.map( THREE.Math.degToRad );
				postRotation.push( eulerOrder );

				postRotation = new THREE.Euler().fromArray( postRotation );
				postRotation = new THREE.Quaternion().setFromEuler( postRotation ).inverse();

			}

			var quaternion = new THREE.Quaternion();
			var euler = new THREE.Euler();

			var quaternionValues = [];

			for ( var i = 0; i < values.length; i += 3 ) {

				euler.set( values[ i ], values[ i + 1 ], values[ i + 2 ], eulerOrder );

				quaternion.setFromEuler( euler );

				if ( preRotation !== undefined ) quaternion.premultiply( preRotation );
				if ( postRotation !== undefined ) quaternion.multiply( postRotation );

				quaternion.toArray( quaternionValues, ( i / 3 ) * 4 );

			}

			return new THREE.QuaternionKeyframeTrack( modelName + '.quaternion', times, quaternionValues );

		},

		generateMorphTrack: function ( rawTracks ) {

			var curves = rawTracks.DeformPercent.curves.morph;
			var values = curves.values.map( function ( val ) {

				return val / 100;

			} );

			var morphNum = sceneGraph.getObjectByName( rawTracks.modelName ).morphTargetDictionary[ rawTracks.morphName ];

			return new THREE.NumberKeyframeTrack( rawTracks.modelName + '.morphTargetInfluences[' + morphNum + ']', curves.times, values );

		},

		// For all animated objects, times are defined separately for each axis
		// Here we'll combine the times into one sorted array without duplicates
		getTimesForAllAxes: function ( curves ) {

			var times = [];

			// first join together the times for each axis, if defined
			if ( curves.x !== undefined ) times = times.concat( curves.x.times );
			if ( curves.y !== undefined ) times = times.concat( curves.y.times );
			if ( curves.z !== undefined ) times = times.concat( curves.z.times );

			// then sort them and remove duplicates
			times = times.sort( function ( a, b ) {

				return a - b;

			} ).filter( function ( elem, index, array ) {

				return array.indexOf( elem ) == index;

			} );

			return times;

		},

		getKeyframeTrackValues: function ( times, curves, initialValue ) {

			var prevValue = initialValue;

			var values = [];

			var xIndex = - 1;
			var yIndex = - 1;
			var zIndex = - 1;

			times.forEach( function ( time ) {

				if ( curves.x ) xIndex = curves.x.times.indexOf( time );
				if ( curves.y ) yIndex = curves.y.times.indexOf( time );
				if ( curves.z ) zIndex = curves.z.times.indexOf( time );

				// if there is an x value defined for this frame, use that
				if ( xIndex !== - 1 ) {

					var xValue = curves.x.values[ xIndex ];
					values.push( xValue );
					prevValue[ 0 ] = xValue;

				} else {

					// otherwise use the x value from the previous frame
					values.push( prevValue[ 0 ] );

				}

				if ( yIndex !== - 1 ) {

					var yValue = curves.y.values[ yIndex ];
					values.push( yValue );
					prevValue[ 1 ] = yValue;

				} else {

					values.push( prevValue[ 1 ] );

				}

				if ( zIndex !== - 1 ) {

					var zValue = curves.z.values[ zIndex ];
					values.push( zValue );
					prevValue[ 2 ] = zValue;

				} else {

					values.push( prevValue[ 2 ] );

				}

			} );

			return values;

		},

		// Rotations are defined as Euler angles which can have values  of any size
		// These will be converted to quaternions which don't support values greater than
		// PI, so we'll interpolate large rotations
		interpolateRotations: function ( curve ) {

			for ( var i = 1; i < curve.values.length; i ++ ) {

				var initialValue = curve.values[ i - 1 ];
				var valuesSpan = curve.values[ i ] - initialValue;

				var absoluteSpan = Math.abs( valuesSpan );

				if ( absoluteSpan >= 180 ) {

					var numSubIntervals = absoluteSpan / 180;

					var step = valuesSpan / numSubIntervals;
					var nextValue = initialValue + step;

					var initialTime = curve.times[ i - 1 ];
					var timeSpan = curve.times[ i ] - initialTime;
					var interval = timeSpan / numSubIntervals;
					var nextTime = initialTime + interval;

					var interpolatedTimes = [];
					var interpolatedValues = [];

					while ( nextTime < curve.times[ i ] ) {

						interpolatedTimes.push( nextTime );
						nextTime += interval;

						interpolatedValues.push( nextValue );
						nextValue += step;

					}

					curve.times = inject( curve.times, i, interpolatedTimes );
					curve.values = inject( curve.values, i, interpolatedValues );

				}

			}

		},

	};

	// parse an FBX file in ASCII format
	function TextParser() {}

	TextParser.prototype = {

		constructor: TextParser,

		getPrevNode: function () {

			return this.nodeStack[ this.currentIndent - 2 ];

		},

		getCurrentNode: function () {

			return this.nodeStack[ this.currentIndent - 1 ];

		},

		getCurrentProp: function () {

			return this.currentProp;

		},

		pushStack: function ( node ) {

			this.nodeStack.push( node );
			this.currentIndent += 1;

		},

		popStack: function () {

			this.nodeStack.pop();
			this.currentIndent -= 1;

		},

		setCurrentProp: function ( val, name ) {

			this.currentProp = val;
			this.currentPropName = name;

		},

		parse: function ( text ) {

			this.currentIndent = 0;

			this.allNodes = new FBXTree();
			this.nodeStack = [];
			this.currentProp = [];
			this.currentPropName = '';

			var self = this;

			var split = text.split( /[\r\n]+/ );

			split.forEach( function ( line, i ) {

				var matchComment = line.match( /^[\s\t]*;/ );
				var matchEmpty = line.match( /^[\s\t]*$/ );

				if ( matchComment || matchEmpty ) return;

				var matchBeginning = line.match( '^\\t{' + self.currentIndent + '}(\\w+):(.*){', '' );
				var matchProperty = line.match( '^\\t{' + ( self.currentIndent ) + '}(\\w+):[\\s\\t\\r\\n](.*)' );
				var matchEnd = line.match( '^\\t{' + ( self.currentIndent - 1 ) + '}}' );

				if ( matchBeginning ) {

					self.parseNodeBegin( line, matchBeginning );

				} else if ( matchProperty ) {

					self.parseNodeProperty( line, matchProperty, split[ ++ i ] );

				} else if ( matchEnd ) {

					self.popStack();

				} else if ( line.match( /^[^\s\t}]/ ) ) {

					// large arrays are split over multiple lines terminated with a ',' character
					// if this is encountered the line needs to be joined to the previous line
					self.parseNodePropertyContinued( line );

				}

			} );

			return this.allNodes;

		},

		parseNodeBegin: function ( line, property ) {

			var nodeName = property[ 1 ].trim().replace( /^"/, '' ).replace( /"$/, '' );

			var nodeAttrs = property[ 2 ].split( ',' ).map( function ( attr ) {

				return attr.trim().replace( /^"/, '' ).replace( /"$/, '' );

			} );

			var node = { name: nodeName };
			var attrs = this.parseNodeAttr( nodeAttrs );

			var currentNode = this.getCurrentNode();

			// a top node
			if ( this.currentIndent === 0 ) {

				this.allNodes.add( nodeName, node );

			} else { // a subnode

				// if the subnode already exists, append it
				if ( nodeName in currentNode ) {

					// special case Pose needs PoseNodes as an array
					if ( nodeName === 'PoseNode' ) {

						currentNode.PoseNode.push( node );

					} else if ( currentNode[ nodeName ].id !== undefined ) {

						currentNode[ nodeName ] = {};
						currentNode[ nodeName ][ currentNode[ nodeName ].id ] = currentNode[ nodeName ];

					}

					if ( attrs.id !== '' ) currentNode[ nodeName ][ attrs.id ] = node;

				} else if ( typeof attrs.id === 'number' ) {

					currentNode[ nodeName ] = {};
					currentNode[ nodeName ][ attrs.id ] = node;

				} else if ( nodeName !== 'Properties70' ) {

					if ( nodeName === 'PoseNode' )	currentNode[ nodeName ] = [ node ];
					else currentNode[ nodeName ] = node;

				}

			}

			if ( typeof attrs.id === 'number' ) node.id = attrs.id;
			if ( attrs.name !== '' ) node.attrName = attrs.name;
			if ( attrs.type !== '' ) node.attrType = attrs.type;

			this.pushStack( node );

		},

		parseNodeAttr: function ( attrs ) {

			var id = attrs[ 0 ];

			if ( attrs[ 0 ] !== '' ) {

				id = parseInt( attrs[ 0 ] );

				if ( isNaN( id ) ) {

					id = attrs[ 0 ];

				}

			}

			var name = '', type = '';

			if ( attrs.length > 1 ) {

				name = attrs[ 1 ].replace( /^(\w+)::/, '' );
				type = attrs[ 2 ];

			}

			return { id: id, name: name, type: type };

		},

		parseNodeProperty: function ( line, property, contentLine ) {

			var propName = property[ 1 ].replace( /^"/, '' ).replace( /"$/, '' ).trim();
			var propValue = property[ 2 ].replace( /^"/, '' ).replace( /"$/, '' ).trim();

			// for special case: base64 image data follows "Content: ," line
			//	Content: ,
			//	 "/9j/4RDaRXhpZgAATU0A..."
			if ( propName === 'Content' && propValue === ',' ) {

				propValue = contentLine.replace( /"/g, '' ).replace( /,$/, '' ).trim();

			}

			var currentNode = this.getCurrentNode();
			var parentName = currentNode.name;

			if ( parentName === 'Properties70' ) {

				this.parseNodeSpecialProperty( line, propName, propValue );
				return;

			}

			// Connections
			if ( propName === 'C' ) {

				var connProps = propValue.split( ',' ).slice( 1 );
				var from = parseInt( connProps[ 0 ] );
				var to = parseInt( connProps[ 1 ] );

				var rest = propValue.split( ',' ).slice( 3 );

				rest = rest.map( function ( elem ) {

					return elem.trim().replace( /^"/, '' );

				} );

				propName = 'connections';
				propValue = [ from, to ];
				append( propValue, rest );

				if ( currentNode[ propName ] === undefined ) {

					currentNode[ propName ] = [];

				}

			}

			// Node
			if ( propName === 'Node' ) currentNode.id = propValue;

			// connections
			if ( propName in currentNode && Array.isArray( currentNode[ propName ] ) ) {

				currentNode[ propName ].push( propValue );

			} else {

				if ( propName !== 'a' ) currentNode[ propName ] = propValue;
				else currentNode.a = propValue;

			}

			this.setCurrentProp( currentNode, propName );

			// convert string to array, unless it ends in ',' in which case more will be added to it
			if ( propName === 'a' && propValue.slice( - 1 ) !== ',' ) {

				currentNode.a = parseNumberArray( propValue );

			}

		},

		parseNodePropertyContinued: function ( line ) {

			var currentNode = this.getCurrentNode();

			currentNode.a += line;

			// if the line doesn't end in ',' we have reached the end of the property value
			// so convert the string to an array
			if ( line.slice( - 1 ) !== ',' ) {

				currentNode.a = parseNumberArray( currentNode.a );

			}

		},

		// parse "Property70"
		parseNodeSpecialProperty: function ( line, propName, propValue ) {

			// split this
			// P: "Lcl Scaling", "Lcl Scaling", "", "A",1,1,1
			// into array like below
			// ["Lcl Scaling", "Lcl Scaling", "", "A", "1,1,1" ]
			var props = propValue.split( '",' ).map( function ( prop ) {

				return prop.trim().replace( /^\"/, '' ).replace( /\s/, '_' );

			} );

			var innerPropName = props[ 0 ];
			var innerPropType1 = props[ 1 ];
			var innerPropType2 = props[ 2 ];
			var innerPropFlag = props[ 3 ];
			var innerPropValue = props[ 4 ];

			// cast values where needed, otherwise leave as strings
			switch ( innerPropType1 ) {

				case 'int':
				case 'enum':
				case 'bool':
				case 'ULongLong':
				case 'double':
				case 'Number':
				case 'FieldOfView':
					innerPropValue = parseFloat( innerPropValue );
					break;

				case 'Color':
				case 'ColorRGB':
				case 'Vector3D':
				case 'Lcl_Translation':
				case 'Lcl_Rotation':
				case 'Lcl_Scaling':
					innerPropValue = parseNumberArray( innerPropValue );
					break;

			}

			// CAUTION: these props must append to parent's parent
			this.getPrevNode()[ innerPropName ] = {

				'type': innerPropType1,
				'type2': innerPropType2,
				'flag': innerPropFlag,
				'value': innerPropValue

			};

			this.setCurrentProp( this.getPrevNode(), innerPropName );

		},

	};

	// Parse an FBX file in Binary format
	function BinaryParser() {}

	BinaryParser.prototype = {

		constructor: BinaryParser,

		parse: function ( buffer ) {

			var reader = new BinaryReader( buffer );
			reader.skip( 23 ); // skip magic 23 bytes

			var version = reader.getUint32();

			console.log( 'THREE.FBXLoader: FBX binary version: ' + version );

			var allNodes = new FBXTree();

			while ( ! this.endOfContent( reader ) ) {

				var node = this.parseNode( reader, version );
				if ( node !== null ) allNodes.add( node.name, node );

			}

			return allNodes;

		},

		// Check if reader has reached the end of content.
		endOfContent: function ( reader ) {

			// footer size: 160bytes + 16-byte alignment padding
			// - 16bytes: magic
			// - padding til 16-byte alignment (at least 1byte?)
			//	(seems like some exporters embed fixed 15 or 16bytes?)
			// - 4bytes: magic
			// - 4bytes: version
			// - 120bytes: zero
			// - 16bytes: magic
			if ( reader.size() % 16 === 0 ) {

				return ( ( reader.getOffset() + 160 + 16 ) & ~ 0xf ) >= reader.size();

			} else {

				return reader.getOffset() + 160 + 16 >= reader.size();

			}

		},

		// recursively parse nodes until the end of the file is reached
		parseNode: function ( reader, version ) {

			var node = {};

			// The first three data sizes depends on version.
			var endOffset = ( version >= 7500 ) ? reader.getUint64() : reader.getUint32();
			var numProperties = ( version >= 7500 ) ? reader.getUint64() : reader.getUint32();

			// note: do not remove this even if you get a linter warning as it moves the buffer forward
			var propertyListLen = ( version >= 7500 ) ? reader.getUint64() : reader.getUint32();

			var nameLen = reader.getUint8();
			var name = reader.getString( nameLen );

			// Regards this node as NULL-record if endOffset is zero
			if ( endOffset === 0 ) return null;

			var propertyList = [];

			for ( var i = 0; i < numProperties; i ++ ) {

				propertyList.push( this.parseProperty( reader ) );

			}

			// Regards the first three elements in propertyList as id, attrName, and attrType
			var id = propertyList.length > 0 ? propertyList[ 0 ] : '';
			var attrName = propertyList.length > 1 ? propertyList[ 1 ] : '';
			var attrType = propertyList.length > 2 ? propertyList[ 2 ] : '';

			// check if this node represents just a single property
			// like (name, 0) set or (name2, [0, 1, 2]) set of {name: 0, name2: [0, 1, 2]}
			node.singleProperty = ( numProperties === 1 && reader.getOffset() === endOffset ) ? true : false;

			while ( endOffset > reader.getOffset() ) {

				var subNode = this.parseNode( reader, version );

				if ( subNode !== null ) this.parseSubNode( name, node, subNode );

			}

			node.propertyList = propertyList; // raw property list used by parent

			if ( typeof id === 'number' ) node.id = id;
			if ( attrName !== '' ) node.attrName = attrName;
			if ( attrType !== '' ) node.attrType = attrType;
			if ( name !== '' ) node.name = name;

			return node;

		},

		parseSubNode: function ( name, node, subNode ) {

			// special case: child node is single property
			if ( subNode.singleProperty === true ) {

				var value = subNode.propertyList[ 0 ];

				if ( Array.isArray( value ) ) {

					node[ subNode.name ] = subNode;

					subNode.a = value;

				} else {

					node[ subNode.name ] = value;

				}

			} else if ( name === 'Connections' && subNode.name === 'C' ) {

				var array = [];

				subNode.propertyList.forEach( function ( property, i ) {

					// first Connection is FBX type (OO, OP, etc.). We'll discard these
					if ( i !== 0 ) array.push( property );

				} );

				if ( node.connections === undefined ) {

					node.connections = [];

				}

				node.connections.push( array );

			} else if ( subNode.name === 'Properties70' ) {

				var keys = Object.keys( subNode );

				keys.forEach( function ( key ) {

					node[ key ] = subNode[ key ];

				} );

			} else if ( name === 'Properties70' && subNode.name === 'P' ) {

				var innerPropName = subNode.propertyList[ 0 ];
				var innerPropType1 = subNode.propertyList[ 1 ];
				var innerPropType2 = subNode.propertyList[ 2 ];
				var innerPropFlag = subNode.propertyList[ 3 ];
				var innerPropValue;

				if ( innerPropName.indexOf( 'Lcl ' ) === 0 ) innerPropName = innerPropName.replace( 'Lcl ', 'Lcl_' );
				if ( innerPropType1.indexOf( 'Lcl ' ) === 0 ) innerPropType1 = innerPropType1.replace( 'Lcl ', 'Lcl_' );

				if ( innerPropType1 === 'Color' || innerPropType1 === 'ColorRGB' || innerPropType1 === 'Vector' || innerPropType1 === 'Vector3D' || innerPropType1.indexOf( 'Lcl_' ) === 0 ) {

					innerPropValue = [
						subNode.propertyList[ 4 ],
						subNode.propertyList[ 5 ],
						subNode.propertyList[ 6 ]
					];

				} else {

					innerPropValue = subNode.propertyList[ 4 ];

				}

				// this will be copied to parent, see above
				node[ innerPropName ] = {

					'type': innerPropType1,
					'type2': innerPropType2,
					'flag': innerPropFlag,
					'value': innerPropValue

				};

			} else if ( node[ subNode.name ] === undefined ) {

				if ( typeof subNode.id === 'number' ) {

					node[ subNode.name ] = {};
					node[ subNode.name ][ subNode.id ] = subNode;

				} else {

					node[ subNode.name ] = subNode;

				}

			} else {

				if ( subNode.name === 'PoseNode' ) {

					if ( ! Array.isArray( node[ subNode.name ] ) ) {

						node[ subNode.name ] = [ node[ subNode.name ] ];

					}

					node[ subNode.name ].push( subNode );

				} else if ( node[ subNode.name ][ subNode.id ] === undefined ) {

					node[ subNode.name ][ subNode.id ] = subNode;

				}

			}

		},

		parseProperty: function ( reader ) {

			var type = reader.getString( 1 );

			switch ( type ) {

				case 'C':
					return reader.getBoolean();

				case 'D':
					return reader.getFloat64();

				case 'F':
					return reader.getFloat32();

				case 'I':
					return reader.getInt32();

				case 'L':
					return reader.getInt64();

				case 'R':
					var length = reader.getUint32();
					return reader.getArrayBuffer( length );

				case 'S':
					var length = reader.getUint32();
					return reader.getString( length );

				case 'Y':
					return reader.getInt16();

				case 'b':
				case 'c':
				case 'd':
				case 'f':
				case 'i':
				case 'l':

					var arrayLength = reader.getUint32();
					var encoding = reader.getUint32(); // 0: non-compressed, 1: compressed
					var compressedLength = reader.getUint32();

					if ( encoding === 0 ) {

						switch ( type ) {

							case 'b':
							case 'c':
								return reader.getBooleanArray( arrayLength );

							case 'd':
								return reader.getFloat64Array( arrayLength );

							case 'f':
								return reader.getFloat32Array( arrayLength );

							case 'i':
								return reader.getInt32Array( arrayLength );

							case 'l':
								return reader.getInt64Array( arrayLength );

						}

					}

					if ( typeof Zlib === 'undefined' ) {

						console.error( 'THREE.FBXLoader: External library Inflate.min.js required, obtain or import from https://github.com/imaya/zlib.js' );

					}

					var inflate = new Zlib.Inflate( new Uint8Array( reader.getArrayBuffer( compressedLength ) ) ); // eslint-disable-line no-undef
					var reader2 = new BinaryReader( inflate.decompress().buffer );

					switch ( type ) {

						case 'b':
						case 'c':
							return reader2.getBooleanArray( arrayLength );

						case 'd':
							return reader2.getFloat64Array( arrayLength );

						case 'f':
							return reader2.getFloat32Array( arrayLength );

						case 'i':
							return reader2.getInt32Array( arrayLength );

						case 'l':
							return reader2.getInt64Array( arrayLength );

					}

				default:
					throw new Error( 'THREE.FBXLoader: Unknown property type ' + type );

			}

		}

	};

	function BinaryReader( buffer, littleEndian ) {

		this.dv = new DataView( buffer );
		this.offset = 0;
		this.littleEndian = ( littleEndian !== undefined ) ? littleEndian : true;

	}

	BinaryReader.prototype = {

		constructor: BinaryReader,

		getOffset: function () {

			return this.offset;

		},

		size: function () {

			return this.dv.buffer.byteLength;

		},

		skip: function ( length ) {

			this.offset += length;

		},

		// seems like true/false representation depends on exporter.
		// true: 1 or 'Y'(=0x59), false: 0 or 'T'(=0x54)
		// then sees LSB.
		getBoolean: function () {

			return ( this.getUint8() & 1 ) === 1;

		},

		getBooleanArray: function ( size ) {

			var a = [];

			for ( var i = 0; i < size; i ++ ) {

				a.push( this.getBoolean() );

			}

			return a;

		},

		getUint8: function () {

			var value = this.dv.getUint8( this.offset );
			this.offset += 1;
			return value;

		},

		getInt16: function () {

			var value = this.dv.getInt16( this.offset, this.littleEndian );
			this.offset += 2;
			return value;

		},

		getInt32: function () {

			var value = this.dv.getInt32( this.offset, this.littleEndian );
			this.offset += 4;
			return value;

		},

		getInt32Array: function ( size ) {

			var a = [];

			for ( var i = 0; i < size; i ++ ) {

				a.push( this.getInt32() );

			}

			return a;

		},

		getUint32: function () {

			var value = this.dv.getUint32( this.offset, this.littleEndian );
			this.offset += 4;
			return value;

		},

		// JavaScript doesn't support 64-bit integer so calculate this here
		// 1 << 32 will return 1 so using multiply operation instead here.
		// There's a possibility that this method returns wrong value if the value
		// is out of the range between Number.MAX_SAFE_INTEGER and Number.MIN_SAFE_INTEGER.
		// TODO: safely handle 64-bit integer
		getInt64: function () {

			var low, high;

			if ( this.littleEndian ) {

				low = this.getUint32();
				high = this.getUint32();

			} else {

				high = this.getUint32();
				low = this.getUint32();

			}

			// calculate negative value
			if ( high & 0x80000000 ) {

				high = ~ high & 0xFFFFFFFF;
				low = ~ low & 0xFFFFFFFF;

				if ( low === 0xFFFFFFFF ) high = ( high + 1 ) & 0xFFFFFFFF;

				low = ( low + 1 ) & 0xFFFFFFFF;

				return - ( high * 0x100000000 + low );

			}

			return high * 0x100000000 + low;

		},

		getInt64Array: function ( size ) {

			var a = [];

			for ( var i = 0; i < size; i ++ ) {

				a.push( this.getInt64() );

			}

			return a;

		},

		// Note: see getInt64() comment
		getUint64: function () {

			var low, high;

			if ( this.littleEndian ) {

				low = this.getUint32();
				high = this.getUint32();

			} else {

				high = this.getUint32();
				low = this.getUint32();

			}

			return high * 0x100000000 + low;

		},

		getFloat32: function () {

			var value = this.dv.getFloat32( this.offset, this.littleEndian );
			this.offset += 4;
			return value;

		},

		getFloat32Array: function ( size ) {

			var a = [];

			for ( var i = 0; i < size; i ++ ) {

				a.push( this.getFloat32() );

			}

			return a;

		},

		getFloat64: function () {

			var value = this.dv.getFloat64( this.offset, this.littleEndian );
			this.offset += 8;
			return value;

		},

		getFloat64Array: function ( size ) {

			var a = [];

			for ( var i = 0; i < size; i ++ ) {

				a.push( this.getFloat64() );

			}

			return a;

		},

		getArrayBuffer: function ( size ) {

			var value = this.dv.buffer.slice( this.offset, this.offset + size );
			this.offset += size;
			return value;

		},

		getString: function ( size ) {

			// note: safari 9 doesn't support Uint8Array.indexOf; create intermediate array instead
			var a = [];

			for ( var i = 0; i < size; i ++ ) {

				a[ i ] = this.getUint8();

			}

			var nullByte = a.indexOf( 0 );
			if ( nullByte >= 0 ) a = a.slice( 0, nullByte );

			return THREE.LoaderUtils.decodeText( new Uint8Array( a ) );

		}

	};

	// FBXTree holds a representation of the FBX data, returned by the TextParser ( FBX ASCII format)
	// and BinaryParser( FBX Binary format)
	function FBXTree() {}

	FBXTree.prototype = {

		constructor: FBXTree,

		add: function ( key, val ) {

			this[ key ] = val;

		},

	};

	// ************** UTILITY FUNCTIONS **************

	function isFbxFormatBinary( buffer ) {

		var CORRECT = 'Kaydara FBX Binary  \0';

		return buffer.byteLength >= CORRECT.length && CORRECT === convertArrayBufferToString( buffer, 0, CORRECT.length );

	}

	function isFbxFormatASCII( text ) {

		var CORRECT = [ 'K', 'a', 'y', 'd', 'a', 'r', 'a', '\\', 'F', 'B', 'X', '\\', 'B', 'i', 'n', 'a', 'r', 'y', '\\', '\\' ];

		var cursor = 0;

		function read( offset ) {

			var result = text[ offset - 1 ];
			text = text.slice( cursor + offset );
			cursor ++;
			return result;

		}

		for ( var i = 0; i < CORRECT.length; ++ i ) {

			var num = read( 1 );
			if ( num === CORRECT[ i ] ) {

				return false;

			}

		}

		return true;

	}

	function getFbxVersion( text ) {
		var versionRegExp = /FBXVersion: (\d+)/;

		var match = text.match( versionRegExp );
		if ( match ) {

			var version = parseInt( match[ 1 ] );
			return version;

		}
		throw new Error( 'THREE.FBXLoader: Cannot find the version number for the file given.' );

	}

	// Converts FBX ticks into real time seconds.
	function convertFBXTimeToSeconds( time ) {

		return time / 46186158000;

	}

	var dataArray = [];

	// extracts the data from the correct position in the FBX array based on indexing type
	function getData( polygonVertexIndex, polygonIndex, vertexIndex, infoObject ) {

		var index;

		switch ( infoObject.mappingType ) {

			case 'ByPolygonVertex' :
				index = polygonVertexIndex;
				break;
			case 'ByPolygon' :
				index = polygonIndex;
				break;
			case 'ByVertice' :
				index = vertexIndex;
				break;
			case 'AllSame' :
				index = infoObject.indices[ 0 ];
				break;
			default :
				console.warn( 'THREE.FBXLoader: unknown attribute mapping type ' + infoObject.mappingType );

		}

		if ( infoObject.referenceType === 'IndexToDirect' ) index = infoObject.indices[ index ];

		var from = index * infoObject.dataSize;
		var to = from + infoObject.dataSize;

		return slice( dataArray, infoObject.buffer, from, to );

	}

	var tempEuler = new THREE.Euler();
	var tempVec = new THREE.Vector3();

	// generate transformation from FBX transform data
	// ref: https://help.autodesk.com/view/FBX/2017/ENU/?guid=__files_GUID_10CDD63C_79C1_4F2D_BB28_AD2BE65A02ED_htm
	// ref: http://docs.autodesk.com/FBX/2014/ENU/FBX-SDK-Documentation/index.html?url=cpp_ref/_transformations_2main_8cxx-example.html,topicNumber=cpp_ref__transformations_2main_8cxx_example_htmlfc10a1e1-b18d-4e72-9dc0-70d0f1959f5e
	function generateTransform( transformData ) {

		var lTranslationM = new THREE.Matrix4();
		var lPreRotationM = new THREE.Matrix4();
		var lRotationM = new THREE.Matrix4();
		var lPostRotationM = new THREE.Matrix4();

		var lScalingM = new THREE.Matrix4();
		var lScalingPivotM = new THREE.Matrix4();
		var lScalingOffsetM = new THREE.Matrix4();
		var lRotationOffsetM = new THREE.Matrix4();
		var lRotationPivotM = new THREE.Matrix4();

		var lParentGX = new THREE.Matrix4();
		var lGlobalT = new THREE.Matrix4();

		var inheritType = ( transformData.inheritType ) ? transformData.inheritType : 0;

		if ( transformData.translation ) lTranslationM.setPosition( tempVec.fromArray( transformData.translation ) );

		if ( transformData.preRotation ) {

			var array = transformData.preRotation.map( THREE.Math.degToRad );
			array.push( transformData.eulerOrder );
			lPreRotationM.makeRotationFromEuler( tempEuler.fromArray( array ) );

		}

		if ( transformData.rotation ) {

			var array = transformData.rotation.map( THREE.Math.degToRad );
			array.push( transformData.eulerOrder );
			lRotationM.makeRotationFromEuler( tempEuler.fromArray( array ) );

		}

		if ( transformData.postRotation ) {

			var array = transformData.postRotation.map( THREE.Math.degToRad );
			array.push( transformData.eulerOrder );
			lPostRotationM.makeRotationFromEuler( tempEuler.fromArray( array ) );

		}

		if ( transformData.scale ) lScalingM.scale( tempVec.fromArray( transformData.scale ) );

		// Pivots and offsets
		if ( transformData.scalingOffset ) lScalingOffsetM.setPosition( tempVec.fromArray( transformData.scalingOffset ) );
		if ( transformData.scalingPivot ) lScalingPivotM.setPosition( tempVec.fromArray( transformData.scalingPivot ) );
		if ( transformData.rotationOffset ) lRotationOffsetM.setPosition( tempVec.fromArray( transformData.rotationOffset ) );
		if ( transformData.rotationPivot ) lRotationPivotM.setPosition( tempVec.fromArray( transformData.rotationPivot ) );

		// parent transform
		if ( transformData.parentMatrixWorld ) lParentGX = transformData.parentMatrixWorld;

		// Global Rotation
		var lLRM = lPreRotationM.multiply( lRotationM ).multiply( lPostRotationM );
		var lParentGRM = new THREE.Matrix4();
		lParentGX.extractRotation( lParentGRM );

		// Global Shear*Scaling
		var lParentTM = new THREE.Matrix4();
		var lLSM;
		var lParentGSM;
		var lParentGRSM;

		lParentTM.copyPosition( lParentGX );
		lParentGRSM = lParentTM.getInverse( lParentTM ).multiply( lParentGX );
		lParentGSM = lParentGRM.getInverse( lParentGRM ).multiply( lParentGRSM );
		lLSM = lScalingM;

		var lGlobalRS;
		if ( inheritType === 0 ) {

			lGlobalRS = lParentGRM.multiply( lLRM ).multiply( lParentGSM ).multiply( lLSM );

		} else if ( inheritType === 1 ) {

			lGlobalRS = lParentGRM.multiply( lParentGSM ).multiply( lLRM ).multiply( lLSM );

		} else {

			var lParentLSM = new THREE.Matrix4().copy( lScalingM );

			var lParentGSM_noLocal = lParentGSM.multiply( lParentLSM.getInverse( lParentLSM ) );

			lGlobalRS = lParentGRM.multiply( lLRM ).multiply( lParentGSM_noLocal ).multiply( lLSM );

		}

		// Calculate the local transform matrix
		var lTransform = lTranslationM.multiply( lRotationOffsetM ).multiply( lRotationPivotM ).multiply( lPreRotationM ).multiply( lRotationM ).multiply( lPostRotationM ).multiply( lRotationPivotM.getInverse( lRotationPivotM ) ).multiply( lScalingOffsetM ).multiply( lScalingPivotM ).multiply( lScalingM ).multiply( lScalingPivotM.getInverse( lScalingPivotM ) );

		var lLocalTWithAllPivotAndOffsetInfo = new THREE.Matrix4().copyPosition( lTransform );

		var lGlobalTranslation = lParentGX.multiply( lLocalTWithAllPivotAndOffsetInfo );
		lGlobalT.copyPosition( lGlobalTranslation );

		lTransform = lGlobalT.multiply( lGlobalRS );

		return lTransform;

	}

	// Returns the three.js intrinsic Euler order corresponding to FBX extrinsic Euler order
	// ref: http://help.autodesk.com/view/FBX/2017/ENU/?guid=__cpp_ref_class_fbx_euler_html
	function getEulerOrder( order ) {

		order = order || 0;

		var enums = [
			'ZYX', // -> XYZ extrinsic
			'YZX', // -> XZY extrinsic
			'XZY', // -> YZX extrinsic
			'ZXY', // -> YXZ extrinsic
			'YXZ', // -> ZXY extrinsic
			'XYZ', // -> ZYX extrinsic
			//'SphericXYZ', // not possible to support
		];

		if ( order === 6 ) {

			console.warn( 'THREE.FBXLoader: unsupported Euler Order: Spherical XYZ. Animations and rotations may be incorrect.' );
			return enums[ 0 ];

		}

		return enums[ order ];

	}

	// Parses comma separated list of numbers and returns them an array.
	// Used internally by the TextParser
	function parseNumberArray( value ) {

		var array = value.split( ',' ).map( function ( val ) {

			return parseFloat( val );

		} );

		return array;

	}

	function convertArrayBufferToString( buffer, from, to ) {

		if ( from === undefined ) from = 0;
		if ( to === undefined ) to = buffer.byteLength;

		return THREE.LoaderUtils.decodeText( new Uint8Array( buffer, from, to ) );

	}

	function append( a, b ) {

		for ( var i = 0, j = a.length, l = b.length; i < l; i ++, j ++ ) {

			a[ j ] = b[ i ];

		}

	}

	function slice( a, b, from, to ) {

		for ( var i = from, j = 0; i < to; i ++, j ++ ) {

			a[ j ] = b[ i ];

		}

		return a;

	}

	// inject array a2 into array a1 at index
	function inject( a1, index, a2 ) {

		return a1.slice( 0, index ).concat( a2 ).concat( a1.slice( index ) );

	}

	return FBXLoader;

} )();
