#!/bin/bash

# ANSI escape codes
# https://en.wikipedia.org/wiki/ANSI_escape_code
CYAN='\033[1;36m'
RESET='\033[0m'

echo -e "${CYAN}Pulling most recent repo...${RESET}"
git pull
echo -e "${CYAN}Running npm install...${RESET}"
npm install --prefix ./webapp/
echo -e "${CYAN}Building app...${RESET}"
npm run build --prefix ./webapp/
echo -e "${CYAN}Restarting web process...${RESET}"
pm2 restart nextjs

echo -e "${CYAN}Site is available"
