# COM S Animation

## Space Sequence

Our project aims to solve an important issue in the education space: creating animations easily and efficiency while also having the flexibility to customize for any purpose.

## Build & Run

### Requirements

- [Node.js](https://nodejs.org/en/)

### Run

- Download or clone the repository
- Navigate to the repository folder, then the `webapp` folder; `cd space-sequence/webapp`
- Run `npm install` to install dependencies
- Run `npm run start` to build and run the development site; this should automatically open the demo site in your default browser
- View the site at http://localhost:3000

## Usage

### Commands

*Note: these commands were used as a guide, their actual implementation may be different*

*\<required\> [optional]*

**Creating objects**
*Created objects auto-generate a short unique ID, which is displayed on the animation screen for use. Clicking object id's auto-copy's the ID to the virtual console (if in use) and to clipboard.*

`circle [<x> <y> [z]] [<w> <h>]` creates a new circle object at optional position with optional size
`rect [<x> <y> [z]] [<w> <h>]`
`line [<x> <y> [z]] [length]`
`line [<x> <y> [z] / <object_id>] [<x> <y> [z] / <object_id>]` creates a new line between given objects or points
`empty [<x> <y> [z]]` creates an "empty object" at the given position for use with other objects (like creating lines to the empty, etc.)
`group <object_id> <object_id> ...` groups infinitely many objects together; this creates a unique object_id for the group; grouping groups together ungroups previous groups and creates a union of the groups; individual objects can still be manipulated by invoking their ID
`ungroup <object_id>` alias for `delete <object_id>`


**Altering objects**
*Objects with the given ID are altered via commands. If the keyframe is being recorded, the transform will be included in the animation.*

`position <object_id> <x> <y> [z]`
`size <object_id> <w> <h>`
`scale <object_id> <scale>`
`scale <object_id> [x_scale] [y_scale] [z_scale]`
`rotation <object_id> <degrees_or_radians> <vector>` rotates the object about the given vector
`rotation <object_id> <degrees_or_radians> <x/y/z>` rotates the object about the chosen axis (x, y, or z)
`fill <object_id> <alpha-hex>`
`fill <object_id> <r> <g> <b> [a]`
`border <object_id> <width> <alpha-hex>`
`border <object_id> <width> <r> <g> <b> [a]`
`reset <object_id> [keyframe_id]` resets the object to its inital state or to its state at given keyframe
`hide <object_id>` hides the object completely, indepedent of alpha values
`show <object_id>` shows the object again

**Animating**
*Each keyframe has a unique ID and can be copied, similar to object ID's above. Keyframes identify a point in time in which the object is interpolated between the last keyframe's state and the next keyframe's state. Once a keyframe is started, all object-altering commands (e.g. position, creating new objects, etc.) are recorded and added to the keyframe.*

`keyframe create` creates a new keyframe and adds all objects and their states to it
`keyframe duration <keyframe_id> <duration>` sets the keyframe's duration in milliseconds
`keyframe clear <keyframe_id>` clears the given keyframe of all data except its duration and position in the sequence (essentially reverts it to an empty keyframe, a copy of the previous one)
`keyframe goto <keyframe_id>` sets the visual state of the animation to the given keyframe's state
`keyframe return` effectively a macro for `keyframe goto` for the last created keyframe
`keyframe preview <keyframe_id>` plays the animation from the keyframe preceding the given keyframe to the given keyframe
`keyframe preview <keyframe_id> <keyframe_id>` plays the animations between the two keyframes

`macro record [name]` starts recording a new macro; commands from this point on will be recorded and added to this macro
`macro stop` stops recording the macro
`macro [name_or_id]` executes an existing macro; they can also be executed like a command by invoking their name

`sequence create [animation_name]` creates a new animation; commands from this point on are part of the animation
`sequence duration <duration>` sets the duration of the animation in milliseconds
`sequence play` plays the animation from beginning to end

`save` export commands to text file available to download; this command is never recorded

**Utility**

`variable create <name> <value>` create a variable to be used by name in place of actual values from this point on
`variable set <name> <value>` change the value of an existing variable

`data <id>` returns a map of the given object/keyframe/macro/variable data to be used in variables or simply for display; values are *by reference*, but readonly

`delete <object_id>`
`delete <keyframe_id>`
`delete <macro_id>`
`delete <variable_id>`

`help` lists all existing commands and short descriptions; this command is never recorded
`man <command>` our equivalent of man pages for our commands; this command is never recorded
